<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeleteUserController extends Controller
{
    /**
     * Display form for removing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $this->authorize('view', auth()->user());

        return view('auth.profile.destroy', [
            'user' => auth()->user()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $this->authorize('delete', auth()->user());

        $user = User::whereSlug(auth()->user()->slug)->first();
        $user->forceDelete();
        auth()->logout();

        return redirect(route('welcome'));
    }
}
