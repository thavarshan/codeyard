<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\UserRequest;
use App\Traits\HandleAutherization;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UpdatePasswordController extends Controller
{
    use HandleAutherization;

    /**
     * Update the password for the user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function update(UserRequest $request)
    {
        $this->authorize('update', auth()->user());
        $this->checkEmail($request->email);

        auth()->user()->fill([
            'password' => Hash::make($request->password)
        ])->save();

        return redirect(route('profile.settings'))
            ->with('success', 'Profile details updated');
    }
}
