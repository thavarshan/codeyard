<?php

namespace App\Http\Controllers;

use App\Task;
use App\Project;
use Illuminate\Http\Request;
use App\Http\Requests\TaskRequest;

class TaskController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\TaskRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaskRequest $request, Project $project)
    {
        $this->authorize('update', $project);

        $project->addTask($request->toArray());

        return redirect($project->path());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\TaskRequest  $request
     * @param  \App\Project  $project
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(TaskRequest $request, Project $project, Task $task)
    {
        $this->authorize('update', $task->project);

        $task->update([
            'name' => $request->name,
            'description' => $request->description,
        ]);

        $request->completed ? $task->complete() : $task->incomplete();

        return redirect($project->path());
    }
}
