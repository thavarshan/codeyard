<?php

namespace App\Traits;

use App\User;

trait HandleAutherization
{
    /**
     * Check if current user making request is authorised.
     *
     * @param  \App\User    $user
     * @return boolean
     */
    public function isOwner(User $user)
    {
        auth()->user()->id === $user->id ?
            $this->allow() : $this->deny();
    }

    /**
     * Check if provided email matches authenticated user's email.
     *
     * @param  string $email
     * @return \Illuminate\Http\Response|bool
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function checkEmail($email)
    {
        $email == auth()->user()->email
            ? $this->allow()
            : $this->deny();
    }

    /**
     * Allow action.
     *
     * @return bool
     */
    protected function allow()
    {
        return true;
    }

    /**
     * Deny action.
     *
     * @param  string $message
     * @return \Illuminate\Http\Response|bool
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    protected function deny($message = null)
    {
        abort(
            403,
            $message ?: 'You do not have permission to access this content'
        );
    }
}
