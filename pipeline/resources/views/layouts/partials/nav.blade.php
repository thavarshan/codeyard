<nav class="navbar navbar-expand-md navbar-light tw-bg-white tw-flex tw-items-center">
    <div class="container">
        <a class="tw-flex tw-items-center tw-leading-none tw-py-4 tw-pr-2 tw-mr-4 tw-tracking-wide tw-font-bold hover:tw-no-underline" href="{{ url('/') }}">
            <img src="{{ asset('img/favicon.png') }}" alt="{{ config('app.name') }}" class="tw-h-4 tw-w-4 mr-1">
            <span class="tw-font-bold tw-text-indigo tw-text-lg">
                {{ config('app.name') }}
            </span>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto tw-items-center">
                @auth
                    <li class="nav-item">
                        <a href="/" class="nav-link">
                            Home
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="/projects" class="nav-link">
                            Projects
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="/" class="nav-link">
                            About
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="/" class="nav-link">
                            Support
                        </a>
                    </li>
                @endauth
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto tw-items-center">
                <li class="nav-item">
                    <a href="{{ route('projects.create') }}" class="nav-link tw-leading-normal
                    tw-py-2
                    tw-px-4
                    tw-text-indigo
                    tw-rounded-full
                    tw-bg-white
                    tw-mr-2
                    tw-border-2 tw-border-indigo
                    hover:tw-text-white
                    hover:tw-bg-indigo">
                        <i class="fas fa-plus mr-1"></i>
                        New Project
                    </a>
                </li>

                <li class="nav-item dropdown ml-2">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle p-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        <img class="tw-w-10 tw-h-10 tw-rounded-full" src="{{ asset('img/user.jpg') }}" alt="{{ auth()->user()->name }}">
                    </a>

                    <div class="dropdown-menu dropdown-menu-right tw-border tw-border-grey-lighter tw-shadow" aria-labelledby="navbarDropdown">
                        <div class="pb-2 px-4">
                            <span class="tw-text-grey tw-text-xs">
                                Signed in as,
                            </span>
                            <h6 class="mb-0 tw-font-semibold">
                                {{ auth()->user()->name }}
                            </h6>
                        </div>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item tw-font-medium" href="{{ route('profile', ['user' => auth()->user()->slug]) }}">
                            Profile
                        </a>
                        <a class="dropdown-item tw-text-sm tw-font-medium" href="#">
                            Projects
                        </a>
                        <a class="dropdown-item tw-text-sm tw-font-medium" href="#">
                            Tasks
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item tw-text-sm tw-font-medium" href="#">
                            Support
                        </a>
                        <a class="dropdown-item tw-text-sm tw-font-medium" href="{{ route('profile.settings') }}">
                            Settings
                        </a>
                        <a class="dropdown-item tw-text-sm tw-font-medium" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            {{ __('Sign Out') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
