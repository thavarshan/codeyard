<div class="container">
    <div class="row">
        <div class="col">
            <div class="tw-w-full tw-py-12 text-center">
                <span class="tw-text-grey tw-text-xs">
                    &copy; {{ date("Y") }} <strong>{{ config('app.name') }}</strong>
                </span>
            </div>
        </div>
    </div>
</div>
