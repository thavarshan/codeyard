<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="{{ asset('css/fontawesome.css') }}">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/tailwind.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <header id="header" class="tw-w-full">
            <nav class="navbar navbar-light tw-flex tw-items-center">
                <div class="container">
                    <a class="tw-flex tw-items-center tw-leading-none tw-py-4 tw-pr-2 tw-mr-4 tw-tracking-wide tw-font-bold hover:tw-no-underline" href="{{ url('/') }}">
                        <img src="{{ asset('img/favicon.png') }}" alt="{{ config('app.name') }}" class="tw-h-4 tw-w-4 mr-1">
                        <span class="tw-font-bold tw-text-indigo tw-text-lg">
                            {{ config('app.name') }}
                        </span>
                    </a>

                    <div class="" id="navbarSupportedContent">
                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto tw-items-center">
                            <!-- Authentication Links -->
                            <li class="nav-item">
                                <a class="nav-link
                                tw-leading-normal
                                tw-text-indigo-light
                                hover:tw-text-indigo-lighter
                                tw-no-underlin"
                                href="{{ route('login') }}">
                                    Sign In <span class="ml-1">&#8594;</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>

        </header><!-- /header -->

        <main class="py-4">
            @yield('content')
        </main>

        <footer class="tw-w-full tw-mt-8">
            @include('layouts.partials.foooter')
        </footer>

        @yield('modal')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/fontawesome.js') }}"></script>
</body>
</html>
