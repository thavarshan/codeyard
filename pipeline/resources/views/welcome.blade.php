@extends('layouts.landing')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 order-last order-md-first">
                <div class="hero tw-flex tw-items-center md:tw-py-32">
                    <div class="tw-text-center md:tw-text-left">
                        <h1 class="tw-font-thin tw-mb-6 tw-text-4xl">
                            Control your <span class="tw-font-medium">Project</span><br class="tw-hidden lg:tw-inline">
                            and <span class="tw-font-medium">make amazing</span> experience.
                        </h1>

                        <p class="tw-text-grey-dark tw-leading-loose tw-text-base tw-mb-10">
                            <span class="tw-font-semibold tw-text-black">{{ config('app.name') }}</span> help you to make amazing team experience <br class="tw-hidden lg:tw-inline">
                            and effective flow. Will give the best result.
                        </p>

                        <div>
                            <a href="{{ route('projects.index') }}" class="tw-inline-block tw-border-2 tw-border-indigo hover:tw-bg-indigo-dark tw-text-indigo tw-font-normal tw-py-2 tw-px-8 tw-rounded-full hover:tw-text-white">
                                Get Started <span class="ml-1">&#8594;</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6 tw-flex tw-items-center tw-py-12 md:tw-py-32">
                <div class="tw-w-full">
                    <img src="{{ asset('img/hero.png') }}" class="tw-w-full" />
                </div>
            </div>
        </div>
    </div>
@endsection
