<div class="form-group">
    <label for="title" class="tw-block tw-uppercase tw-tracking-wide tw-text-grey tw-text-xs tw-font-bold tw-mb-2">
        Project Title
    </label>

    <input id="title" type="text" class="tw-bg-grey-lighter tw-appearance-none tw-border-2 tw-border-grey-lighter tw-rounded tw-w-full tw-py-2 tw-px-4 tw-text-grey-darker tw-leading-tight focus:tw-outline-none focus:tw-bg-white focus:tw-border-indigo{{ $errors->has('title') ? ' tw-border-red-lighter' : '' }}" name="title" value="{{ $project->title ?: old('title') }}" placeholder="Project Title" required>

    @if ($errors->has('title'))
        <span class="tw-block tw-py-2 tw-text-xs tw-text-red-lighter" role="alert">
            <strong>{{ $errors->first('title') }}</strong>
        </span>
    @endif

    <p class="tw-text-grey tw-text-xs tw-font-medium py-2">
        Great project names are short and memorable.
    </p>
</div>

<div class="form-group">
    <label for="description" class="tw-block tw-uppercase tw-tracking-wide tw-text-grey tw-text-xs tw-font-bold tw-mb-2">
        Project Description
    </label>

    <textarea id="description" rows="7" class="tw-bg-grey-lighter tw-appearance-none tw-border-2 tw-border-grey-lighter tw-rounded tw-w-full tw-py-2 tw-px-4 tw-text-grey-darker tw-leading-tight focus:tw-outline-none focus:tw-bg-white focus:tw-border-indigo{{ $errors->has('description') ? ' tw-border-red-lighter' : '' }}" name="description" placeholder="Project Description" required>{{ $project->description ?: old('description') }}</textarea>

    @if ($errors->has('description'))
        <span class="tw-block tw-py-2 tw-text-xs tw-text-red-lighter" role="alert">
            <strong>{{ $errors->first('description') }}</strong>
        </span>
    @endif
</div>

<div class="form-group tw-mb-12">
    <label for="notes" class="tw-block tw-uppercase tw-tracking-wide tw-text-grey tw-text-xs tw-font-bold tw-mb-2">
        General Notes
    </label>

    <input id="notes" type="text" class="tw-bg-grey-lighter tw-appearance-none tw-border-2 tw-border-grey-lighter tw-rounded tw-w-full tw-py-2 tw-px-4 tw-text-grey-darker tw-leading-tight focus:tw-outline-none focus:tw-bg-white focus:tw-border-indigo{{ $errors->has('notes') ? ' tw-border-red-lighter' : '' }}" name="notes" value="{{ $project->notes ?: old('notes') }}" placeholder="Project Notes">

    @if ($errors->has('notes'))
        <span class="tw-block tw-py-2 tw-text-xs tw-text-red-lighter" role="alert">
            <strong>{{ $errors->first('notes') }}</strong>
        </span>
    @endif
</div>

<div class="form-group mb-0 tw-flex tw-items-center">
    <button type="submit" class="tw-btn mr-3">
        {{ $buttonText }}
    </button>

    <a class="btn btn-link text-danger" href="{{ route('projects.show', ['id' => $project->id]) }}">
        Cancel
    </a>
</div>
