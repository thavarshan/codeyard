<div class="card tw-border tw-border-grey-lighter tw-shadow overflow-hidden">
    <div class="card-body px-6 py-4">
        <div class="tw-mb-4 tw-flex">
            <a class="tw-flex-1" href="{{ route('projects.show', ['id' => $project->id]) }}">
                <span class="tw-font-bold tw-text-lg tw-text-grey-darkest tw-mb-2">
                    {{ $project->title }}
                </span>
            </a>

            <div class="dropdown tw-inline text-right ml-2">
                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-v"></i>
                </a>

                <div class="dropdown-menu dropdown-menu-right tw-dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <a class="dropdown-item" href="{{ route('projects.edit', ['id' => $project->id]) }}">Edit</a>
                    <form action="{{ route('projects.destroy', ['id' => $project->id]) }}"
                        class="tw-inline"
                        method="POST">
                        @csrf @method('DELETE')
                        <button class="dropdown-item">
                            Delete
                        </button>
                    </form>
                </div>
            </div>
        </div>

        <p class="tw-text-grey-darker tw-text-sm">
            {{ str_limit($project->description, $words = 100, $end='...') }}
        </p>
    </div>

    <div class="card-footer border-0 bg-white">
        <span class="tw-text-grey-dark tw-text-xs">
            Due: {{ now()->format('F j, Y') }}
        </span>
    </div>
</div>
