<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content border-0 tw-shadow">
            <div class="modal-header border-0 tw-pl-8">
                <h5 class="modal-title tw-font-thin mb-0" id="exampleModalLabel">New Task</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <i class="far fa-times-circle"></i>
                    </span>
                </button>
            </div>
            <div class="modal-body tw-p-8">
                <form action="{{ route('tasks.store', ['id' => $id]) }}" method="POST" accept-charset="utf-8">
                    @csrf

                    <div class="form-group">
                        <label for="name" class="tw-block tw-uppercase tw-tracking-wide tw-text-grey tw-text-xs tw-font-bold tw-mb-2">
                            Task Name
                        </label>

                        <input id="name" type="text" class="tw-bg-grey-lighter tw-appearance-none tw-border-2 tw-border-grey-lighter tw-rounded tw-w-full tw-py-2 tw-px-4 tw-text-grey-darker tw-leading-tight focus:tw-outline-none focus:tw-bg-white focus:tw-border-indigo{{ $errors->has('name') ? ' tw-border-red-lighter' : '' }}" name="name" value="{{ old('name') }}" placeholder="Task Name" required autofocus>

                        @if ($errors->has('name'))
                            <span class="tw-block tw-py-2 tw-text-xs tw-text-red-lighter" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="description" class="tw-block tw-uppercase tw-tracking-wide tw-text-grey tw-text-xs tw-font-bold tw-mb-2">
                            Task Description
                        </label>

                        <textarea id="description" rows="7" class="tw-bg-grey-lighter tw-appearance-none tw-border-2 tw-border-grey-lighter tw-rounded tw-w-full tw-py-2 tw-px-4 tw-text-grey-darker tw-leading-tight focus:tw-outline-none focus:tw-bg-white focus:tw-border-indigo{{ $errors->has('description') ? ' tw-border-red-lighter' : '' }}" name="description" placeholder="Task Description">{{ old('description') }}</textarea>

                        @if ($errors->has('description'))
                            <span class="tw-block tw-py-2 tw-text-xs tw-text-red-lighter" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group mb-0">
                        <button type="submit" class="tw-btn">Create Task</button>
                        <button type="button" class="btn btn-link text-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
