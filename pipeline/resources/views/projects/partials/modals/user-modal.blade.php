<div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="userModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content border-0 tw-shadow">
            <div class="modal-header border-0 tw-pl-8">
                <h5 class="modal-title tw-font-thin mb-0" id="userModalLabel">Invite New User</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <i class="far fa-times-circle"></i>
                    </span>
                </button>
            </div>

            <div class="modal-body tw-p-8">
                <div class="mb-4 px-1">
                    <ul class="nav m-0 p-0">
                        @foreach ($project->members as $member)
                            <li class="nav-item tw--ml-2">
                                <a class="d-inline-block tw-border-2 tw-border-white tw-rounded-full" href="#" data-toggle="tooltip" data-placement="top" title="{{ $member->name }}">
                                    <img class="tw-w-10 tw-h-10 tw-rounded-full" src="{{ asset('img/user.jpg') }}" alt="{{ $member->name }}">
                                </a>
                            </li>
                        @endforeach

                        <li class="nav-item tw--ml-2">
                            <a class="d-inline-block tw-border-2 tw-border-white tw-rounded-full tw-relative" href="#" data-toggle="tooltip" data-placement="top" title="{{ $project->owner->name }}">
                                <img class="tw-w-10 tw-h-10 tw-rounded-full" src="{{ asset('img/user.jpg') }}" alt="{{ $project->owner->name }}">

                                <span class="tw-rounded-full tw-h-3 tw-w-3 tw-bg-yellow-dark tw-absolute tw-pin-r tw-pin-b"></span>
                            </a>
                        </li>
                    </ul>
                </div>

                <form action="{{ $project->path() . '/invitations' }}" method="POST" accept-charset="utf-8">
                    @csrf

                    <div class="form-group">
                        <label for="email" class="tw-block tw-uppercase tw-tracking-wide tw-text-grey tw-text-xs tw-font-bold tw-mb-2">
                            User Email
                        </label>

                        <input id="email" type="text" class="tw-bg-grey-lighter tw-appearance-none tw-border-2 tw-border-grey-lighter tw-rounded tw-w-full tw-py-2 tw-px-4 tw-text-grey-darker tw-leading-tight focus:tw-outline-none focus:tw-bg-white focus:tw-border-indigo{{ $errors->has('email') ? ' tw-border-red-lighter' : '' }}" name="email" value="{{ old('email') }}" placeholder="User Email" required autofocus>

                        @if ($errors->{ $bag ?? 'default' }->any())
                            <ul class="field mt-6 list-reset">
                                @foreach ($errors->{ $bag ?? 'default' }->all() as $error)
                                    <span class="tw-block tw-py-2 tw-text-xs tw-text-red-lighter" role="alert">
                                        <strong>{{ $error }}</strong>
                                    </span>
                                @endforeach
                            </ul>
                        @endif
                    </div>

                    <div class="form-group mb-0">
                        <button type="submit" class="tw-btn">Invite</button>
                        <button type="button" class="btn btn-link text-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
