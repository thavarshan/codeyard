<div class="card border-0 mb-2">
    <div class="card-body">
        <form action="{{ $task->path() }}" method="POST" class="tw-relative tw-flex">
            @method('PATCH')
            @csrf

            <input name="completed" type="checkbox" class="tw-inlilne-block tw-cursor-pointer tw-mr-2" id="customCheck{{ $task->id }}" onChange="this.form.submit()" {{ $task->completed ? 'checked' : '' }}>
            <label class="tw-font-medium tw-cursor-pointer tw-block tw-w-full m-0" for="name">
                <input name="name" value="{{ $task->name }}"
                    class="tw-w-full focus:tw-outline-none {{ $task->completed ? 'tw-text-grey tw-line-through' : '' }}">
            </label>
        </form>
    </div>
</div>
