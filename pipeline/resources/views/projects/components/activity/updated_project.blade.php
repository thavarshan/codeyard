<div>
    @if (count($activity->changes['after']) == 1)
        <span class="tw-font-bold">
            {{ $activity->username }}
        </span>
        updated the <span class="tw-text-grey-dark">{{ key($activity->changes['after']) }}</span> of the project
    @else
        <span class="tw-font-bold">
            {{ $activity->username }}
        </span>
        updated the project
    @endif
</div>

<div>
    <span class="tw-text-grey tw-text-xs">
        {{ $activity->created_at->diffForHumans() }}
    </span>
</div>


