<div>
    <span class="tw-font-bold">
        {{ $activity->username }}
    </span>
    completed task
    <span class="tw-font-bold">
        {{ $activity->subject->name }}
    </span>
</div>

<div>
    <span class="tw-text-grey tw-text-xs">
        {{ $activity->created_at->diffForHumans() }}
    </span>
</div>
