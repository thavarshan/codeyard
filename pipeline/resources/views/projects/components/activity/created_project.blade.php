<div>
    <span class="tw-font-bold">
        {{ $activity->username }}
    </span>
    created a new project
    <span class="tw-font-bold">
        {{ $project->title }}
    </span>
</div>

<div>
    <span class="tw-text-grey tw-text-xs">
        {{ $project->created_at->diffForHumans() }}
    </span>
</div>
