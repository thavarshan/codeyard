@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mb-4">
            <div class="col">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb tw-bg-grey-lightest">
                    <li class="breadcrumb-item tw-text-sm">
                        <a href="/" class="tw-text-indigo hover:tw-text-indigo-lighter">Home</a>
                    </li>
                    <li class="breadcrumb-item tw-text-sm active" aria-current="page">
                        Projects
                    </li>
                  </ol>
                </nav>
            </div>
        </div>

        <div class="row">
            <div class="col-md-9">
                <div class="mb-5 tw-flex">
                    <div class="tw-felx tw-flex-1 tw-items-start">
                        <h5 class="tw-font-thin mb-0">
                            All Projects
                        </h5>

                        <span class="tw-text-grey tw-text-sm">
                            List of all projects available
                        </span>
                    </div>

                    <div class="tw-flex tw-flex-1 tw-justify-end tw-items-start">
                        <form action="#" method="POST" accept-charset="utf-8">
                            @csrf

                            <div class="form-group mb-0 tw-relative">
                                <input type="text" class="tw-bg-grey-lighter tw-appearance-none tw-border-2 tw-border-grey-lighter tw-rounded-full tw-w-full tw-py-2 tw-pl-8 tw-pr-4 tw-text-grey-darker tw-leading-tight focus:tw-outline-none focus:tw-bg-white focus:tw-border-indigo{{ $errors->has('query') ? ' tw-border-red-lighter' : '' }}" placeholder="Search Projects" name="">
                                <button type="submit" class="tw-absolute tw-pin-l tw-pin-t tw-text-grey tw-mt-3 tw-ml-3">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="card-columns">
                    @forelse ($projects as $project)
                        @include ('projects.partials.project-card')
                    @empty
                        <div>
                            <span class="tw-text-grey-dark">
                                No projects found
                            </span>
                        </div>
                    @endforelse
                </div>
            </div>

            <div class="col-md-3">
                <div class="mb-3">
                    <h6 class="tw-font-thin mb-0">
                        Latest Updates
                    </h6>
                </div>

                <div>
                    <span class="tw-text-grey-dark">
                        No latest updates
                    </span>
                </div>
            </div>
        </div>
    </div>
@endsection
