@extends ('layouts.app')

@section ('content')
    <div class="container">
        <div class="row mb-4">
            <div class="col">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb tw-bg-grey-lightest">
                    <li class="breadcrumb-item tw-text-sm">
                        <a href="/" class="tw-text-indigo hover:tw-text-indigo-lighter">Home</a>
                    </li>
                    <li class="breadcrumb-item tw-text-sm"><a href="{{ route('projects.index') }}" class="tw-text-indigo hover:tw-text-indigo-lighter">Projects</a></li>
                    <li class="breadcrumb-item tw-text-sm active" aria-current="page">{{ $project->title }}</li>
                  </ol>
                </nav>
            </div>
        </div>

        <div class="row">
            <div class="col-md-9">
                <div class="mb-4">
                    <h5 class="mb-0">
                        <div class="dropdown">
                            <span class="tw-font-medium">{{ $project->title }}</span>
                            <a class="btn btn-link tw-text-grey dropdown-toggle px-0" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-chevron-circle-down"></i>
                            </a>

                            <div class="dropdown-menu tw-dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <a class="dropdown-item" href="{{ route('projects.edit', ['id' => $project->id]) }}">Edit</a>
                                <form class="tw-inline" action="{{ $project->path() }}" method="POST" accept-charset="utf-8">
                                    @csrf
                                    @method ('DELETE')
                                    <button type="submit" class="dropdown-item text-danger">Delete</button>
                                </form>
                            </div>
                        </div>
                    </h5>
                </div>

                <div class="tw-mb-10">
                    <p class="tw-text-grey-dark mb-3">
                        {{ $project->description }}
                    </p>

                    <div class="mb-4">
                        <span class="tw-text-sm tw-text-grey">
                            Last updated {{ $project->updated_at->diffForHumans() }}
                        </span>
                    </div>

                    <div>
                        <ul class="nav m-0 p-0">
                            @foreach ($project->members as $member)
                                <li class="nav-item tw--ml-2">
                                    <a class="d-inline-block tw-border-2 tw-border-white tw-rounded-full" href="#" data-toggle="tooltip" data-placement="top" title="{{ $member->name }}">
                                        <img class="tw-w-10 tw-h-10 tw-rounded-full" src="{{ asset('img/user.jpg') }}" alt="{{ $member->name }}">
                                    </a>
                                </li>
                            @endforeach

                            <li class="nav-item tw--ml-2">
                                <a class="d-inline-block tw-border-2 tw-border-white tw-rounded-full tw-relative" href="#" data-toggle="tooltip" data-placement="top" title="{{ $project->owner->name }}">
                                    <img class="tw-w-10 tw-h-10 tw-rounded-full" src="{{ asset('img/user.jpg') }}" alt="{{ $project->owner->name }}">

                                    <span class="tw-rounded-full tw-h-3 tw-w-3 tw-bg-yellow-dark tw-absolute tw-pin-r tw-pin-b"></span>
                                </a>
                            </li>

                            @can ('manage', $project)
                                <li class="nav-item">
                                    <a class="d-inline-block tw-border-2 tw-border-white tw-rounded-full tw-ml-2" href="#" data-toggle="modal" data-target="#userModal">
                                        <span class="fas fa-plus-circle tw-w-10 tw-h-10"></span>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </div>
                </div>

                <div class="project-tabs">
                    <ul class="nav nav-pills nav-justified mb-3 tw-rounded tw-overflow-hidden tw-bg-grey-lighter tw-px-4 tw-py-2" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="task-tab" data-toggle="tab" href="#task" role="tab" aria-controls="task" aria-selected="true">
                                Tasks
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="activity-tab" data-toggle="tab" href="#activity" role="tab" aria-controls="activity" aria-selected="false">
                                Activity
                            </a>
                        </li>
                    </ul>

                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active tw-rounded tw-overflow-hidden" id="task" role="tabpanel" aria-labelledby="task-tab">
                            <div class="tw-bg-grey-lighter tw-p-4">
                                @forelse ($project->tasks as $task)
                                    @include ('projects.components.task')
                                @empty
                                    <span class="tw-text-grey-dark">Project has no tasks</span>
                                @endforelse

                                <div class="mt-3 pt-3">
                                    <a href="#" class="btn btn-link py-0 px-0" data-toggle="modal" data-target="#exampleModal">
                                        <i class="fas fa-plus-circle"></i> Add New
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="activity" role="tabpanel" aria-labelledby="activity-tab">
                            <div class="list-group tw-bg-grey-lighter tw-p-4">
                                @foreach ($project->activity as $activity)
                                    @include ('projects.components.activity')
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-3">
                <div class="tw-rounded tw-bg-grey-lighter tw-p-4">
                    <h6 class="tw-uppercase tw-tracking-wide tw-text-grey-dark tw-text-xs mb-2">
                        General Notes
                    </h6>

                    <form action="{{ $project->path() }}" method="POST" accept-charset="utf-8">
                        @csrf
                        @method('PATCH')

                        <textarea rows="5" name="notes" class="tw-w-full tw-bg-grey-lighter tw-text-grey tw-text-sm tw-leading-normal tw-pb-3 mb-3 focus:tw-outline-none" onChange="this.form.submit()">{{ $project->notes ?: 'Anything special that you want to make a note of?'  }}</textarea>
                    </form>

                    <span class="tw-block tw-text-xs tw-text-grey">
                        Click below "General Notes" to add or edit the note.
                    </span>
                </div>
            </div>
        </div>
    </div>
@endsection

@section ('modal')
    <!-- New Task Modal -->
    @include ('projects.partials.modals.task-modal', [
        'id' => $project->id
    ])

    <!-- New User Modal -->
    @include ('projects.partials.modals.user-modal', [
        'project' => $project
    ])
@endsection
