@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mb-4">
            <div class="col">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb tw-bg-grey-lightest">
                    <li class="breadcrumb-item tw-text-sm">
                        <a href="/" class="tw-text-indigo hover:tw-text-indigo-lighter">Home</a>
                    </li>
                    <li class="breadcrumb-item tw-text-sm">
                        <a href="{{ route('projects.index') }}" class="tw-text-indigo hover:tw-text-indigo-lighter">Projects</a>
                    </li>
                    <li class="breadcrumb-item tw-text-sm active" aria-current="page">Create Project</li>
                  </ol>
                </nav>
            </div>
        </div>

        <div class="row">
            <div class="col-md-9">
                <div class="mb-5">
                    <h5 class="tw-font-thin mb-0">
                        Create New Project
                    </h5>

                    <span class="tw-text-grey tw-text-sm">
                        A repository contains all the files for your project, including the revision history.
                    </span>
                </div>

                <form class="tw-max-w-md" action="{{ route('projects.store') }}" method="POST" accept-charset="utf-8">
                    @csrf

                    @include('projects.partials.form', [
                        'project' => new App\Project,
                        'buttonText' => 'Create Project'
                    ])
                </form>
            </div>

            <div class="col-md-3">

            </div>
        </div>
    </div>
@endsection
