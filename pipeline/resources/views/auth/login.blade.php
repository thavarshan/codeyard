@extends('auth.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="tw-h-screen tw-flex tw-flex-col tw-pt-12">
                    <div class="tw-mb-8">
                        <h5 class="tw-mb-4 tw-font-thin tw-text-3xl">
                            Sign In
                        </h5>
                        <span class="tw-block tw-text-grey">
                            Welcome back to <span class="tw-font-bold tw-text-grey-dark">{{ config('app.name') }}</span>. Please sign in to create new projects and add tasks.
                        </span>
                    </div>

                    <form class="tw-block" method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group">
                            <label for="email" class="tw-block tw-uppercase tw-tracking-wide tw-text-grey tw-text-xs tw-font-bold tw-mb-2">
                                Email
                            </label>

                            <input id="email" type="email" class="tw-bg-grey-lighter tw-appearance-none tw-border-2 tw-border-grey-lighter tw-rounded tw-w-full tw-py-2 tw-px-4 tw-text-grey-darker tw-leading-tight focus:tw-outline-none focus:tw-bg-white focus:tw-border-indigo{{ $errors->has('email') ? ' tw-border-red-lighter' : '' }}" name="email" value="{{ old('email') }}" placeholder="john.doe@example.com" required autofocus>

                            @if ($errors->has('email'))
                                <span class="tw-block tw-py-2 tw-text-xs tw-text-red-lighter" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="password" class="tw-block tw-uppercase tw-tracking-wide tw-text-grey tw-text-xs tw-font-bold tw-mb-2">{{ __('Password') }}</label>

                            <input id="password" type="password" class="tw-bg-grey-lighter tw-appearance-none tw-border-2 tw-border-grey-lighter tw-rounded tw-w-full tw-py-2 tw-px-4 tw-text-grey-darker tw-leading-tight focus:tw-outline-none focus:tw-bg-white focus:tw-border-indigo{{ $errors->has('password') ? ' tw-border-red-lighter' : '' }}" name="password" placeholder="Password" required>

                            @if ($errors->has('password'))
                                <span class="tw-block tw-py-2 tw-text-xs tw-text-red-lighter" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group tw-mb-8">
                            <div class="form-check custom-control custom-checkbox tw-flex tw-items-center">
                                <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="custom-control-label tw-block tw-text-grey tw-text-sm tw-font-medium tw-mb-2" for="remember">
                                    Keep me signed in
                                </label>
                            </div>
                        </div>

                        <div class="form-group mb-0 tw-flex tw-justify-between tw-items-center">
                            <button type="submit" class="tw-btn">
                                Sign In
                            </button>

                            @if (Route::has('password.request'))
                                <a class="tw-text-indigo-light hover:tw-text-indigo-lighter tw-no-underline" href="{{ route('password.request') }}">
                                    Forgot your password?
                                </a>
                            @endif
                        </div>
                    </form>

                    <div class="tw-mb-8 tw-mt-8">
                        <span class="tw-text-grey">
                            Don't have an account yet?
                            <a href="{{ route('register') }}" class="tw-text-indigo-light hover:tw-text-indigo-lighter tw-no-underline">sign up</a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
