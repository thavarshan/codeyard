@extends('auth.profile.layouts.base')

@section('body')
    <div>
        <h5 class="tw-font-thin mb-2">
            Cancel Your Account
        </h5>

        <p class="mb-5 tw-text-grey-dark">
            Are you <strong>really</strong> sure that you want to cancel? Don't forget you'll be missing out on a lot of cool stuff. And, come on, I thought we were friends? WTH?
        </p>

        <div>
            <form action="{{ route('profile.destroy') }}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" class="tw-btn">
                    Yes, I'm sure.
                </button>
            </form>
        </div>
    </div>
@endsection
