<ul class="nav flex-column text-center">
    <li class="nav-item">
        <a class="nav-link tw-text-indigo-light hover:tw-text-indigo-lighter tw-no-underline" href="{{ route('profile', ['user' => auth()->user()->slug]) }}">Profile</a>
    </li>
    <li class="nav-item">
        <a class="nav-link tw-text-indigo-light hover:tw-text-indigo-lighter tw-no-underline" href="{{ route('profile.settings') }}">Settings</a>
    </li>
    <li class="nav-item">
        <a class="nav-link tw-text-indigo-light hover:tw-text-indigo-lighter tw-no-underline disabled" href="#">Support</a>
    </li>
    <li class="nav-item">
        <a class="nav-link tw-text-indigo-light hover:tw-text-indigo-lighter tw-no-underline" href="{{ route('profile.cancel') }}">Cancel</a>
    </li>
    <li class="nav-item">
        <a class="nav-link tw-text-indigo-light hover:tw-text-indigo-lighter tw-no-underline" href="{{ route('logout') }}"
           onclick="event.preventDefault();
            document.getElementById('logout-form').submit();">
            {{ __('Sign out') }}
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </li>
</ul>
