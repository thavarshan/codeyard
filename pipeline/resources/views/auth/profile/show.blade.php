@extends('auth.profile.layouts.base')

@section('body')
    <div>
        <div class="mb-5">
            <h5 class="tw-font-thin mb-0">
                Your Profile
            </h5>
        </div>

        <div>
            <h6 class="tw-font-thin">
                Recent Activity
            </h6>
        </div>
    </div>
@endsection
