@extends('auth.profile.layouts.base')

@section('body')
    <div>
        <div class="mb-5">
            <h5 class="tw-font-thin mb-0">
                Your Profile Settings
            </h5>
        </div>

        <form method="POST" action="{{ route('profile.update') }}">
            @csrf
            @method('PATCH')

            <div class="form-group row">
                <div class="col-lg-6 mb-3 mb-lg-0">
                    <label class="tw-label" for="first_name">First Name</label>

                    <input id="first_name" type="text" class="tw-input {{ $errors->has('first_name') ? ' tw-border-red-lighter' : '' }}" name="first_name" value="{{ $user->first_name ?: old('first_name') }}" placeholder="First name" required autofocus>

                    @if ($errors->has('first_name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="col-lg-6 mb-3 mb-lg-0">
                    <label class="tw-label" for="last_name">Last Name</label>

                    <input id="last_name" type="text" class="tw-input {{ $errors->has('last_name') ? ' tw-border-red-lighter' : '' }}" name="last_name" value="{{ $user->last_name ?: old('last_name') }}" placeholder="Last name" required>

                    @if ($errors->has('last_name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label class="tw-label" for="name">Username</label>

                <input id="name" type="text" class="tw-input {{ $errors->has('name') ? ' tw-border-red-lighter' : '' }}" name="name" value="{{ $user->name ?: old('name') }}" placeholder="Username" required autofocus>

                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif

                <span class="form-text text-muted">
                    <small>
                        Your username needs to be unique
                    </small>
                </span>
            </div>

            <div class="form-group mb-5">
                <label class="tw-label" for="email">Email</label>

                <input id="email" type="email" class="tw-input {{ $errors->has('email') ? ' tw-border-red-lighter' : '' }}" name="email" value="{{ $user->email ?: old('email') }}" placeholder="Email address" required>

                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group mb-0">
                <button type="submit" class="tw-btn">
                    {{ __('Update profile') }}
                </button>
            </div>
        </form>
    </div>

    <div class="my-5">
        &nbsp;
    </div>

    <div class="mt-5">
        <div class="mb-5">
            <h5 class="tw-font-thin mb-0">
                Update Your Security Settings
            </h5>
        </div>

        <form action="{{ route('profile.update.password') }}" method="POST">
            @csrf
            @method('PATCH')

            <div class="form-group">
                <label class="tw-label" for="email">Email</label>

                <input id="email" type="email" class="tw-input {{ $errors->has('email') ? ' tw-border-red-lighter' : '' }}" name="email" {{-- value="{{ old('email') }}" --}} placeholder="Email address" value="{{ auth()->user()->email }}" required>

                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif

                <span class="form-text text-muted">
                    <small>
                        Confirm your email address
                    </small>
                </span>
            </div>

            <div class="form-group">
                <label class="tw-label" for="password">Password</label>

                <input
                    id="password"
                    type="password"
                    class="tw-input {{ $errors->has('password') ? ' tw-border-red-lighter' : '' }}"
                    name="password"
                    placeholder="Password" >

                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif

                <span class="form-text text-muted">
                    <small>
                        Make sure your password is at least 8 characters long, contains uppercase letters, lowercase letters, numbers, and characters
                    </small>
                </span>
            </div>

            <div class="form-group mb-5">
                <label class="tw-label" for="password-confirm">Confirm Password</label>

                <input
                    id="password-confirm"
                    type="password"
                    class="tw-input"
                    name="password_confirmation"
                    placeholder="Confirm password">
            </div>

            <div class="form-group mb-0">
                <button type="submit" class="tw-btn">
                    {{ __('Reset password') }}
                </button>
            </div>
        </form>
    </div>
@endsection

