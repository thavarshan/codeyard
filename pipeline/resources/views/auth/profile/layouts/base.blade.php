@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row pt-5">
            <div class="col-md-2 d-none d-md-block">
                <div class="text-center mb-2">
                    <img class="tw-w-32 tw-h-32 tw-rounded-full" src="{{ asset('img/user.jpg') }}" alt="{{ auth()->user()->name }}">
                </div>

                <div class="text-center mb-5">
                    <h5 class="tw-font-thin mb-2">
                        {{ $user->first_name }} {{ $user->last_name }}
                    </h5>
                    <h6 class="tw-text-grey tw-italic">
                        {{ '@' . $user->name }}
                    </h6>
                </div>

                @include('auth.profile.partials.links')
            </div>

            <div class="col-lg-5 col-md-8 offset-md-1">
                @yield('body')
            </div>
        </div>
    </div>
@endsection
