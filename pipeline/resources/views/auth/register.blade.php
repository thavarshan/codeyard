@extends('auth.layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="tw-h-screen tw-flex tw-flex-col tw-pt-12">
                    <div class="tw-mb-8">
                        <h5 class="tw-mb-4 tw-font-thin tw-text-3xl">
                            Sign Up
                        </h5>
                        <span class="tw-block tw-text-grey">
                            Welcome to <span class="tw-font-bold tw-text-grey-dark">{{ config('app.name') }}</span>. Create an account to participate in projects.
                        </span>
                    </div>

                    <form class="tw-block" method="POST" action="{{ route('register') }}">
                        @csrf

                        {{-- <div class="form-group">
                            <label for="first_name" class="tw-label">
                                First Name
                            </label>

                            <input id="first_name" type="text" class="tw-bg-grey-lighter tw-appearance-none tw-border-2 tw-border-grey-lighter tw-rounded tw-w-full tw-py-2 tw-px-4 tw-text-grey-darker tw-leading-tight focus:tw-outline-none focus:tw-bg-white focus:tw-border-indigo{{ $errors->has('first_name') ? ' tw-border-red-lighter' : '' }}" name="first_name" value="{{ old('first_name') }}" placeholder="John" required autofocus>

                            @if ($errors->has('first_name'))
                                <span class="tw-block tw-py-2 tw-text-xs tw-text-red-lighter" role="alert">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="last_name" class="tw-label">
                                Last Name
                            </label>

                            <input id="last_name" type="text" class="tw-bg-grey-lighter tw-appearance-none tw-border-2 tw-border-grey-lighter tw-rounded tw-w-full tw-py-2 tw-px-4 tw-text-grey-darker tw-leading-tight focus:tw-outline-none focus:tw-bg-white focus:tw-border-indigo{{ $errors->has('last_name') ? ' tw-border-red-lighter' : '' }}" name="last_name" value="{{ old('last_name') }}" placeholder="Doe" required autofocus>

                            @if ($errors->has('last_name'))
                                <span class="tw-block tw-py-2 tw-text-xs tw-text-red-lighter" role="alert">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div> --}}

                        <div class="form-group">
                            <label for="name" class="tw-label">
                                Username
                            </label>

                            <input id="name" type="text" class="tw-input {{ $errors->has('name') ? ' tw-border-red-lighter' : '' }}" name="name" value="{{ old('name') }}" placeholder="JohnDoe" required autofocus>

                            @if ($errors->has('name'))
                                <span class="tw-block tw-py-2 tw-text-xs tw-text-red-lighter" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="email" class="tw-label">
                                Email
                            </label>

                            <input id="email" type="email" class="tw-input {{ $errors->has('email') ? ' tw-border-red-lighter' : '' }}" name="email" value="{{ old('email') }}" placeholder="john.doe@example.com" required>

                            @if ($errors->has('email'))
                                <span class="tw-block tw-py-2 tw-text-xs tw-text-red-lighter" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="password" class="tw-label">{{ __('Password') }}</label>

                            <input id="password" type="password" class="tw-input {{ $errors->has('password') ? ' tw-border-red-lighter' : '' }}" name="password" placeholder="Password" required>

                            @if ($errors->has('password'))
                                <span class="tw-block tw-py-2 tw-text-xs tw-text-red-lighter" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group tw-mt-8">
                            <span class="tw-text-grey tw-text-sm">
                                By clicking "Sign Up" you agree to our <a class="tw-text-indigo-light hover:tw-text-indigo-lighter tw-no-underline" href="#">Privacy Policy</a> and <a class="tw-text-indigo-light hover:tw-text-indigo-lighter tw-no-underline" href="#">Terms &amp; Conditions</a>.
                            </span>
                        </div>

                        <div class="form-group mb-0 tw-flex tw-justify-between tw-items-center">
                            <button type="submit" class="tw-btn">
                                Sign Up
                            </button>
                        </div>
                    </form>

                    <div class="tw-mb-8 tw-mt-8">
                        <span class="tw-text-grey">
                            Already have an account?
                            <a href="{{ route('login') }}" class="tw-text-indigo-light hover:tw-text-indigo-lighter tw-no-underline">sign in</a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

