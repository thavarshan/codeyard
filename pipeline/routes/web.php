<?php

Route::get('/', function () {
    return view('welcome');
})->name('welcome')->middleware('guest');

Route::group(
    [
    'prefix' => 'projects',
    'middleware' => 'auth',
],
    function () {
        Route::get('/', 'ProjectController@index')
            ->name('projects.index');

        Route::post('/', 'ProjectController@store')
            ->name('projects.store');

        Route::get('/create', 'ProjectController@create')
            ->name('projects.create');

        Route::get('/{project}', 'ProjectController@show')
            ->name('projects.show');

        Route::get('/{project}/edit', 'ProjectController@edit')
            ->name('projects.edit');

        Route::patch('/{project}', 'ProjectController@update')
            ->name('projects.update');

        Route::delete('/{project}', 'ProjectController@destroy')
            ->name('projects.destroy');

        Route::post('/{project}/tasks', 'TaskController@store')
            ->name('tasks.store');

        Route::patch('/{project}/tasks/{task}', 'TaskController@update')
            ->name('tasks.update');

        Route::post('/{project}/invitations', 'ProjectInvitationsController@store');
    }
);

Route::group(
    [
    'prefix' => 'profile',
    'middleware' => 'auth',
    'namespace' => 'Auth'
],
    function () {
        Route::get('/settings', 'ProfileController@edit')
            ->name('profile.settings');

        Route::patch('/settings/update', 'ProfileController@update')
            ->name('profile.update');

        Route::patch(
            '/settings/update/password',
            'UpdatePasswordController@update'
        )->name('profile.update.password');

        Route::get('/cancel', 'DeleteUserController@show')
            ->name('profile.cancel');

        Route::delete('/destroy', 'DeleteUserController@destroy')
            ->name('profile.destroy');

        Route::get('/{user}', 'ProfileController@show')
            ->name('profile');
    }
);

Auth::routes();
