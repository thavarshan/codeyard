<?php

namespace Tests\Unit;

use App\Project;
use Tests\TestCase;
use Facades\Tests\Setup\ProjectFactory;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ActivityTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_has_a_user()
    {
        $user = $this->signIn();

        $project = create(Project::class, [
            'owner_id' => auth()->id()
        ]);

        $this->assertEquals(auth()->id(), $project->activity->first()->user->id);
    }
}
