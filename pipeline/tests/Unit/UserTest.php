<?php

namespace Tests\Unit;

use App\User;
use App\Project;
use Tests\TestCase;
use Facades\Tests\Setup\ProjectFactory;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    protected $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = create('App\User');
    }

    /** @test */
    public function a_user_can_determine_their_avatar_path()
    {
        $this->assertEquals(
            asset('images/avatars/default.svg'),
            $this->user->avatar_path
        );

        $this->user->avatar_path = 'avatars/me.jpg';

        $this->assertEquals(
            asset('avatars/me.jpg'),
            $this->user->avatar_path
        );
    }

    /** @test */
    public function a_user_has_accessible_projects()
    {
        $john = create(User::class);
        $this->signIn($john);

        $project = create(Project::class, [
            'owner_id' => auth()->id()
        ]);

        $this->assertCount(1, $john->accessibleProjects());

        $sally = create(User::class);
        $nick = create(User::class);

        $project = tap(ProjectFactory::ownedBy($sally)->create())->invite($nick);

        $this->assertCount(1, $john->accessibleProjects());

        $project->invite($john);

        $this->assertCount(2, $john->accessibleProjects());
    }
}
