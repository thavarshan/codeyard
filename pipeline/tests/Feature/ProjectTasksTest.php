<?php

namespace Tests\Feature;

use App\Project;
use Facades\Tests\Setup\ProjectFactory;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectTasksTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function guests_cannot_add_tasks_to_projects()
    {
        $this->signIn();
        $project = create('App\Project');
        auth()->logout();

        $this->post($project->path() . '/tasks')->assertRedirect('login');
    }

    /** @test */
    public function only_the_owner_of_a_project_may_add_tasks()
    {
        $this->signIn();

        $project = factory('App\Project')->create();

        $this->post($project->path() . '/tasks', ['name' => 'Test task'])
            ->assertStatus(403);

        $this->assertDatabaseMissing('tasks', ['name' => 'Test task']);
    }

    /** @test */
    public function only_the_owner_of_a_project_may_update_a_task()
    {
        $this->signIn();

        $project = ProjectFactory::withTasks(1)->create();

        $this->patch($project->tasks[0]->path(), ['name' => 'changed'])
             ->assertStatus(403);

        $this->assertDatabaseMissing('tasks', ['name' => 'changed']);
    }

    /** @test */
    public function a_project_can_have_tasks()
    {
        $this->withoutExceptionHandling();

        $this->signIn();

        $project = ProjectFactory::create();

        $this->actingAs($project->owner)
             ->post($project->path() . '/tasks', ['name' => 'Test task']);

        $this->get($project->path())
            ->assertSee('Test task');
    }

    /** @test */
    public function a_task_can_be_updated()
    {
        $this->signIn();

        $project = ProjectFactory::withTasks(1)->create();

        $this->actingAs($project->owner)
             ->patch($project->tasks[0]->path(), [
                'name' => 'changed'
            ]);

        $this->assertDatabaseHas('tasks', [
            'name' => 'changed'
        ]);
    }

    /** @test */
    public function a_task_can_be_completed()
    {
        $this->signIn();

        $project = ProjectFactory::withTasks(1)->create();

        $this->actingAs($project->owner)
            ->patch($project->tasks[0]->path(), [
                'name' => 'changed',
                'completed' => true
            ]);

        $this->assertDatabaseHas('tasks', [
            'name' => 'changed',
            'completed' => true
        ]);
    }

    /** @test */
    public function a_task_can_be_marked_as_incomplete()
    {
        $this->withoutExceptionHandling();

        $this->signIn();

        $project = ProjectFactory::withTasks(1)->create();

        $this->actingAs($project->owner)
            ->patch($project->tasks[0]->path(), [
                'name' => 'changed',
                'completed' => true
            ]);

        $this->patch($project->tasks[0]->path(), [
            'name' => 'changed',
            'completed' => false
        ]);

        $this->assertDatabaseHas('tasks', [
            'name' => 'changed',
            'completed' => false
        ]);
    }

    /** @test */
    public function a_task_requires_a_name()
    {
        $this->signIn();

        $project = ProjectFactory::create();

        $attributes = factory('App\Task')->raw(['name' => '']);

        $this->actingAs($project->owner)
             ->post($project->path() . '/tasks', $attributes)
            ->assertSessionHasErrors('name');
    }
}
