<?php

Route::get('/', 'PageController@index')->name('home');

Route::post('/store', 'PageController@store')->name('pages.store');
Route::get('/{page}', 'PageController@show')->name('pages.show');
