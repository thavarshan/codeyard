<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;
use App\Http\Requests\PageRequest;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('welcome', [
            'subtitle' => 'Welcome',
            'pages' => Page::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageRequest $request)
    {
        $page = Page::create([
            'name' => $request->name,
            'slug' => str_slug($request->name)
        ]);

        $this->generatePage($page->slug);

        return redirect($page->path());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        return view('pages.' . $page->slug, [
            'subtitle' => $page->name
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        //
    }

    /**
     * Generate page blade file in resources/views folder
     *
     * @param  string $slug
     * @return Illuminate\Http\Response
     */
    public function generatePage($slug)
    {
        $path = resource_path('views/pages/');
        $source = $path . '_base.blade.php';
        $page = $path . $slug . '.blade.php';

        if (! file_exists($page)) {
            return copy($source, $page);
        }

        unlink($page);
        return copy($source, $page);
    }
}
