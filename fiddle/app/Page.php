<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    /**
     * Attributes to ignore when mass assigning
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * A page ahs a designated path
     *
     * @return string
     */
    public function path()
    {
        return "/pages/{$this->slug}";
    }

    /**
     * Route model binding identifies slug
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
