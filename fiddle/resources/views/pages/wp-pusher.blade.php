@extends('layouts.app')

@section('body')
    <div class="nav-container mx-16">
        <div class="navbar flex flex-col md:flex-row justify-between items-center py-8 mb-8">
            <a href="{!! route('home') !!}" class="mb-4 md:mb-0">
                <span class="font-bold text-lg text-indigo-dark">
                    Fiddle<sup>®</sup>
                </span>
            </a>
            <div class="flex flex-col md:flex-row font-bold text-sm">
                <div class="mx-5 mb-4 md:mb-0">
                    <a href="#" class="hover:text-indigo-dark">Download</a>
                </div>
                <div class="mx-5 mb-4 md:mb-0">
                    <a href="#" class="hover:text-indigo-dark">Learn More</a>
                </div>
                <div class="mx-5 mb-4 md:mb-0">
                    <a href="#" class="hover:text-indigo-dark">Pricing</a>
                </div>
                <div class="mx-5 mb-8 md:mb-0">
                    <a href="#" class="hover:text-indigo-dark">Help</a>
                </div>
                <div class="mx-5 mb-4 md:mb-0">
                    <a href="#" class="px-4 py-3 rounded-lg border-2 border-indigo hover:bg-indigo hover:text-white">Account</a>
                </div>
            </div>
        </div> <!-- End Navbar -->
    </div> <!-- End Navbar Container -->

    <div class="checkout max-w-xl mx-auto">
        <div class="panel flex flex-col md:flex-row mb-8 shadow-lg overflow-hidden md:rounded">
            <div class="panel-left w-full md:w-2/3 bg-white">
                <form class="" action="#" method="post">
                    <h1 class="text-3xl font-normal border-b p-10  border-grey-light">Checkout</h1>
                    <div class="p-10 pt-8 border-b border-grey-light">
                        <div class="flex items-center mb-4">
                            <div class="border-2 border-indigo py-2 px-4 rounded-full font-bold mr-2 text-indigo">1</div>
                            <h2 class="text-lg">Your Basic Information</h2>
                        </div>

                        <div class="row">
                            <div class="col-md-6 mb-4">
                                <label for="first_name" class="block text-sm font-bold text-grey-darkest mb-2">First Name</label>
                                <input type="text" class="rounded w-full text-sm bg-grey-light text-grey-darkesr px-3 py-3" id="first_name" name="" value="" placeholder="First Name">
                            </div>

                            <div class="col-md-6 mb-4">
                                <label for="last_name" class="block text-sm font-bold text-grey-darkest mb-2">Last Name</label>
                                <input type="text" class="rounded w-full text-sm bg-grey-light text-grey-darkesr px-3 py-3" id="last_name" name="" value="" placeholder="Last Name">
                            </div>

                            <div class="col-md-6">
                                <label for="email" class="block text-sm font-bold text-grey-darkest mb-2">Email</label>
                                <input type="email" class="rounded w-full text-sm bg-grey-light text-grey-darkesr px-3 py-3" id="email" name="" value="" placeholder="Email">
                            </div>
                        </div>
                    </div>

                    <div class="p-10 pt-8 border-b border-grey-light">
                        <div class="flex items-center mb-4">
                            <div class="border-2 border-indigo py-2 px-4 rounded-full font-bold mr-2 text-indigo">2</div>
                            <h2 class="text-lg">Payment Information</h2>
                        </div>

                        <div class="flex flex-wrap">
                            <div class="w-full mb-4">
                                <label for="payment" class="block text-sm font-bold text-grey-darkest mb-2">Credit Card</label>
                                <input type="text" class="rounded w-full text-sm bg-grey-light text-grey-darkesr px-3 py-3" id="payment" name="" value="" placeholder="Credit Card Details">
                            </div>
                        </div>
                    </div>

                    <div class="p-10 pt-8">
                        <button type="button" class="w-full bg-green text-white text-lg rounded px-4 py-4 mb-6 hover:bg-green-dark font-bold">Buy WP Pusher</button>

                        <div class="flex items-center justify-center mb-8">
                            <div class="">
                                <span class="font-bold">Need any help?</span> <span class="text-grey-darker">Don't hisitate to <a href="#" class="text-grey-darker underline">contact support</a> </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div> <!-- End Panel Left -->

            <div class="panel-right w-full md:w-1/3 bg-indigo text-white">
                <div class="p-10">
                    <h2 class="font-bold text-xl mb-4">Freelancer</h2>
                    <div class="mb-4">
                        <span class="tet-2xl align-top">$</span>
                        <span class="text-7xl font-light lh-fix">99</span>
                        <span class="stext-lg">/ year</span>
                    </div>
                    <div class="mb-8 italic w-3/4 leading-normal">
                        Automatically renews after 1 year
                    </div>
                    <div class="list-items mb-8">
                        <div class="flex items-cnter mb-4">
                            <div>
                                Use on <span class="font-bold">5</span> client sites
                            </div>
                        </div>
                        <div class="flex items-cnter mb-4">
                            <div>
                                Private Repositories
                            </div>
                        </div>
                        <div class="flex items-cnter mb-4">
                            <div>
                                Email Support
                            </div>
                        </div>
                    </div> <!-- End List Items -->

                    <div class="mb-10 pb-2">
                        <div class="mb-2">
                            Need <span class="font-bold">20</span> client?
                        </div>
                        <a href="#" class="text-white font-bold border-b-2 border-indigo-light">Switch to the Agency plan.</a>
                    </div>

                    <div class="border-b border-indigo-light"></div>

                    <div class="testimonial pt-10 text-lg italic leading-normal mb-4">
                        FTP is horrible to deal with. <span class="font-bold">WP Pusher</span> solves that issue, and the customer support is awesome!
                    </div>
                </div>
            </div> <!-- End Panel Right -->
        </div> <!-- End Panel -->

        <div class="copyright  text-grey-dark text-center text-sm mb-8">
            &copy; 2018 WP Pusher IVS &middot; <a href="#" class="text-indigo">Terms of Service</a>
        </div>
    </div> <!-- End Checkout Container -->
@endsection
