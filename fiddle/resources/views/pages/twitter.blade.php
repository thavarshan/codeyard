@extends('layouts.app')

@section('body')
	<nav class="bg-white">
		<div class="flex container mx-auto items-center py-4">
			<div class="w-2/5">
				<a href="#"class="top-nav-item">
					<i class="fas fa-home fa-lg"></i> Home
				</a>
				<a href="#"class="top-nav-item">
					<i class="fas fa-bolt fa-lg"></i> Moments
				</a>
				<a href="#"class="top-nav-item">
					<i class="fas fa-bell fa-lg"></i> Notifications
				</a>
				<a href="#"class="top-nav-item">
					<i class="fas fa-envelope fa-lg"></i> Messages
				</a>
			</div>

			<div class="w-1/5 text-center">
				<a href="#">
					<i class="fab fa-twitter fa-lg text-blue"></i>
				</a>
			</div>

			<div class="w-2/5 flex justify-end items-center">
				<div class="mr-4 relative">
					<input type="text" class="bg-grey-lighter h-8 px-4 py-2 text-xs w-48 rounded-full outline-none" placeholder="Search Twitter">
					<span class="absolute flex items-center pin-r pin-y mr-3">
						<i class="fas fa-search text-grey"></i>
					</span>
				</div>
				<div class="mr-4">
					<a href="#" class="inline-block">
						<img class="h-8 w-8 rounded-full" src="{!! asset('img/user.jpg') !!}" alt="Avatar">
					</a>
				</div>
				<div>
					<button type="button" class="bg-indigo text-white font-bold py-2 px-4 rounded-full hover:bg-indigo-dark">
						Tweet
					</button>
				</div>
			</div>
		</div>
	</nav> <!-- End Nav -->

	<div class="hero h-112"></div> <!-- Hero seaction -->

	<div class="bg-white shadow">
		<div class="container mx-auto flex items-center relative">
			<div class="w-1/4">
				<img src="{!! asset('img/user.jpg') !!}" alt="Logo" class="rounded-full h-48 w-48 absolute pin-x pin-t -mt-24">
			</div>

			<div class="w-1/2">
				<ul class="list-reset flex">
					<li class="text-center py-3 px-4 border-b-2 border-solid border-transparent hover:border-indigo">
						<a href="#" class="text-grey-darker font-medium">
							<div class="text-sm font-bold tracking-tight mb-1">Tweets</div>
							<div class="text-lg tracking-tight font-bold text-indigo">60</div>
						</a>
					</li>
					<li class="text-center py-3 px-4 border-b-2 border-solid border-transparent hover:border-indigo">
						<a href="#" class="text-grey-darker font-medium">
							<div class="text-sm font-bold tracking-tight mb-1">Following</div>
							<div class="text-lg tracking-tight font-bold text-indigo">4</div>
						</a>
					</li>
					<li class="text-center py-3 px-4 border-b-2 border-solid border-transparent hover:border-indigo">
						<a href="#" class="text-grey-darker font-medium">
							<div class="text-sm font-bold tracking-tight mb-1">Followers</div>
							<div class="text-lg tracking-tight font-bold text-indigo">3,810</div>
						</a>
					</li>
					<li class="text-center py-3 px-4 border-b-2 border-solid border-transparent hover:border-indigo">
						<a href="#" class="text-grey-darker font-medium">
							<div class="text-sm font-bold tracking-tight mb-1">Likes</div>
							<div class="text-lg tracking-tight font-bold text-indigo">9</div>
						</a>
					</li>
					<li class="text-center py-3 px-4 border-b-2 border-solid border-transparent hover:border-indigo">
						<a href="#" class="text-grey-darker font-medium">
							<div class="text-sm font-bold tracking-tight mb-1">Moments</div>
							<div class="text-lg tracking-tight font-bold text-indigo">1</div>
						</a>
					</li>
				</ul>
			</div>

			<div class="w-1/4 flex justify-end items-center">
				<div class="mr-6">
					<button type="button" class="bg-indigo hover:bg-indigo-dark text-white font-bold py-2 px-4 rounded-full">
						Follow
					</button>
				</div>

				<div class="">
					<a href="#" class="text-grey-dark">
						<i class="fas fa-ellipsis-v fa-lg"></i>
					</a>
				</div>
			</div>
		</div> <!-- End Container -->
	</div> <!-- End Hero Section Bar -->

	<div class="container mx-auto flex mt-3 text-sm leading-normal">
		<div class="w-1/4 pr-6 mt-8 mb-4">
			<h1>
				<a href="#" class="text-black">Talisman</a>
			</h1>
			<div class="mb-4">
				<a class="text-grey-darker" href="#">@talisman</a>
			</div>

			<div class="mb-4">
				A sandbox practice area for Tailwind CSS related experiments.
			</div>

			<div class="mb-2">
				<i class="fas fa-link text-grey-darker mr-1"></i>
				<a href="{!! route('home') !!}">talisman.com</a>
			</div>
			<div class="mb-4">
				<i class="fas fa-calendar text-grey-darker mr-1"></i>
				<a href="{!! route('home') !!}">Joined October 2018</a>
			</div>

			<div class="mb-4">
				<button type="button" class="bg-indigo hover:bg-indigo-dark text-white font-bold py-2 px-4 rounded-full w-full h-10">
					Tweet to Talisman
				</button>
			</div>

			<div class="mb-4">
				<i class="fas fa-user text-grey-darker mr-1"></i>
				<a href="#">27 Followers you know</a>
			</div>

			<div class="mb-4">
				<a href="#">
					<img class="rounded-full h-12 w-12" src="{!! asset('img/f1.jpg') !!}" alt="follower-1">
				</a>
				<a href="#">
					<img class="rounded-full h-12 w-12" src="{!! asset('img/f2.jpg') !!}" alt="follower-1">
				</a>
				<a href="#">
					<img class="rounded-full h-12 w-12" src="{!! asset('img/f3.jpg') !!}" alt="follower-1">
				</a>
				<a href="#">
					<img class="rounded-full h-12 w-12" src="{!! asset('img/f4.jpg') !!}" alt="follower-1">
				</a>
				<a href="#">
					<img class="rounded-full h-12 w-12" src="{!! asset('img/f1.jpg') !!}" alt="follower-1">
				</a>
				<a href="#">
					<img class="rounded-full h-12 w-12" src="{!! asset('img/f2.jpg') !!}" alt="follower-1">
				</a>
				<a href="#">
					<img class="rounded-full h-12 w-12" src="{!! asset('img/f3.jpg') !!}" alt="follower-1">
				</a>
				<a href="#">
					<img class="rounded-full h-12 w-12" src="{!! asset('img/f4.jpg') !!}" alt="follower-1">
				</a>
				<a href="#">
					<img class="rounded-full h-12 w-12" src="{!! asset('img/f1.jpg') !!}" alt="follower-1">
				</a>
				<a href="#">
					<img class="rounded-full h-12 w-12" src="{!! asset('img/f2.jpg') !!}" alt="follower-1">
				</a>
			</div>

			<div class="mb-4">
				<i class="fas fa-image text-grey-darker mr-1"></i>
				<a href="#">Photos and videos</a>
			</div>

			<div class="mb-4">
				<a href="#">
					<img src="" class="h-20 w-20 mr-1 mb-1" alt="">
				</a>
				<a href="#">
					<img src="" class="h-20 w-20 mr-1 mb-1" alt="">
				</a>
				<a href="#">
					<img src="" class="h-20 w-20 mr-1 mb-1" alt="">
				</a>
				<a href="#">
					<img src="" class="h-20 w-20 mr-1 mb-1" alt="">
				</a>
				<a href="#">
					<img src="" class="h-20 w-20 mr-1 mb-1" alt="">
				</a>
			</div>
		</div>

		<div class="w-1/2 bg-white mb-4">
			<div class="p-3 text-lg font-bold border-b border-solid border-grey-light">
				<a href="#" class="text-black mr-6">Tweets</a>
				<a href="#" class="mr-6">Tweets &amp; Replies</a>
				<a href="#">Media</a>
			</div>

			<div class="flex border-b border-solid border-grey-light">
				<div class="w-1/8 text-right pl-3 pt-3">

				</div>
			</div>
		</div>

		<div class="w-1/4">

		</div>
	</div>
@endsection
