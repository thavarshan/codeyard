@extends('layouts.app')

@section('body')

@include('sandbox.partials.navbar')

<div class="flex justify-center items-center py-32">
    <div class="w-full md:w-1/2">
        <div class="flex flex-wrap flex-col md:flex-row justify-around items-center">
            <div class="dropdown relative mb-4 md:mb-0 z-50">
                <a href="#" id="dropdown-btn-1" class="inline-block dropdown-btn px-5 py-3 bg-indigo text-white rounded-lg font-bold hover:bg-indigo-light focus:bg-indigo-dark focus:shadow-inner">
                    Dropdown
                </a>

                @include('sandbox.partials.custom-dropdown')
            </div>

            <div class="modal mb-4 md:mb-0">
                <a href="#" id="modal-btn-1" class="inline-block modal-btn px-5 py-3 bg-indigo text-white rounded-lg font-bold hover:bg-indigo-light focus:bg-indigo-dark focus:shadow-inner">
                    Modal
                </a>
            </div>

            <div class="bootstrap-modal mb-4 md:mb-0">
                <a href="#" class="inline-block px-5 py-3 bg-indigo text-white rounded-lg font-bold hover:bg-indigo-light focus:bg-indigo-dark focus:shadow-inner" data-toggle="modal" data-target="#exampleModal">
                    Bootstrap Modal
                </a>
            </div>

            @include('sandbox.partials.boot-dropdown')
        </div>
    </div>
</div>

@include('sandbox.partials.custom-modal')
@include('sandbox.partials.boot-modal')

@endsection
