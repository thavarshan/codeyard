@extends('layouts.app')

@section('body')
    <div class="min-h-screen bg-indigo-darkest">
        <div class="container mx-auto px-6 pt-32">
            <div class="row py-4 justify-center">
                <div class="col-lg-5 col-md-8 mb-6">
                    <form action="section-3_submit" class="blockl" method="post" accept-charset="utf-8">
                        <div class="mb-8 text-center">
                            <h3 class="text-white text-5xl">
                                Fiddle<sup>®</sup>
                            </h3>
                            <span class="text-grey-light font-semibold tracking-normal leading-normal">Welcome Back!</span>
                        </div>
                        <div class="mb-4">
                            <label class="block">
                                <input type="text" class="w-full outline-none p-4 rounded-lg bg-indigo-darker font-semibold text-grey-light text-center focus:bg-indigo-darkest border border-indigo-darker" name="email" value="" placeholder="Email" autofocus="email">
                            </label>

                            <span class="hidden text-red-dark font-semibold text-xs block text-center mt-3">
                                Something went wrong here!
                            </span>
                        </div>

                        <div class="mb-4">
                            <label class="block">
                                <input type="password" class="w-full outline-none p-4 rounded-lg bg-indigo-darker font-bold text-grey-light text-center focus:bg-indigo-darkest border border-indigo-darker" name="" value="" placeholder="Password">
                            </label>

                            <span class="hidden text-red-dark font-semibold text-xs block text-center mt-3">
                                Something went wrong here!
                            </span>
                        </div>

                        <div class="mb-6">
                            <button type="submit" class="w-full outline-none p-4 rounded-lg bg-yellow-dark font-semibold text-black hover:bg-yellow">Sign in <span class="text-black ml-1">&rarr;</span></button>
                        </div>
                    </form>
                </div>

                <!-- <div class="col-md-6 lg:px-24 mb-6">
                    <form action="section-3_submit" class="blockl" method="post" accept-charset="utf-8">
                        <div class="mb-8 text-center">
                            {{-- <h3 class="text-white text-5xl font-semibold ">
                                Fiddle<sup>®</sup>
                            </h3> --}}
                            <span class="text-grey-light font-semibold tracking-normal leading-normal">Let's get started</span>
                        </div>
                        <div class="mb-4">
                            <label class="block">
                                <input type="text" class="w-full outline-none p-4 rounded-lg bg-indigo-darker font-semibold text-grey-light text-center" name="username" value="" placeholder="Username" autofocus="username">
                            </label>

                            <span class="hidden text-red-dark font-semibold text-xs block text-center mt-3">
                                Something went wrong here!
                            </span>
                        </div>

                        <div class="mb-4">
                            <label class="block">
                                <input type="text" class="w-full outline-none p-4 rounded-lg bg-indigo-darker font-semibold text-grey-light text-center" name="email" value="" placeholder="Email" autofocus="email">
                            </label>

                            <span class="hidden text-red-dark font-semibold text-xs block text-center mt-3">
                                Something went wrong here!
                            </span>
                        </div>

                        <div class="mb-4">
                            <label class="block">
                                <input type="text" class="w-full outline-none p-4 rounded-lg bg-indigo-darker font-bold text-grey-light text-center" name="" value="" placeholder="Password">
                            </label>

                            <span class="hidden text-red-dark font-semibold text-xs block text-center mt-3">
                                Something went wrong here!
                            </span>
                        </div>

                        <div class="mb-6">
                            <button type="submit" class="w-full outline-none p-4 rounded-lg bg-yellow-dark font-semibold text-black hover:bg-yellow">Create an account <span class="text-black ml-1">&rarr;</span></button>
                        </div>
                    </form>
                </div> -->
            </div>
        </div>
    </div>
@endsection
