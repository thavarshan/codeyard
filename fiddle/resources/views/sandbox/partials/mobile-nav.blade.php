<div id="mobile-menu" class="absolute pin-y pin-x bg-indigo-darker z-99 hidden">
	<div class="flex max-w-sm mx-auto pt-32 justify-center">
		<a href="#" id="ham-menu-btn-close" class="absolute pin-t pin-r mt-2 mr-6">
			<span class="text-3xl text-white">
				&times;
			</span>
		</a>
		<ul class="list-reset flex flex-col text-center justify-center">
			<li class="mb-6">
				<a href="#" class="py-4 px-6 text-lg md:text-3xl text-indigo-lighter hover:text-white">
					Home
				</a>
			</li>

			<li class="mb-6">
				<a href="#" class="py-4 px-6 text-lg md:text-3xl text-indigo-lighter hover:text-white">
					About
				</a>
			</li>

			<li class="mb-6">
				<a href="#" class="py-4 px-6 text-lg md:text-3xl text-indigo-lighter hover:text-white">
					Support
				</a>
			</li>

			<li class="mb-6">
				<a href="#" class="py-4 px-6 text-lg md:text-3xl text-indigo-lighter hover:text-white">
					Sign in
				</a>
			</li>

			<li class="mb-6 mt-6">
				<a href="#" class="py-4 px-6 border border-white rounded-lg bg-white text-lg md:text-3xl text-black hover:bg-indigo-dark hover:text-white hover:border-indigo-dark">
					Sign up
				</a>
			</li>
		</ul>
	</div>
</div>
