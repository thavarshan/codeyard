<div class="dropdown relative mb-4 md:mb-0 z-50">
	<button class="dropdown-toggle inline-block px-5 py-3 bg-indigo text-white rounded-lg font-bold hover:bg-indigo-light focus:bg-indigo-dark focus:shadow-inner leading-normal" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		Dropdown Bootstrap
	</button>
	<div class="dropdown-menu absolute pin-t pin-r overflow-hidden bg-white rounded-lg shadow hidden mt-3" aria-labelledby="dropdownMenuButton">
		<a class="dropdown-item block whitespace-no-wrap w-full py-3 px-5 font-bold text-black leading-normal tracking-normal text-sm hover:bg-indigo-dark hover:text-white" href="#">Action</a>
		<a class="dropdown-item block whitespace-no-wrap w-full py-3 px-5 font-bold text-black leading-normal tracking-normal text-sm hover:bg-indigo-dark hover:text-white" href="#">Another action</a>
		<a class="dropdown-item block whitespace-no-wrap w-full py-3 px-5 font-bold text-black leading-normal tracking-normal text-sm hover:bg-indigo-dark hover:text-white" href="#">Something else here</a>
	</div>
</div>
