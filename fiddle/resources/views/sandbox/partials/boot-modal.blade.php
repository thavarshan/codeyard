<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						@include('layouts.svg.cancel', ['classes' => 'h-6 w-6'])
					</span>
				</button>
				<h5 class="modal-title" id="exampleModalLabel">
					Donec id elit non mi
				</h5>
			</div>
			<div class="modal-body">
				<p>
					Donec ullamcorper nulla non metus auctor fringilla. Maecenas sed diam eget risus varius blandit sit amet non magna. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.
				</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="py-3 uppercase text-grey-dark tracking-wide font-semibold text-sm" data-dismiss="modal">Close</button>
				<button type="button" class="px-4 py-3 uppercase text-white tracking-wide font-semibold text-sm rounded-lg bg-indigo" data-dismiss="modal">Save changes</button>
			</div>
		</div>
	</div>
</div>
