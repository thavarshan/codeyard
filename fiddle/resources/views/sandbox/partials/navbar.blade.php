<nav class="bg-indigo-darker py-2 items-center">
	<div class="container mx-auto px-6 flex">
		<a href="#" class="block leading-none py-4 pr-2 mr-4 tracking-wide font-bold">
			<span class="text-white">
				Fiddle<sup>®</sup>
			</span>
		</a>
		<a href="#" id="ham-menu-btn" class="block md:hidden ml-auto py-4 px-2 leading-none text-sm text-indigo-lightest font-semibold hover:text-white">
			<span class="fas fa-bars"></span>
		</a>
		<div class="hidden md:flex flex-grow">
			<ul class="list-reset flex flex-row mr-auto items-center">
				<li class="nav-item">
					<a href="#" class="block py-4 px-2 leading-none text-sm text-indigo-lightest font-semibold hover:text-white">
						Home
					</a>
				</li>
				<li class="nav-item">
					<a href="#" class="block py-4 px-2 leading-none text-sm text-indigo-lightest font-semibold hover:text-white">
						About
					</a>
				</li>
				<li class="nav-item">
					<a href="#" class="block py-4 px-2 leading-none text-sm text-indigo-lightest font-semibold hover:text-white">
						Support
					</a>
				</li>
			</ul>

			<ul class="list-reset flex flex-row ml-auto items-center">
				<li class="nav-item">
					<a href="#" class="block py-4 px-2 leading-none text-sm text-indigo-lightest font-semibold hover:text-white">
						Sign In
					</a>
				</li>
				<li class="nav-item ml-4">
					<a href="#" class="block py-3 px-4 leading-none text-sm text-black font-semibold hover:text-white border-white rounded-lg bg-white hover:bg-indigo-dark hover:border-indigo-dark">
						Sign Up
					</a>
				</li>
			</ul>
		</div>
	</div>
</nav>


@include('sandbox.partials.mobile-nav')
