<div id="dropdown-menu-1" class="dropdown-menu block absolute pin-r pin-t mt-16 rounded-lg overflow-hidden w-auto bg-white shadow hidden">
	<span class="block whitespace-no-wrap w-full py-3 px-5 leading-tight tracking-normal">
		<span class="font-medium text-grey-darker text-xs">
			Signed in as
		</span>
		<h6 class="font-bold text-lg text-black">
			Thavarshan
		</h6>
	</span>
	<hr class="border-0 bg-grey-light h-px">
	<a href="#" class="block whitespace-no-wrap w-full py-3 px-5 font-bold text-black leading-normal tracking-normal text-sm hover:bg-indigo-dark hover:text-white">
		Your profile
	</a>
	<a href="#" class="block whitespace-no-wrap w-full py-3 px-5 font-bold text-black leading-normal tracking-normal text-sm hover:bg-indigo-dark hover:text-white">
		Your repositories
	</a>
	<a href="#" class="block whitespace-no-wrap w-full py-3 px-5 font-bold text-black leading-normal tracking-normal text-sm hover:bg-indigo-dark hover:text-white">
		Your gists
	</a>
	<hr class="border-0 bg-grey-light h-px">
	<a href="#" class="block whitespace-no-wrap w-full py-3 px-5 font-bold text-black leading-normal tracking-normal text-sm hover:bg-indigo-dark hover:text-white">
		Support &amp; Help
	</a>
	<a href="#" class="block whitespace-no-wrap w-full py-3 px-5 font-bold text-black leading-normal tracking-normal text-sm hover:bg-indigo-dark hover:text-white">
		Settings &amp; Privacy
	</a>
	<a href="#" class="block whitespace-no-wrap w-full py-3 px-5 font-bold text-black leading-normal tracking-normal text-sm hover:bg-indigo-dark hover:text-white">
		Sign out
	</a>
</div>
