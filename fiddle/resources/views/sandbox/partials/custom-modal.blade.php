<div id="modal-1" class="modal absolute pin-y pin-x bg-black-20 z-50 hidden justify-center p-6">
	<div class="absolute modal-content rounded-lg overflow-hidden bg-white shadow-lg max-w-sm mt-16">
		<div class="modal-body p-8 select-none">
			<a href="#" class="close h-6 w-6 text-grey-dark float-right hover:text-grey-darkest">
				@include('layouts.svg.cancel', ['classes' => ''])
			</a>
			<h4 class="text-xl font-bold text-black leading-tight tracking-normal mb-4">
				Aenean lacinia bibendum nulla sed consectetur
			</h4>
			<p class="mb-10">
				Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Sed posuere consectetur est at lobortis. Donec id elit non mi porta gravida at eget metus.
			</p>
			<div class="text-right">
				<a href="#" class="close inline-block modal-btn px-5 py-3 bg-indigo text-white rounded-lg font-bold hover:bg-indigo-light focus:bg-indigo-dark focus:shadow-inner">
					Ok, got it!
				</a>
			</div>
		</div>
	</div>
</div>
