@extends('layouts.app')

@section('body')
<header class="h-screen bg-indigo-darker">
    <!-- <nav class="block py-5">
        <div class="container mx-auto px-6 md:px-0">
            <div class="row items-center">
                <div class="w-1/4 px-3">
                    <a href="{!! route('home') !!}" class="text-white text-lg font-bold">
                        Fiddle<sup>®</sup>
                    </a>
                </div>
                <div class="w-3/4 px-3">
                    <div class="flex md:hidden items-center justify-end">
                        <button type="button" class="inline md:hidden text-white hover:text-grey-lighter rounded p-1">
                            <i class="fas fa-bars"></i>
                        </button>
                    </div>
                    <div class="hidden md:flex items-center justify-end">
                        <div class="nav-links text-sm font-bold ml-5">
                            <a href="#" class="text-indigo-lightest hover:text-white">Home</a>
                        </div>
                        <div class="nav-links text-sm font-bold ml-5">
                            <a href="#" class="text-indigo-lightest hover:text-white">About</a>
                        </div>
                        <div class="nav-links text-sm font-bold ml-5">
                            <a href="#" class="text-indigo-lightest hover:text-white">Services</a>
                        </div>
                        <div class="nav-links text-sm font-bold ml-5">
                            <a href="#" class="text-indigo-lightest hover:text-white">Contact</a>
                        </div>
                        <div class="nav-links text-sm font-bold ml-5 inline lg:block">
                            <a href="#" class="text-indigo px-4 py-2 rounded-full bg-white hover:bg-indigo-lightest border-2 border-transparent" data-toggle="modal" data-target="#exampleModal">Sign Up</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav> -->

    @include('sandbox.partials.navbar')

    <section class="py-32">
        <div class="container mx-auto px-6 flex">
            <div class="row">
                <div class="w-full flex items-center px-3 text-center md:text-left">
                    <div class="main w-full">
                        <h6 class="tracking-wide leading-normal text-white uppercase">Welcome to</h6>
                        <h1 class="mb-0 text-6xl text-white leading-normal tracking-normal">
                            Fiddle<sup>®</sup>
                        </h1>
                        <p class="text-white text-xl leading-normal mb-3">
                            Etiam porta sem malesuada magna mollis euismod.<br class="hidden md:inline">
                            Curabitur blandit tempus porttitor.
                        </p>
                        <p class="text-sm text-white opacity-75 mb-12">
                            Nullam id dolor id nibh ultricies vehicula ut id elit.
                        </p>
                        <div class="mt-8 w-full">
                            <a href="#" class="text-indigo border-2 border-transparent bg-white rounded-full px-6 py-3 hover:bg-indigo-lightest mr-4">Get started</a>
                            <a href="#" class="text-white border-2 border-white rounded-full px-6 py-3 hover:bg-indigo-lightest hover:text-indigo">Learn more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</header>
@endsection
