@extends('layouts.app')

@section('body')
<div class="bst-container h-screen">
	<div class="row mt-12">
		<div class="col-lg-6 mb-8">
			<a href="{{ route('sandbox.section1') }}" class="block hover:shadow-lg">
				<div class="bg-indigo flex flex-1 justify-center items-center p-3 rounded-lg">
					<span class="text-white text-2xl font-semibold">Landing Page</span>
				</div>
			</a>
		</div>
		<div class="col-lg-6 mb-8">
			<a href="{{ route('sandbox.section2') }}" class="block hover:shadow-lg">
				<div class="bg-indigo flex flex-1 justify-center items-center p-3 rounded-lg">
					<span class="text-white text-2xl font-semibold">Component Experiments</span>
				</div>
			</a>
		</div>
        <div class="col-lg-6 mb-8">
            <a href="{{ route('sandbox.section3') }}" class="block hover:shadow-lg">
                <div class="bg-indigo flex flex-1 justify-center items-center p-3 rounded-lg">
                    <span class="text-white text-2xl font-semibold">Login Page</span>
                </div>
            </a>
        </div>
	</div>
</div>
@endsection
