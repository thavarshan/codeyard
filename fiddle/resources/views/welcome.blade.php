@extends('layouts.app')

@section('body')
    <div class="min-h-screen flex justify-center items-center">
        <div class="container mx-auto">
            <div class="flex flex-wrap justify-center text-center">
                <div class="w-full">
                    <h1 class="text-5xl font-sans mb-4">
                        <a class="text-indigo-dark no-underline" href="{!! route('home') !!}">
                            Fiddle<sup>®</sup>
                        </a>
                    </h1>
                </div>

                <div class="w-full mb-6">
                    <div class="px-3 py-2 flex justify-center">
                        <form class="w-full md:w-1/2" method="POST" action="{{ route('pages.store') }}">
                            @csrf
                            <div class="mb-4">
                                <input class="w-full text-center text-grey p-4 bg-white rounded-lg w-full border border-solid {{ $errors->has('name') ? ' border-red' : 'border-transparent' }} font-semibold outline-none"  id="name" type="text" name="name" value="{{ old('name') }}"  placeholder="Page Name" required autofocus>

                                @if ($errors->has('name'))
                                    <div class="block mt-3 text-center">
                                        <span class="text-red text-xs font-semibold">
                                            {{ $errors->first('name') }}
                                        </span>
                                    </div>
                                @endif
                            </div>

                            <div class="mb-4">
                                <button type="submit" class="inline focus:outline-none border-2 border-indigo focus:bg-indigo-light sm:text-lg bg-indigo hover:bg-indigo-light hover:border-indigo-light rounded-lg text-white font-bold px-6 py-3 no-underline mb-3 text-center">
                                    Create Page <span class="text-white ml-1">&rarr;</span>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="w-full md:w-2/5 flex flex-wrap justify-center items-center py-1">
                    @forelse($pages as $page)
                        <div class="leading-normal tracking-tight font-medium">
                            <a href="{!! route('pages.show', ['page' => $page->slug]) !!}" class="text-indigo-light no-underline hover:text-indigo-dark mx-3">{{ $page->name }}</a>
                        </div>
                    @empty
                        No pages found.
                    @endforelse
                </div>
            </div>
        </div>
    </div>
@endsection
