<!doctype html>
<html lang="{{ app()->getLocale() }}">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<link rel="stylesheet" href="{{ mix('css/app.css') }}">
		<link rel="stylesheet" href="{{ asset('css/fontawesome.css') }}">

		<title>{{ config('app.name') }} | {{ $subtitle }}</title>
	</head>

	<body class="font-sans antialiased text-grey-dark leading-normal bg-indigo-lightest">

		<div id="app">
            @yield('body')
		</div>

        <!--  Javascript  -->
		<script src="{{ mix('js/app.js') }}"></script>

		<script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
		<script src="{{ asset('js/popper/popper.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap/bootstrap.js') }}"></script>
		<script src="{{ asset('js/custom-components.js') }}"></script>
		<script src="{{ asset('js/fontawesome.js') }}"></script>
	</body>

</html>
