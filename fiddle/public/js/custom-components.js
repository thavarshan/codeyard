// (function(){})();

(function(){
    // Dropdown Button
    var btn = document.getElementById("dropdown-btn-1");
    var menu = document.getElementById("dropdown-menu-1");

    if (btn) {
        btn.onclick = function() {
            menu.classList.toggle("hidden");
        };
    }

    // Modal
    // Get the modal
    var modal = document.getElementById('modal-1');
    // Get the button that opens the modal
    var modalBtn = document.getElementById("modal-btn-1");
    // Get the <a> element that closes the modal
    var closeBtn = document.getElementsByClassName("close")[0];

    function toggleModal() {
        var el = modal.classList;
        if (el.contains("flex")) {
            el.remove("flex");
            el.add("hidden");
        } else {
            el.remove("hidden");
            el.add("flex");
        }
    }

    // When the user clicks on the button, open the modal
    if (modalBtn) {
        modalBtn.onclick = function() {
            toggleModal()
        }
    }
    // When the user clicks on <span> (x), close the modal
    if (closeBtn) {
        closeBtn.onclick = function() {
            toggleModal()
        }
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            toggleModal()
        }

        if (!event.target.matches('.dropdown-btn')) {
            var dropdowns = document.getElementsByClassName("dropdown-menu");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (!openDropdown.classList.contains('hidden')) {
                    openDropdown.classList.add('hidden');
                }
            }
        }
    }

    // Mobile Menu
    var hamMenuBtn = document.getElementById('ham-menu-btn');
    var hamMenuBtnClose = document.getElementById('ham-menu-btn-close');

    function toggleMenu() {
        var mobileMenu = document.getElementById('mobile-menu');
        mobileMenu.classList.toggle('hidden');
    }

    if (hamMenuBtn) {
        hamMenuBtn.onclick = function() {
            toggleMenu();
        };
    }

    if (hamMenuBtnClose) {
        hamMenuBtnClose.onclick = function() {
            toggleMenu();
        };
    }
})();

/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
// function myFunction() {
//  document.getElementById("myDropdown").classList.toggle("show");
// }
// Close the dropdown menu if the user clicks outside of it
// window.onclick = function(event) {
//  if (!event.target.matches('.dropbtn')) {
//      var dropdowns = document.getElementsByClassName("dropdown-content");
//      var i;
//      for (i = 0; i < dropdowns.length; i++) {
//          var openDropdown = dropdowns[i];
//          if (openDropdown.classList.contains('show')) {
//              openDropdown.classList.remove('show');
//          }
//      }
//  }
// }

// <div class="dropdown">
//  <button onclick="myFunction()" class="dropbtn">Dropdown</button>
//  <div id="myDropdown" class="dropdown-content">
//      <a href="#">Link 1</a>
//      <a href="#">Link 2</a>
//      <a href="#">Link 3</a>
//  </div>
// </div>


// modal
// <!-- Trigger/Open The Modal -->
// <button id="myBtn">Open Modal</button>
//
// <!-- The Modal -->
// <div id="myModal" class="modal">
//
// <!-- Modal content -->
//  <div class="modal-content">
//      <span class="close">&times;</span>
//      <p>Some text in the Modal..</p>
//  </div>
//
// </div>
