<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Front End Controllers
// Home Page
Route::get('/', [
  'uses' => 'FrontendController@index',
  'as' => 'home'
]);

// Single Post Page
Route::get('/post/{slug}', [
  'uses' => 'FrontendController@singlePost',
  'as' => 'post.single'
]);

// Category Filter Page
Route::get('/category/{id}', [
  'uses' => 'FrontendController@category',
  'as' => 'category.single'
]);

// Tag Filter Page
Route::get('/tag/{id}', [
  'uses' => 'FrontendController@tag',
  'as' => 'tag.single'
]);

// Author Filter Page
Route::get('/author/{id}', [
  'uses' => 'FrontendController@author',
  'as' => 'author.single'
]);

// Results Filter Page
Route::get('/results', [
  'uses' => 'FrontendController@search',
  'as' => 'results'
]);

// Back End Controllers
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {

  // Admin dashboard view
  Route::get('/', [
    'uses' => 'AdminController@index',
    'as' => 'admin'
  ]);
  Route::get('/dash', [
    'uses' => 'AdminController@index',
    'as' => 'dash'
  ]);

  // Posts Controls
  Route::get('/post', [
    'uses' => 'PostsController@index',
    'as' => 'post'
  ]);
  Route::get('/post/create', [
    'uses' => 'PostsController@create',
    'as' => 'post.create'
  ]);
  Route::get('/post/edit/{id}', [
    'uses' => 'PostsController@edit',
    'as' => 'post.edit'
  ]);
  Route::get('/post/delete/{id}', [
    'uses' => 'PostsController@destroy',
    'as' => 'post.delete'
  ]);
  Route::get('/post/trashed', [
    'uses' => 'PostsController@trashed',
    'as' => 'post.trashed'
  ]);
  Route::get('/post/restore/{id}', [
    'uses' => 'PostsController@restore',
    'as' => 'post.restore'
  ]);
  Route::get('/post/kill/{id}', [
    'uses' => 'PostsController@kill',
    'as' => 'post.kill'
  ]);
  Route::post('/post/store', [
    'uses' => 'PostsController@store',
    'as' => 'post.store'
  ]);
  Route::post('/post/update/{id}', [
    'uses' => 'PostsController@update',
    'as' => 'post.update'
  ]);

  // Categories Control
  Route::get('/category', [
    'uses' => 'CategoriesController@index',
    'as' => 'category'
  ]);
  Route::get('/category/show', [
    'uses' => 'CategoriesController@index',
    'as' => 'category.show'
  ]);
  Route::get('/category/create', 'CategoriesController@create');
  Route::post('/category/store', [
    'uses' => 'CategoriesController@store',
    'as' => 'category.store'
  ]);
  Route::get('/category/edit/{id}', [
    'uses' => 'CategoriesController@edit',
    'as' => 'category.edit'
  ]);
  Route::post('/category/update/{id}', [
    'uses' => 'CategoriesController@update',
    'as' => 'category.update'
  ]);
  Route::get('/category/delete/{id}', [
    'uses' => 'CategoriesController@destroy',
    'as' => 'category.delete'
  ]);

  // Tags Control
  Route::get('/tag', [
    'uses' => 'TagsController@index',
    'as' => 'tag'
  ]);
  Route::get('/tag/show', [
    'uses' => 'TagsController@index',
    'as' => 'tag.show'
  ]);
  Route::get('/tag/create', 'TagsController@create');
  Route::post('/tag/store', [
    'uses' => 'TagsController@store',
    'as' => 'tag.store'
  ]);
  Route::get('/tag/edit/{id}', [
    'uses' => 'TagsController@edit',
    'as' => 'tag.edit'
  ]);
  Route::post('/tag/update/{id}', [
    'uses' => 'TagsController@update',
    'as' => 'tag.update'
  ]);
  Route::get('/tag/delete/{id}', [
    'uses' => 'TagsController@destroy',
    'as' => 'tag.delete'
  ]);

  // Users Control
  Route::get('/user/index', [
    'uses' => 'UsersController@index',
    'as' => 'user.index'
  ]);
  Route::get('/user/create', [
    'uses' => 'UsersController@create',
    'as' => 'user.create'
  ]);
  Route::post('/user/store', [
    'uses' => 'UsersController@store',
    'as' => 'user.store'
  ]);
  Route::get('/user/admin/{id}', [
    'uses' => 'UsersController@admin',
    'as' => 'user.admin'
  ]);
  Route::get('/user/not-admin/{id}', [
    'uses' => 'UsersController@not_admin',
    'as' => 'user.not.admin'
  ]);
  Route::get('/user/delete/{id}', [
    'uses' => 'UsersController@destroy',
    'as' => 'user.delete'
  ]);

  // User Profile Control
  Route::get('/user/profile', [
    'uses' => 'ProfileController@index',
    'as' => 'user.profile'
  ]);
  Route::get('/user/settings/', [
    'uses' => 'ProfileController@settings',
    'as' => 'user.settings'
  ]);
  Route::post('/user/settings/update', [
    'uses' => 'ProfileController@update',
    'as' => 'user.settings.update'
  ]);
  Route::post('/user/settings/update-password', [
    'uses' => 'ProfileController@update_password',
    'as' => 'user.settings.update.password'
  ]);

  // Site Settings Control
  Route::get('/settings', [
    'uses' => 'SettingsController@index',
    'as' => 'settings'
  ]);
  Route::post('/settings/update', [
    'uses' => 'SettingsController@update',
    'as' => 'settings.update'
  ]);

});

Auth::routes();
