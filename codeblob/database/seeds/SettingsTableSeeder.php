<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
      \App\Setting::create([
        'site_icon' => 'ion-code',
        'site_name' => 'CODEBLOB',
        'tagline' => 'Beta is Latin for still doesn\'t work',
        'email' => 'admin@codeblob.com'
      ]);
    }
}
