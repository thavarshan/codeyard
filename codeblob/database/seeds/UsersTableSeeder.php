<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    $user = App\User::create([
      'name' => 'Admin',
      'email' => 'admin@codeblob.com',
      'password' => bcrypt('secret'),
      'admin' => 1
    ]);

    App\Profile::create([
      'user_id' => $user->id,
      'avatar' => 'uploads/avatar/user.png',
      'about' => 'Any fool can use a computer. Many do.',
      'facebook' => 'http://facebook.com/thavarshan',
      'twitter' => 'http://twitter.com/thavarshan',
      'github' => 'http://github.com/thavarshan'
    ]);
  }
}
