<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
      return view('admin.user.profile')->with('user', Auth::user());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function settings() {
      return view('admin.user.settings')->with('user', Auth::user());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
      //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
      $this->validate($request, [
        'name' => 'required',
        'email' => 'required|email',
        'facebook' => 'required|url',
        'twitter' => 'required|url',
        'github' => 'required|url'
      ]);

      $user = Auth::user();

      if($request->hasFile('avatar')) {
        $avatar = $request->avatar;
        $avatar_new_name = time().$avatar->getClientOriginalName();
        $avatar->move('uploads/avatar', $avatar_new_name);

        $user->profile->avatar = 'uploads/avatar/'.$avatar_new_name;
        $user->profile->save();
      }

      $user->name = $request->name;
      $user->email = $request->email;
      $user->profile->about = $request->about;
      $user->profile->facebook = $request->facebook;
      $user->profile->twitter = $request->twitter;
      $user->profile->github = $request->github;

      $user->save();
      $user->profile->save();

      return redirect()->back()->with('status', 'Changes saved');
    }

    public function update_password(Request $request) {
      $this->validate($request, [
        'password' => 'required'
      ]);

      $user = Auth::user();
      $user->password = bcrypt($request->password);
      $user->save();

      return redirect()->back()->with('status', 'Password updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
