<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Post;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
      $categories = Category::get();
      return view('admin.category.show')->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
      $categories = Category::get();
      return view('admin.category.show')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
      $this->validate($request, [
        'category' => 'required'
      ]);
      $category = new Category;
      $category->name = $request->category;
      $category->save();

      return redirect()->back()->with('status', 'Category created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
      $categories = Category::get();
      $edit = Category::find($id);
      return view('admin.category.edit')->with(['edit' => $edit, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
      $update = Category::find($id);
      $update->name = $request->category;
      $update->update();
      return redirect()->back()->with('status', 'Category updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
      $update = Category::find($id);

      foreach ($update->posts as $post) {
        $post->forceDelete();
      }
      $update->delete();
      return redirect()->back()->with('status', 'Category deleted!');
    }
}
