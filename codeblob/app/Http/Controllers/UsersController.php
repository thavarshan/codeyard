<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Profile;
use App\Post;
use App\Category;
use App\Tag;

class UsersController extends Controller {

  function __construct() {
    $this->middleware('admin');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

      return view('admin.user.index')->with('users', User::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

      return view('admin.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
      $this->validate($request, [
        'name' => 'required',
        'email' => 'required|email',
      ]);

      $user = User::create([
        'name' => $request->name,
        'email' => $request->email,
        'password' => bcrypt('secret')
      ]);

      $profile = Profile::create([
        'user_id' => $user->id,
        'avatar' => 'uploads/avatar/user.png'
      ]);

      return redirect()->route('user.index')->with('status', "New user created");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function admin($id) {
      $user = User::find($id);
      $user->admin = 1;
      $user->save();

      return redirect()->back()->with('status', 'User is now an Administrator');
    }

    public function not_admin($id) {
      $user = User::find($id);
      $user->admin = 0;
      $user->save();

      return redirect()->back()->with('status', 'User is now a Subscriber');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
      $user = User::find($id);
      $image = \DB::table('profiles')->where('user_id', $id)->first();
      $file = $image->avatar;
      if($image->avatar !== 'user.png') {
        $filename = public_path().'/'.$file;
        unlink($filename);
      }
      $user->profile->delete();
      $user->delete();

      return redirect()->back()->with('status', 'User removed');
    }
}
