<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;

class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $tags = Tag::get();
      return view('admin.tag.show')->with('tags', $tags);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
      $tags = Tag::get();
      return view('admin.tag.show')->with('tags', $tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
      $this->validate($request, [
        'tag' => 'required'
      ]);
      $tag = new Tag;
      $tag->tag = $request->tag;
      $tag->save();

      return redirect()->back()->with('status', 'Tag created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
      $tags = Tag::get();
      $edit = Tag::find($id);
      return view('admin.tag.edit')->with(['edit' => $edit, 'tags' => $tags]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
      $update = Tag::find($id);
      $update->tag = $request->tag;
      $update->update();
      return redirect()->back()->with('status', 'Tag updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
      $update = Tag::find($id);
      $update->delete();
      return redirect()->back()->with('status', 'Tag deleted!');
    }
}
