<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\User;
use App\Post;
use App\Category;
use App\Tag;

class PostsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
      $posts = Post::orderBy('created_at', 'desc')->get();
      $trashed = Post::onlyTrashed()->get();
      return view('admin.post.show')->with('posts', $posts)
                                    ->with('trashed', $trashed);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
      $categories = Category::get();
      $tags = Tag::get();

      if($categories->count() == 0 || $tags->count() == 0) {
        return redirect()->back()->with('info', 'You must have Categories and Tags before you can create Posts');
      }
      return view('admin.post.create')->with('categories', $categories)->with('tags', $tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
      $this->validate($request, [
        'title' => 'required|max:255',
        'featured' => 'required|image',
        'content' => 'required',
        'category_id' => 'required',
        'tags' => 'required'
      ]);

      $featured = $request->featured;
      $featured_new_name = time().$featured->getClientOriginalName();
      $featured->move('uploads/post', $featured_new_name);

      $post = Post::create([
        'title' => $request->title,
        'featured' => 'uploads/post/'.$featured_new_name,
        'content' => $request->content,
        'category_id' => $request->category_id,
        'slug' => str_slug($request->title),
        'user_id' => Auth::id()
      ]);

      $post->tags()->attach($request->tags);

      return redirect()->back()->with('status', 'Post published!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
      $post = Post::find($id);

      return view('admin.post.edit')->with('post', $post)
                                    ->with('categories', Category::all())
                                    ->with('users', User::all())
                                    ->with('tags', Tag::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
      $this->validate($request, [
        'title' => 'required',
        'content' => 'required',
      ]);

      $post = Post::find($id);

      if($request->hasFile('featured')) {
        $featured = $request->featured;
        $featured_new_name = time().$featured->getClientOriginalName();

        $featured->move('uploads/post', $featured_new_name);

        $post->featured = 'uploads/post/'.$featured_new_name;
      }

      $post->title = $request->title;
      $post->content = $request->content;
      $post->category_id = $request->category_id;
      $post->user_id = $request->user_id;

      $post->save();

      $post->tags()->sync($request->tags);

      return redirect()->route('post')->with('status', 'Post updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
      $post = Post::find($id);
      $post->delete();

      return redirect()->back()->with('status', 'Post deleted');
    }

    public function trashed() {
      $posts = Post::onlyTrashed()->get();

      return view('admin.post.trashed')->with('posts', $posts)
                                        ->with('all', Post::all());
    }

    public function restore($id) {
      $post = Post::withTrashed()->where('id', $id)->first();
      $post->restore();

      return redirect()->back()->with('status', 'Post restored');
    }

    public function kill($id) {
      $image = \DB::table('posts')->where('id', $id)->first();
        $file = $image->featured;
        $filename = public_path().'/'.$file;
        unlink($filename);
      $post = Post::withTrashed()->where('id', $id)->first();
      $post->forceDelete();

      return redirect()->back()->with('status', 'Post deleted permanantly');
    }
}
