<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;

class SettingsController extends Controller {

  function __construct() {
    $this->middleware('admin');
  }

  public function index() {
    return view('admin.settings.index')->with('settings', Setting::first());
  }

  public function update() {
    $this->validate(request(), [
      'site_name' => 'required',
      'tagline' => 'required',
      'email' => 'required'
    ]);

    $settings = Setting::first();
    $settings->site_name = request()->site_name;
    if(request()->site_icon) {
      $settings->site_icon = request()->site_icon;
    }
    $settings->tagline = request()->tagline;
    $settings->email = request()->email;

    $settings->save();

    return redirect()->back()->with('status', 'Changes saved');
  }
}
