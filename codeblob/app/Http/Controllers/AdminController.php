<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User;
use App\Post;
use App\Category;
use App\Tag;

class AdminController extends Controller {
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
      $posts = Post::all();
      $latest_posts = Post::orderBy('created_at', 'desc')->take(5)->get();
      $categories = Category::all();
      $tags = Tag::all();
      $users = User::all();
      $users_new = User::orderBy('created_at', 'desc')->take(5)->get();

      return view('admin.dash')->with('posts', $posts)
                              ->with('latest_posts', $latest_posts)
                              ->with('categories', $categories)
                              ->with('tags', $tags)
                              ->with('users', $users)
                              ->with('users_new', $users_new);
    }
}
