<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Tag;
use App\User;
use App\Category;
use App\Setting;

class FrontendController extends Controller{

  public function index() {
    return view('welcome')->with('posts', Post::orderBy('created_at', 'desc')->get())
                          ->with('latest', Post::orderBy('created_at', 'desc')->take(1)->get())
                          ->with('categories', Category::orderBy('created_at', 'desc')->take(10)->get())
                          ->with('settings', Setting::first());
  }

  public function singlePost($slug) {
    $post = Post::where('slug', $slug)->first();
    $next_id = Post::where('id', '>', $post->id)->min('id');
    $prev_id = Post::where('id', '<', $post->id)->max('id');

    return view('single')->with('post', $post)
                              ->with('categories', Category::orderBy('created_at', 'desc')->take(10)->get())
                              ->with('next', Post::find($next_id))
                              ->with('prev', Post::find($prev_id))
                              ->with('title', $post->title)
                              ->with('tags', Tag::all())
                              ->with('settings', Setting::first());
  }

  public function category($id) {
    $category = Category::find($id);

    return view('category')->with('category', $category)
                          ->with('categories', Category::orderBy('created_at', 'desc')->take(10)->get())
                          ->with('title', $category->name)
                          ->with('settings', Setting::first());
  }

  public function tag($id) {
    $tag = Tag::find($id);

    return view('tag')->with('tag', $tag)
                      ->with('categories', Category::orderBy('created_at', 'desc')->take(10)->get())
                      ->with('title', $tag->tag)
                      ->with('settings', Setting::first());
  }

  public function author($id) {
    $user = User::find($id);

    return view('author')->with('user', $user)
                        ->with('categories', Category::orderBy('created_at', 'desc')->take(10)->get())
                        ->with('title', $user->name)
                        ->with('settings', Setting::first());
  }

  public function search() {
    $posts = Post::where('title', 'like', '%' . request('query') . '%')->get();

    return view('results')->with('posts', $posts)
                          ->with('title', 'Search results for ' .request('query'))
                          ->with('categories', Category::orderBy('created_at', 'desc')->take(10)->get())
                          ->with('settings', Setting::first());
  }
}
