@extends('layouts.base')

@section('content')
<div class="container mb-5">
  <div class="row">
    <div class="col-sm-12">
      <img src="{{ asset($post->featured) }}" alt="" width="100%" class="mb-5">
    </div>
  </div>
</div>

  <div class="container">
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <h1 class="desplay-3">{{ $post->title }}</h1>
        <p class="font-weight-bold">
          <small>
            Posted by
            <span class="mx-1"><a href="#"><i class="ion-person"></i> {{ $post->user->name }}</a></span>
            -

            <span class="mx-1"><a href="{{ route('category.single', ['id' => $post->category->id]) }}"><i class="ion-star"></i> {{ $post->category->name }}</a></span>
            <span class="mx-1"><i class="ion-clock"></i> {{ $post->created_at->diffForHumans() }}</span>
          </small>
          <hr class="m-y-md mb-5">
        </p>

        <div class="mt-3 mb-5">
          {!! $post->content !!}
        </div>

        <div class="tags-wrap my-5 text-center">
          @foreach($post->tags as $tag)
          <a href="{{ route('tag.single', ['id' => $tag->id]) }}" class="btn btn-outline-dark mb-1">{{ $tag->tag }}</a>
          @endforeach
        </div>

        <div class="user-data my-5">
          <div class="jumbotron bg-light">
            <div class="container">
              <div class="row">
                <div class="col-sm-2 text-right offset-sm-1">
                  <img src="{{ asset($post->user->profile->avatar) }}" alt="{{ $post->user->name }}" class="rounded-circle" height="70px">
                </div>
                <div class="col-sm-8">
                  <h6 class="mb-0">{{ $post->user->name }}</h6>
                  <p>
                    <small>
                      {{ $post->user->profile->about }}
                    </small>
                  </p>
                  <ul class="nav mb-o">
                    <li class="nav-item">
                      <a class="nav-link" href="{{ $post->user->profile->facebook }}"><i class="ion-social-facebook"></i></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="{{ $post->user->profile->twitter }}"><i class="ion-social-twitter"></i></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="{{ $post->user->profile->github }}"><i class="ion-social-github"></i></a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

  <div class="container">
    <div class="row mt-5 pt-5">
      <div class="col-sm-6 text-left">
      @if($prev)
      <a href="{{ route('post.single', ['slug' => $prev->slug]) }}" class="text-dark nav-link">
        <div class="btn-warp clearfix">
          <div class="btn-icon float-left mr-3">
            <i class="ion-ios-arrow-left display-4"></i>
          </div>
          <div class="btn-text float-left py-2 mt-3">
            Previous Post
          </div>
        </div>
      </a>
      @endif
      </div>

      <div class="col-sm-6 text-right">
        @if($next)
        <a href="{{ route('post.single', ['slug' => $next->slug]) }}" class="text-dark nav-link">
          <div class="btn-warp clearfix">
            <div class="btn-icon float-right ml-3">
              <i class="ion-ios-arrow-right display-4"></i>
            </div>
            <div class="btn-text float-right py-2 mt-3">
              Next Post
            </div>
          </div>
        </a>
        @endif
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row my-5 py-5">
      <div class="col-sm-12">
        <div class="card border-0">
          <div class="card-body">
            <div id="disqus_thread"></div>
              <script>
                var disqus_config = function () {
                this.page.url = "{{ route('post.single', ['slug' => $post->slug]) }}";  // Replace PAGE_URL with your page's canonical URL variable
                this.page.identifier = "post-{{ $post->slug }}"; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                };

                (function() { // DON'T EDIT BELOW THIS LINE
                var d = document, s = d.createElement('script');
                s.src = 'https://blob-thav.disqus.com/embed.js';
                s.setAttribute('data-timestamp', +new Date());
                (d.head || d.body).appendChild(s);
                })();
              </script>
              <noscript>
                Please enable JavaScript to view the comments
              </noscript>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
