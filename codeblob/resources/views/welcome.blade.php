@extends('layouts.base')

@section('content')
  @if (Route::has('login'))
  @guest
  <div class="bg-primary section-welcome">
    <div class="container mb-5 py-5">
      <div class="row pt-2">
        <div class="col-md-6">
          <div class="mt-5">
            <h1 class="display-2 text-white mb-4">Built for <span class="thin text-info">junior</span> developers</h1>
            <p class="lead text-white mb-4">
              <strong>CODEBLOB</strong> is a development blogging area for noob developers who are just starting out and are in the process of learning. Each junior dev creates an account and keeps tabs on everything that he/she has learned on her journey to become a developer. And in the proccess share it with eachother.
            </p>
            <p class="text-white mt-5">
              <small><i class="ion-code mr-1"></i> with <i class="ion-heart text-danger mx-1"></i> by <a class="text-white" target="_blank" href="http://github.com/thavarshan"><em>Thavarshan</em></a></small>
            </p>
          </div>
        </div>

        <div class="col-md-4 offset-md-2">
          <div class="card mt-5">
            <div class="card-body">
              <form method="POST" action="{{ route('register') }}">
                @csrf
                <fieldset class="form-group">
                  <label for="name">Name</label>
                  <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="Your Name" required autofocus>
                  @if ($errors->has('name'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('name') }}</strong>
                    </span>
                  @endif
                </fieldset>
                <fieldset class="form-group">
                  <label for="email">E-Mail Address</label>
                  <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Your Email Address" required>
                  @if ($errors->has('email'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                  @endif
                </fieldset>
                <fieldset class="form-group">
                  <label for="password">Password</label>
                  <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
                  @if ($errors->has('password'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                  @endif
                </fieldset>
                <fieldset class="form-group">
                  <label for="password-confirm">Confirm Password</label>
                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Re-enter Password" required>
                </fieldset>
                <button class="btn btn-success btn-block" type="submit">Register</button>
              </form>
              <p class="mt-3 mb-0 text-center"><small>Already have an account? <a href="{{ route('login') }}">Login</a></small></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endguest
  @endif

  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        @if($latest)
        @foreach($latest as $new)
        <div class="jumbotron bg-latest" style="background: url({{ $new->featured }}) no-repeat center center; background-size: cover;">
          <div class="col-md-5">
            <h1 class="text-white">{{ $new->title }}</h1>
            <hr class="m-y-md bg-light">
            <p class="text-white">
              <small>By <a class="mx-1 text-white" href="{{ route('author.single', ['id' => $new->user->id]) }}"><i class="ion-person"></i> {{ $new->user->name }}</a> &#183; <a href="{{ route('category.single', ['id' => $new->category_id]) }}" class="mx-1 text-white"><i class="ion-star"></i> {{ $new->category->name }}</a> &#183; <span class="mx-1"><i class="ion-clock"></i> {{ $new->created_at->diffForHumans() }}</span></small>
            </p>
            <p>
              <a class="btn btn-outline-light" href="{{ route('post.single', ['slug' => $new->slug]) }}" role="button">Continue reading</a>
            </p>
          </div>
        </div>
        @endforeach
        @else
        <p>
          No posts available
        </p>
        @endif
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12">
        @if(count($posts) > 0)
        <div class="grid">
          @foreach($posts as $post)
          <div class="grid-item">
            <div class="card card-post">
              <img class="card-img-top" src="{{ $post->featured }}" alt="{{ $post->title }}">
              <div class="card-body">
                <h4 class="card-title mb-0">{{ $post->title }}</h4>
                <p>
                  <small>
                  By <a href="#" class="mx-1"><i class="ion-person"></i> {{ $post->user->name }}</a> &#183; <a class="mx-1" href="{{ route('category.single', ['id' => $post->category_id]) }}"><i class="ion-pricetag"></i> {{ $post->category->name }}</a> &#183; <span class="mx-1"><i class="ion-clock"></i> {{ $post->created_at->diffForHumans() }}</span>
                  </small>
                </p>
                <p class="card-text">{{ str_replace('<p>', '', str_limit($post->content, $limit = 100, $end = '...')) }}</p>
                <a class="mx-1 btn btn-link btn-block" href="{{ route('post.single', ['slug' => $post->slug ]) }}">Continue reading</a>
              </div>
            </div>
          </div>
          @endforeach
        </div>
        @else
        <p>
          No posts available
        </p>
        @endif
      </div>
    </div>
  </div>
@endsection
