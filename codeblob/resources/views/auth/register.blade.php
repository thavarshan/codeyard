@extends('layouts.auth-reg')

@section('content')
<div class="container">
  <div class="row pt-2">
    <div class="col-md-5 offset-md-2">
      <h1 class="mb-0">Join <span class="display-4">{{ config('app_name', 'CODEBLOB') }}</span></h1>
      <p class="lead mb-5">
        The best way to design, build, and ship software.
      </p>
      <h5 class="mb-4">Create your personal account </h5>
      <form method="POST" action="{{ route('register') }}">
        @csrf
        <fieldset class="form-group">
          <label for="name">Name</label>
          <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="Your Name" required autofocus>
          <small class="text-muted">This will be your username.</small>
          @if ($errors->has('name'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('name') }}</strong>
            </span>
          @endif
        </fieldset>
        <fieldset class="form-group">
          <label for="email">E-Mail Address</label>
          <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Your Email Address" required>
          <small class="text-muted">We'll occasionally send updates about your account to this inbox. We'll never share your email address with anyone.</small>
          @if ($errors->has('email'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('email') }}</strong>
            </span>
          @endif
        </fieldset>
        <fieldset class="form-group">
          <label for="password">Password</label>
          <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
          <small class="text-muted">Use at least one lowercase letter, one numeral, and seven characters.</small>
          @if ($errors->has('password'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('password') }}</strong>
            </span>
          @endif
        </fieldset>
        <fieldset class="form-group">
          <label for="password-confirm">Confirm Password</label>
          <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Re-enter Password" required>
        </fieldset>
        <fieldset class="form-group row py-4">
          <p class="col-sm-12">
            By clicking on "Create an account" below, you are agreeing to the Terms of Service and the Privacy Policy.
          </p>
        </fieldset>
        <button class="btn btn-success" type="submit">Create an Account</button>
      </form>
    </div>

    <div class="col-md-3">
      <div class="card" id="love-codeblob-card">
        <div class="card-header">
          <h5 class="card-title mb-0">You'll love {{ config('app_name', 'CODEBLOB') }}</h5>
        </div>
        <div class="card-body">
          <p class="mb-0">
            <strong>Unlimited</strong> collaborators<br />
            <strong>Unlimited</strong> Knowledge
          </p>
          <hr>
          <ul>
            <li>Great communication </li>
            <li>Frictionless development</li>
            <li>Open source community</li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
