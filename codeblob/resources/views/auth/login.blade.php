@extends('layouts.auth')

@section('content')
<div class="container">
  <div class="row mt-5 mb-3 pt-5 text-center">
    <div class="col-sm-12">
      <h3><span class="thin mr-2">Login to</span> <a href="{{ route('home') }}" class="text-dark"><i class="ion-code"></i> {{ config('app.name', 'CODEBLOB') }}</a></h3>
    </div>
  </div>
  <div class="row">
        <div class="col-md-4 offset-md-4">
          <div class="card">
            <div class="card-body">
              <form class="form-signin" method="POST" action="{{ route('login') }}">
                @csrf
                <fieldset class="form-group">
                  <label for="email">Email</label>
                  <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email address" required autofocus>
                  @if ($errors->has('email'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                  @endif
                </fieldset>
                <fieldset class="form-group">
                  <label for="password">Password</label>
                  <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
                  @if ($errors->has('password'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                  @endif
                </fieldset>
                <div class="checkbox mb-3">
                  <label>
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember me
                  </label>

                  <span class="float-right"><small><a href="{{ route('password.request') }}">Forgot password?</a></small></span>
                </div>
                <button class="btn btn-success btn-block" type="submit">Login</button>
              </form>
            </div>
          </div>

          <div class="card mt-3">
            <div class="card-body text-center">
              <span><small>New to <strong>{{ config('app.name', 'BLOB') }}</strong>? <a href="{{ route('register') }}">Create an account</a>.</small></span>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
