@extends('layouts.base')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-4 offset-md-4">
        <form method="POST" action="{{ route('password.request') }}">
            @csrf
          <input type="hidden" name="token" value="{{ $token }}">
          <div class="form-group row">
            <label for="email">E-Mail Address</label>
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email or old('email') }}" required autofocus>
            @if ($errors->has('email'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group row">
              <label for="password">Password</label>
              <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
              @if ($errors->has('password'))
                <span class="invalid-feedback">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
              @endif
          </div>
          <div class="form-group row">
            <label for="password-confirm">Confirm Password</label>
            <input id="password-confirm" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" required>
            @if ($errors->has('password_confirmation'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
              </span>
            @endif
          </div>
          <div class="form-group row">
            <button type="submit" class="btn btn-primary">
              Reset Password
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection
