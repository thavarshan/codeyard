@extends('layouts.base')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-4 offset-md-4">
      <form method="POST" action="{{ route('password.email') }}">
        @csrf
        <div class="form-group row">
          <label for="email">E-Mail Address</label>
          <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
          @if ($errors->has('email'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('email') }}</strong>
            </span>
          @endif
        </div>
        <button type="submit" class="btn btn-primary">
          Send Password Reset Link
        </button>
      </form>
    </div>
  </div>
</div>
@endsection
