<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'CODEBLOB') }} | Register</title>

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,600|Roboto:400,400i" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}">

  <!-- Core CSS & Resets
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/bootstrap-grid.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/bootstrap-reboot.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/bootstrap-theme.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
  <link rel="stylesheet" href="{{ asset('css/bootstrap-resets.css') }}">

  <!-- Global CSS & Pages
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="{{ asset('/') }}/css/global.css">
  <link rel="stylesheet" href="{{ asset('/') }}/css/pages.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

  <!-- Bootstrap core JavaScript
  ================================================== -->
  <script src="{{ asset('js/jquery-3.2.1.min.js') }}" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
  <script src="{{ asset('js/jquery-2.2.4.min.js') }}" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
  <script src="{{ asset('js/jquery-1.12.4.min.js') }}" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>

</head>
<body>

  <header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
      <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}"><i class="ion-code"></i> {{ config('app.name', 'CODEBLOB') }}</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          @if(isset($categories))
          <ul class="navbar-nav mr-auto">
            @foreach($categories as $category)
            <li class="nav-item">
              <a href="" class="nav-link">
                @if( $category->name !== 'Uncategorized')
                  {{ $category->name }}
                @endif
              </a>
            </li>
            @endforeach
          </ul>
          @endif
          <ul class="navbar-nav ml-auto">
            @if (Route::has('login'))
            @auth
            <li class="nav-item dropdown ml-2">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img src="{{ asset(Auth::user()->profile->avatar) }}" alt="{{ Auth::user()->name }}" class="rounded-circle" height="22px">
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <div class="dropdown-header">
                  <small>Logged in as</small>
                  <h6 class="mb-0">{{ Auth::user()->name }}</h6>
                </div>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('admin') }}"><i class="ion-home"></i> Dashboard</a>
                <a class="dropdown-item" href="{{ route('user.profile') }}"><i class="ion-person"></i> Profile</a>
                <a class="dropdown-item" href="{{ route('post.create') }}"><i class="ion-compose"></i> New Post</a>
                <a class="dropdown-item" href="{{ route('dash') }}"><i class="ion-android-settings"></i> Settings</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();"><i class="ion-power"></i> Logout</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
                </form>
              </div>
            </li>
            @else
            <li class="nav-item">
              <a class="nav-link" href="{{ route('login') }}">Login</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('register') }}">Register</a>
            </li>
            @endauth
            @endif
          </ul>
        </div>
      </div>
    </nav>
  </header>

  <!-- Notification display area -->
  @if(count($errors) > 0)
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true" class="ion-ios-close-empty"></span>
      </button>
      <ul class="list-unstyled mb-0">
        @foreach($errors->all() as $error)
          <li>
            {{ $error }}
          </li>
        @endforeach
      </ul>
    </div>
  @endif

  @if(session('status') )
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true" class="ion-ios-close-empty"></span>
      </button>
      {{ session('status') }}
    </div>
  @endif

  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <section class="section-base">
    @yield('content')
  </section>

<!-- End Document
–––––––––––––––––––––––––––––––––––––––––––––––––– -->

</body>
</html>
