@extends('layouts.admin-base')

@section('content')
<div class="container">
  <div class="row mb-4">
    <div class="col-sm-12">
      <h1 class="float-left">Site Settings</h1>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-6">
      <form action="{{ route('settings.update') }}" method="post" enctype="multipart/form-data">
        @csrf
        <fieldset class="form-group">
          <label for="site_name">Site Name</label>
          <input class="form-control" type="text" name="site_name" value="{{ $settings->site_name }}">
        </fieldset>
        <fieldset class="form-group">
          <label for="site_icon">Site Icon</label>
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text" id="site_icon"><i class="{{ $settings->site_icon }}"></i></span>
            </div>
            <input type="text" name="site_icon" class="form-control" value="{{ $settings->site_icon }}" aria-label="site_icon" aria-describedby="site_icon">
            <small class="text-muted mt-2">If you want an Icon to be shown next to the <em>Site Name</em>, you can choose from a list of icons at <a target="_blank" href="http://ionicons.com"><em>ionicons.com</em></a>. If you do not want to show any please leave the field blanks</small>
          </div>
        </fieldset>
        <fieldset class="form-group">
          <label for="tagline">Tagline</label>
          <input class="form-control" type="text" name="tagline" value="{{ $settings->tagline }}">
        </fieldset>
        <fieldset class="form-group">
          <label for="email">Admin Email</label>
          <input class="form-control" type="email" name="email" value="{{ $settings->email }}">
        </fieldset>
        <fieldset class="form-group">
          <button type="submit" class="btn btn-success">Save Changes</button>
        </fieldset>
      </form>
    </div>
  </div>
</div>
@endsection
