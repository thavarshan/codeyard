@extends('layouts.admin-base')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <h1>Dashboard</h1>
    </div>
  </div>

  <div class="row mt-5">
    <div class="col-md-6">
      <div class="clearfix">
        <h5 class="mb-3 float-left">Latest Posts</h5>
        <a href="{{ route('post') }}"><span class="float-right"><i class="badge badge-primary badge-pill">{{ count($posts) }}</i> Total posts</span></a>
      </div>
      <div class="list-group">
        @if($latest_posts)
        @foreach($latest_posts as $latest)
        <a href="{{ route('post') }}" class="list-group-item list-group-item-action flex-column align-items-start">
          <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">{{ $latest->title }}</h5>
            <small>{{ $latest->created_at->diffForHumans() }}</small>
          </div>
          <p class="mb-1">{{ str_replace('<p>', '', str_limit($latest->content, $limit = 100, $end = '...')) }}</p>
          <small class="text-dark"><strong>{{ $latest->user->name }}</strong></small>
        </a>
        @endforeach
        @else
        <p>
          No posts published.
        </p>
        @endif
      </div>
    </div>

    <div class="col-md-6">
      <div class="row mb-3">
        <div class="col-md-6">
          <div class="card">
            <div class="card-body">
              <div class="clearfix">
                <h5 class="card-title float-left">Categories</h5>
                <span class="float-right badge badge-primary badge-pill">{{ count($categories) }}</span>
              </div>
              @if($categories)
              @foreach($categories as $category)
              <span class="badge badge-primary">{{ $category->name }}</span>
              @endforeach
              @else
              <p>
                No categories available
              </p>
              @endif
              <p class="mb-0 mt-3"><a href="#" class="btn btn-link btn-block"><i class="ion-plus mr-1"></i>Create New Category</a></p>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="card">
            <div class="card-body">
              <div class="clearfix">
                <h5 class="card-title float-left">Tags</h5>
                <span class="float-right badge badge-primary badge-pill">{{ count($tags) }}</span>
              </div>
              @if($tags)
              @foreach($tags as $tag)
              <span class="badge badge-secondary">{{ $tag->tag }}</span>
              @endforeach
              @else
              <p>
                No tags available
              </p>
              @endif
              <p class="mb-0 mt-3"><a href="#" class="btn btn-link btn-block"><i class="ion-plus mr-1"></i>Create New Tag</a></p>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div class="clearfix">
            <h5 class="mb-3 float-left">New Users</h5>
            <a href="{{ route('user.index') }}"><span class="float-right"><i class="badge badge-primary badge-pill">{{ count($users) }}</i> Total users</span></a>
          </div>
          <ul class="list-group">
            @if($users_new)
            @foreach($users_new as $user)
            <li class="list-group-item clearfix">
              <div class="float-left">
                <img src="{{ asset($user->profile->avatar) }}" alt="" class="rounded-circle mr-3" height="40px">
              </div>
              <div class="float-left">
                <h6 class="mb-0">{{ $user->name }} @if($user->admin) <span class="ion-flash text-success ml-1"></span> @else <span class="ion-flash-off text-warning ml-1"></span> @endif</h6>
                <span class="text-muted"><small>{{ $user->email }}</small></span>
              </div>
              <div class="float-right text-right">
                <span class="text-muted"><small>{{ $user->created_at->diffForHumans() }}</small></span>
              </div>
            </li>
            @endforeach
            @else
            <p>
              No new users yet
            </p>
            @endif
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
