@extends('layouts.admin-base')

@section('content')
<div class="container">
  <div class="row mb-4">
    <div class="col-sm-12">
      <h1>Tags</h1>
    </div>
  </div>

  <div class="row">
    <div class="col-md-6">
      @if(count($tags) > 0)
      <ul class="list-group">
        @foreach($tags as $tag)
        <li class="list-group-item d-flex justify-content-between align-items-center">
          <span class="ion-pricetag"> {{ $tag->tag }}</span>
          <span>
            <small>
              <a href="{{ route('tag.edit', ['id' => $tag->id]) }}" class="mr-2"><span class="ion-edit"> Edit</span></a>
              <a href="{{ route('tag.delete', ['id' => $tag->id]) }}" class="text-danger"><span class="ion-trash-a"> Delete</span></a>
            </small>
          </span>
        </li>
        @endforeach
      </ul>
      @else
      <p>
        No categories available
      </p>
      @endif
    </div>

    <div class="col-md-6">
      <div class="card">
        <div class="card-body">
          <h6 class="card-title">Create New Tag</h6>
          <form class="mb-4" action="{{ route('tag.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <fieldset class="form-group">
              <input type="text" name="tag" id="tag" class="form-control" placeholder="Tag Name" value="">
            </fieldset>
            <button type="submit" class="btn btn-success"><i class="ion-plus"></i> Create Tag</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
