@extends('layouts.admin-base')

@section('content')
<div class="container">
  <div class="row mb-4">
    <div class="col-sm-12">
      <div class="clearfix">
        <h1 class="float-left">Create New Post</h1>
        <a href="{{ route('post') }}" class="btn btn-success float-right"><i class="ion-eye"></i> View All Posts</a>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-8">
      <form action="{{ route('post.store') }}" method="post" enctype="multipart/form-data" id="createForm">
        @csrf
        <fieldset class="form-group">
          <label for="title">Post Title</label>
          <input class="form-control" type="text" id="title" name="title" placeholder="Post Title">
        </fieldset>
        <fieldset class="form-group">
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text">Upload</span>
            </div>
            <div class="custom-file">
              <input type="file" class="custom-file-input mb-0" name="featured" id="featured">
              <label class="custom-file-label mb-0" for="featured">Add featured image</label>
            </div>
          </div>
        </fieldset>
        <fieldset class="form-group">
          <label for="content">Post Content</label>
          <textarea name="content" class="form-control" rows="8" cols="80" id="content"></textarea>
          <script>
            $(document).ready(function() {
              $('#content').summernote({
                placeholder: 'Tell us what you discovered.',
                tabsize: 2,
                height: 200
              });
            });
          </script>
        </fieldset>
        <fieldset  class="form-group">
          <label for="category">Category</label>
          @if(count($categories) > 0)
          <select class="form-control" id="category" name="category_id">
            @foreach($categories as $category)
            <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
          </select>
          @else
          <select class="form-control" id="category" name="category_id">
            <option value="">Uncategorized</option>
          </select>
          @endif
        </fieldset>
        <fieldset class="form-group my-5">
          <label>Tags</label>
          @if(count($tags) > 0)
          <div class="form-check">
            @foreach($tags as $tag)
            <span class="mr-4 mb-1">
              <input class="form-check-input" type="checkbox" name="tags[]" value="{{ $tag->id }}" id="tags">
              <label class="form-check-label" for="tags">
                {{ $tag->tag }}
              </label>
            </span>
            @endforeach
          </div>
          @endif
        </fieldset>
        <button type="submit" name="create" class="btn btn-success">Publish</button>
      </form>
    </div>

    <div class="col-md-4">
      <div class="card">
        <div class="card-body">
          <h6 class="card-title">Create New Category</h6>
          <form class="mb-4" action="{{ route('category.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <fieldset class="form-group">
              <input type="text" name="category" id="category" class="form-control" placeholder="New Category">
            </fieldset>
            <button type="submit" class="btn btn-outline-dark"><i class="ion-plus"></i> Create Category</button>
          </form>
          <h6 class="card-title">Available Categories</h6>
          @if(count($categories) > 0)
          @foreach($categories as $category)
          <span class="badge badge-category badge-dark mb-1"><i class="ion-star"></i> {{ $category->name }}</span>
          @endforeach
          @else
          <p>
            No categories available
          </p>
          @endif
          <p class="mt-3">
            <a href="{{ route('category.show') }}">Categories Page</a>
          </p>
        </div>
      </div>

      <div class="card">
        <div class="card-body">
          <h6 class="card-title">Create New Tag</h6>
          <form class="mb-4" action="{{ route('tag.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <fieldset class="form-group">
              <input type="text" name="tag" id="tag" class="form-control" placeholder="New Category">
            </fieldset>
            <button type="submit" class="btn btn-outline-dark"><i class="ion-plus"></i> Create Tag</button>
          </form>
          <h6 class="card-title">Available Tags</h6>
          @if(count($tags) > 0)
          @foreach($tags as $tag)
          <span class="badge badge-tag badge-secondary mb-1"><i class="ion-pricetag"></i> {{ $tag->tag }}</span>
          @endforeach
          @else
          <p>
            No tags available
          </p>
          @endif
          <p class="mt-3">
            <a href="{{ route('tag.show') }}">Tags Page</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
