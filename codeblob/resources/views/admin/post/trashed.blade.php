@extends('layouts.admin-base')

@section('content')
<div class="container">
  <div class="row mb-4">
    <div class="col-sm-12">
      <h1 class="float-left">Posts</h1>
      <a href="{{ route('post.create') }}" class="btn btn-primary float-right"><i class="ion-compose"></i> Add New</a>
    </div>
  </div>

  <div class="row mt-2">
    <div class="col-sm-12">
      <nav aria-label="breadcrumb my-3">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><small><a href="{{ route('post') }}">&#40;{{ count($all) }}&#41; All</a></small></li>
          <li class="breadcrumb-item"><small><a href="{{ route('post.trashed') }}">&#40;{{ count($posts) }}&#41; Trash</a></small></li>
        </ol>
      </nav>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12">
      @if(count($posts) > 0)
      <table class="table">
        <thead>
          <tr>
            <th scope="col">Title</th>
            <th scope="col">Author</th>
            <th scope="col">Category</th>
            <th scope="col">Date</th>
          </tr>
        </thead>
        <tbody>
          @foreach($posts as $post)
          <tr>
            <td>
              <p><strong>{{ $post->title }}</strong></p>
              <span><small><a href="{{ route('post.edit', ['id' => $post->id]) }}" class="mr-1"><i class="ion-edit"></i> Edit</a></small></span>
              <span><small><a href="{{ route('post.restore', ['id' => $post->id]) }}" class="text-success mr-1"><i class="ion-checkmark-round"></i> Restore</a></small></span>
              <span><small><a href="{{ route('post.kill', ['id' => $post->id]) }}" class="text-danger"><i class="ion-close"></i> Delete Permanantly</a></small></span>
            </td>
            <td><small>Admin</small></td>
            <td><small>{{ $post->category_id }}</small></td>
            <td><small>{{ $post->created_at->format('F j, Y') }}</small></td>
          </tr>
          @endforeach
        </tbody>
      </table>
      @else
      <p>
        No posts found
      </p>
      @endif
    </div>
  </div>
</div>
@endsection
