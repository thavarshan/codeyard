@extends('layouts.admin-base')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-4 offset-md-4 mt-5">
      <div class="card">
        <div class="card-body">
          <h3 class="mb-5 text-center">Add New User</h3>
          <form method="POST" action="{{ route('user.store') }}">
            @csrf
            <fieldset class="form-group">
              <label for="name">Name</label>
              <input id="name" type="text" class="form-control" name="name" placeholder="User Name" required>
            </fieldset>
            <fieldset class="form-group">
              <label for="email">Email</label>
              <input id="email" type="email" class="form-control" name="email" placeholder="User Email" required>
            </fieldset>
            <button class="btn btn-success btn-block" type="submit">Add User</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
