@extends('layouts.admin-base')

@section('content')
<div class="container">
  <div class="row mb-4">
    <div class="col-sm-12">
      <h1 class="float-left">Users</h1>
      <a href="{{ route('user.create') }}" class="btn btn-success float-right"><i class="ion-compose"></i> Add New</a>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12">
      @if(count($users) > 0)
      <table class="table">
        <thead>
          <tr>
            <th scope="col">Basic Info</th>
            <th scope="col">Role</th>
            <th scope="col">Social</th>
            <th scope="col">About</th>
            <th scope="col" class="text-center">Posts</th>
            <th scope="col">Joined</th>
          </tr>
        </thead>
        <tbody>
          @foreach($users as $user)
          <tr>
            <td style="width: 32%">
              <div class="clearfix">
                <div class="float-left mr-3 py-2">
                  <img src="{{ asset($user->profile->avatar) }}" alt="{{ $user->name }}" class="rounded-circle" height="32px">
                </div>
                <div class="float-left">
                  <strong>{{ $user->name }}</strong><br />
                  <small>{{ $user->email }}</small>
                </div>
              </div>
              @if(Auth::id() !== $user->id)
              <div class="mt-2">
                <small>
                  <a href="{{ route('user.delete', ['id' => $user->id]) }}" class="text-danger"><i class="ion-close-round"></i> Remove Permanantly</a>
                </small>
              </div>
              @endif
            </td>
            <td>
              <small>
                @if($user->admin == 1)
                  <span class="text-success"><strong>Administrator</strong></span><br />
                  @if(Auth::id() !== $user->id)
                  <a href="{{ route('user.not.admin', ['id' => $user->id]) }}" class="text-danger">Make Subscriber</a>
                  @endif
                @else
                  <strong>Member</strong><br />
                  <a href="{{ route('user.admin', ['id' => $user->id]) }}" class="text-info">Make Admin</a>
                @endif
              </small>
            </td>
            <td>
              <a href="{{ $user->profile->facebook }}" class="mr-3"><span class="ion-social-facebook"></span></a>
              <a href="{{ $user->profile->twitter }}" class="mr-3"><span class="ion-social-twitter"></span></a>
              <a href="{{ $user->profile->github }}"><span class="ion-social-github"></span></a>
            </td>
            <td style="width: 25%">
              <p class="mb-0">
                <small>{{ $user->profile->about }}</small>
              </p>
            </td>
            <td class="text-center">{{ count($user->posts) }}</td>
            <td><small>{{ $user->created_at->format('F j, Y') }}</small></td>
          </tr>
          @endforeach
        </tbody>
      </table>
      @else
      <p>
        No posts found
      </p>
      @endif
    </div>
  </div>
</div>
@endsection
