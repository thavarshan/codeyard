@extends('layouts.admin-base')

@section('content')
<div class="container">
  <div class="row mb-4">
    <div class="col-sm-12">
      <h1>Profile</h1>
    </div>
  </div>

  <div class="row">
    <div class="col-md-3">
      <div class="card text-center">
        <div class="card-body">
          <div class="user-profile-img mb-2">
            <img src="{{ asset($user->profile->avatar) }}" alt="{{ $user->name }}" class="rounded-circle" height="80px">
          </div>
          <h5 class="card-title mb-0">{{ $user->name }}   @if($user->admin) <span class="text-success ion-flash"></span></span> @else  <span class="text-warning ion-flash-off"></span> @endif</h5>
          <p class="text-muted mb-0">
            <small>{{ $user->email }}</small>
          </p>
          <ul class="nav justify-content-center mb-3">
            <li class="nav-item">
              <a class="nav-link" target="_blank" href="{{ url($user->profile->facebook) }}"><i class="ion-social-facebook"></i></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" target="_blank" href="{{ url($user->profile->twitter) }}"><i class="ion-social-twitter"></i></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" target="_blank" href="{{ url($user->profile->github) }}"><i class="ion-social-github"></i></a>
            </li>
          </ul>
          <p class="card-text">
            {{ $user->profile->about }}
          </p>
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="list-group">
        @if(count($user->posts) > 0)
        @foreach($user->posts as $post)
        <a href="{{ route('post.edit', ['id' => $post->id]) }}" class="list-group-item list-group-item-action flex-column align-items-start">
          <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">{{ $post->title }}</h5>
            <small class="text-muted">{{ $post->created_at->diffForHumans() }}</small>
          </div>
          <p class="mb-1">
            {{ str_replace('<p>', '', str_limit($post->content, $limit = 100, $end = '...')) }}
          </p>
          <small>{{ $post->category->name }}</small>
        </a>
        @endforeach
        @else
        <p>
          You have not published any posts yet.
        </p>
        @endif
      </div>
    </div>
  </div>
</div>
@endsection
