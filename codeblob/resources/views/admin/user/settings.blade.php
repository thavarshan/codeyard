@extends('layouts.admin-base')

@section('content')
<div class="container">
  <div class="row mb-4">
    <div class="col-sm-12">
      <h1>Settings</h1>
    </div>
  </div>

  <div class="row">
    <div class="col-md-3">
      <div class="card text-center">
        <div class="card-body">
          <div class="user-profile-img mb-2">
            <img src="{{ asset($user->profile->avatar) }}" alt="{{ $user->name }}" class="rounded-circle" height="80px">
          </div>
          <h5 class="card-title mb-0">{{ $user->name }}</h5>
          <!-- <p class="mb-0">
            @if($user->admin)
              <span class="text-success">Administrator</span>
            @else
              <span class="text-info">Subscriber</span>
            @endif
          </p> -->
          <p class="text-muted mb-0">
            <small>{{ $user->email }}</small>
          </p>
          <ul class="nav justify-content-center mb-3">
            <li class="nav-item">
              <a class="nav-link" target="_blank" href="route($user->profile->facebook)"><i class="ion-social-facebook"></i></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" target="_blank" href="route($user->profile->twitter)"><i class="ion-social-twitter"></i></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" target="_blank" href="route($user->profile->github)"><i class="ion-social-github"></i></a>
            </li>
          </ul>
          <p class="card-text">
            {{ $user->profile->about }}
          </p>
        </div>
      </div>
    </div>

    <div class="col-md-6">
      <h6>Account Settings</h6>
      <hr>
      <form action="{{ route('user.settings.update') }}" class="mb-5" method="post" enctype="multipart/form-data">
        @csrf
        <fieldset class="form-group">
          <label for="avatar">Change Avatar</label>
          <div class="custom-file">
            <input type="file" name="avatar" class="custom-file-input" id="customFile">
            <label class="custom-file-label" name="avatar" for="customFile">Choose New Avatar</label>
          </div>
        </fieldset>
        <fieldset class="form-group">
          <label for="name">Name</label>
          <input class="form-control" type="text" name="name" value="{{ $user->name }}">
        </fieldset>
        <fieldset class="form-group">
          <label for="email">Email</label>
          <input class="form-control" type="email" name="email" value="{{ $user->email }}">
        </fieldset>
        <fieldset class="form-group">
          <label for="about">About</label>
          <textarea name="about" rows="8" cols="80" class="form-control">
            {{ $user->profile->about }}
          </textarea>
        </fieldset>
        <fieldset class="form-group">
          <div class="form-row">
            <div class="col">
              <label for="email">Facebook</label>
              <input type="text" class="form-control" name="facebook" value="{{ $user->profile->facebook }}">
            </div>
            <div class="col">
              <label for="email">Twitter</label>
              <input type="text" class="form-control" name="twitter" value="{{ $user->profile->twitter }}">
            </div>
            <div class="col">
              <label for="email">GitHub</label>
              <input type="text" class="form-control" name="github" value="{{ $user->profile->github }}">
            </div>
          </div>
        </fieldset>
        <fieldset class="form-group">
          <button type="submit" class="btn btn-success">Save Changes</button>
        </fieldset>
      </form>

      <h6>Privacy Settings</h6>
      <hr>
      <form action="{{ route('user.settings.update.password') }}" method="post" enctype="multipart/form-data">
        @csrf
        <fieldset class="form-group">
          <label for="password">Password</label>
          <input class="form-control" type="password" name="password" placeholder="New Password">
        </fieldset>
        <fieldset class="form-group">
          <button type="submit" class="btn btn-success">Save Changes</button>
        </fieldset>
      </form>
    </div>
  </div>
</div>
@endsection
