@extends('layouts.base')

@section('content')
  <div class="container">
    <h2 class="mb-5">{{ $title }}</h2>
    <div class="row">
      <div class="col-sm-12">
        @if(count($user->posts) > 0)
        <div class="grid">
          @foreach($user->posts as $post)
          <div class="grid-item">
            <div class="card card-post">
              <img class="card-img-top" src="{{ $post->featured }}" alt="{{ $post->title }}">
              <div class="card-body">
                <h4 class="card-title mb-0">{{ $post->title }}</h4>
                <p>
                  <small>
                  By <a href="#" class="mx-1"><i class="ion-person"></i> {{ $post->user->name }}</a> &#183; <a class="mx-1" href="{{ route('category.single', ['id' => $post->category_id]) }}"><i class="ion-pricetag"></i> {{ $post->category->name }}</a> &#183; <span class="mx-1"><i class="ion-clock"></i> {{ $post->created_at->diffForHumans() }}</span>
                  </small>
                </p>
                <p class="card-text">{{ str_replace('<p>', '', str_limit($post->content, $limit = 100, $end = '...')) }}</p>
                <a class="mx-1 btn btn-link btn-block" href="{{ route('post.single', ['slug' => $post->slug ]) }}">Continue reading</a>
              </div>
            </div>
          </div>
          @endforeach
        </div>
        @else
        <p>
          No posts available
        </p>
        @endif
      </div>
    </div>
  </div>
@endsection
