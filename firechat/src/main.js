import Vue from 'vue';
import App from './App.vue';
import router from './routes'
import VueChatScroll from 'vue-chat-scroll'
import '@/assets/css/tailwind.css';
import '@/assets/css/fontawesome.css';

Vue.use(VueChatScroll);

Vue.config.productionTip = false;

new Vue({
    router,
    render: h => h(App),
}).$mount('#app');
