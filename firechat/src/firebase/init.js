import firebase from 'firebase';
import firestore from 'firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyB8xmyQUvUy1e20l2UZ6pLupEolNyjkpBk",
    authDomain: "firechat-d61b7.firebaseapp.com",
    databaseURL: "https://firechat-d61b7.firebaseio.com",
    projectId: "firechat-d61b7",
    storageBucket: "",
    messagingSenderId: "381320450006",
    appId: "1:381320450006:web:9d3a6e2f88ec32c8"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

export default firebaseApp.firestore();
