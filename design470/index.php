<?php include 'includes/header.php'; ?>

  <!-- Hero Section -->
  <section class="section-hero">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <div class="hero-area">
            <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="5000">

              <!-- Wrapper for slides -->
              <div class="carousel-inner">
                <div class="item slides active">
                  <div class="slide slide-1 animated zoomInSp">
                    <div class="row">
                      <div class="col-xs-12 col-sm-4">
                        <h1 id="slide-1-cap" class="slide-caption animated fadeInRight delay-6">The principles of true art is not to portray, but to evoke.</h1>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="item slides">
                  <div class="slide slide-2 animated zoomInSp">
                    <div class="row">
                      <div class="col-xs-12 col-sm-offset-4 col-sm-4 text-center">
                        <h2 id="slide-2-cap" class="slide-caption animated fadeInUp delay-6">Art enables us to find ourselves and lose ourselves at the same time.</h2>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="item slides">
                  <div class="slide slide-3 animated zoomInSp">
                    <div class="row">
                      <div class="col-xs-12 col-sm-offset-4 col-sm-4">
                        <h2 id="slide-3-cap" class="slide-caption animated fadeInUp delay-6">You don’t take a photograph, you make it.</h2>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section><!-- Hero Section End -->

  <!-- Introduction Sectoin -->
  <section class="section-intro" id="about">
    <div class="container-fluid">
      <div class="row js--wp-2">
        <div class="col-md-3 col-sm-3 col-xs-12">
          <div class="sub-title">
            <h5 class="text-right">ayubowan</h5>
          </div>
        </div>

        <div class="col-md-3 col-sm-12">
          <p class="large-type">We’re a digital agency mainly focused on Graphic Design, Web Interactions and Branding, based in<br />Sri Lanka.</p>
        </div>

        <div class="col-md-3 col-sm-12">
          <p class="serif">
            If you’re looking for creative ideas that drive high conversion rates, content that inspires action and design that just won’t let you look away, then you’re in the right place. We’re <strong>Design470</strong> and our mission is to portrait your ideas on a digital canvas, so that anyone can look at it and go “Wow”. Work with us and we’ll figure out what you want (or what you need) and then make sure you get it.
          </p>
        </div>
      </div>
    </div>
  </section><!-- Introduction Sectoin End -->

  <!-- Services Section -->
  <section class="section-services" id="services">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <div class="background-light">
            <div class="row mb-2 js--wp-3">
              <div class="col-md-3">
                <div class="sub-title">
                  <h5 class=" text-right">services</h5>
                </div>
              </div>

              <div class="col-md-9">
                <blockquote class="type-md">
                  “We warn you against not believing that designing is a science.”
                </blockquote>
              </div>
            </div>

            <div class="row">
              <div class="col-md-4">
                <div class="card js--wp-3-1">
                  <div>
                    <i class="fa fa-desktop icon-big color-grey" aria-hidden="true"></i>
                  </div>
                  <div>
                    <h4>Digital</h4>
                  </div>
                  <div>
                    <p>
                      Your website is your most powerful communication and sales weapon. It’s where most people first encounter your brand and – if you get it right – where they start the conversion from prospects to potential clients.
                    </p>
                  </div>
                </div>
              </div>

              <div class="col-md-4">
                <div class="card js--wp-3-2">
                  <div>
                    <i class="fa fa-tags icon-big color-grey" aria-hidden="true"></i>
                  </div>
                  <div>
                    <h4>Branding</h4>
                  </div>
                  <div>
                    <p>
                      If a customer visited your website but your name was missing, would they still know it was you?
                    </p>
                    <p>
                      Your brand defines you, it represents your values, sets you apart from your competitors.
                    </p>
                  </div>
                </div>
              </div>

              <div class="col-md-4">
                <div class="card js--wp-3-3">
                  <div>
                    <i class="fa fa-book icon-big color-grey" aria-hidden="true"></i>
                  </div>
                  <div>
                    <h4>Publication</h4>
                  </div>
                  <div>
                    <p>
                      There’s something special about a beautifully designed printed publication. The best ones contain stories that resonate with people, news that excites them and beautifully presented products that have them reaching for their wallets.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section> <!-- Services Section End -->

  <!-- Portfolio Section -->
  <section class="section-portfolio" id="portfolio">
    <div class="container-fluid js--wp-4">
      <div class="row mb-5">
        <div class="col-md-3 col-sm-3">
          <div class="sub-title">
            <h5 class=" text-right">portfolio</h5>
          </div>
        </div>

        <div class="col-md-6 col-sm-12">
          <p class="mb-3 large-type">
            We believe our work speaks for itself...
          </p>
        </div>
      </div>

      <div class="row">
        <div class="col-md-9">
          <div class="row">
            <div class="col-md-3">
              <a href="portfolio-asphaltech.php">
                <div class="portfolio-item">
                  <img src="https://scontent.fcmb3-1.fna.fbcdn.net/v/t1.0-9/23376581_1986021624986759_4231593370199362959_n.jpg?oh=fa4f35b6d217c8c86a9a37e0484b37a7&oe=5A9DC720" />
                  <div class="portfolio-overlay">
                    <div class="portfolio-overlay-text">Asphaltech Victoria</div>
                  </div>
                </div>

                <a href="portfolio-mrm.php">
                  <div class="portfolio-item">
                    <img src="images/mrm-logo1.jpg" />
                    <div class="portfolio-overlay">
                      <div class="portfolio-overlay-text">MRM Group (PVT) LTD</div>
                    </div>
                  </div>
                </a>
              </a>
            </div>

            <div class="col-md-9">
              <a href="portfolio-latrobe-uni.php">
                <div class="portfolio-item">
                  <img src="https://scontent.fcmb3-1.fna.fbcdn.net/v/t1.0-9/22549488_1976817819240473_178314797807889024_n.jpg?oh=ee982cbda2a47de62f622449c2bc359d&oe=5AAE57DB" />
                  <div class="portfolio-overlay">
                    <div class="portfolio-overlay-text">La Trobe University Engineering &amp; IT Showcase 2017</div>
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>

        <div class="col-md-3">
          <a href="portfolio-ieee-sc2017.php">
            <div class="portfolio-item ht-50">
              <img src="https://scontent.fcmb3-1.fna.fbcdn.net/v/t1.0-9/22554759_1976819849240270_4471286704784239194_n.jpg?oh=545cd78e4df0a942a0802869627f9835&oe=5A9DB079" />
              <div class="portfolio-overlay">
                <div class="portfolio-overlay-text">IEEE Sections Congress 2017</div>
              </div>
            </div>
          </a>

          <a href="portfolio-ieee-africa.php">
            <div class="portfolio-item ht-50">
              <img src="images/iam.jpg" />
              <div class="portfolio-overlay">
                <div class="portfolio-overlay-text">IEEE Africa Newsletter</div>
              </div>
            </div>
          </a>

        </div>
      </div>

      <div class="row">
        <div class="col-xs-12">
          <a href="portfolio-ieee-sc-web.php">
            <div class="portfolio-item">
              <img src="images/sc-web-prev.jpg" />
              <div class="portfolio-overlay">
                <div class="portfolio-overlay-text">IEEE Sections Congress Web</div>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  </section><!-- Portfolio Section End -->

  <!-- Team Section -->
  <!-- <section class="scetion-team" id="team">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <div class="background-light">
            <div class="row">
              <div class="col-md-3 js--wp-5">
                <div class="sub-title">
                  <h5 class=" text-right">team</h5>
                </div>
              </div>

              <div class="col-md-3 mb-3">
                <p class="large-type">
                  "None of us is as smart as all of us." Meet the elements of the team that brings color to the canvas.
                </p>
              </div>

              <div class="col-md-6">
                <div class="row">
                  <div class="col-md-6">
                    <div class="team-member js--wp-5-1">
                      <img class="team-pic" alt="Anuradha Edirisuriya" src="images/anuradha.jpg" />
                      <div class="cover">
                        <div class="vertical-align">
                          <h5>Anuradha Edirisuriya</h5>
                          <p>
                            Design Lead
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="team-member js--wp-5-2">
                      <img class="team-pic" alt="Thavarshan" src="https://scontent.fcmb3-1.fna.fbcdn.net/v/t1.0-9/22489746_1630390187010567_7703395876748682197_n.jpg?oh=4dcded313960b0c35552b01f4c4aaf2a&oe=5AA459E8" />
                      <div class="cover">
                        <div class="vertical-align">
                          <h5>Thavarshan Thayananthajothy</h5>
                          <p>
                            Digital Artist
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="team-member js--wp-5-3">
                      <img class="team-pic" alt="Thavarshan Thayananthajothy" src="https://media-exp2.licdn.com/media/AAEAAQAAAAAAAAyXAAAAJDJhYWVlMWMyLTM0NGUtNDZmNS05YjY0LWM0NDBkZjJmNDZjYg.jpg" />
                      <div class="cover">
                        <div class="vertical-align">
                          <h5>Subodha Charles</h5>
                          <p>
                            Content Creator
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="team-member js--wp-5-3">
                      <img class="team-pic" alt="Eddie Custovic" src="https://scontent.fcmb4-1.fna.fbcdn.net/v/t1.0-1/18447029_10155151032206745_8488977112354828822_n.jpg?oh=b1558cedcc0f2868522e37ebf8c1955e&oe=5A91EA8D" />
                      <div class="cover">
                        <div class="vertical-align">
                          <h5>Eddie Custovic</h5>
                          <p>
                            Founder
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="team-member js--wp-5-3">
                      <img class="team-pic" alt="Peter Moar" src="images/peter_moar.jpg" />
                      <div class="cover">
                        <div class="vertical-align">
                          <h5>Peter Moar</h5>
                          <p>
                            Founder
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section><!-- Team Section End --> -->

  <!-- Contact Section -->
  <section class="section-contact" id="contact">
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <form role="form" id="contactForm" data-toggle="validator" class="">
            <div class="row">
              <div class="col-md-3 col-sm-3 js--wp-6">
                <div class="sub-title">
                  <h5 class=" text-right">get in touch</h5>
                </div>
              </div>

              <div class="col-md-3 col-sm-9">
                <p class="large-type mb-3">
                  Every collaboration starts with a hello. Hit us up with a message.
                </p>
              </div>
              <div class="col-md-5 col-sm-12 js--wp-7">
                <div class="row">
                  <div class="form-group col-md-12 col-sm-6">
                    <input type="text" class="form-control" id="name" placeholder="Enter name" required>
                    <div class="help-block with-errors"></div>
                  </div>
                  <div class="form-group col-md-12 col-sm-6">
                    <input type="email" class="form-control" id="email" placeholder="Enter email" required>
                    <div class="help-block with-errors"></div>
                  </div>
              </div>
              <div class="form-group">
                <textarea id="message" class="form-control" rows="5" placeholder="Enter your message" required></textarea>
                <div class="help-block with-errors"></div>
              </div>
              <button type="submit" id="form-submit" class="btn btn-primary pull-right ">Send Message</button>
              <div id="msgSubmit" class="h3 label text-center hidden"></div>
              <div class="clearfix"></div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section> <!-- Contact Section End -->

<?php include 'includes/footer.php'; ?>
