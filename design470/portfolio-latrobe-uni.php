<?php include 'includes/header.php'; ?>

<section class="section-portfolio">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="soldier-header">
          <h1>La Trobe University Engineering &amp; IT Showcase 2017</h1>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12">
        <div class="portfolio-slider">

          <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
              <li data-target="#myCarousel" data-slide-to="3"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
              <div class="item active">
                <img src="images/projects/latrobe-uni/ltit1.jpg" alt="La Trobe University Engineering &amp; IT Showcase 2017">
              </div>

              <div class="item">
                <img src="images/projects/latrobe-uni/ltit2.jpg" alt="La Trobe University Engineering &amp; IT Showcase 2017">
              </div>

              <div class="item">
                <img src="images/projects/latrobe-uni/ltit3.jpg" alt="La Trobe University Engineering &amp; IT Showcase 2017">
              </div>

              <div class="item">
                <img src="images/projects/latrobe-uni/ltit4.jpg" alt="La Trobe University Engineering &amp; IT Showcase 2017">
              </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
              <span class="ion-chevron-left"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
              <span class="ion-chevron-right"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>

        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12 text-center">
        <a href="http://design470.com/#portfolio" class="btn btn-primary mt-3">Back to Portfolio</a>
      </div>
    </div>
  </div>
</section>

<?php include 'includes/footer.php'; ?>
