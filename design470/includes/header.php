<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Design470 | Creativity Unleashed</title>
  <meta name="description" content="If you’re looking for creative ideas that drive high conversion rates, content that inspires action and design that just won’t let you look away, then you’re in the right place. We’re Design470 and our mission is to portrait your ideas on a digital canvas, so that anyone can look at it and go “Wow”. Work with us and we’ll figure out what you want (or what you need) and then make sure you get it.">
  <meta name="author" content="Design470">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="https://fonts.googleapis.com/css?family=Lora|Montserrat:200,600|Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome.css">
    <link rel="stylesheet" href="css/ionicons.min.css">

  <!-- CSS Primary
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/animate.css">
  <link rel="stylesheet" href="css/hover.css">

  <!-- CSS Resets
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/bootstrap-resets.css">

  <!-- CSS Globals & Re-Usables
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/global.css">
  <link rel="stylesheet" href="css/reuse.css">
  <link rel="stylesheet" href="css/preloader.css">
  <link rel="stylesheet" href="css/navmenu.css">
  <link rel="stylesheet" href="css/slider.css">
  <link rel="stylesheet" href="css/mediaqueries.css">
  <link rel="stylesheet" href="css/animations.css">
  <link rel="stylesheet" href="css/portfolio.css">

  <!-- GUIDES -->
  <link rel="stylesheet" href="css/guides.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="images/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
  <link rel="manifest" href="images/favicon/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="images/favicon/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">


  <!-- Javascript
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="js/jquery-1.9.1.min.js"><\/script>')</script>
  <script src="js/jquery-1.11.2.min.js"></script>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/waypoints/1.1.6/waypoints.min.js'></script>
  <script src='https://masonry.desandro.com/masonry.pkgd.js'></script>

</head>
<body>

  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <!-- Website Pre-Loader -->
  <div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
  </div>
  <!-- Website Pre-Loader End -->

  <!-- Main Menu Overlay -->
  <div id="myNav" class="overlay animated">
    <div class="container">
      <div class="row">
        <div id="nav-icon1">
          <span></span>
          <span></span>
          <span></span>
        </div>
        </a>
      </div>
      <script>
        /* Open */
        function togMenu() {
          $(".overlay").fadeToggle();
          $("#openbtn").toggleClass('open');
        }
      </script>
      <div class="row">
        <div class="overlay-content">
          <a href="http://design470.com/#top" class="navLink animated delay-1" onclick="togMenu()">Home</a>
          <a href="http://design470.com/#about" class="navLink animated delay-2" onclick="togMenu()">About</a>
          <a href="http://design470.com/#services" class="navLink animated delay-3" onclick="togMenu()">Services</a>
          <a href="http://design470.com/#portfolio" class="navLink animated delay-4" onclick="togMenu()">Portfolio</a>
          <a href="http://design470.com/#team" class="navLink animated delay-5" onclick="togMenu()">Team</a>
          <a href="http://design470.com/#contact" class="navLink animated delay-6" onclick="togMenu()">Contact</a>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="footer-copyrights-area-menu">
            <p class="copyright-menu">
              &copy; Desgin470. All rights reserved.
            </p>
          </div>
        </div>
      </div>
    </div>
  </div><!-- Main Menu End -->

  <!-- Website Header Section -->
  <div id="top"></div>
  <header>
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <a href="http://design470.com">
            <img class="logo pull-left" src="images/logo.png" alt="Design470" />
          </a>
          <!-- Use any element to open/show the overlay navigation menu -->
          <div href="" id="openbtn" class="menu-bt op pull-right">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
        </div>
      </div>
    </div>
  </header><!-- Website Header Section End -->
