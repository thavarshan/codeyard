<!-- Footer Section -->
<footer>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="background-light have-padding">
          <div class="row">
            <div class="footer-social-area">
              <div class="col-md-3 col-sm-3 col-xs-12 br-align br-mb-2">
                <h6>Design470</h6>
                <p>
                  74/5E, Indigolla,<br />
                  Gampaha,<br />
                  Sri Lanka
                </p>
              </div>

              <div class="col-md-6 col-sm-6 col-xs-12 text-center br-mb-2">
                <img class="footer-logo" src="images/logo.png" />
                <div class="social-nav">
                  <a target="_blank" href="https://www.facebook.com/design470/"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                  <a target="_blank" href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                </div>
              </div>

              <div class="col-md-3 col-sm-3 col-xs-12 text-right br-align">
                <h6>Say Hello</h6>
                <p>
                  +94 71 582 4665<br />
                  +1 352 216 8940<br />
                  <a href="mailto:reachus@design470.com">reachus@design470.com</a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12">
        <div class="footer-copyrights-area">
          <p class="copyright">
            &copy; Desgin470. All rights reserved.
          </p>
        </div>
      </div>
    </div>
  </div>
</footer><!-- Footer Section End -->

<!-- End Document
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<!-- End Document Javascript
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<script src="js/modernizr-2.6.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/autoScroll.js" type="text/javascript"></script>
<script src="js/app.js"></script>
<script src="js/preloader.js"></script>
<script type="text/javascript" src="js/validator.min.js"></script>
<script type="text/javascript" src="js/form-scripts.js"></script>

</body>
</html>
