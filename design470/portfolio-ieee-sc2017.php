<?php include 'includes/header.php'; ?>

<section class="section-portfolio">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="soldier-header">
          <h1>IEEE Section Congress 2017</h1>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12">
        <div class="portfolio-slider">

          <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class=""></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
              <li data-target="#myCarousel" data-slide-to="3"></li>
              <li data-target="#myCarousel" data-slide-to="4"></li>
              <li data-target="#myCarousel" data-slide-to="5"></li>
              <li data-target="#myCarousel" data-slide-to="6"></li>
              <li data-target="#myCarousel" data-slide-to="7"></li>
              <li data-target="#myCarousel" data-slide-to="8"></li>
              <li data-target="#myCarousel" data-slide-to="9"></li>
              <li data-target="#myCarousel" data-slide-to="10"></li>
              <li data-target="#myCarousel" data-slide-to="11"></li>
              <li data-target="#myCarousel" data-slide-to="12"></li>
              <li data-target="#myCarousel" data-slide-to="13"></li>
              <li data-target="#myCarousel" data-slide-to="14"></li>
              <li data-target="#myCarousel" data-slide-to="15"></li>
              <li data-target="#myCarousel" data-slide-to="16"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
              <div class="item active">
                <img src="images/projects/sc2017/1.jpg" alt="IEEE Sections Congress 2017">
              </div>
              <div class="item ">
                <img src="images/projects/sc2017/2.jpg" alt="IEEE Sections Congress 2017">
              </div>
              <div class="item ">
                <img src="images/projects/sc2017/3.jpg" alt="IEEE Sections Congress 2017">
              </div>
              <div class="item ">
                <img src="images/projects/sc2017/4.jpg" alt="IEEE Sections Congress 2017">
              </div>
              <div class="item ">
                <img src="images/projects/sc2017/5.jpg" alt="IEEE Sections Congress 2017">
              </div>
              <div class="item ">
                <img src="images/projects/sc2017/6.jpg" alt="IEEE Sections Congress 2017">
              </div>
              <div class="item ">
                <img src="images/projects/sc2017/7.jpg" alt="IEEE Sections Congress 2017">
              </div>
              <div class="item ">
                <img src="images/projects/sc2017/8.jpg" alt="IEEE Sections Congress 2017">
              </div>
              <div class="item ">
                <img src="images/projects/sc2017/9.jpg" alt="IEEE Sections Congress 2017">
              </div>
              <div class="item ">
                <img src="images/projects/sc2017/10.jpg" alt="IEEE Sections Congress 2017">
              </div>
              <div class="item ">
                <img src="images/projects/sc2017/11.jpg" alt="IEEE Sections Congress 2017">
              </div>
              <div class="item ">
                <img src="images/projects/sc2017/12.jpg" alt="IEEE Sections Congress 2017">
              </div>
              <div class="item ">
                <img src="images/projects/sc2017/13.jpg" alt="IEEE Sections Congress 2017">
              </div>
              <div class="item ">
                <img src="images/projects/sc2017/14.jpg" alt="IEEE Sections Congress 2017">
              </div>
              <div class="item ">
                <img src="images/projects/sc2017/15.jpg" alt="IEEE Sections Congress 2017">
              </div>
              <div class="item ">
                <img src="images/projects/sc2017/16.jpg" alt="IEEE Sections Congress 2017">
              </div>
              <div class="item ">
                <img src="https://scontent.fcmb3-1.fna.fbcdn.net/v/t1.0-9/22554759_1976819849240270_4471286704784239194_n.jpg?oh=545cd78e4df0a942a0802869627f9835&oe=5A9DB079" alt="IEEE Sections Congress 2017">
              </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
              <span class="ion-chevron-left"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
              <span class="ion-chevron-right"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>

        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12 text-center">
        <a href="http://design470.com/#portfolio" class="btn btn-primary mt-3">Back to Portfolio</a>
      </div>
    </div>
  </div>
</section>

<?php include 'includes/footer.php'; ?>
