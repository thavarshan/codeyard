/* SCROLL ANIMATIONS */
$('.js--wp-2').waypoint(function() {
  $('.js--wp-2').addClass('animated fadeInUp');
}, { offset: 630 });

$('.js--wp-3').waypoint(function() {
  $('.js--wp-3').addClass('animated fadeIn');
}, { offset: 670 });

$('.js--wp-3-1').waypoint(function() {
  $('.js--wp-3-1').addClass('animated zoomIn delay-1');
}, { offset: 600 });

$('.js--wp-3-2').waypoint(function() {
  $('.js--wp-3-2').addClass('animated zoomIn delay-2');
}, { offset: 600 });

$('.js--wp-3-3').waypoint(function() {
  $('.js--wp-3-3').addClass('animated zoomIn delay-3');
}, { offset: 600 });

$('.js--wp-4').waypoint(function() {
  $('.js--wp-4').addClass('animated zoomIn');
}, { offset: 670 });

$('.js--wp-5').waypoint(function() {
  $('.js--wp-5').addClass('animated zoomIn');
}, { offset: 670 });


$('.js--wp-5-1').waypoint(function() {
  $('.js--wp-5-1').addClass('animated zoomIn');
}, { offset: 600 });

$('.js--wp-5-2').waypoint(function() {
  $('.js--wp-5-2').addClass('animated zoomIn');
}, { offset: 600 });

$('.js--wp-5-3').waypoint(function() {
  $('.js--wp-5-3').addClass('animated zoomIn');
}, { offset: 650 });

$('.js--wp-6').waypoint(function() {
  $('.js--wp-6').addClass('animated fadeInLeft');
}, { offset: 670 });

$('.js--wp-7').waypoint(function() {
  $('.js--wp-7').addClass('animated fadeInRight');
}, { offset: 670 });

$(document).ready(function(){
	$("#openbtn").click(function(){
		$(this).toggleClass('open');
    $(".overlay").fadeToggle();
    $(".navLink").addClass("fadeInUp");
	});

});
