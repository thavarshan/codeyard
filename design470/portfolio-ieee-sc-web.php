<?php include 'includes/header.php'; ?>

<section class="section-portfolio">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="soldier-header">
          <h1>IEEE Section Congress Web Site</h1>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12">
        <div class="portfolio-slider">
          <div class="screenshot-display">
            <div class="screenshot-contain">
              <img src="images/projects/sc-web/sc-web.jpg" alt="IEEE Section Congress" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12 text-center">
        <a href="http://design470.com/#portfolio" class="btn btn-primary mt-3">Back to Portfolio</a>
      </div>
    </div>
  </div>
</section>

<?php include 'includes/footer.php'; ?>
