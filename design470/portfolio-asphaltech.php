<?php include 'includes/header.php'; ?>

<section class="section-portfolio">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="soldier-header">
          <h1>Asphaltec Victoria</h1>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12">
        <div class="row">
          <div class="col-xs-12 mb-3 text-center">
            <img class="ast-img" src="images/projects/asphaltech/as1.jpg" alt="asphaltech">
          </div>

          <div class="col-xs-12 mb-3 text-center">
            <img class="ast-img" src="images/projects/asphaltech/as2.jpg" alt="asphaltech">
          </div>

          <div class="col-xs-12 mb-3 text-center ast-cont">
            <img class="ast-img" src="images/projects/asphaltech/as3.jpg" alt="asphaltech">
          </div>

          <div class="col-xs-12 mb-3 text-center ast-cont">
            <img class="ast-img" src="images/projects/asphaltech/as4.jpg" alt="asphaltech">
          </div>

          <div class="col-xs-12 mb-3 text-center ast-cont">
            <img class="ast-img" src="images/projects/asphaltech/as5.jpg" alt="asphaltech">
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12 text-center">
        <a href="http://design470.com/#portfolio" class="btn btn-primary mt-3">Back to Portfolio</a>
      </div>
    </div>
  </div>
</section>

<?php include 'includes/footer.php'; ?>
