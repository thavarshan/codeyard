<?php include 'inc/header.php'; ?>
  <section class="section-contact">
    <div class="container">
      <div class="row">
        <div class="tz-gallery">
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <a class="lightbox" href="images/gallery/img1.jpg">
                        <img src="images/gallery/img1.jpg" alt="Park">
                    </a>
                </div>
                <div class="col-sm-6 col-md-4">
                    <a class="lightbox" href="images/gallery/img3.jpg">
                        <img src="images/gallery/img3.jpg" alt="Bridge">
                    </a>
                </div>
                <div class="col-sm-12 col-md-4">
                    <a class="lightbox" href="images/gallery/img4.jpg">
                        <img src="images/gallery/img4.jpg" alt="Tunnel">
                    </a>
                </div>
                <div class="col-sm-6 col-md-4">
                    <a class="lightbox" href="images/gallery/img5.jpg">
                        <img src="images/gallery/img5.jpg" alt="Coast">
                    </a>
                </div>
                <div class="col-sm-6 col-md-4">
                    <a class="lightbox" href="images/gallery/img6.jpg">
                        <img src="images/gallery/img6.jpg" alt="Rails">
                    </a>
                </div>
                <div class="col-sm-6 col-md-4">
                    <a class="lightbox" href="images/gallery/img7.jpg">
                        <img src="images/gallery/img7.jpg" alt="Traffic">
                    </a>
                </div>
                <div class="col-sm-6 col-md-4">
                    <a class="lightbox" href="images/gallery/img8.jpg">
                        <img src="images/gallery/img8.jpg" alt="Rocks">
                    </a>
                </div>
                <div class="col-sm-6 col-md-4">
                    <a class="lightbox" href="images/gallery/img9.jpg">
                        <img src="images/gallery/img9.jpg" alt="Benches">
                    </a>
                </div>
                <div class="col-sm-6 col-md-4">
                    <a class="lightbox" href="images/gallery/img10.jpg">
                        <img src="images/gallery/img10.jpg" alt="Sky">
                    </a>
                </div>
            </div>
            <script>
              baguetteBox.run('.tz-gallery');
            </script>
        </div>
      </div>
    </div>
  </section>
<?php include 'inc/footer.php'; ?>
