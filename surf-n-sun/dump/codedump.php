<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <div class="container">
      <div class="row mb-5">
        <div class="col-md-6">
          <div class="card">
            <img class="card-img-top" src="images/srilanka.jpeg" alt="Sri Lanka">
            <div class="card-block text-center">
              <h4 class="card-title">Sri Lanka</h4>
              <p class="card-text">Famed to be the pearl of the Indian Ocean, would possibly deceive a person as if it was an illusion of a paradise on earth. The stretches of coasts around the island make it glow in gold adding more fantasy made by the evening sunset. Owning the world’s most prominent virgin forests with a surprisingly high degree of biodiversity, Sri Lanka truly is a spectacle for travellers. The lush green mountains and spouting waterfalls, provide the ultimate romantic escape. Our world famous hospitality provide the opportunity to relish all of these almost with no avarice. We invite you, to discover many worlds within this small island.</p>
              <a href="#" class="btn btn-primary">Read More</a>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="card">
            <img class="card-img-top" src="images/arugambay.jpeg" alt="Sri Lanka">
            <div class="card-block text-center">
              <h4 class="card-title">Arugambay</h4>
              <p class="card-text">A small fishing village located on the East coast of Sri Lanka. It's natural beauty is still untouched with pristine shores and no shortage of adventure around the area. Located in close proximity to many of Sri Lanka's largest national wildlife parks - Yala, Lahungala, Kumana Bird Sanctuary and the natural Mangrove conservation of Pottuvil Lagoon, all within one hour’s travel, Arugambay is a must visit destination in the Sri Lankan map. Significant cultural artifacts such as ancient temples and cave dwellings that date back nearly two thousand years testify the truly historical and cultural significance of this area.</p>
              <a href="#" class="btn btn-primary">Read More</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>


<div class="alert bg-primary color-white">
  <h4 class="color-white">Flying</h4>

  <p>
    Fly into Sri-Lanka's only international airport - Bandaranaike International Airport - also called Katunayake International Airport.  You can arrange a car service from the airport or call us and we will pick you up!
  </p>

  <h4 class="color-white">Airport Pick Up</h4>

  <p>
    We can pick you up at the airport and pack all your gear and drive you here.  The trip takes about 8 hours so we will stop along the way for refreshments and to stretch our legs.  There are many scenic roads along the way.  We can also arrange to pick you up from a hotel in Colombo.
  </p>

  <p>
    Contact us and let us know and we will arrange your travel.
  </p>

  <h4 class="color-white">Bus Routes from</h4>

  <p>
    Colombo: The CTB Pottuvil / Panama bus leaves at 5am from the Pettah bus stand every day and arrives at Pottuvil at 4pm.  You can take a three wheeler (tuk tuk) from Pottuvil to Arugam.
  </p>

  <p>
    Hikkaduwa and Matara: Take the Moneragla bus which departs from the Hikkaduwa and Matara bus stands respectively.  Bus departs at 5am and arrives at Moneragla at 1130am.
  </p>

  <p>
    From Moneragla you should take the CTB Pottuvil / Panama bus to Pottuvil.  The bus departs at noon.
  </p>
</div>
