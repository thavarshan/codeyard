<?php include 'inc/header-front.php'; ?>

  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <section class="section-welcome clearfix">
    <div class="container">
      <div class="row mb-5">
        <div class="col-lg-12 text-center">
          <h1 class="title mb-5">Arugambay</h1>
          <p>
            A small fishing village located on the East coast of Sri Lanka. It's natural beauty is still untouched with pristine shores and no shortage of adventure around the area. Located in close proximity to many of Sri Lanka's largest national wildlife parks - Yala, Lahungala, Kumana Bird Sanctuary and the natural Mangrove conservation of Pottuvil Lagoon, all within one hour’s travel, Arugambay is a must visit destination in the Sri Lankan map. Significant cultural artifacts such as ancient temples and cave dwellings that date back nearly two thousand years testify the truly historical and cultural significance of this area.
          </p>
        </div>
      </div>

      <div class="row">
        <div class="col-md-4">
          <div class="frame">
            <div class="frame-img">
              <img src="images/sandy-beaches.jpg" alt="Arugam Bay Beaches">
            </div>
            <div class="frame-text">
              <h3>Warm Sandy Beaches</h3>
            </div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="frame">
            <div class="frame-img">
              <img src="images/awsome-waves.jpg" alt="Arugam Bay Waves">
            </div>
            <div class="frame-text">
              <h3>Breathtaking Waves</h3>
            </div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="frame">
            <div class="frame-img">
              <img src="images/exotic-foods.jpg" alt="Sri Lankan Food">
            </div>
            <div class="frame-text">
              <h3>Exotic Food</h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="section-video">
    <div class="container">
      <div class="row text-center color-white">
        <div class="col-sm-12">
          <h1 class="special-font color-white font-large">Sri Lanka</h1>
          <p>
            Famed to be the pearl of the Indian Ocean, would possibly deceive a person as if it was an illusion of a paradise on earth. The stretches of coasts around the island make it glow in gold adding more fantasy made by the evening sunset. Owning the world’s most prominent virgin forests with a surprisingly high degree of biodiversity, Sri Lanka truly is a spectacle for travellers. The lush green mountains and spouting waterfalls, provide the ultimate romantic escape. Our world famous hospitality provide the opportunity to relish all of these almost with no avarice. We invite you, to discover many worlds within this small island.
          </p>
          <!-- Button HTML (to Trigger Modal) -->
          <a href="#myModal" class="btn btn-lg btn-outline-light" data-toggle="modal">
            Experience Sri Lanka &nbsp;<span class="ion-play"></span>
          </a>
        </div>

        <!-- Modal HTML -->
        <div id="myModal" class="modal fade">
          <div class="modal-dialog modal-lg">
              <div class="modal-content bg-black">
                  <div class="modal-header">
                    <button type="button" class="close btn btn-link icon-md" data-dismiss="modal" aria-hidden="true">
                      <i class="ion-ios-close-outline color-white"></i>
                    </button>
                  </div>
                  <div class="modal-body">
                    <iframe id="video" width="560" height="315" src="https://www.youtube-nocookie.com/embed/msz3DxifmzQ?rel=0&amp;showinfo=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="container">
      <div class="row mb-5 text-center">
        <div class="col-sm-12">
          <h1 class="title">Your stay with Surf'n Sun</h1>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3">
          <a href="#" data-toggle="modal" data-target="#modalFood">
            <div class="img-container">
              <div class="img-title">
                <h3 class="color-white">Food</h3>
                <p>
                  Have a glimpse of our food culture
                </p>
              </div>
              <div class="img-overlay"></div>
              <img src="images/food.jpeg" alt="Sri Lankan Food">
            </div>
          </a>
          <!-- Modal FOOD -->
          <div id="modalFood" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal FOOD content-->
              <div class="modal-content">
                <div class="modal-header">
                  <h4>Food</h4>
                  <button type="button" class="close btn btn-link icon-md" data-dismiss="modal" aria-hidden="true">
                    <i class="ion-ios-close-outline"></i>
                  </button>
                </div>
                <div class="modal-body px-5">
                  <p>
                    We offer a wide range of food in our restaurant. Our speciality is authentic Italian Pizza! We also offer other western meals but why not get an exotic taste of authentic Sri Lankan food and fruites, some straight from our garden. If you like spicy foods you will LOVE Sri Lankan food, featuring a whole range of fresh sea food as well. Don't worry, if you cant handle spices, just let us know and we will tone it down for you.
                  </p>
                  <p>
                    Every weekend we arrange BBQ evenings. We use only local organic foods.
                  </p>
                  <p>
                    In the evenings we show surf movies and play chill raggae music in our bar. You can also watch movies during the day if you want to enjoy our satellite TV.
                  </p>
                </div>
                <div class="modal-footer">

                </div>
              </div>

            </div>
          </div>
        </div>

        <div class="col-md-9">
          <a href="#" data-toggle="modal" data-target="#modalRooms">
            <div class="img-container">
              <div class="img-title">
                <h3 class="color-white">Rooms</h3>
                <p>
                  Rest your weary head between adventures
                </p>
              </div>
              <div class="img-overlay"></div>
              <img src="images/room.jpg" alt="Sri Lankan Rooms">
            </div>
          </a>

          <!-- Modal ROOMS -->
          <div id="modalRooms" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal ROOMS content-->
              <div class="modal-content">
                <div class="modal-header">
                  <h4>Rooms</h4>
                  <button type="button" class="close btn btn-link icon-md" data-dismiss="modal" aria-hidden="true">
                    <i class="ion-ios-close-outline"></i>
                  </button>
                </div>
                <div class="modal-body px-5">
                  <p>
                    Wake up to the morning symphony of birds of paradise that live freely in our gardens. We offer a number of double roomed surf bungalows and cabanas. Amenities include daily room cleaning, laundry and full food services. The rooms are clean and spacious.
                  </p>
                  <p>
                    We have 2 types of double bed cabanas for <strong>$35.00</strong> to <strong>$40.00</strong> with breakfast per day. Single bed cabanas are <strong>$30.00</strong> with breakfast per day.
                  </p>
                  <p>
                    <strong>Please call us to check latest price.</strong>
                  </p>
                </div>
                <div class="modal-footer text-center">
                  <a href="contact.php" class="btn btn-primary text-center"><span class="ion-android-call"></span> Reach out to us</a>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-9">
          <!-- Trigger Modal Services -->
          <a href="#" data-toggle="modal" data-target="#modalServices">
            <div class="img-container">
              <div class="img-title">
                <h3 class="color-white">Services</h3>
                <p>
                  The extra mile to make your stay comfortable
                </p>
              </div>
              <div class="img-overlay"></div>
              <img src="images/airport.jpg" alt="Sri Lankan Services">
            </div>
          </a>

          <!-- Modal SERVICES -->
          <div id="modalServices" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal SERVICES content-->
              <div class="modal-content">
                <div class="modal-header">
                  <h4>Services</h4>
                  <button type="button" class="close btn btn-link icon-md" data-dismiss="modal" aria-hidden="true">
                    <i class="ion-ios-close-outline"></i>
                  </button>
                </div>
                <div class="modal-body px-5">
                  <div class="mb-5">
                    <h5>Airport Pickup</h5>
                    <p>
                      We can pick you up at the airport, pack all your gear and drive you here. The trip takes about 8 hours so we will stop along the way for refreshments and to stretch our legs. There are many scenic roads along the way. We can also arrange to pick you up from a hotel in Colombo.
                    </p>
                    <a href="contact.php" class="btn btn-outline-primary text-center"><span class="ion-android-call"></span> Reach out to us</a>
                  </div>

                  <div class="mt-5">
                    <h5>Safari Tours</h5>
                    <p>
                      We offer safari tours to the following places by our 4WD LandRover.
                      <ul>
                        <li>Yala National Park</li>
                        <li>Kumana Bird Sanctuary</li>
                        <li>Ruins and Ancient Civilizations</li>
                      </ul>
                    </p>
                    <p>
                      <a href="contact.php" class="btn btn-outline-primary text-center">Learn More</a>
                    </p>
                  </div>
                </div>
                <div class="modal-footer">
                  <!-- Empty -->
                </div>
              </div>

            </div>
          </div>
        </div>

        <div class="col-md-3">
          <!-- Trigger Modal Hospitality -->
          <a href="#" data-toggle="modal" data-target="#modalHospitality">
            <div class="img-container">
              <div class="img-title">
                <h3 class="color-white">Hospitality</h3>
                <p>
                  Be the guest jewel on our cushion of hospitality
                </p>
              </div>
              <div class="img-overlay"></div>
              <img src="images/hospitality.jpg" alt="Sri Lankan Hospitality">
            </div>
          </a>

          <!-- Modal HOSPITALITY -->
          <div id="modalHospitality" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal HOSPITALITY content-->
              <div class="modal-content">
                <div class="modal-header">
                  <h4>Hospitality</h4>
                  <button type="button" class="close btn btn-link icon-md" data-dismiss="modal" aria-hidden="true">
                    <i class="ion-ios-close-outline"></i>
                  </button>
                </div>
                <div class="modal-body px-5">
                  <p>
                    We believe that hospitality is about creating space for someone to feel seen, heard and comfortable without crossing any privacy boundaries. You will get the taste of world famous Sri Lankan hospitality at Surf'n Sun.
                  </p>
                  <p>
                    We are a nation that reserves the best for the guest. If you visit any home in Sri Lanka, you can be sure to be invited for a cup of tea and that will definitely include treating yourself to some nice delicacies even if it is not an everyday luxury for the common man. The best quality tea set, tastiest tea, best chair in the house will always be reserved for the guest.
                  </p>
                  <p>
                    When you come to Surf'n Sun we assure you that we will do our best to give you the holiday you are looking for. When you come here, you come to our home and we will do our best to make you feel as though it's your home too.
                  </p>
                </div>
                <div class="modal-footer">
                  <a href="contact.php" class="btn btn-primary text-center"><span class="ion-android-call"></span> Reach out to us</a>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  </section>

  <section class="section-safari">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="mt-5">
            <p class="color-white">
              Safari Adventures
            </p>
            <h1 class="color-white one-off-title">Enjoying the wildlife and being one with nature</h1>
            <a href="adventures.php" class="btn btn-outline-light">Read More</a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="section-surf">
    <div class="container">
      <div class="row mb-5 text-center">
        <div class="col-sm-12">
          <h1 class="title mb-5">Surfing in Arugambay</h1>
          <p>
            Arugam Bay is a legendary world class break with waves for everyone from beginners to pros.  The area is predominantly right hand breaks with un-crowded lineups. There are reef breaks, point breaks and beach breaks.  There is one main point and many smaller points around the area. The same swells that turn Indonesia on deliver the goods to Sri Lanka.  The most consistent SW swell season is during April - October.  Expect waves from 2-6ft with regular offsore winds and clean conditions.  Water temperatures are 27c / 82f all year long so bring board shorts.
          </p>
        </div>
      </div>

      <div class="row">
        <div class="col-md-4">
          <div class="frame" data-children=".frame-text" id="collapse1Controle">
            <div class="frame-img">
              <img src="images/surfer.jpg" alt="surfnsun" />
              <h3 class="color-white frame-title">Arugam Point</h3>
            </div>
            <div class="frame-text" class="collapse show" id="collapse1">
              <p>
                This is the main break at Arugam Bay. The point is a long right-hand point/reef break that breaks at the headland in front of Arugam Bay. It is also the best swell magnet of all the points and you can almost always guarantee that the The Point will be a couple of feet bigger than any of the other breaks. It breaks from anything between 2 and 6 feet but tends to max out after 6 foot. Due to its location it also attracts the biggest crowds but seems to be handle it as it often sections in a few spots. On a good day it provides a clean wall that will barrel in the sections and give you a 400-meter ride right through to the inside. When you ride all the way through the best option is to paddle a few yards to the beach and walk back out to the entry point (booties are advisable as the reef starts from ankle depth). There is also a café run by some of the locals that provides a great vantage point to watch the action from and take stock up on energy for the next session.
              </p>
            </div>
            <div class="frame-pull-down text-center">
              <a id="down-1"  href="javascript:void(0)"><span id="down-span-1" class="ion-chevron-down"></span></a>
            </div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="frame" id="collapse2Controle" data-children=".frame-text">
            <div class="frame-img">
              <img src="images/pottuvil.jpg" alt="surfnsun" />
              <h3 class="color-white frame-title">Pottuvil Point</h3>
            </div>
            <div class="frame-text" class="collapse show" id="collapse2">
              <p>
                Pottuvil Point is every surfers dream tropical wave. A long deserted sandy beach doted with some huge boulders at the waters edge, make this wave a favorite with some of the seasoned veterans. Less crowded because of the ½ hour tuk tuk ride north from Arugam Bay, Pottuvil point provides 800 meter rides from the outside section right through to the beach on the inside. The unique thing about this wave is that for most of it you can be working a four foot face and be only a few meters from the beach as the wave grinds down the sandy point. The outside section sucks up and throw's out as the swell raps into the point giving a 30 meter wall to work with before it fades as it hits deeper water for about 10 seconds. The wave then tends to double up as it hits a shallow sand bottom section that will have you hanging in there for all you are worth just to try and make the next 40 to 80 meter section. Failure can leave you standing in knee deep water with a mouth full of sand if you manage not to get slammed into one of the boulder's first.
              </p>
              <p>
                If conditions are right and you can make it through this section then the wave peels perfectly meters from the beach for an eternity until it closes out in the bay and you begin the long walk back. Pottuvil Point needs a decent size swell before it starts working at all and a large swell before the middle section is makeable. If The Point at Arugam is 6 foot Pottuvil will be 4-5. Beware there is little or no shelter at Pottuvil so bring plenty of drinking water.
              </p>
            </div>
            <div class="frame-pull-down text-center">
              <a id="down-2"  href="javascript:void(0)"><span id="down-span-2" class="ion-chevron-down"></span></a>
            </div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="frame"id="collapse3Controle" data-children=".frame-text">
            <div class="frame-img">
              <img src="images/crocodile.jpg" alt="surfnsun" />
              <h3 class="color-white frame-title">Crocodile Rock</h3>
            </div>
            <div class="frame-text" class="collapse show" id="collapse3">
              <p>
                A ½ hour tuk tuk ride to the south will leave you with a twenty minute walk along another beautiful beach to reach Crocodile Rock. I don't know if the place is named after one of the rocks on the point or some of the large salt water crocs that live in the lagoon 50 meters from the break but it is a beautiful spot for a day surf mission. This is the smallest of the three points, and you need a medium to large swell for Croc Rock to work at all. When the point is 6 foot then Croc Rock will be 3 foot. It is however a perfect wave for the beginner/intermediate or long board enthusiast.
              </p>
              <p>
                A sucky take-off section over sand leads on to a long wall that breaks right down the point for about 400 meters much like inside Pottuvil point. Again bring lots of water and make sure that your tuk-tuk driver is there to pick you up after a session and a long walk back in the hot tropical sun.
              </p>
            </div>
            <div class="frame-pull-down text-center">
              <a id="down-3"  href="javascript:void(0)"><span id="down-span-3" class="ion-chevron-down"></span></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="section-gallery">
    <div class="container mb-5">
      <div class="row">
        <div class="col-sm-12 text-center">
          <h1 class="title">A Sneak Peek at What's to Come</h1>
        </div>
      </div>
    </div>

    <ul class="clearfix gallery mb-5">
      <li>
        <div class="thumb">
          <img src="images/gallery/img1.jpg" alt="">
        </div>
      </li>
      <li>
        <div class="thumb">
          <img src="images/gallery/img2.jpg" alt="">
        </div>
      </li>
      <li>
        <div class="thumb">
          <img src="images/gallery/img3.jpg" alt="">
        </div>
      </li>
      <li>
        <div class="thumb">
          <img src="images/gallery/img4.jpg" alt="">
        </div>
      </li>
      <li>
        <div class="thumb">
          <img src="images/gallery/img5.jpg" alt="">
        </div>
      </li>
      <li>
        <div class="thumb">
          <img src="images/gallery/img6.jpg" alt="">
        </div>
      </li>
      <li>
        <div class="thumb">
          <img src="images/gallery/img7.jpg" alt="">
        </div>
      </li>
      <li>
        <div class="thumb">
          <img src="images/gallery/img8.jpg" alt="">
        </div>
      </li>
      <li>
        <div class="thumb">
          <img src="images/gallery/img9.jpg" alt="">
        </div>
      </li>
      <li>
        <div class="thumb">
          <img src="images/gallery/img10.jpg" alt="">
        </div>
      </li>
    </ul>

    <div class="container mb-5">
      <div class="row">
        <div class="col-sm-12 text-center">
          <a href="#" class="btn btn-outline-primary">View More</a>
        </div>
      </div>
    </div>
  </section>

  <section class="section-contact pt-0">
    <div class="container">
      <div class="row mb-5">
        <div class="col-sm-12 text-center">
          <h1 class="title mb-5">Get In Touch</h1>
        </div>
      </div>
      <div class="row mb-5">
        <div class="col-md-6">
          <h3 class="mb-4">Drop Us a Line</h3>
          <form role="form" id="contactForm" data-toggle="validator" class="mb-5">
            <div class="row">
              <div class="form-group col-sm-12">
                <input type="text" class="form-control" id="name" placeholder="Enter name" required>
                <div class="help-block with-errors text-danger pt-2"></div>
              </div>
            </div>
            <div class="row">
              <div class="form-group col-sm-6">
                <input type="email" class="form-control" id="email" placeholder="Enter email" required>
                <div class="help-block with-errors text-danger pt-2"></div>
              </div>
              <div class="form-group col-sm-6">
                <input type="tel" class="form-control" id="telephone" placeholder="Enter Telephone Number">
              </div>
            </div>
            <div class="row">
              <div class="form-group col-sm-12">
                <textarea id="message" class="form-control" rows="5" placeholder="Enter your message" required></textarea>
                <div class="help-block with-errors text-danger pt-2"></div>
              </div>
            </div>
            <button type="submit" id="form-submit" class="btn btn-primary">Send Message</button>
            <a href="contact.php" class="btn btn-info">
              Visit FAQ Area
            </a>
            <div id="msgSubmit" class="h3 label text-center hidden"></div>
            <div class="clearfix"></div>
          </form>

          <p class="alert bg-primary mt-5 mb-5">
            <span class="color-white"><i class="ion-ios-telephone"></i> +94 77 606 5099</span> <br>
            <a class="color-white" href="https://www.facebook.com/SURF-N-SUN-Arugam-Bay-125309190876868/"><span class="ion-social-facebook"> /surfnsun</a> <br>
            <a class="color-white" href="mailto:surfnsaman@yahoo.co.in"><span class="ion-email"></span> surfnsaman@yahoo.co.in</a>
          </p>
        </div>

        <div class="col-md-6">
          <div id="map" style="width: 100%; height: 600px;"></div>
          <script>
            function initMap() {
              var location = {lat: 6.841475, lng: 81.832195};
              var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 17,
                center: location
              });
              var marker = new google.maps.Marker({
                position: location,
                map: map
              });
            }
          </script>
          <script async defer
          src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDTHM6KSj41TAMVI72wcGdViqqQup6L9QA&callback=initMap">
          </script>
        </div>
      </div>
    </div>
  </section>

<?php include 'inc/footer.php'; ?>
