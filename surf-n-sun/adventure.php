<?php include 'inc/header.php'; ?>
  <section class="section-contact">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="float-right">
            <ol class="breadcrumb pagination">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Adventures</li>
            </ol>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <h1 class="one-off-title mb-5">Experience  all the adventures Sri Lanka has to offer</h1>
        </div>
      </div>

      <div class="row mb-5">
        <div class="col-sm-12">
          <div class="post-img">
            <div class="post-img-overlay"></div>
            <img src="images/elephants.jpeg" alt="Arugambay Adventures" width="100%">
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <p class="font-szie-md mb-5">
            Sri Lanka has a rich and exotic variety of wildlife and long tradition of conservation.  We have multiple national parks that are home to varieties of elephants, leopards, sloth bears, wild boar, porcupines, sambhur, deer, monkeys, wild buffalo, ant-eaters, civet cats, jackals, mongeese, loris (unique to Sri Lanka), several varieties of lizards, squirrels, reptiles, amphibians and whole host of bird life.  Each park has its own facilities.
          </p>
          
          <h3 class="mb-3">Surf n’ Sun offers safari tours to the following places by our 4WD Landrover</h3>

          <h5>Yala (Ruhunu) National Park</h5>

          <p>
            This is Sri Lanka’s most famous national park. Situated at the south east of the island, the park is renowned for its wildlife and its fine coast line with coral reefs.  It also boasts a large number of important cultural ruins from ancient civilizations, some dating back more than a thousand years and showing a high level of cultural development.
          </p>

          <p>
            Its northern boundaries border the Lahugala elephant sanctuary and it has the added bonus of a scenic ocean front.  The terrain is varied from flat plains alternating with rocky out crops.  The vegetation ranges from open park land to dense jungle.  Water holes, small lakes, lagoons and streams provide water for the animals and birds where a large number of wild elephants roam.  If you are lucky you may view herds of more than 50 animals.
          </p>

          <h5>Kumana Bird Sanctuary</h5>

          <p>
            Also known as Yala east National park where a multitude of birds breed and roost.  One of the most significant features of the park is the ‘Kumana Villu’ – a 200 hectare natural swamp lake fed the ‘Kombukkan Oya’ through a half mile narrow channel.  It is in this mangrove swamp that many water birds nest in May and June ranging from pelicans, painted storks, spoon bills, white ibis, herons, egrets and little cormorants.  Also the very rare black necked stork has been spotted.
          </p>

          <p>
            Closer by the village we can also arrange early morning, evening and night safaris.  You can see wild elephants, crocodiles, deer, wild boar and many others.
          </p>

          <h5>Ruins & Ancient Civilizations</h5>

          <p>
            You can see remains of ancient civilizations from Sri Lanka kingdom period.  Also as this area was one of the earliest constituents of Buddhism you will find many ancient Buddhist temples dating back thousands of years!
          </p>

          <p>
            One example is ‘Kuddimbigala temple’, which is over 2000 years old and you will still find monks practicing meditation there.  It is located in the heart of the jungle but is still as serene and peaceful as its earliest times.
          </p>

          <p class="text-center">
            <a href="contact.php" class="btn btn-outline-primary text-center"><span class="ion-android-call"></span> Reach out to us</a>
          </p>
        </div>
      </div>
    </div>
  </section>
<?php include 'inc/footer.php'; ?>
