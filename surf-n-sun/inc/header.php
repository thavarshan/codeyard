<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="A small fishing village located on the East coast of Sri Lanka. It's natural beauty is still untouched with pristine shores and no shortage of adventure around the area. Located in close proximity to many of Sri Lanka's largest national wildlife parks - Yala, Lahungala, Kumana Bird Sanctuary and the natural Mangrove conservation of Pottuvil Lagoon, all within one hour’s travel, Arugambay is a must visit destination in the Sri Lankan map. Significant cultural artifacts such as ancient temples and cave dwellings that date back nearly two thousand years testify the truly historical and cultural significance of this area.">
  <meta name="author" content="Surf'n Sun">
  <title>Surf'n Sun | Arugambay Sri Lanka</title>

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400|Playfair+Display" rel="stylesheet">
  <link rel="stylesheet" href="css/ionicons.min.css">
  <link rel="stylesheet" href="css/external-font.css">

  <!-- Core CSS & Resets
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="css/animate.css">
  <link rel="stylesheet" href="css/hover.css">
  <link rel="stylesheet" href="css/menu.css">
  <link rel="stylesheet" href="css/bootstrap-resets.css">
  <link rel="stylesheet" href="css/queries.css">

  <!-- Global CSS & Pages
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/global.css">
  <link rel="stylesheet" href="css/reusable.css">

  <!-- Page Styles -->
  <link rel="stylesheet" href="css/about.css">
  <link rel="stylesheet" href="css/reservations.css">
  <link rel="stylesheet" href="css/adventure.css">
  <link rel="stylesheet" href="css/contact.css">

  <!-- Gallery -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
  <link rel="stylesheet" href="css/fluid-gallery.css">

  <!-- Preloader -->
  <link rel="stylesheet" href="css/preloader.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

  <!-- Bootstrap core JavaScript
  ================================================== -->
  <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
  <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
  <script src="js/preloader.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/validator.min.js"></script>
  <script src="js/app.js"></script>
  <script src="js/menu.js"></script>
  <script>
    baguetteBox.run('.tz-gallery');
  </script>

</head>
<body>

  <!-- Preloader -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>

  <div>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
      <div class="container">
        <a class="navbar-brand" href="#">
          <img src="images/logo-white.png" alt="Surf'n Sun">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav  ml-auto text-center">
            <li class="nav-item">
              <a class="nav-link" href="index.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="about.php">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="reservations.php">Reservations</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="adventure.php">Adventures</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="gallery.php">Gallery</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="contact.php">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </div>
