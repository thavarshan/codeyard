<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="A small fishing village located on the East coast of Sri Lanka. It's natural beauty is still untouched with pristine shores and no shortage of adventure around the area. Located in close proximity to many of Sri Lanka's largest national wildlife parks - Yala, Lahungala, Kumana Bird Sanctuary and the natural Mangrove conservation of Pottuvil Lagoon, all within one hour’s travel, Arugambay is a must visit destination in the Sri Lankan map. Significant cultural artifacts such as ancient temples and cave dwellings that date back nearly two thousand years testify the truly historical and cultural significance of this area.">
  <meta name="author" content="Surf'n Sun">
  <title>Surf'n Sun | Arugambay Sri Lanka</title>

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400|Playfair+Display" rel="stylesheet">
  <link rel="stylesheet" href="css/ionicons.min.css">
  <link rel="stylesheet" href="css/external-font.css">

  <!-- Core CSS & Resets
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/bootstrap-reboot.min.css">
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="css/animate.css">
  <link rel="stylesheet" href="css/hover.css">
  <link rel="stylesheet" href="css/menu.css">
  <link rel="stylesheet" href="css/bootstrap-resets.css">
  <link rel="stylesheet" href="css/queries.css">

  <!-- Global CSS & Pages
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/global.css">
  <link rel="stylesheet" href="css/reusable.css">
  <link rel="stylesheet" href="css/pages.css">

  <!-- Preloader -->
  <link rel="stylesheet" href="css/preloader.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

  <!-- Bootstrap core JavaScript
  ================================================== -->
  <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>
  <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
  <script src="js/preloader.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/validator.min.js"></script>
  <script src="js/app.js"></script>
  <script src="js/menu.js"></script>
</head>
<body>

  <!-- Preloader -->
  <div id="preloader">
    <div id="status">&nbsp;</div>
  </div>

  <!-- Main Menu -->
  <div id="mySidenav" class="sidenav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><span class="ion-android-close"></span></a>
    <a href="#">Home</a>
    <a href="about.php">About</a>
    <a href="reservations.php">Reservations</a>
    <a href="adventure.php">Adventure</a>
    <a href="gallery.php">Gallery</a>
    <a href="contact.php">Contact</a>
  </div>

  <header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-light">
      <div class="container">
        <a class="navbar-brand" href="#">
          <img src="images/logo-white.png" alt="Surf'n Sun">
        </a>

        <!-- Use any element to open the sidenav -->
        <span onclick="openNav()" class="ion-navicon color-white menu-icon"></span>
      </div>
    </nav>

    <div class="container-fluid text-center">
      <div class="row hero">
        <div class="col-sm-12">
          <h1 id="hero-title" class="special-font">Welcome to the one country<br> with many worlds </h1>
          <a href="#" class="btn btn-primary">Visit Surf’n Sun</a>
          <a href="#" class="btn btn-outline-light">Explore Arugambay</a>
        </div>
      </div>
    </div>
  </header>
