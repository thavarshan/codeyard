<footer>
  <div class="container">
    <!-- FOOTER SECTION 1 -->
    <div class="row">
      <div class="col-sm-12">

      </div>
    </div>
    <!-- FOOTER SECTION 2 -->
    <div class="row">
      <div class="col-sm-12">
        <div class="footer-section-2 clearfix">
          <div class="float-md-left">
            <ul class="footer-menu-2 clearfix">
              <li><a href="about.php">About Us</a></li>
              <li><a href="contact.php">Contact Us</a></li>
              <li><a href="services.php">Services</a></li>
              <li><a href="reservations.php">Book a Room</a></li>
              <li><a href="contact.php">FAQ</a></li>
              <li><a href="terms.php">Term &amp; Conditions</a></li>
              <li><a href="privacy.php">Privacy Policy</a></li>
            </ul>
            <span class="copyright">
              &copy; 2017 Surf-n Sun. All rights reserved. No part of this site may be reproduced without our written permission.
            </span>
          </div>
          <a href="#" class="float-md-right footer-logo-container"><img src="images/logo-black.png" alt="Surf'n Sun" class="footer-logo"></a>
        </div>
      </div>
    </div>
  </div>
</footer>

<!-- End Document
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>
