<?php include 'inc/header.php'; ?>
  <section class="section-about">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h1 class="mb-5 float-left">Arugambay</h1>
          <div class="float-right">
            <ol class="breadcrumb pagination">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">About</li>
            </ol>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="mb-5">
            <p>
              Arugambay is a small fishing village located on the East coast of Sri Lanka. Its natural beauty is still untouched and it is located near many Sri Lanka’s largest national wildlife parks. Yala, Lahungala National Park, The Kumana Bird Sanctuary and the natural mangrove conservation of Pottuvil Lagoon are all within one hour’s travel.
            </p>

            <p>
              Arugam Bay is a legendary world class break with waves for everyone from beginners to pros.  The area is predominantly right hand breaks with un-crowded lineups. There are reef breaks, point breaks and beach breaks.  There is one main point and many smaller points around the area. The same swells that turn Indonesia on deliver the goods to Sri Lanka.  The most consistent SW swell season is during April - October.  Expect waves from 2-6ft with regular offsore winds and clean conditions.  Water temperatures are 27c / 82f all year long so bring board shorts.
            </p>

            <p>
              There are also significant culture artifacts such as ancient temples and cave dwellings that date back nearly two thousand years that testify the truly historical and cultural significance of this area.
            </p>

            <p>
              The nearest town is Pottuvil which is linked by a bridge to Arugambay. There you will find many city amenities such as local markets, small shops, banks and hospitals.
            </p>
          </div>
        </div>

        <div class="col-md-6">
          <img src="images/bg3.jpg" alt="Arugambay" width="100%">
        </div>
      </div>
    </div>
  </section>
<?php include 'inc/footer.php'; ?>
