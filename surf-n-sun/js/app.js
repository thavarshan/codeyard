$(document).ready(function () {
  // Collapse 1
  $('#collapse1Controle').click(function (e) {
    e.stopPropagation();
    var target = $(this).parent().find('#collapse1');
    $('#collapse1').not(target).slideUp("fast");
    target.slideToggle("fast");
  });
  $("#collapse1").on("click", function (e) {
    e.stopPropagation();
  });

  // Collapse 2
  $('#collapse2Controle').click(function (e) {
    e.stopPropagation();
    var target = $(this).parent().find('#collapse2');
    $('#collapse2').not(target).slideUp("fast");
    target.slideToggle("fast");
  });
  $("#collapse2").on("click", function (e) {
    e.stopPropagation();
  });

  // Collapse 3
  $('#collapse3Controle').click(function (e) {
    e.stopPropagation();
    var target = $(this).parent().find('#collapse3');
    $('#collapse3').not(target).slideUp("fast");
    target.slideToggle("fast");
  });
  $("#collapse3").on("click", function (e) {
    e.stopPropagation();
  });
});

// Collapse 1
$(document).on("click", function () {
  $("#collapse1").slideUp("fast");
});

// Collapse 2
$(document).on("click", function () {
  $("#collapse2").slideUp("fast");
});

// Collapse 3
$(document).on("click", function () {
  $("#collapse3").slideUp("fast");
});


$(document).ready(function(){
  /* Get iframe src attribute value i.e. YouTube video url
  and store it in a variable */
  var url = $("#Video").attr('src');

  /* Assign empty url value to the iframe src attribute when
  modal hide, which stop the video playing */
  $("#myModal").on('hide.bs.modal', function(){
    $("#Video").attr('src', '');
  });

  /* Assign the initially stored url back to the iframe src
  attribute when modal is displayed again */
  $("#myModal").on('show.bs.modal', function(){
    $("#Video").attr('src', url);
  });
});

$(document).ready(function(){
  $("#collapse1Controle").click(function(){
    $("#down-span-1").toggleClass("up");
  });

  $("#collapse2Controle").click(function(){
    $("#down-span-2").toggleClass("up");
  });

  $("#collapse3Controle").click(function(){
    $("#down-span-3").toggleClass("up");
  });
});


// Contact Section Functionality
$("#contactForm").validator().on("submit", function (event) {
    if (event.isDefaultPrevented()) {
        // handle the invalid form...
        formError();
        submitMSG(false, "Ooops! Something’s not right, Let’s try again");
    } else {
        // everything looks good!
        event.preventDefault();
        submitForm();
    }
});


function submitForm(){
    // Initiate Variables With Form Content
    var name = $("#name").val();
    var email = $("#email").val();
    var message = $("#message").val();

    $.ajax({
        type: "POST",
        url: "php/form-process.php",
        data: "name=" + name + "&email=" + email + "&message=" + message,
        success : function(text){
            if (text == "success"){
                formSuccess();
            } else {
                formError();
                submitMSG(false,text);
            }
        }
    });
}

function formSuccess(){
  $("#contactForm")[0].reset();
  submitMSG(true, "Got it! You’ll hear back from us shortly")
}

function formError(){
  $("#contactForm").removeClass().addClass('animated shake').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
    $(this).removeClass();
  });
}

function submitMSG(valid, msg){
  if(valid){
    var msgClasses = "h3 text-center tada animated";
  } else {
    var msgClasses = "h3 text-center text-danger";
  }
  $("#msgSubmit").removeClass().addClass(msgClasses).text(msg);
}
