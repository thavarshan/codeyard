<?php include 'inc/header.php'; ?>
  <section class="section-contact">
    <div class="container">
      <div class="row mb-5">
        <div class="col-sm-12">
          <h1>Get In Touch</h1>
        </div>
      </div>

      <div class="row mb-5">
        <div class="col-md-6">
          <form role="form" id="contactForm" data-toggle="validator" class="mb-5">
            <div class="form-group">
              <input type="text" class="form-control" id="name" placeholder="Enter name" required>
              <div class="help-block with-errors text-danger pt-2"></div>
            </div>
            <div class="form-group">
              <input type="email" class="form-control" id="email" placeholder="Enter email" required>
              <div class="help-block with-errors text-danger pt-2"></div>
            </div>
            <div class="form-group">
              <input type="tel" class="form-control" id="telephone" placeholder="Enter Telephone Number E.G: 0776065099">
            </div>
            <div class="form-group">
              <textarea id="message" class="form-control" rows="5" placeholder="Enter your message" required></textarea>
              <div class="help-block with-errors text-danger pt-2"></div>
            </div>
            <button type="submit" id="form-submit" class="btn btn-primary pull-right " data-toggle="button" aria-pressed="false" autocomplete="off">Send Message</button>
            <div id="msgSubmit" class="h3 label text-center hidden"></div>
            <div class="clearfix"></div>
          </form>

          <p class="mb-5">
            <span class="text-primary"><i class="ion-ios-telephone"></i> +94 77 606 5099</span> <br>
            <a href="https://www.facebook.com/SURF-N-SUN-Arugam-Bay-125309190876868/"><span class="ion-social-facebook"> /surfnsun</a> <br>
            <a href="mailto:surfnsaman@yahoo.co.in"><span class="ion-email"></span> surfnsaman@yahoo.co.in</a>
          </p>
        </div>

        <div class="col-md-6">
          <h3>Frequently Asked Questions</h3>
          <hr class="mb-4">
          <div id="exampleAccordion" data-children=".item">
            <div class="item mb-4">
              <a data-toggle="collapse" data-parent="#exampleAccordion" href="#exampleAccordion1" aria-expanded="true" aria-controls="exampleAccordion1">
                <h4 class="text-primary"><i class="ion-ios-plus-outline"></i> How to reach Surf n’ Sun from the airport?</h4>
              </a>
              <div id="exampleAccordion1" class="collapse" role="tabpanel">
                <div class="pl-4 mb-3">
                  <h5>Flying</h5>

                  <p>
                    Fly into Sri-Lanka's only international airport - Bandaranaike International Airport - also called Katunayake International Airport.  You can arrange a car service from the airport or call us and we will pick you up!
                  </p>

                  <h5>Airport Pick Up</h5>

                  <p>
                    We can pick you up at the airport and pack all your gear and drive you here.  The trip takes about 8 hours so we will stop along the way for refreshments and to stretch our legs.  There are many scenic roads along the way.  We can also arrange to pick you up from a hotel in Colombo.
                  </p>

                  <p>
                    Contact us and let us know and we will arrange your travel.
                  </p>

                  <h5>Bus Routes from</h5>

                  <p>
                    Colombo: The CTB Pottuvil / Panama bus leaves at 5am from the Pettah bus stand every day and arrives at Pottuvil at 4pm.  You can take a three wheeler (tuk tuk) from Pottuvil to Arugam.
                  </p>

                  <p>
                    Hikkaduwa and Matara: Take the Moneragla bus which departs from the Hikkaduwa and Matara bus stands respectively.  Bus departs at 5am and arrives at Moneragla at 1130am.
                  </p>

                  <p>
                    From Moneragla you should take the CTB Pottuvil / Panama bus to Pottuvil.  The bus departs at noon.
                  </p>
                </div>
              </div>
            </div>
            <div class="item mb-4">
              <a data-toggle="collapse" data-parent="#exampleAccordion" href="#exampleAccordion2" aria-expanded="false" aria-controls="exampleAccordion2">
                <h4 class="text-primary"><i class="ion-ios-plus-outline"></i> How’s the weather around Arugambay?</h4>
              </a>
              <div id="exampleAccordion2" class="collapse" role="tabpanel">
                <p class="mb-3 pl-4">
                  The area has the advantage of avoiding the South West and North East monsoons that affect Sri Lanka and has pleasant tropical weather year round.  The afternoon can get very hot around April but a cool drink and shade is never hard to find here.  It is a major surfing attraction but the area has lovely weather year round and the town has a very relaxed, mellow atmosphere which encourages visitors to stay for months on end at times.
                </p>
              </div>
            </div>
            <div class="item mb-4">
              <a data-toggle="collapse" data-parent="#exampleAccordion" href="#exampleAccordion3" aria-expanded="false" aria-controls="exampleAccordion3">
                <h4 class="text-primary"><i class="ion-ios-plus-outline"></i> Where can I go for shopping and find other essentials?</h4>
              </a>
              <div id="exampleAccordion3" class="collapse" role="tabpanel">
                <p class="mb-3 pl-4">
                  The nearest town is Pottuvil which is linked by a bridge to Arugambay. There you will find many city amenities such as local markets, small shops, banks and a hospital.
                </p>
              </div>
            </div>
            <div class="item mb-4">
              <a data-toggle="collapse" data-parent="#exampleAccordion" href="#exampleAccordion4" aria-expanded="false" aria-controls="exampleAccordion4">
                <h4 class="text-primary"><i class="ion-ios-plus-outline"></i> General info about Sri Lanka</h4>
              </a>
              <div id="exampleAccordion4" class="collapse" role="tabpanel">
                <p class="mb-3 pl-4">
                  Full Name     Democratic Socialist Republic of Sri Lanka<br />
                  Capital City     Colombo (official); Sri Jayewardenepura Kotte (legislative)<br />
                  Area     66,000 sq km or 25,483 sq miles<br />
                  Population     20,000,000<br />
                  Time Zone     GMT/UTC  5.5 ()<br />
                  Daylight Saving     not in use<br />
                  Languages     Sinhalese (official), Tamil (official), English (other)<br />
                  Religion     69Buddhist, 8Muslim, 7Hindu 6Christian<br />
                  Currency     Sri Lankan Rupee (Rs).  The last time we checked the exchange was 120 Rs. to the dollar but usually averages around 100 Rs. to the dollar.<br />
                  Electricity     230V 50HzHz<br />
                  Electric Plug Details     South African/Indian-style plug with two circular metal pins above a large circular grounding pin<br />
                  Country Dialing Code     94
                </p>
              </div>
            </div>
            <div class="item mb-4">
              <a data-toggle="collapse" data-parent="#exampleAccordion" href="#exampleAccordion5" aria-expanded="false" aria-controls="exampleAccordion5">
                <h4 class="text-primary"><i class="ion-ios-plus-outline"></i> General info about Arugambay Area</h4>
              </a>
              <div id="exampleAccordion5" class="collapse" role="tabpanel">
                <div class="mb-3 pl-4">
                  <h5>Arugambay</h5>
                  <p>
                    Arugam Bay is located on the South East side of Sri Lanka and is only accessible via a monitored bridge.  It is quite removed from the civil strife occuring in the North and is safe.   It is also strategically unimportant which bodes well for visitors and residents alike.
                  </p>
                  <h5>2004 Tsunami Effects</h5>
                  <p>
                    Many of the buildings were destroyed in the 2004 tsunami. Due to its popularity among tourists the area has managed a slow recovery by private initiatives only.  No help has been received from any official source or International donations. An exception is uncoordinated support for fishing folk as well as many school rebuilding programs, sadly resulting in a contiuation to provide only separate schools for each Community.
                  </p>
                  <p>
                    However the residents of Arugam Bay and its denizens have gathered together since then to rebuild the town and we are proud to say that Arugam is now back on its feet and as beautiful as ever.  However the signs of the tsunami still linger and help is always appreciated.  If you want to get involved lets us know and we can provide you with some options.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row mt-5">
        <div class="col-sm-12">
          <div id="map" style="width: 100%; height: 600px;"></div>
          <script>
            function initMap() {
              var location = {lat: 6.841475, lng: 81.832195};
              var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 17,
                center: location
              });
              var marker = new google.maps.Marker({
                position: location,
                map: map
              });
            }
          </script>
          <script async defer
          src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDTHM6KSj41TAMVI72wcGdViqqQup6L9QA&callback=initMap">
          </script>
        </div>
      </div>
    </div>
  </section>
<?php include 'inc/footer.php'; ?>
