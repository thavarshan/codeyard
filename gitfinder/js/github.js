class GitHub {
    constructor() {
        this.clientId = 'c12b84b57b7c8511c63a';
        this.clientSecret = '5e837583cfb727574284d6639d91ac6675e0286d';
        this.reposCount = 5;
        this.reposSort = 'created: asc';
    }

    async getUser(user) {
        const profileResponse = await fetch(`https://api.github.com/users/${user}?client_id=${this.clientId}&client_secret=${this.clientSecret}`);

        const reposResponse = await fetch(`https://api.github.com/users/${user}/repos?per_page=${this.reposCount}&sort=${this.reposSort}&client_id=${this.clientId}&client_secret=${this.clientSecret}`);

        const profileData = await profileResponse.json();
        const reposData = await reposResponse.json();

        return {
            profile: profileData,
            repos: reposData
        }
    }
}
