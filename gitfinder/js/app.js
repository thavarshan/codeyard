// Initialize GitHub
const github = new GitHub;

// Initialize UI
const ui = new UI;

// Search input
const search = document.getElementById('search');

// Search input event listener
search.addEventListener('keyup', (e) => {
    // Get input text
    const text = e.target.value;

    if (text !== '') {
        // Make http call
        github.getUser(text)
            .then(data => {
                if (data.profile.message === 'Not Found') {
                    // Show alert
                    ui.showAlert();
                } else {
                    // Show profile
                    ui.showProfile(data.profile);
                    ui.showRepos(data.repos);
                }
            });
    } else {
        // Clear profile
        ui.clearProfile();
    }
});
