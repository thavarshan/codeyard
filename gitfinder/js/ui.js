class UI {
    constructor() {
        this.profile = document.getElementById('profile');
        this.repos = document.getElementById('repos');
    }

    // Display user profile
    showProfile(user) {
        this.profile.innerHTML = `
        <div class="card-body">
            <div class="row">
                <div class="col">
                    <div class="flex items-center mb-3">
                        <div class="overflow-hidden w-10 h-10 rounded-full mr-3">
                            <img class="w-full" src="${user.avatar_url}" alt="${user.login}">
                        </div>

                        <div>
                            <p class="text-gray-900 leading-none font-semibold font-mono">
                                ${user.name}
                            </p>

                            <p class="text-gray-500 text-xs">${user.login}</p>
                        </div>
                    </div>

                    <p class="text-gray-600 max-w-xs mb-3 text-xs">
                        ${user.bio}
                    </p>

                    <p class="mb-2 text-blue-500 text-xs">
                        ${user.location}
                    </p>
                </div>

                <div class="col">
                    <div class="row">
                        <div class="col-md-6 mb-3 text-center">
                            <div class="mb-1">
                                <h1 class="text-3xl">${user.public_repos}</h1>

                                <span class="text-gray-600 text-xs font-mono">Repositories</span>
                            </div>
                        </div>

                        <div class="col-md-6 mb-3 text-center">
                            <div class="mb-1">
                                <h1 class="text-3xl">${user.public_gists}</h1>

                                <span class="text-gray-600 text-xs font-mono">Gists</span>
                            </div>
                        </div>

                        <div class="col-md-6 mb-3 text-center">
                            <div class="mb-1">
                                <h1 class="text-3xl">${user.followers}</h1>

                                <span class="text-gray-600 text-xs font-mono">Follwers</span>
                            </div>
                        </div>

                        <div class="col-md-6 mb-3 text-center">
                            <div class="mb-1">
                                <h1 class="text-3xl">${user.following}</h1>

                                <span class="text-gray-600 text-xs font-mono">Follwing</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        `;
    }

    showRepos(repos) {
        let output = '';

        repos.forEach((repo) => {
            output += `
                <div class="card-body">
                    <a href="${repo.url}">
                        <div class="d-flex w-100 justify-content-between mb-2">
                            <h5 class="mb-1 font-medium text-base">${repo.name}</h5>

                            <span class="text-sm text-gray-600">${moment(repo.pushed_at).format('lll')}</span>
                        </div>

                        <p class="mb-1 text-xs text-gray-500 max-w-sm">
                            ${repo.description}
                        </p>

                        <small>
                            <span class="badge badge-primary">${repo.language}</span>
                        </small>
                    </a>
                </div>
            `;
        });

        this.repos.innerHTML = output;
    }

    // Clear search results
    clearProfile() {
        this.profile.innerHTML = '';
    }

    // Show not found alert
    showAlert() {
        $('#alertModal').modal('show');
    }
}
