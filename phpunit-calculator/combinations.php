<?php

$words = array("Quick", "Brown", "Fox");

// For an array of n words, return an array of n! strings, each containing the words in a different order.
function wordcombos ($words) {
    if ( count($words) <= 1 ) {
        $result = $words;
    } else {
        $result = array();
        for ( $i = 0; $i < count($words); ++$i ) {
            $firstword = $words[$i];
            $remainingwords = array();
            for ( $j = 0; $j < count($words); ++$j ) {
                if ( $i <> $j ) $remainingwords[] = $words[$j];
            }
            $combos = wordcombos($remainingwords);
            for ( $j = 0; $j < count($combos); ++$j ) {
                $result[] = $firstword . ' ' . $combos[$j];
            }
        }
    }
    return $result;
}

print_r (wordcombos($words));