<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>SandBox</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="public/css/bootstrap.css">
		<link rel="stylesheet" href="public/css/bootstrap-grid.css">
		<link rel="stylesheet" href="public/css/bootstrap-reboot.css">
	</head>
	<body>
		<main class="py-4">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-6">
						<div class="card">
							<div class="card-body">
								<h5 class="text-center mb-4">PHP Unit Testing Application</h5>
								<div class="text-uppercase mb-3">
									<small>OUTPUT</small>
								</div>
								<div class="bg-light border rounded px-3 py-4">

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>

		<script src="public/js/bootstrap.bundle.js"></script>
		<script src="public/js/bootstrap.js"></script>
	</body>
</html>
