<?php

namespace Tests\Unit\Calculator;

use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{

    /** @test */
    public function can_set_single_operation()
    {
        $addition = new \App\Calculator\Addition;
        $addition->setOperands([5, 10]);

        $caluculator = new \App\Calculator\Calculator;
        $caluculator->setOperation($addition);

        $this->assertCount(1, $caluculator->getOperations());
    }

    /** @test */
    public function can_set_multiple_operation()
    {
        $addition1 = new \App\Calculator\Addition;
        $addition1->setOperands([5, 10]);

        $addition2 = new \App\Calculator\Addition;
        $addition2->setOperands([10, 10]);

        $caluculator = new \App\Calculator\Calculator;
        $caluculator->setOperations([$addition1, $addition2]);

        $this->assertCount(2, $caluculator->getOperations());
    }

    /** @test */
    public function operations_are_ignored_if_not_instance_of_operation_interface()
    {
        $addition = new \App\Calculator\Addition;
        $addition->setOperands([5, 10]);

        $caluculator = new \App\Calculator\Calculator;
        $caluculator->setOperations([$addition, 'cats']);

        $this->assertCount(1, $caluculator->getOperations());
    }

    /** @test */
    public function can_calculate_result()
    {
        $addition = new \App\Calculator\Addition;
        $addition->setOperands([5, 10]);

        $caluculator = new \App\Calculator\Calculator;
        $caluculator->setOperations([$addition]);

        $this->assertEquals(15, $caluculator->calculate());
    }

    /** @test */
    public function calculate_method_returns_multiple_results()
    {
        $addition = new \App\Calculator\Addition;
        $addition->setOperands([5, 10]);

        $division = new \App\Calculator\Division;
        $division->setOperands([100, 2]);

        $caluculator = new \App\Calculator\Calculator;
        $caluculator->setOperations([$addition, $division]);

        $this->assertInternalType('array', $caluculator->calculate());
        $this->assertEquals(15, $caluculator->calculate()[0]);
        $this->assertEquals(50, $caluculator->calculate()[1]);
    }

}
