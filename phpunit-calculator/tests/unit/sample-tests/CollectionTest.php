<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Support\Collection;

class CollectionTest extends TestCase
{
	/** @test */
	public function empty_instantiated_collection_returns_no_items()
	{
		$collection = new Collection([]);

		$this->assertEmpty($collection->get());
	}

	/** @test */
	public function count_is_correct_for_items_passed_in()
	{
		$collection = new Collection([
			'name' => 'Thavarshan',
			'email' => 'tjthavarshan@gmail.com'
		]);

		$this->assertEquals(2, $collection->count());
	}

	/** @test */
	public function items_returned_match_items_passed_in()
	{
		$collection = new Collection([
			'name' => 'Thavarshan',
			'email' => 'tjthavarshan@gmail.com'
		]);

		$this->assertCount(2, $collection->get());
		$this->assertEquals($collection->get()['name'], 'Thavarshan');
		$this->assertEquals($collection->get()['email'], 'tjthavarshan@gmail.com');
	}

	/** @test */
	public function collection_is_instance_of_iterator_aggregate()
	{
		$collection = new Collection();
		$item = 'hi';

		$this->assertInstanceOf('IteratorAggregate', new Collection());
	}

	/** @test */
	public function collection_can_be_iterated()
	{
		$collection = new Collection([
			'a', 'b', 'c'
		]);

		$items = [];

		foreach ($collection as $item) {
			$items[] = $item;
		}

		$this->assertCount(3, $items);
		$this->assertInstanceOf('ArrayIterator', $collection->getIterator());
	}

	/** @test */
	public function collection_can_be_merged_with_another_collection()
	{
		$collection1 = new Collection([
			'a', 'b', 'c'
		]);
		$collection2 = new Collection([
			'1', '2', '3'
		]);

		$collection1->merge($collection2);

		$this->assertCount(6, $collection1->get());
		$this->assertEquals(6, $collection1->count());
	}

	/** @test */
	public function can_add_to_existing_collection()
	{
		$collection = new Collection(['a', 'b', 'c']);

		$collection->add(['1', '2', '3']);

		$this->assertCount(6, $collection->get());
	}

	/** @test */
	public function returns_json_encode_items()
	{
		$collection = new Collection([
			['username' => 'thavarshan'],
			['username' => 'gumball'],
		]);

		$this->assertInternalType('string', $collection->toJson());
		$this->assertEquals('[{"username":"thavarshan"},{"username":"gumball"}]', $collection->toJson());
	}

	/** @test */
	public function json_encoding_a_collection_object()
	{
		$collection = new Collection([
			['username' => 'thavarshan'],
			['username' => 'gumball'],
		]);

		$encoded = json_encode($collection);

		$this->assertInternalType('string', $encoded);
		$this->assertEquals('[{"username":"thavarshan"},{"username":"gumball"}]', $encoded);
	}
}
