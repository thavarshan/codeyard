<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Models\User;

class UserTest extends TestCase
{

	protected $user;

	public function setUp()
	{
		$this->user = new User;
	}

	/** @test */
	public function that_we_can_get_the_first_name()
	{
		$this->user->setFirstName('Thavarshan');

		$this->assertEquals($this->user->getFirstName(), 'Thavarshan');
	}

	/** @test */
	public function that_we_can_get_the_last_name()
	{
		$this->user->setLastName('Thayananthajothy');

		$this->assertEquals($this->user->getLastName(), 'Thayananthajothy');
	}

	/** @test */
	public function that_we_can_get_the_full_name()
	{
		$this->user->setFirstName('Thavarshan');
		$this->user->setLastName('Thayananthajothy');

		$this->assertEquals($this->user->getFullName(), 'Thavarshan Thayananthajothy');
	}

	/** @test */
	public function first_and_last_names_are_trimmed()
	{
		$this->user->setFirstName('Thavarshan      ');
		$this->user->setLastName('       Thayananthajothy');

		$this->assertEquals($this->user->getFirstName(), 'Thavarshan');
		$this->assertEquals($this->user->getLastName(), 'Thayananthajothy');
	}

	/** @test */
	public function email_can_be_set()
	{
		$this->user->setEmail('tjthavarshan@gmail.com');

		$this->assertEquals($this->user->getEmail(), 'tjthavarshan@gmail.com');
	}

	/** @test */
	public function email_variables_contain_correct_values()
	{
		$this->user->setFirstName('Thavarshan');
		$this->user->setLastName('Thayananthajothy');
		$this->user->setEmail('tjthavarshan@gmail.com');

		$emailVariables = $this->user->getEmailVariables();

		$this->assertArrayHasKey('full_name', $emailVariables);
		$this->assertArrayHasKey('email', $emailVariables);
	}

}
