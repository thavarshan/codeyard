@extends('layouts.base')

@section('content')

<div class="card">
  <div class="card-body">
    <h5 class="mb-4">Create New Discussion</h5>
    <form action="{{ route('discussions.store') }}" method="post" enctype="multipart/form-data">
      @csrf
      <fieldset class="form-group">
        <label>Discussion Title</label>
        <input type="text" class="form-control" name="title" placeholder="Discussion Title" value="{{ old('title') }}">
      </fieldset>
      <fieldset class="form-group">
        <label>Ask Question</label>
        <textarea class="form-control" rows="3" name="content" placeholder="Discussion Content">{{ old('content') }}</textarea>
      </fieldset>
      <fieldset class="form-group">
        <label>Pick a channel</label>
        <select class="form-control" name="channel_id">
          @foreach($channels as $channel)
          <option value="{{ $channel->id }}">{{ $channel->title }}</option>
          @endforeach
        </select>
      </fieldset>
      <fieldset class="form-group">
        <button type="submit" class="btn btn-primary">Create Discussion</button>
      </fieldset>
    </form>
  </div>
</div>


@endsection
