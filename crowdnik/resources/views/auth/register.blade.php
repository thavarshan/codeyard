@extends('layouts.auth')

@section('content')
<section class="section-padded">
  <div class="container">
      <div class="row justify-content-center">
          <div class="col-md-5">
            <div class="card border-0">
              <div class="card-body">
                <h3 class="mb-4 text-center">Join {{ config('app.name') }} today.</h3>
                <form method="POST" action="{{ route('register') }}">
                  @csrf
                  <div class="form-group">
                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="{{ __('Name') }}" required autofocus>
                    @if ($errors->has('name'))
                      <span class="invalid-feedback">
                        <strong>{{ $errors->first('name') }}</strong>
                      </span>
                    @endif
                  </div>
                  <div class="form-group">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="{{ __('Email') }}" required>
                    @if ($errors->has('email'))
                      <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                      </span>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('gender') ? ' is-invalid' : '' }}">
                    <select name="gender" class="form-control">
                      <option value="1">Male</option>
                      <option value="0">Female</option>
                    </select>
                    @if ($errors->has('gender'))
                      <span class="invalid-feedback">
                        <strong>{{ $errors->first('gender') }}</strong>
                      </span>
                    @endif
                  </div>
                  <div class="form-group row">
                    <div class="col-md-6">
                      <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="{{ __('Password') }}" required>
                      @if ($errors->has('password'))
                        <span class="invalid-feedback">
                          <strong>{{ $errors->first('password') }}</strong>
                        </span>
                      @endif
                    </div>
                    <div class="col-md-6">
                      <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="{{ __('Confirm Password') }}" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">
                      Sign up
                    </button>
                  </div>
                  <div class="form-group">
                    <small class="text-muted">
                      By signing up, you agree to the Terms of Service and Privacy Policy, including Cookie Use. Others will be able to find you by email or phone number when provided.
                    </small>
                  </div>
                </form>
                <div class="mt-3">
                  Have an account? <a href="{{ route('login') }}">Login</a>
                </div>
              </div>
            </div>
          </div>
      </div>
  </div>
</section>
@endsection
