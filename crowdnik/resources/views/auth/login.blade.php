@extends('layouts.auth')

@section('content')
<section class="section-padded">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-5">
        <div class="card">
            <div class="card-body">
              <h3 class="mb-4 text-center">Login to {{ config('app.name') }}</h3>
              <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                  <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
                  @if ($errors->has('email'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group">
                  <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
                  @if ($errors->has('password'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                  @endif
                </div>
                <div class="form-group">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                    </label>
                  </div>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-primary">
                    {{ __('Login') }}
                  </button>
                  <a class="btn btn-link" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                  </a>
                </div>
              </form>
            </div>
            <div class="card-footer border-top-0 py-4">
              New to bombble? <a href="{{ route('register') }}">Sign up now »</a>
            </div>
          </div>
      </div>
    </div>
  </div>
</section>
@endsection
