@extends('layouts.front')

@section('content')
<section class="section-front hw-full greeting bg-primary">
  <div class="container">
    <div class="row">
      <div class="col-md-8 offset-md-2 text-center text-white pt-5 mt-cover">
        <h1 class="display-3 mb-3">Welcome to crowdnik</h1>
        <p class="lead">Cover is a one-page template for building simple and beautiful home pages. Download, edit the text, and add your own fullscreen background photo to make it your own.</p>
        <p class="lead mt-3">
          <a href="{{ route('register') }}" class="btn btn-light">Get Started</a>
          <a href="#" class="btn btn-outline-light">Learn more</a>
        </p>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12 text-center py-5 mt-5 text-white">
        <small>copyright &copy; {{ date("Y") }} <strong>crowdnik</strong></small>
      </div>
    </div>
  </div>
</section>
@endsection
