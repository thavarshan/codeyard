@extends('layouts.base')

@section('content')
<h5>New Channel</h5>
@if(!empty($edditable))
<div class="card">
  <div class="card-body">
    <form action="{{ route('channels.update', ['id' => $edditable->id]) }}" method='post'>
      @csrf
      {{ method_field('PUT') }}
      <fieldset class="form-group">
        <label>Channel Title</label>
        <input type="text" class="form-control" name="title" value="{{ $edditable->title }}">
      </fieldset>
      <fieldset class="form-group">
        <button type="submit" class="btn btn-primary">Update Channel</button>
        <a href="{{ route('channels.create') }}" class="btn btn-outline-primary">New Channel</a>
      </fieldset>
    </form>
  </div>
</div>
@else
<div class="card">
  <div class="card-body">
    <form action="{{ route('channels.store') }}" method='post'>
      @csrf
      <fieldset class="form-group">
        <label>Channel Title</label>
        <input type="text" class="form-control" name="title" placeholder="Channel Title">
      </fieldset>
      <fieldset class="form-group">
        <button type="submit" class="btn btn-primary">Create Channel</button>
      </fieldset>
    </form>
  </div>
</div>
@endif

<h5>All Channels</h5>
<div class="list-group">
  @if(count($channels) > 0)
  @foreach($channels as $channel)
  <li class="list-group-item d-flex justify-content-between align-items-center mb-2 border-0">
    <a href="{{ route('channels.edit', ['id' => $channel->id]) }}"><i class="ion-edit"></i> {{ $channel->title }}</a>
    <span>
      <form action="{{ route('channels.destroy', ['id' => $channel->id]) }}" method="post">
        @csrf
        {{ method_field('DELETE') }}
        <button type="submit" class="btn btn-link text-danger"><i class="ion-trash-a"></i> Delete</button>
      </form>
    </span>
  </li>
  @endforeach
  @else
  <li class="list-group-item">
    No channels available
  </li>
  @endif
</div>


@endsection
