@extends('layouts.base')

@section('content')

<div class="list-group list-group-flush">
  @if(count($discussions) > 0)
  @foreach($discussions as $discussion)
  <div class="list-group-item flex-column align-items-start border-0 mb-4">
    <div class="user mb-3 clearfix w-100">
      <div class="float-left mr-2">
        <img src="{{ asset($discussion->user->avatar) }}" alt="..." class="rounded-circle" height="30px">
      </div>
      <div class="float-left pt-2">
        <h6 class="mb-0">{{ $discussion->user->name }}
        <small><span class="ion-ribbon-b text-warning"></span> {{ $discussion->user->points }}</small>
        </h6>
      </div>
      @if(Auth::user()->id == $discussion->user->id)
      <div class="dropdown show float-right icon-md">
        <a class="dropdown-toggle text-muted" href="#" role="button" id="post_action" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        </a>

        <div class="dropdown-menu" aria-labelledby="post_action">
          <a class="dropdown-item" href="{{ route('discussions.edit', ['slug' => $discussion->slug ]) }}">Edit</a>
          <a class="dropdown-item" href="{{ route('discussions.delete', ['slug' => $discussion->slug ]) }}">Delete</a>
        </div>
      </div>
      @endif
      <span class="float-right mr-2"><small>{{ $discussion->created_at->diffForHumans() }}</small></span>
    </div>
    <div class="forum-title">
      <a href="{{ route('discuss', ['slug' => $discussion->slug]) }}" class="text-dark"><h5 class="mb-2">{{ $discussion->title }}</h5></a>
    </div>
    <p class="text-muted mb-0">
      {!! Markdown::convertToHtml(str_limit($discussion->content, 100)) !!}
    </p>
    <a href="{{ route('channel', ['slug' => $discussion->channel->slug]) }}" class="badge badge-primary">{{ $discussion->channel->title }}</a>
    <div class="action py-2 mt-2">
      @if($discussion->isliked())
      <a title="unlike" class="text-danger" href="{{ route('unlike', ['id' => $discussion->id]) }}"><span class="like ion-ios-heart mr-2"> {{ count($discussion->likes) }}</span></a>
      @else
      <a title="like" href="{{ route('like', ['id' => $discussion->id]) }}"><span class="like ion-ios-heart-outline text-danger mr-2"> {{ count($discussion->likes) }}</span></a>
      @endif
      <a title="replies" class="text-success" href="{{ route('discuss', ['slug' => $discussion->slug]) }}"><span class="ion-reply mr-2"> {{ count( $discussion->replies) }}</span></a>
      @if($discussion->isWatching())
      <a href="{{ route('unwatch', ['id' => $discussion->id]) }}" title="unwatch" class="text-primary">
        <span class="ion-eye-disabled"> {{ count( $discussion->watching) }}</span>
      </a>
      @else
      <a href="{{ route('watch', ['id' => $discussion->id]) }}" title="watch" class="text-primary">
        <span class="ion-eye"> {{ count( $discussion->watching) }}</span>
      </a>
      @endif
      @if($discussion->hasBest())
      <small><span class="badge badge-danger float-right">Closed</span></small>
      @else
      <small><span class="badge badge-success float-right">Open</span></small>
      @endif
    </div>
  </div>
  @endforeach
  @else
  <p>
    There are no discussions available
  </p>
  @endif
</div>

<div class="row my-4">
  <div class="col-sm-12 text-center">
    <ul class="pagination justify-content-center">
      {!! $discussions->links() !!}
    </ul>
  </div>
</div>
@endsection
