<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,400,700" rel="stylesheet">
  <link rel="stylesheet" href="{{asset('css/ionicons.min.css')}}">

  <!-- Core CSS & Resets
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="{{asset('css/app.css')}}">
  <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/bootstrap-grid.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/bootstrap-reboot.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/animate.css')}}">
  <link rel="stylesheet" href="{{asset('css/bootstrap-resets.css')}}">

  <!-- Global CSS & Pages
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="{{asset('css/global.css')}}">
  <link rel="stylesheet" href="{{asset('css/pages.css')}}">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

  <!-- Bootstrap core JavaScript
  ================================================== -->
  <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
  <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
  <script src="{{ asset('js/app.js') }}"></script>
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>

</head>
<body>
  <header>
    <nav class="navbar navbar-expand-lg navbar-light bg-white">
      <div class="container">
        <a class="navbar-brand" href="{{ route('home') }}"><i class="ion-person-stalker"></i> {{ config('app.name', 'Laravel') }}</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link" href="{{ route('home') }}"><i class="ion-home"></i> Home</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link" href="{{ route('discussions') }}"><i class="ion-chatbubbles"></i> Discussions</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#"><i class="ion-help-buoy"></i> FAQ</a>
            </li>
          </ul>
          <ul class="navbar-nav ml-auto">
            @if(Auth::user()->admin == 1)
            <li class="nav-item">
              <a class="nav-link" href="{{ route('channels.create') }}"><i class="ion-bookmark"></i> New Channel</a>
            </li>
            @endif
            <li class="nav-item">
              <a class="nav-link" href="{{ route('discussions.create') }}"><i class="ion-plus-circled"></i> New Discussion</a>
            </li>
            <!-- Authentication Links -->
            @if(Auth::check())
              <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                  <img src="{{ asset(Auth::user()->avatar) }}" alt="{{ Auth::user()->name }}" class="rounded-circle" height="22px">
                </a>

                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <div class="dropdown-header">
                    <h6 class="mb-0 text-dark">{{ Auth::user()->name }}</h6>
                    <small class="text-muted">
                      <span class="ion-ribbon-b text-warning"></span> {{ Auth::user()->points }}
                    </small>
                  </div>
                  <div class="dropdown-divider"></div>
                  <a href="{{ route('profile', ['slug' => Auth::user()->slug]) }}" class="dropdown-item"><i class="ion-person dropdown-icons"></i> Profile</a>
                  <a href="{{ route('profile.settings') }}" class="dropdown-item"><i class="ion-gear-a dropdown-icons"></i> Settings</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="{{ route('logout') }}"
                     onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                      <i class="ion-power dropdown-icons"></i> {{ __('Logout') }}
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                  </form>
                </div>
              </li>
            @endif
          </ul>
        </div>
      </div>
    </nav>
  </header>

  <!-- Notification display area -->
  @if(count($errors) > 0)
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true" class="ion-ios-close-empty"></span>
      </button>
      <ul class="list-unstyled mb-0">
        @foreach($errors->all() as $error)
          <li>
            {{ $error }}
          </li>
        @endforeach
      </ul>
    </div>
  @endif

  @if(session('error') )
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true" class="ion-ios-close-empty"></span>
      </button>
      {{ session('error') }}
    </div>
  @endif

  @if(session('success') )
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true" class="ion-ios-close-empty"></span>
      </button>
      {{ session('success') }}
    </div>
  @endif

  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <main class="py-4">

    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <div class="card">
            <div class="card-body">
              <div class="list-group mb-4">
                <div class="list-group-item border-0">
                  <h6><i class="ion-pricetags"></i> Channels</h6>
                  <hr class="mb-0">
                </div>
                @if(count($channels) > 0)
                @foreach($channels as $channel)
                <a href="{{ route('channel', ['slug' => $channel->slug]) }}" class="list-group-item d-flex justify-content-between align-items-center border-0">
                  {{ $channel->title }}
                  <span class="badge badge-primary badge-pill">{{ count($channel->discussions) }}</span>
                </a>
                @endforeach
                @else
                <p class="list-group-item d-flex justify-content-between align-items-center border-0 mb-0">
                  <small class="text-muted">Channels unavailable</small>
                </p>
                @endif
              </div>

              <div class="list-group mb-4">
                <div class="list-group-item border-0">
                  <h6><i class="ion-chatbubbles"></i> Discussoins</h6>
                  <hr class="mb-0">
                </div>
                @if(count($discussions) > 0)
                @foreach($discussions as $discussion)
                <a href="{{ route('discuss', ['slug' => $discussion->slug]) }}" class="list-group-item d-flex justify-content-between align-items-center border-0">
                  {{ $discussion->title }}
                  <span class="badge badge-primary badge-pill">{{ count($discussion->replies) }}</span>
                </a>
                @endforeach
                @else
                <p class="list-group-item d-flex justify-content-between align-items-center border-0 mb-0">
                  <small class="text-muted">Discussions unavailable</small>
                </p>
                @endif
              </div>

              <div class="list-group">
                <div class="list-group-item border-0">
                  <h6><i class="ion-chatbubble-working"></i> My Discussoins</h6>
                  <hr class="mb-0">
                </div>
                @if(count(Auth::user()->discussions) > 0)
                @foreach(Auth::user()->discussions as $user_discusions)
                <a href="{{ route('discuss', ['slug' => $user_discusions->slug]) }}" class="list-group-item d-flex justify-content-between align-items-center border-0">
                  {{ $user_discusions->title }}
                  <span class="badge badge-primary badge-pill">{{ count($user_discusions->replies) }}</span>
                </a>
                @endforeach
                @else
                <p class="list-group-item d-flex justify-content-between align-items-center border-0 mb-0">
                  <small class="text-muted">You have no discussions yet</small>
                </p>
                @endif
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-9">
          @yield('content')
        </div>
      </div>
    </div>

  </main>

  <notification :id="{{ Auth::id() }}"></notification>

<!-- End Document
–––––––––––––––––––––––––––––––––––––––––––––––––– -->

</body>
</html>
