@extends('layouts.base')

@section('content')

@if(!$bestAnswer)
<div class="discussion-status mb-4">
  <button class="btn btn-success mr-3" disabled><i class="ion-information-circled"></i> Open</button>
  Discussion was <span class="text-success">Opened</span> by <strong>{{ $discussion->user->name }}</strong> on <span class="text-primary">{{ $discussion->created_at->format('F j, Y') }}</span>
</div>
@else
<div class="discussion-status mb-4">
  <button class="btn btn-danger mr-3" disabled><i class="ion-information-circled"></i> Closed</button>
  Discussion was <span class="text-danger">Closed</span> by <strong>{{ $discussion->user->name }}</strong> on <span class="text-primary">{{ $bestAnswer->created_at->format('F j, Y') }}</span>
</div>
@endif

<div class="list-group list-group-flush">
  <div class="card">
    <div class="card-body">
      <div class="user mb-3 clearfix w-100">
        <div class="float-left mr-2">
          <img src="{{ asset($discussion->user->avatar) }}" alt="..." class="rounded-circle" height="50px">
        </div>
        <div class="float-left pt-1">
          <h5 class="mb-0">{{ $discussion->user->name }}</h5>
          <small><span class="ion-ribbon-b text-warning"></span> {{ $discussion->user->points }}</small>
        </div>
        <span class="float-right"><small>{{ $discussion->created_at->diffForHumans() }}</small></span>
      </div>
      <div class="forum-title mb-3">
        <h3 class="mb-0">{{ $discussion->title }}</h3>
        <span class="text-muted">Under</span> <a href="{{ route('channel', ['slug' => $discussion->channel->slug]) }}">{{ $discussion->channel->title }}</a>
      </div>
      <p class="lead">
        {!! Markdown::convertToHtml($discussion->content) !!}
      </p>
      <div class="action py-2">
        @if($discussion->isliked())
        <a title="unlike" class="text-danger" href="{{ route('unlike', ['id' => $discussion->id]) }}"><span class="like ion-ios-heart mr-2"> {{ count($discussion->likes) }}</span></a>
        @else
        <a title="like" class="text-danger" href="{{ route('like', ['id' => $discussion->id]) }}"><span class="like ion-ios-heart-outline mr-2"> {{ count($discussion->likes) }}</span></a>
        @endif
        <span class="ion-reply mr-2 text-success"> {{ count( $discussion->replies) }}</span>
        @if($discussion->isWatching())
        <a href="{{ route('unwatch', ['id' => $discussion->id]) }}" title="watch" class="text-primary">
          <span class="ion-eye-disabled"> {{ count( $discussion->watching) }}</span>
        </a>
        @else
        <a href="{{ route('watch', ['id' => $discussion->id]) }}" title="unwatch" class="text-primary">
          <span class="ion-eye"> {{ count( $discussion->watching) }}</span>
        </a>
        @endif
      </div>
    </div>
    <div class="card-mid bg-light p-3">
      <form action="{{ route('discussions.reply', ['id' => $discussion->id]) }}" method="post">
        @csrf
        <div class="form-group mb-0 row">
          <div class="col-sm-1 text-center pr-0">
            <img src="{{ asset(Auth::user()->avatar) }}" alt="{{ Auth::user()->name }}" class="rounded-circle" height="40px">
            <p class="text-center">
              <small><span class="ion-ribbon-b text-warning"></span> {{ Auth::user()->points }}</small>
            </p>
          </div>
          <div class="col-sm-11">
            <textarea class="form-control reply-textarea" name="reply" rows="3" placeholder="Reply to discussion"></textarea>
            <button type="submit" class="btn btn-primary mt-2">Reply</button>
          </div>
        </div>
      </form>
    </div>
    <div class="card-mid bg-light-blue">
      @if($bestAnswer)
      <div class="card-body">
        <div class="reply-best-answer row py-3">
          <div class="col-sm-12">
            <h6 class="mb-3">Best Answer</h6>
          </div>
          <div class="col-sm-1 text-center pr-0">
            <img src="{{ asset($bestAnswer->user->avatar) }}" alt="{{ $bestAnswer->user->name }}" class="rounded-circle" height="40px">
            <p class="text-center">
              <small><span class="ion-ribbon-b text-warning"></span> {{ $bestAnswer->user->points }}</small>
            </p>
          </div>
          <div class="col-sm-11">
            <h6>{{ $bestAnswer->user->name }}<small> - {{ $bestAnswer->created_at->diffForHumans() }}</small></h6>
            <p class="mb-0">
              {{ $bestAnswer->content }}
            </p>
            <p>
              @if($bestAnswer->isliked())
              <a title="Unlike" class="text-danger" href="{{ route('reply.unlike', ['id' => $bestAnswer->id]) }}"><span class="like ion-ios-heart mr-2"> {{ count($bestAnswer->reply_likes) }}</span></a>
              @else
              <a title="Like" class="text-danger" href="{{ route('reply.like', ['id' => $bestAnswer->id]) }}"><span class="like ion-ios-heart-outline mr-2"> {{ count($bestAnswer->reply_likes) }}</span></a>
              @endif
              @if(!$bestAnswer)
              <a href="{{ route('best', ['id' => $bestAnswer->id]) }}" title="Mark as best answer" class="text-warning"><i class="ion-trophy"></i></a>
              @endif
            </p>
          </div>
        </div>
      </div>
      @endif
    </div>
    <div class="card-footer border-0 bg-white">
      @if($discussion->replies->count() > 0)
      @foreach($discussion->replies as $reply)
        @if(!$reply->best_answer == 1)
        <div class="reply row py-3">
          <div class="col-sm-1 text-center pr-0">
            <img src="{{ asset($reply->user->avatar) }}" alt="{{ $reply->user->name }}" class="rounded-circle" height="40px">
            <p class="text-center">
              <small><span class="ion-ribbon-b text-warning"></span> {{ Auth::user()->points }}</small>
            </p>
          </div>
          <div class="col-sm-11">
            @if(Auth::user()->id == $reply->user_id)
            <div class="dropdown show float-right">
              <a class="text-muted" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="ion-android-arrow-dropdown"></i>
              </a>

              <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <!-- Button trigger modal -->
                <a href="#" class="dropdown-item">
                  <span data-toggle="modal" data-target="#replyEditor">
                    Edit
                  </span>
                </a>
                <a href="{{ route('discussions.reply.delete', ['id' => $reply->id]) }}" class="dropdown-item">
                  Delete
                </a>
              </div>

              <!-- Modal -->
              <div class="modal fade" id="replyEditor" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h6>Edit your reply</h6>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="ion-ios-close-empty"></span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form action="{{ route('discussions.reply.update', ['id' => $reply->id]) }}" method="post">
                        @csrf
                        <div class="form-group mb-0 row">
                          <div class="col-sm-2 text-center pr-0">
                            <img src="{{ asset(Auth::user()->avatar) }}" alt="{{ Auth::user()->name }}" class="rounded-circle" height="40px">
                            <p class="text-center">
                              <small><span class="ion-ribbon-b text-warning"></span> {{ Auth::user()->points }}</small>
                            </p>
                          </div>
                          <div class="col-sm-10">
                            <textarea class="form-control" name="reply" rows="3" placeholder="Reply to discussion">{{ $reply->content }}</textarea>
                            <button type="submit" class="btn btn-primary mt-2">Edit</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @endif
            <h6>{{ $reply->user->name }}<small> - {{ $reply->created_at->diffForHumans() }}</small></h6>
            <p class="mb-0">
              {!! Markdown::convertToHtml($reply->content) !!}
            </p>
            <p>
              @if($reply->isliked())
              <a title="Unlike" class="text-danger" href="{{ route('reply.unlike', ['id' => $reply->id]) }}"><span class="like ion-ios-heart mr-2"> {{ count($reply->reply_likes) }}</span></a>
              @else
              <a title="Like" class="text-danger" href="{{ route('reply.like', ['id' => $reply->id]) }}"><span class="like ion-ios-heart-outline mr-2"> {{ count($reply->reply_likes) }}</span></a>
              @endif
              @if(!$bestAnswer && !Auth::user()->id == $reply->user->id)
              <a href="{{ route('best', ['id' => $reply->id]) }}" title="Mark as best answer" class="text-warning"><i class="ion-trophy"></i></a>
              @endif
            </p>
          </div>
        </div>
        @endif
      @endforeach
      @else
      <p class="py-3 mb-0">
        No replies yet
      </p>
      @endif
    </div>
  </div>
</div>

@endsection
