<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Like;
use App\Watcher;
use Auth;

class Discussions extends Model
{
  protected $fillable = ['title', 'content', 'user_id', 'channel_id', 'slug'];

  public function channel() {
    return $this->belongsTo('App\Channel');
  }

  public function replies($value='')
  {
    return $this->hasMany('App\Reply');
  }

  public function user()
  {
    return $this->belongsTo('App\User');
  }

  public function likes()
  {
    return $this->hasMany('App\Like');
  }

  public function watching()
  {
    return $this->hasMany('App\Watcher');
  }

  public function isliked()
  {
    $id = Auth::id();

    $likers = array();
    foreach($this->likes as $like) {
      array_push($likers, $like->user->id);
    }

    if(in_array($id, $likers)) {
      return true;
    } else {
      return false;
    }
  }

  public function isWatching()
  {
    $id = Auth::id();

    $watchers = array();
    foreach($this->watching as $watcher) {
      array_push($watchers, $watcher->user->id);
    }

    if(in_array($id, $watchers)) {
      return true;
    } else {
      return false;
    }
  }

  public function hasBest() {
    $result = false;
    foreach ($this->replies as $reply) {
      if($reply->best_answer) {
        $result = true;
        break;
      }
    }
    return $result;
  }

}
