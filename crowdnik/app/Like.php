<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Discussions;
use App\User;

class Like extends Model
{
  protected $fillable = ['discussions_id', 'user_id',];

  public function discussion() {
    return $this->belongsTo('App\Discussions');
  }

  public function user()
  {
    return $this->belongsTo('App\User');
  }
}
