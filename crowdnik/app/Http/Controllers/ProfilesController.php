<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfilesController extends Controller
{
  public function profile($slug) {
    $user = User::where('slug', $slug)->first();

    return view('profile.index')->with('user', $user);
  }

  public function settings() {
    $user = Auth::user();
    return view('profile.settings')->with('user', $user);
  }

  public function update(Request $request) {

    $this->validate($request, [
      'first_name' => 'required',
      'last_name' => 'required',
      'email' => 'required|email',
    ]);

    Auth::user()->update([
      'email' => $request->email
    ]);

    Auth::user()->profile()->update([
      'first_name' => $request->first_name,
      'last_name' => $request->last_name,
      'location' => $request->location,
      'job' => $request->occupation,
      'about' => $request->about
    ]);

    return redirect()->back()->with('success', 'Changes saved');
  }

  public function update_password(Request $request) {

    $this->validate($request, [
      'password' => 'required',
      'email' => 'required|email',
    ]);

    if($request->email === Auth::user()->email) {
      Auth::user()->update([
        'password' => Hash::make($request->password),
        'email' => $request->email
      ]);
    } else {
      return redirect()->back()->with('error', 'Your email does not match');
    }

    return redirect()->back()->with('success', 'Changes saved');
  }

  public function update_avatar(Request $request) {
    $this->validate($request, [
      'avatar' => 'required|image'
    ]);

    if($request->hasFile('avatar')) {
      $avatar = $request->avatar;
      $avatar_new_name = time().$avatar->getClientOriginalName();
      $avatar->move('uploads/avatar', $avatar_new_name);

      $user = Auth::user();
      $user->avatar = 'uploads/avatar/'.$avatar_new_name;
      $user->save();
    }

    return redirect()->back()->with('success', 'Avatar updated');
  }
}
