<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Channel;
use App\Discussions;
use Notification;
use App\Reply;
use App\Like;
use App\Reply_Like;
use Auth;
use App\User;
use App\Watcher;

class DiscussionsController extends Controller
{
  public function create() {
    return view('discussions.create');
  }

  public function store(Request $request)
  {
    $this->validate($request, [
      'title' => 'required',
      'content' => 'required',
      'channel_id' => 'required'
    ]);

    Discussions::create([
      'title' => $request->title,
      'content' => $request->content,
      'channel_id' => $request->channel_id,
      'slug' => str_slug($request->title),
      'user_id' => Auth::id()
    ]);

    return redirect()->route('home')->with('success', 'Discussion created');
  }

  public function edit($slug)
  {
    return view('discussions.edit')->with('discussion', Discussions::where('slug', $slug)->first());
  }

  public function update(Request $request)
  {
    $this->validate($request, [
      'content' => 'required',
      'title' => 'required'
    ]);
    $discussion = Discussions::find($request->id);
    $discussion->title = $request->title;
    $discussion->content = $request->content;

    $discussion->save();

    return redirect()->route('discuss', ['slug' => $discussion->slug]);
  }

  public function delete($slug) {
    $discussion = Discussions::where('slug', $slug);
    $discussion->delete();

    return redirect()->back();
  }

  public function reply(Request $request, $id) {
    $this->validate($request, [
      'reply' => 'required'
    ]);

    $discussion = Discussions::find($id);

    Reply::create([
      'content' => $request->reply,
      'user_id' => Auth::id(),
      'discussions_id' => $id
    ]);

    $watchers = array();
    foreach($discussion->watching as $watcher) {
      array_push($watchers, User::find($watcher->user->id));
    }

    Notification::send($watchers, new \App\Notifications\NewReplyAdded($discussion));

    return redirect()->back();
  }

  public function reply_update(Request $request, $id) {
    $reply = Reply::find($id);
    $reply->content = $request->reply;
    $reply->save();

    return redirect()->back();
  }

  public function reply_delete(Request $request, $id) {
    $reply = Reply::find($id);
    $reply->delete();

    return redirect()->back();
  }

  public function like($id)
  {
    $discussion = Discussions::find($id);
    $like = Like::create([
      'user_id' => Auth::id(),
      'discussions_id' => $id
    ]);

    return redirect()->back();
  }

  public function unlike($id)
  {
    $like = Like::where('discussions_id', $id);
    $like->delete();

    return redirect()->back();
  }

  public function channel($slug)
  {
    $channel = Channel::where('slug', $slug)->first();
    return view('channel')->with('channel', $channel);
  }
}
