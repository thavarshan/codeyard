<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Channel;
use App\Discussions;
use App\Reply;
use Ap\Like;
use App\Reply_Like;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
      return view('home');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function discussions()
    {
      return view('discussions');
    }

    public function discuss($slug) {
      $discussion = Discussions::where('slug', $slug)->first();
      $bestAnswer = $discussion->replies()->where('best_answer', 1)->first();
      return view('discuss')->with('discussion', $discussion)
                            ->with('bestAnswer', $bestAnswer);
    }
}
