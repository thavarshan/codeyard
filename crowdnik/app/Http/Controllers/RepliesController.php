<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Channel;
use App\Discussions;
use Notification;
use App\Reply;
use App\Reply_Like;
use Auth;
use App\User;
use App\Watcher;

class RepliesController extends Controller
{
  public function best_answer($id)
  {
    $reply = Reply::find($id);
    $reply->best_answer = 1;
    $reply->save();

    return redirect()->back();
  }

  public function like($id)
  {
    $discussion = Reply::find($id);
    $like = Reply_Like::create([
      'user_id' => Auth::id(),
      'reply_id' => $id
    ]);

    return redirect()->back();
  }

  public function unlike($id)
  {
    $like = Reply_Like::where('reply_id', $id);
    $like->delete();

    return redirect()->back();
  }
}
