<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Watcher;
use Auth;

class WatchersController extends Controller
{
  public function watch($id)
  {
    Watcher::create([
      'discussions_id' => $id,
      'user_id' => Auth::id()
    ]);

    return redirect()->back();
  }

  public function unwatch($id)
  {
    $watcher = Watcher::where('discussions_id', $id);
    $watcher->delete();

    return redirect()->back();
  }
}
