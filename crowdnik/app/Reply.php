<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Discussions;
use App\Reply_Like;

class Reply extends Model
{
  protected $fillable = ['content', 'user_id', 'discussions_id', 'best_answer'];

  public function discussion()
  {
    return $this->belongsTo('App\Discussions');
  }

  public function user()
  {
    return $this->belongsTo('App\User');
  }

  public function reply_likes()
  {
    return $this->hasMany('App\Reply_Like');
  }

  public function isliked()
  {
    $id = Auth::id();

    $likers = array();
    foreach($this->reply_likes as $like) {
      array_push($likers, $like->user->id);
    }

    if(in_array($id, $likers)) {
      return true;
    } else {
      return false;
    }
  }
}
