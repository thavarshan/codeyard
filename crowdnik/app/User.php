<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Like;
use App\Watcher;
use App\Discussions;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'gender', 'avatar', 'slug', 'points'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function discussions()
    {
      return $this->hasMany('App\Discussions');
    }

    public function replies()
    {
      return $this->hasMany('App\Reply');
    }

    public function watching()
    {
      return $this->hasMany('App\Watcher');
    }

    // public function likes()
    // {
    //   return $this->hasMany('App\Like');
    // }
}
