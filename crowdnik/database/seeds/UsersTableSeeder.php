<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      App\User::create([
        'name' => 'Admin',
        'email' => 'admin@crowdnik.com',
        'avatar' => 'defaults/avatar/user.png',
        'admin' => 1,
        'gender' => 1,
        'password' => Hash::make('alpha26'),
        'slug' => str_slug('admin')
      ]);
    }
}
