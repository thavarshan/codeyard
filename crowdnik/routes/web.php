<?php
use Illuminate\Routing\Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Landing Page
Route::get('/', [
  'uses' => 'FrontendController@index',
  'as' => 'front'
])->middleware('guest');

Auth::routes();

// Autherized Personel only
Route::group(['middleware' => 'auth'], function() {
  // Home Controller
  Route::get('/home', 'HomeController@index')->name('home');
  // Discussions Page
  Route::get('/discussions', [
    'uses' => 'HomeController@discussions',
    'as' => 'discussions'
  ]);
  Route::get('/discussions/create', [
    'uses' => 'DiscussionsController@create',
    'as' => 'discussions.create'
  ]);
  Route::post('/discussions/store', [
    'uses' => 'DiscussionsController@store',
    'as' => 'discussions.store'
  ]);
  Route::get('/discussions/edit/{slug}', [
    'uses' => 'DiscussionsController@edit',
    'as' => 'discussions.edit'
  ]);
  Route::post('/discussions/update', [
    'uses' => 'DiscussionsController@update',
    'as' => 'discussions.update'
  ]);
  Route::get('/discussions/delete/{slug}', [
    'uses' => 'DiscussionsController@delete',
    'as' => 'discussions.delete'
  ]);
  Route::get('/discuss/{slug}', [
    'uses' => 'HomeController@discuss',
    'as' => 'discuss'
  ]);
  Route::post('/discuss/reply/{id}', [
    'uses' => 'DiscussionsController@reply',
    'as' => 'discussions.reply'
  ]);
  Route::post('/discuss/reply/update/{id}', [
    'uses' => 'DiscussionsController@reply_update',
    'as' => 'discussions.reply.update'
  ]);
  Route::get('/discuss/reply/delete/{id}', [
    'uses' => 'DiscussionsController@reply_delete',
    'as' => 'discussions.reply.delete'
  ]);
  // Channels Filter Page
  Route::get('/channel/{slug}', [
    'uses' => 'DiscussionsController@channel',
    'as' => 'channel'
  ]);
  // Like Controlls
  Route::get('like/{id}', [
    'uses' => 'DiscussionsController@like',
    'as' => 'like'
  ]);
  Route::get('unlike/{id}', [
    'uses' => 'DiscussionsController@unlike',
    'as' => 'unlike'
  ]);
  // Like Controlls Replies
  Route::get('reply/like/{id}', [
    'uses' => 'RepliesController@like',
    'as' => 'reply.like'
  ]);
  Route::get('reply/unlike/{id}', [
    'uses' => 'RepliesController@unlike',
    'as' => 'reply.unlike'
  ]);
  // Watch Controlls
  Route::get('watch/{id}', [
    'uses' => 'WatchersController@watch',
    'as' => 'watch'
  ]);
  Route::get('unwatch/{id}', [
    'uses' => 'WatchersController@unwatch',
    'as' => 'unwatch'
  ]);
  // Best Answer Controlls
  Route::get('/discuss/best/{id}', [
    'uses' => 'RepliesController@best_answer',
    'as' => 'best'
  ]);
  // Profile Controller
  Route::get('/profile', [
    'uses' => 'ProfilesController@index',
    'as' => 'profile'
  ]);
  // Profile Settings
  Route::get('/profile/settings', [
    'uses' => 'ProfilesController@settings',
    'as' => 'profile.settings'
  ]);
});

// Channels Controller
Route::resource('channels', 'ChannelsController')->middleware('admin');
