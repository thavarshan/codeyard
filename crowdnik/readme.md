## Installation

After cloning or downloading the repository

- Run ```$ php artisan key:generate```
- Run ```$ php artisan migrate```
- Run ```$ php artisan db:seed```
- Run ```$ php artisan vendor:publish```
- The go to app->Providers->AppServiceProvider.php
- uncomment
 View::share('channels', Channel::orderBy('created_at', 'desc')->get());
 View::share('discussions', Discussions::orderBy('created_at', 'desc')->paginate(12));
 in boot function.
