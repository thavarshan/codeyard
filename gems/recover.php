<?php  require_once("components/config.php") ?>

<?php  include(TEMPLATE_LINK . DS . "header.php") ?>

<section class="section-recover pad">
    <div class="row">
        <?php display_message(); ?>
        <?php recover_password(); ?>
    </div>
   
    <div class="row">
        <div class="col span-1-of-3">
            &nbsp;
        </div>
        
        <div class="col span-1-of-3">
           
            <h3>Recover Password</h3>
            <p><Strong>Enter your Email address to receive a password reset link.</Strong></p>
           
            <div class="form-container-2">
                <form id="register-form"  method="post" role="form" autocomplete="off">

                    <div class="row">
                        <input type="email" name="email" id="email" placeholder="Email Address" autocomplete="off" />
                    </div>

                    <div class="row">
                        <div class="col span-2-of-5">
                            <button type="submit" name="cancel_submit" id="cencel-submit" class="full-w btn hvr-rectangle-in">
                                Cancel
                            </button>
                        </div>
                        
                        <div class="col span-3-of-5">
                            <button type="submit" name="recover-submit" id="recover-submit" class="full-w btn hvr-rectangle-in">
                                Send Password Reset Link
                            </button>
                        </div>
                    </div>

                    <div class="row pad">
                        <input type="hidden" class="hide" name="token" id="token" value="<?php echo token_generator(); ?>">
                    </div>
                </form>
            </div>
        </div>
        
        <div class="col span-1-of-3">
            &nbsp;
        </div>
    </div>
</section>

<!-- Footer -->
<?php include(TEMPLATE_LINK . DS . "footer.php");?>