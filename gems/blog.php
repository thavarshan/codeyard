<?php  require_once("components/config.php") ?>

<?php  include(TEMPLATE_LINK . DS . "header.php") ?>

<!-- About Section -->
<section class="section-blog clearfix pad products">

    <div class="row">
        <div class="col span-8-of-11">        
            <?php posts(); ?>        
        </div>

        <div class="col span-3-of-11">
            <div class="sidebar-box">
                <div class="row">
                    <div class="catagories">
                        <h2>Categories</h2>
                    </div>
                </div>

                <div class="row">
                    <ul class="shop-catagories-ul">
                        <li><a class="normal" href='blog.php'>All</a>
                            <?php blog_categories(); ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</section>

<!-- Footer -->
<?php include(TEMPLATE_LINK . DS . "footer.php");?>
