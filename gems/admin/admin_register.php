<?php include('components/includes/header.php'); ?>

<!-- content -->
<div class="row-full pad clearfix" id="content-container-body">
    <form id="reg_form" action="admin_register.php" method="post" enctype="multipart/form-data"> 
        <div class="col span-1-of-2">
            <div class="row sep">
                <h3>Registration</h3>
            </div>
            
            <div class="row">
                <div class="col span-2-of-2">
                    <?php validate_user_registration(); ?>
                    <?php display_message(); ?>
                </div>
            </div>

            <div class="row sep">
                <div class="col span-2-of-2">
                    <input type="text" name="username" id="username" placeholder="Username" autocomplete="name" required>
                </div>
            </div>

            <div class="row sep">
                <div class="col span-1-of-2">
                    <input type="text" name="first_name" id="first_name" placeholder="First Name" autocomplete="name" required>
                </div>

                <div class="col span-1-of-2">
                    <input type="text" name="last_name" id="last_name" placeholder="Last Name" autocomplete="name" required>
                </div>
            </div>
            
            <div class="row sep">
                <div class="col span-1-of-2">
                    <input type="password" name="password" id="password" placeholder="Password" required>
                </div>

                <div class="col span-1-of-2">
                    <input type="password" name="confirm_password"  placeholder="Confirm Password" required>
                </div>
            </div>

            <div class="row sep">
                <div class="col span-1-of-2">
                    <input type="email" name="email" id="email" placeholder="Email Address" autocomplete="email" required>
                </div>

                <div class="col span-1-of-2">
                    <input type="text" name="position" id="position" placeholder="Position" autocomplete="on" required>
                </div>
            </div>
            
            <div class="row sep">
                <div class="col span-2-of-2">
                    <input type="text" name="address" id="address" placeholder="Address" required>
                </div>
            </div>

            <div class="row sep">
                <div class="col span-1-of-2">
                    <input type="tel" name="telephone" id="telephone" placeholder="Telephone" required>
                </div>
                
               <div class="col span-1-of-2">
                   <input type="file" name="avatar" />
               </div>
            </div>
            
            
        </div>

        <div class="col span-1-of-2 left-pad">

            <div class="row">
                <h3>Terms and Conditions</h3>
                <br />
            </div>

            <div class="row">
                <p>
                    By clicking on "Register" you agree to The Company's' Terms and Conditions
                </p>
                <br />
                <p>
                    While rare, prices are subject to change based on exchange rate fluctuations - should such a fluctuation happen, we may request an additional payment. You have the option to request a full refund or to pay the new price. (Paragraph 13.5.8)
                </p>
                <br />
                <p>
                    Should there be an error in the description or pricing of a product, we will provide you with a full refund (Paragraph 13.5.6)
                </p>
                <br />
                <p>
                    Acceptance of an order by us is dependent on our suppliers ability to provide the product. (Paragraph 13.5.6)
                </p>
                <br />
            </div>
            <div class="row pad">
                <button id="reg" type="submit" class="btn hvr-rectangle-in" name="register-submit" id="register-submit">Register</button>
            </div>
            
        </div>
        
    </form>
</div>

<?php include('components/includes/footer.php'); ?>