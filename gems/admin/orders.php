<?php include('components/includes/header.php'); ?>

<!-- content -->
<div class="row-full pad clearfix" id="content-container-body">
    
    <div class="row">
        <div class="col span-1-of-2">
            <h2>List of all Orders<small>Customer orders from Pearl Gems Website</small></h2>
        </div>

        <div class="col span-1-of-2">
            &nbsp;
        </div>
    </div>
    
    <div class="row">
        <?php display_message(); ?>
    </div>
    
    <div class="row">
        <table class="table-fill">
            <thead>
                <tr>
                    <th class="text-left">ID</th>
                    <th class="text-left">Amount</th>
                    <th class="text-left">Transaction</th>
                    <th class="text-left">Status</th>
                    <th class="text-left">Currency</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody class="table-hover">
                <?php display_orders(); ?>
            </tbody>
        </table>
    </div>

</div>


<?php include('components/includes/footer.php'); ?>