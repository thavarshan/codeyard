<?php 

require_once("../admin/components/admin_config.php");

if(isset($_GET['id'])) {

    $query = query("DELETE FROM users WHERE user_id = " . escape_string($_GET['id']) . " ");
    confirm($query);
    redirect("customers.php");
    set_message("<script>alert('Customer has been removed!')</script>");

} else {

    redirect("customers.php"); 

}


?>