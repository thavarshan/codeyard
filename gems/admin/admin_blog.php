<?php include("components/includes/header.php"); ?>

<!-- content -->
<div class="row-full pad clearfix" id="content-container-body">
    <div class="row">
       <div class="col span-1-of-2">
           <h2>blog entries<small>All entries from Pearl Gems blog</small></h2>
       </div>
       
       <div class="col span-1-of-2">
           <a href="add_post.php" class="btn hvr-rectangle-in add_link">+ Add Post </a>
       </div>
    </div>

    <div class="row">
        <?php display_blog_posts(); ?>
    </div>
</div>

<?php include("components/includes/footer.php"); ?>
