<?php include('components/includes/header.php'); ?>

<!-- content -->
<div class="row-full pad clearfix" id="content-container-body">
       
    <div class="row clearfix">
            <!-- For Statistcs -->
            <div class="col span-1-of-4 stat-count">
                <span class="top-count"><i class="ion-person-stalker"></i> &nbsp;Total Customers</span>
                <div class="count">
                    <?php

                        global $con;

                        $sqlc = "SELECT * FROM users";
                        $resultc = mysqli_query($con, $sqlc);
                        confirm($resultc);
                        $rowsc = mysqli_num_rows($resultc);
                        echo $rowsc;

                    ?>
                </div>
                <div>
                    <?php

                    if($rowsc > 5) {
                        echo '<span class="bottom-count"><i class="green ion-android-arrow-dropup"></i> &nbsp;No. of customers<span class="green ion-android-arrow-dropup"> GOOD!</span></span>';
                    } else {
                        echo '<span class="bottom-count"><i class="red ion-android-arrow-dropdown"></i><span class="red"> &nbsp;Losing customers</span</span>';
                    }

                    ?>
                </div>
            </div>

            <div class="col span-1-of-4 stat-count">
                <span class="top-count"><i class="ion-card"></i> &nbsp;Revenue</span>
                <div class="count">
                    <?php
                    
                        global $con;

                        $sql = "SELECT SUM(product_price) FROM reports";
                        $q = mysqli_query($con, $sql);
                        $row = mysqli_fetch_array($q);
                        
                        if($row[0] == null) {
                            echo "&#36;" . " " . "0" . ".00";
                        } else {
                            echo "&#36;" . " " . $row[0] . ".00";
                        }
                    
                    ?>
                </div>
                <div>
                    <?php

                    if($row[0] > 4000) {
                        echo '<span class="bottom-count"><i class="green ion-android-arrow-dropup"></i> &nbsp;Revenue level &nbsp;<span class="green"> GOOD!</span></span>';
                    } else {
                        echo '<span class="bottom-count"><i class="red ion-android-arrow-dropdown"></i><span class="red"> &nbsp;Revenue going down</span</span>';
                    }

                    ?>
                </div>
            </div>

            <div class="col span-1-of-4 stat-count">
                <span class="top-count"><i class="ion-bag"></i> &nbsp;Total Products</span>
                <div class="count">
                    <?php

                        global $con;

                        $sqlp = "SELECT * FROM products";
                        $resultp = mysqli_query($con, $sqlp);
                        confirm($resultp);
                        $rowsp = mysqli_num_rows($resultp);
                        echo $rowsp;

                    ?>
                </div>
                <div>
                    <?php

                    if($rowsp > 7) {
                        echo '<span class="bottom-count"><i class="green ion-android-arrow-dropup"></i> &nbsp;Stocks &nbsp;<span class="green"> GOOD!</span></span>';
                    } else {
                        echo '<span class="bottom-count red"><i class="red ion-android-arrow-dropdown"></i> &nbsp;Almost out of stock</span>';
                    }

                    ?>
                </div>
            </div>

            <div class="col span-1-of-4 stat-count">
                <span class="top-count"><i class="ion-ios-cart"></i> &nbsp;Total Orders</span>
                <div class="count">
                    <?php
                    
                        global $con;

                        $sqlo = "SELECT * FROM orders";
                        $resulto = mysqli_query($con, $sqlo);
                        confirm($resulto);
                        $rowso = mysqli_num_rows($resulto);
                        echo $rowso;

                    ?>
                </div>
                <div>
                    <?php
                        
                        if($rowso > 7) {
                            echo '<span class="bottom-count"><i class="green ion-android-arrow-dropup"></i> &nbsp;Orders pilling up!</span>';
                        } else {
                            echo '<span class="bottom-count red"><i class="red ion-android-arrow-dropdown"></i> &nbsp;Bad sales</span>';
                        }
                    
                    ?>
                </div>
            </div>
    </div>

    <div class="row clearfix">
       
       <div class="col span-4-of-6">
           <div class="row">
               <h2>recent blog entries<small>recent entries from Pearl Gems blog</small></h1>
               <div class="row">
                   <?php display_blog_posts(); ?>
               </div>
           </div>
       </div>
        
        <div class="col span-2-of-6">
            <div class="row clearfix">
                <h2>recent customers<small>recently joined customers</small></h1>
                <div class="row">
                    <?php recent_customers(); ?>
                </div>
            </div>
        </div>
        
    </div>
   
</div>


<?php include('components/includes/footer.php'); ?>