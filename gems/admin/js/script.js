$(document).ready (function() {
    
    /* ----------- Main-Slider ----------- */
    
    //configuration
    var width = 1250;
    var animationSpeed = 1000;
    var pause = 3000;
    var currentSlide = 1;
    
    //cache DOM
    var $slider = $('.slider');
    var $sliderContainer = $slider.find('.slides');
    var $slides = $sliderContainer.find('.slide');
    
    var interval;
    
    function startSlider() {
        interval = setInterval(function() {
            $sliderContainer.animate({'margin-left': '-='+width}, animationSpeed, function(){
                currentSlide++;
                if(currentSlide === $slides.length) {
                    currentSlide = 1;
                    $sliderContainer.css('margin-left', 0);
                }
            });
        }, pause);
    }
    
    function stopSlider() {
        clearInterval(interval);
    }
    
    $slider.on('mouseenter', stopSlider).on('mouseleave', startSlider);
    
    startSlider();
    
    
    
    /* ----------- Product-Slider ----------- */
        
    //configuration
    var f_width = 1250;
    var f_animationSpeed = 1000;
    var f_pause = 3000;
    var f_currentSlide = 1;

    //cache DOM
    var $f_slider = $('.f-slider');
    var $f_sliderContainer = $f_slider.find('.f-slides');
    var $f_slides = $f_sliderContainer.find('.f-slide');

    var f_interval;

    function f_startSlider() {
        f_interval = setInterval(function() {
            $f_sliderContainer.animate({'margin-left': '-='+width}, f_animationSpeed, function(){
                f_currentSlide++;
                if(f_currentSlide === $slides.length) {
                    f_currentSlide = 1;
                    $f_sliderContainer.css('margin-left', 0);
                }
            });
        }, pause);
    }

    function f_stopSlider() {
        clearInterval(f_interval);
    }

    $f_slider.on('mouseenter', f_stopSlider).on('mouseleave', f_startSlider);

    f_startSlider();

    
});
