<?php include('components/includes/header.php'); ?>

<!-- content -->
<div class="row-full pad clearfix" id="content-container-body">
        <div class="row">
            <form action="add_product.php" method="post" enctype="multipart/form-data">

                <div class="row">
                    <div class="col span-2-of-2">
                        <input type="text" name="product_title" placeholder="Product Title" required/>
                    </div>
                </div>

                <div class="row">
                    <div class="col span-1-of-2">
                        <select name="product_cat" >
                            <option>Select a Category</option>
                            <?php 

                            global $con; 
                            $get_cats = "SELECT * FROM catagories";
                            $run_cats = mysqli_query($con, $get_cats);

                            while ($row_cats = mysqli_fetch_array($run_cats)){
                                $cat_id    = $row_cats['cat_id']; 
                                $cat_title = $row_cats['cat_title'];

                                echo "<option value='$cat_id'>$cat_title</option>";
                            }

                            ?>
                        </select>
                    </div>

                    <div class="col span-1-of-2">
                        <input type="number" placeholder="Quantity" name="product_quantity" required/>
                    </div>
                </div>

                <div class="row">
                    <div class="col span-1-of-3">
                        <input type="text" placeholder="Price" name="product_price" required/>
                    </div>

                    <div class="col span-1-of-3">
                        <input type="text" name="product_keywords" placeholder="Product Tags" required/>
                    </div>

                    <div class="col span-1-of-3">
                        <input type="file" name="product_image" id="product_image" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" multiple />
                        <label class="inputfile-label hvr-rectangle-in" for="product_image">Product Image (Max file size: 2MB)</label>
                    </div>
                </div>

                <div class="row">
                    <div class="col span-2-of-2">
                        <textarea name="product_desc" id="editor1" style="height:200px;" placeholder="Product Description"></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col span-2-of-2">
                        <button type="submit" class="btn hvr-rectangle-in" name="add_product">+ Add Product</button>
                    </div>
                </div>

            </form>
        </div>
</div>

<?php include('components/includes/footer.php'); ?>


<?php 

if(isset($_POST['add_product'])){
    
    global $con;

    //getting the text data from the fields
    $product_title    = escape_string($_POST['product_title']);
    $product_cat      = escape_string($_POST['product_cat']);
    $product_price    = escape_string($_POST['product_price']);
    $product_desc     = escape_string($_POST['product_desc']);
    $product_keywords = escape_string($_POST['product_keywords']);
    $product_quantity = escape_string($_POST['product_quantity']);

    //getting the image from the field
    $product_image     = $_FILES['product_image']['name'];
    $product_image_tmp = $_FILES['product_image']['tmp_name'];

    move_uploaded_file($product_image_tmp,"product_images/$product_image");

    $sql = "INSERT INTO products (product_cat, product_title, product_price, product_quantity, product_desc, product_image, product_keywords) VALUES ('{$product_cat}', '{$product_title}', '{$product_price}', '{$product_quantity}', '{$product_desc}', '{$product_image}', '{$product_keywords}')";
    $result = query($sql);
    confirm($result);

    if($result){

        redirect("products.php");
        set_message("<script>alert('Product has been Added!')</script>");

    } else {
        
        set_message("<script>alert('Was unable to add product')</script>");
        
    }
}
?>