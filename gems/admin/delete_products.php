<?php 

require_once("../admin/components/admin_config.php");

if(isset($_GET['id'])) {

    $query = query("DELETE FROM products WHERE product_id = " . escape_string($_GET['id']) . " ");
    confirm($query);
    redirect("products.php");
    set_message("<script>alert('Product has been deleted!')</script>");

} else {

    redirect("products.php"); 

}


?>