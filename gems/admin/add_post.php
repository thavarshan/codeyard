<?php include('components/includes/header.php'); ?>

<!-- content -->
<div class="row-full pad clearfix" id="content-container-body">
    <div class="row">
        <form action="admin_blog.php" method="post" enctype="multipart/form-data">

            <div class="row">
                <div class="col span-2-of-2">
                    <input type="text" name="title" placeholder="Title" required/>
                </div>
            </div>

            <div class="row">
                <div class="col span-1-of-2">
                    <select name="category" >
                        <option>Select a Category</option>
                        <?php 

                            global $con; 

                            $get = "SELECT * FROM blog_category";
                            $run = mysqli_query($con, $get);

                            while ($row = mysqli_fetch_array($run)){

                                $id    = $row['post_category_id']; 
                                $title = $row['post_category_title'];

                                echo "<option value='$id'>$title</option>";
                            }

                        ?>
                    </select>
                </div>

                <div class="col span-1-of-2">
                    <select name="author" >
                        <option>Author</option>
                        <?php 

                        global $con; 

                        $get = "SELECT * FROM admin_users";
                        $run = mysqli_query($con, $get);

                        while ($row = mysqli_fetch_array($run)){

                            $id    = $row['admin_id']; 
                            $name = $row['username'];

                            echo "<option value='$id'>$name</option>";
                        }

                        ?>
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col span-2-of-2">
                    <textarea name="post" id="editor1" style="height:200px;"></textarea>
                </div>
            </div>

            <div class="row">
                <div class="col span-1-of-3">
                    <button type="submit" class="btn hvr-rectangle-in" name="publish">Publish</button>
                </div>
                
                <div class="col span-1-of-3">
                    <input type="text" name="keywords" placeholder="Keywords" required/>
                </div>

                <div class="col span-1-of-3">
                    <label style="margin-bottom: 10px;"></label>
                    <input type="file" name="featured_image"/>
                </div>
            </div>

        </form>
    </div>
</div>

<?php include("components/includes/footer.php"); ?>


<?php 

if(isset($_POST['publish'])){

    //getting the text data from the fields
    $title         = $_POST['title'];
    $category      = $_POST['category'];
    $post          = $_POST['post'];
    $keywords      = $_POST['keywords'];
    $author        = $_POST['author'];

    //getting the image from the field
    $featured_image     = $_FILES['featured_image']['name'];
    $featured_image_tmp = $_FILES['featured_image']['tmp_name'];

    move_uploaded_file($featured_image_tmp,"../img/blog/$featured_image");

    $insert_post = "INSERT INTO blog_posts (post_category, post_title, post_author, post_body, post_img, keywords) VALUES ('$category', '$title', '$author', '$post', '$featured_image', '$keywords')";

    $publish = mysqli_query($con, $insert_post);

    if($publish){
        echo "<script>alert('Published!')</script>";
    }
}
?>