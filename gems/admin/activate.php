<?php include("components/includes/input_header.php"); ?>

<div class="login-box clearfix">
        <div class="row">
            <?php display_message(); ?>
            <?php activate_user(); ?>
        </div>

    <footer class="row text-left">
        <p>Copyright &copy; <?php echo date('Y'); ?> Pearl Cluster (PVT) LTD<br />Powered by <strong>BASE.net Framework V 2.2.0.1</strong></p>
    </footer>
    </form>
</div>


<!-- Footer -->
<?php include("components/includes/input_footer.php"); ?>