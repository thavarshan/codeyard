<?php 

require_once("../admin/components/admin_config.php");

if(isset($_GET['id'])) {

    $query = query("DELETE FROM reports WHERE report_id = " . escape_string($_GET['id']) . " ");
    confirm($query);
    redirect("reports.php");
    set_message("<script>alert('Report has been deleted!')</script>");

} else {

    redirect("reports.php"); 

}


?>