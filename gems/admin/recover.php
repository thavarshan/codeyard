<?php include('components/includes/input_header.php'); ?>

    <div class="login-box">
        <form id="register-form"  method="post" role="form" autocomplete="off">
            
            <div class="row">
                <?php display_message(); ?>
                <?php recover_password(); ?>
            </div>

            <div class="row sep">
                <input type="email" name="email" id="email" placeholder="Email Address" autocomplete="off" />
            </div>
            
            <div class="row sep">
                <button type="submit" name="recover-submit" id="recover-submit" class="full-w btn hvr-rectangle-in">
                    Send Password Reset Link
                </button>
            </div>

            <div class="row sep">
                <button type="submit" name="cancel_submit" id="cencel-submit" class="full-w btn hvr-rectangle-in">
                    Cancel
                </button>
            </div>

            <div class="row">
                <input type="hidden" class="hide" name="token" id="token" value="<?php echo token_generator(); ?>">
            </div>        
        </form>
        <footer class="row text-left">
            <p>Copyright &copy; <?php echo date('Y'); ?> Pearl Cluster (PVT) LTD<br />Powered by <strong>BASE.net Framework BETA V2.3.4.2</strong></p>
        </footer>
       
    </div>

<!-- Footer -->
<?php include('components/includes/input_footer.php'); ?> 