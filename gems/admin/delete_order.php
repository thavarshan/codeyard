<?php 
    
    require_once("../admin/components/admin_config.php");

    if(isset($_GET['id'])) {
        
        $query = query("DELETE FROM orders WHERE order_id = " . escape_string($_GET['id']) . " ");
        confirm($query);
        redirect("orders.php");
        set_message("<script>alert('Order has been deleted!')</script>");
        
    } else {
        
        redirect("orders.php"); 
        
    }


?>