<?php 

//======
//====== HELPER FUNCTIONS -----------------------------------------
//======

function set_message($msg) {
    if(!empty($msg)) {
        $_SESSION['message'] = $msg;
    } else {
        $msg = "";
    }

}

function display_message() {
    if(isset($_SESSION['message'])) {
        echo $_SESSION['message'];
        unset($_SESSION['message']);
    }
}

function  format_date($date){
    return date('g:ia \o\n l jS F Y', strtotime($date));
}

function  format_date_min($date){
    return date('F j, Y, g:i a', strtotime($date));
}

function  format_date_only($date){
    return date('F j, Y', strtotime($date));
}

function clean($string){
    return htmlentities($string);
}

function redirect($location) {
    header("Location: $location ");
}

function query($sql) {
    global $con;
    return mysqli_query($con, $sql);
}

function confirm($result) {
    global $con;
    if(!$result) {
        die("QUERY FALIED" . mysqli_error($con));
    }
}

function row_count($result){
    return mysqli_num_rows($result);
}

function row_count_stat($result){
    return mysqli_num_rows($result);
    echo mysqli_num_rows($result);
}

function escape($string){
    global $con;
    return mysqli_real_escape_string($con, $string);
}

function escape_string($string) {
    global $con;
    return mysqli_real_escape_string($con, $string);
}

function fetch_array($result) {
    return mysqli_fetch_array($result);
}

function token_generator(){
    $token = $_SESSION['token'] = md5(uniqid(mt_rand(), true));
    return $token;
}
function validation_errors($error_message){
    $alert_error_message = "<div class='row'>{$error_message}</div>";
    return $alert_error_message;
}

function email_exists($email){
    $sql = "SELECT admin_id FROM admin_users WHERE email = '$email'";
    $result = query($sql);
    confirm($result);

    if(row_count($result) >= 1){
        return true;
    }
    return false;
}
function username_exists($username){
    $sql = "SELECT admin_id FROM admin_users WHERE username = '$username'";
    $result = query($sql);
    confirm($result);

    if(row_count($result) >= 1){
        return true;
    }
    return false;
}

function send_email($email, $subject, $msg, $header){
    return mail($email, $subject, $msg, $header);
}

function validate_length($errors, $string, $label, $min, $max){

    if(strlen($string) < $min){
        $errors[] = "Your {$label} cannot be less than {$min} characters";
    }
    else if(strlen($string) > $max){
        $errors[] = "Your {$label} cannot be greater than {$max} characters";
    }

    return $errors;
}

function shortenText($text, $chars = 300) {
    $text = $text." ";
    $text = substr($text, 0, $chars);
    $text = substr($text, 0, strrpos($text, ' '));
    $text = $text;
    $text = $text."...";
    return $text;
}


//======
//====== VALIDATION FUNCTIONS --------------------------------------------------
//======


function validate_user_registration(){
    $errors = [];
    $min = 3;
    $max = 50;


    if($_SERVER['REQUEST_METHOD'] == "POST"){

        $first_name        = clean($_POST['first_name']);
        $last_name         = clean($_POST['last_name']);
        $username          = clean($_POST['username']);
        $email             = clean($_POST['email']);
        $password          = clean($_POST['password']);
        $confirm_password  = clean($_POST['confirm_password']);
        $telephone         = clean($_POST['telephone']);
        $address           = clean($_POST['address']);
        $position          = clean($_POST['position']);
        
        //getting the image from the field
        $avatar            = $_FILES['avatar']['name'];
        $avatar_tmp        = $_FILES['avatar']['tmp_name'];

        move_uploaded_file($avatar_tmp,"../admin/img/avatars/$avatar");


        if(email_exists($email)){
            $errors[] = "<p class='error-p'> Sorry that email is already taken.</p>";
        }
        if(username_exists($username)){
            $errors[] = "<p class='error-p'> Sorry that username is already taken.</p>";
        }

        $errors = validate_length($errors, $first_name, "first name", $min, $max);
        $errors = validate_length($errors, $last_name, "last name", $min, $max);
        $errors = validate_length($errors, $email, "email", $min, $max);

        if($password !== $confirm_password){
            $errors[] = "<p class='error-p'> Your passwords do not match.</p>";
        }

        if(!empty($errors)){
            foreach($errors as $error){
                echo validation_errors($error);
            }
        }
        else{
            if(register_user($first_name, $last_name, $username, $email, $password, $telephone, $address, $position, $avatar)){
                set_message("<p class='sucess-p'> Please check your email or spam folder for an activation link.</p>");
            }
            else{
                set_message("<p class='error-p'> Sorry, we could not register you.</p>");
            }
        }
    }
}


//======
//====== REGISTER USER FUNCTIONS -----------------------------------------------
//======


function register_user($first_name, $last_name, $username, $email, $password, $telephone, $address, $position, $avatar){
    $first_name = escape($first_name);
    $last_name  = escape($last_name);
    $username   = escape($username);
    $email      = escape($email);
    $password   = escape($password);
    $telephone  = escape($telephone);
    $address    = escape($address);
    $position   = escape($position);
    $avatar     = escape($avatar);

    if(email_exists($email) || username_exists($username)){
        return false;
    }

    $password = md5($password);
    $validation = md5($username + microtime());

    $sql = "INSERT INTO admin_users (first_name, last_name, username, email, password, telephone, address, admin_position, avatar, validation_code, active) ";
    $sql.= "values ('{$first_name}', '{$last_name}', '{$username}', '{$email}', '{$password}', '{$telephone}', '{$address}', '{$position}', '{$avatar}', '{$validation}', 0)";
    $result = query($sql);
    confirm($result);


    $subject = "Activate Account - Pearl Gems";
    $msg = "Please click the link below to activate your Account
    http://gemexchange.lk/activate.php?email=$email&code=$validation";

    $header = "From: admin@gemexchange.lk";

    send_email($email, $subject, $msg, $header);

    return true;

}

//======
//====== ACTIVATE USER FUNCTIONS -----------------------------------------------
//======

function activate_user(){
    if($_SERVER['REQUEST_METHOD'] == "GET"){
        if(isset($_GET['email'])){
            $email = clean($_GET['email']);
            $validation = clean($_GET['code']);

            $sql = "SELECT admin_id FROM admin_users WHERE email='".escape($_GET['email'])."' AND validation_code='".escape($_GET['code'])."'";
            $result = query($sql);
            confirm($result);

            if(row_count($result) == 1){
                $sql = "UPDATE admin_users SET active = 1, validation_code = 0 WHERE email='".escape($email)."' AND validation_code='".escape($validation)."'";
                $result2 = query($sql);
                confirm($result2);

                set_message("<p class='sucess-p'>Your account has been activated, please login.</p>");
                redirect("login.php");
            }
            else{
                set_message("<p class='error-p'>Sorry, your account could not be activated.</p>");
                redirect("login.php");
            }
        }
    }
}


//======
//====== USER LOGIN FUNCTIONS -----------------------------------------------
//======


function login_user($email, $password, $remember) {

    $sql = "SELECT password, admin_id FROM admin_users WHERE email = '".escape($email)."' AND active = 1";
    $result = query($sql);

    if(row_count($result) == 1){
        $row = fetch_array($result);

        $db_password = $row['password'];

        if(md5($password) === $db_password) {

            if($remember == "on") {
                setcookie('email', $email, time() + 1200);
            }

            $_SESSION['email'] = $email;

            return true;
        }
    }

    return false;
}

//======
//====== VALIDATE USER LOGIN FUNCTIONS -----------------------------------------
//======

function validate_user_login(){
    $errors = [];

    $min = 3;
    $max = 50;

    if(!isset($_POST['email']) && !isset($_POST['password'])){
        return;
    }

    $email             = clean($_POST['email']);
    $password          = clean($_POST['password']);
    $remember          = isset($_POST['remember']);

    if(empty($email)){
        $errors[] = "Email field cannot be empty.";
    }

    if(empty($password)){
        $errors[] = "Password field cannot be empty.";
    }


    if(!empty($errors)){
        foreach($errors as $error){
            echo validation_errors($error);
        }
    }
    else{
        if(login_user($email, $password, $remember)){
            redirect("index.php");
        }
        else{
            echo validation_errors('<p class="error-p">Your credentials are not correct</p>');
        }
    }
}

//======
//====== LOGGED IN FUNCTION ----------------------------------------------------
//======

function logged_in(){
    if(isset($_SESSION['email']) || isset($_COOKIE['email'])){
        return true;
    } else{
        return false;
    }
}

//======
//====== RECOVER PASSWORD ------------------------------------------------------
//======

function recover_password(){
    if($_SERVER['REQUEST_METHOD'] == "POST"){

        if(isset($_SESSION['token']) && $_POST['token'] === $_SESSION['token']) {

            $email = clean($_POST['email']);

            if(email_exists($email)){
                $validation = md5($email + microtime());

                setcookie('temp_access_code', $validation, time() + 900);

                $sql = "UPDATE admin_users SET validation_code = '".escape($validation)."' WHERE email = '".escape($email)."'";
                $result = query($sql);
                confirm($result);

                $subject = "Please reset your password.";
                $message = "Here is your password reset code: {$validation}
                click here to reset your password: http://gemexchange.lk/admin/code.php?email=$email&code=$validation";
                $header = "From: admin@gemexchange.lk";

                if(!send_email($email, $subject, $message, $header)){
                    echo validation_errors("Email counld not be send");
                }

                set_message("<p class='sucess-p'>Please check your email (including spam folder) for a password reset code.</p>");
                redirect("login.php");

            }
            else{
                echo validation_errors("This email does not exist");
            }
        }

    } else {//redirect if the token is invalid
        //redirect("index.php");//this seems to cause problems while testing
    }

    if(isset($_POST['cancel_submit'])){
        redirect("login.php");
    }
}
//======
//====== CODE VALIDATION -------------------------------------------------------
//======

function validate_code() {

    if(isset($_COOKIE['temp_access_code'])){
        if(!isset($_GET['email']) && !isset($_GET['code'])){
            redirect("recover.php");
        }
        else if(empty($_GET['email']) || empty($_GET['code'])){
            redirect("recover.php");
        }
        else{
            if(isset($_POST['code'])){
                $email = clean($_GET['email']);
                $validation = clean($_POST['code']);
                $sql = "SELECT admin_id FROM admin_users WHERE validation_code='".escape($validation)."' AND email='".escape($email)."'";
                $result = query($sql);

                if(row_count($result) == 1){
                    setcookie('temp_access_code', $validation, time() + 300);
                    redirect("reset.php?email=$email&code=$validation");
                }
                else{
                    echo validation_errors("Sorry, wrong validation code");
                }
            }
        }

    }
    else{
        set_message("<p class='error-p'>Sorry your validation cookie has expired.</p>");
        redirect("recover.php");
    }

    if(isset($_POST['code-cancel'])){
        redirect("login.php");
    }
}

//======
//====== PASSWORD RESET --------------------------------------------------------
//======

function password_reset(){
    if(isset($_COOKIE['temp_access_code'])){



        if( isset($_GET['email']) && isset($_GET['code']) ){
            if( isset($_SESSION['token']) && isset($_POST['token']) ){
                if($_POST['token'] === $_SESSION['token']){

                    if($_POST['password'] === $_POST['confirm_password']){
                        $updated_password = md5($_POST['password']);


                        $sql = "UPDATE admin_users SET password='".escape($updated_password)."', validation_code = 0 WHERE email ='".escape($_GET['email'])."'";
                        query($sql);
                        set_message("<p class='sucess-p'>Your password has been updated, please login.</p>");
                        redirect("login.php");
                    }


                }
            }
        }
    }
    else{
        set_message("<p class='error-p'> Sorry your time has expired.  </p>");
        redirect("recover.php");
    }



}


//======
//====== ADMIN PROFILE ------------------------------------------------------
//======

function admin() {
    
    if(logged_in()) {
        $email = $_SESSION['email'];
        $sql = "SELECT * FROM admin_users WHERE email = '".escape($email)."' AND active = 1";
        $result = query($sql);

        $pic = mysqli_fetch_array($result);
        $ad_id             = $pic['admin_id'];
        $username          = $pic['username'];
        $position          = $pic['admin_position'];  
        $avatar            = $pic['avatar'];

        echo '
        
        <div class="dropdown text-left clearfix">
            <a href="admin_profile.php?id='.$ad_id.'">
                <div class="admin-details text-left">
                <span>'.$username.'</span>
                <i class="ion-arrow-down-b"></i>
                <p class="position text-left">'.$position.' &nbsp;</p>
                </div>
            </a>    
            <div id="dropDown" class="dropdown-content">
                <ul class="dropdown-content-ul">

                    <li><a href="admin_profile.php?id='.$ad_id.'"><i class="ion-ios-person"></i> &nbsp;Profile</a></li>
                    <li><a href="admin_settings.php?id='.$ad_id.'"><i class="ion-ios-redo"></i> &nbsp;Settings</a></li>
                    <li><a href="logout.php"><i class="ion-power"></i> &nbsp;Logout</a></li>

                </ul>
            </div>         
        </div>

        <div id="admin-img">
            <a href="admin_profile.php?id='.$ad_id.'">
                <img src="img/avatars/'.$avatar.'" height="40px"/>
            </a>
        </div>
        
        ';
    }
}

function display_orders() {
    
    $sql = "SELECT * FROM orders ORDER BY order_id DESC";
    $result = query($sql);
    confirm($result);
    
    if(row_count($result) == 0) {

        echo 

            '<tr>
                <p class="error-p">No orders to display.</p>
            </tr>'

            ;

    } else {

            while($row = fetch_array($result)) {

            $order_id          = $row['order_id'];
            $order_amount      = $row['order_amount'];
            $order_transaction = $row['order_transaction'];
            $order_currency    = $row['order_currency'];
            $order_status      = $row['order_status'];


            echo '

            <tr>
                <td class="text-left">'.$order_id.'</td>
                <td class="text-left green">$ '.$order_amount.'.00</td>
                <td class="text-left">'.$order_transaction.'</td>
                <td class="text-left">'.$order_currency.'</td>
                <td class="text-left green">'.$order_status.'</td>
                <td class="text-center">
                    <a class="action-links" href="../admin/delete_order.php?id='.$order_id.'"><i class="ion-trash-a"></i></a>
                </td>
            </tr>

            ';

            } 
        
        }
    
}


function display_reports() {

    $sqli = "SELECT * FROM reports ORDER BY report_id DESC";
    $resultr = query($sqli);
    confirm($resultr);
    
    if(row_count($resultr) == 0) {

        echo 

            '<tr>
                <p class="error-p">No reports to display.</p>
            </tr>'

            ;

    } else {

        while($rowr = fetch_array($resultr)) {


            $report_id             = $rowr['report_id'];
            $order_id              = $rowr['order_id'];
            $product_id            = $rowr['product_id'];
            $product_title         = $rowr['product_title'];
            $product_price         = $rowr['product_price'];
            $product_quantity      = $rowr['product_quantity'];


            echo '

                <tr>
                    <td class="text-left">'.$report_id.'</td>
                    <td class="text-left">'.$order_id.'</td>
                    <td class="text-left">'.$product_id.'</td>
                    <td class="text-left">'.$product_title.'</td>
                    <td class="text-left green">$ '.$product_price.'.00</td>
                    <td class="text-left green">'.$product_quantity.'</td>
                    <td class="text-center">
                    <a class="action-links" href="../admin/delete_report.php?id='.$report_id.'"><i class="ion-trash-a"></i></a>
                </td>
                </tr>

            ';
        }
    }

}


function display_products() {
    
    $sqlp = "SELECT * FROM products ORDER BY product_id DESC";
    $resultp = query($sqlp);
    confirm($resultp);

    while($rowp = fetch_array($resultp)) {

        $product_id        = $rowp['product_id'];
        $product_cat       = $rowp['product_cat'];
        $product_title     = $rowp['product_title'];
        $product_img       = $rowp['product_image'];
        $product_price     = $rowp['product_price'];
        $product_desc      = $rowp['product_desc'];
        $product_quantity  = $rowp['product_quantity'];
        
        $sqlc = "SELECT * FROM catagories WHERE cat_id = '".escape($product_cat)."'";
        $resultc = query($sqlc);
        confirm($resultc);
        $rowc = fetch_array($resultc);
        $product_cat_name  = $rowc['cat_title'];
        
        if($product_quantity == '0') {
            $product_quantity = '<p class="error-p">Out of Stock</p>';
        }

        echo '

            <tr>
                <td class="text-left">'.$product_id.'</td>
                <td class="text-center">
                    <p class="product-title">'.$product_title.'</p>
                    <a href="../admin/edit_product.php?id='.$product_id.'"><img class="product-img" src="product_images/'.$product_img.'" width="100px" /></a>
                </td>
                <td class="text-left">'.$product_cat_name.'</td>
                <td class="text-left green">$ '.$product_price.'.00</td>
                <td class="text-left"><div class="short-p">'.$product_desc.'</div></td>
                <td class="text-center">'.$product_quantity.'</td>
                <td class="text-center">
                    <a class="action-links" href="../admin/edit_product.php?id='.$product_id.'"><i class="ion-edit"></i></a>
                    <a class="action-links" href="../admin/delete_product.php?id='.$product_id.'"><i class="ion-trash-a"></i></a>
                </td>
            </tr>

        ';

    }

}


function display_customers() {

    $sqlu = "SELECT * FROM users";
    $resultu = query($sqlu);
    confirm($resultu);

    while($rowu = fetch_array($resultu)) {

        $user_id    = $rowu['user_id'];
        $fname      = $rowu['first_name'];
        $lname      = $rowu['last_name'];
        $uname      = $rowu['username'];
        $mail       = $rowu['email'];
        $tel        = $rowu['telephone'];
        $add        = $rowu['address'];
        $date       = format_date_min($rowu['join_date']);

        echo '

            <tr>
                <td class="text-left">'.$user_id.'</td>
                <td class="text-left">'.$fname.'</td>
                <td class="text-left">'.$lname.'</td>
                <td class="text-left">'.$uname.'</td>
                <td class="text-left">'.$mail.'</td>
                <td class="text-left">'.$tel.'</td>
                <td class="text-left">'.$add.'</td>
                <td class="text-left">'.$date.'</td>
                <td class="text-center"><a href="../admin/delete_customer.php?id='.$user_id.'"><i class="ion-trash-a"></i></a></td>
            </tr>

        ';

    }

}


function display_admins() {

    $sqlu = "SELECT * FROM admin_users ORDER BY admin_id ASC";
    $resultu = query($sqlu);
    confirm($resultu);

    while($rowu = fetch_array($resultu)) {

        $ad_id      = $rowu['admin_id'];
        $fname      = $rowu['first_name'];
        $lname      = $rowu['last_name'];
        $uname      = $rowu['username'];
        $mail       = $rowu['email'];
        $pos        = $rowu['admin_position'];

        echo '
            
            <tr>
                <td class="text-left">'.$ad_id.'</td>
                <td class="text-left">'.$fname.'</td>
                <td class="text-left">'.$lname.'</td>
                <td class="text-left">'.$uname.'</td>
                <td class="text-left">'.$mail.'</td>
                <td class="text-left">'.$pos.'</td>
                <td class="text-center"><a href="../admin/delete_admin.php?id='.$ad_id.'"><i class="ion-trash-a"></i></a></td>
            </tr>
            
        ';

    }

}

function display_blog_posts() {
    
    $sqlb = "SELECT * FROM blog_posts ORDER BY post_date DESC";
    $resultb = query($sqlb);
    confirm($resultb);

    while($rowb = fetch_array($resultb)) {
        
        $post_author      = $rowb['post_author'];
        
        $sqlu = "SELECT * FROM admin_users WHERE admin_id = '".escape($post_author)."'";
        $resultu = query($sqlu);
        confirm($resultu);
        $rowu = fetch_array($resultu);

        $post_id          = $rowb['post_id'];
        $author           = $rowu['username'];
        $post_title       = $rowb['post_title'];
        $post_body        = shortenText($rowb['post_body']);
        $post_date        = format_date_min($rowb['post_date']);

        echo '

            <div class="blog-entry-container">
                    <div class="content">
                       <ul class="container-ul">
                           <a class="dash-a" href="edit_post.php?id='.$post_id.'">
                               <li>
                                   <h5>'.$post_title.'<br /><small>'.$post_date.' - by <span>'.$author.'</span></small></h5>

                                   <div>'.$post_body.'</div>
                               </li>
                           </a>
                       </ul>
                   </div>
               </div>

        ';

    }
    
}

function recent_customers() {

    $sqlc = "SELECT * FROM users ORDER BY join_date DESC LIMIT 0,5";
    $resultc = query($sqlc);
    confirm($resultc);

    while($rowc = fetch_array($resultc)) {

        $user_id          = $rowc['user_id'];
        $username         = $rowc['username'];
        $email            = $rowc['email'];
        $telephone        = $rowc['telephone'];
        //$avatar         = $rowc['email'];
        $join_date        = format_date_only($rowc['join_date']);
        $active           = $rowc['active'];
        
        if($active == '0') {
            $active = '<p class="unavailable-p"><i class="ion-close unavailable-p"></i> inactive</p>';
        } else {
            $active = '<p class="active"><i class="ion-checkmark green"></i> active</p>';
        }

        echo '

            <div class="blog-entry-container">
                <div class="content">
                    <ul class="container-ul">
                        <a class="dash-a" href="#">
                            <li>
                                <img class="customer-avatar" src="../img/avatar/no-image.png" alt="'.$username.'"/>
                                <h5>'.$username.'<br /><small><span>Joined:</span> '.$join_date.'</small></h5>
                                <p>'.$email.'</p>
                                <p>'.$telephone.'</p>
                                <div>'.$active.'</div>
                            </li>
                        </a>
                    </ul>
                </div>
            </div>

        ';

    }

}


//======
//====== DASHBOARD DISPLAY ------------------------------------------------------
//======



?>