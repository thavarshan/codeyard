<?php require_once("components/admin_config.php"); ?>

<?php
    if(logged_in()) {

    } else {
        redirect("login.php");
    }
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">

        <title>Pearl Gems | Admin Dashboard</title>
        <meta name="description" content="The HTML5 Herald">
        <meta name="author" content="SitePoint">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">  

        <!-- Favicn -->
        <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileColor" content="#000000">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#000000">

        <!-- CSS Style Sheets -->
        <?php  include(TEMPLATE_ADMIN . DS . "css.php") ?>
    </head>

    <body>

        <div class="row-full">
            <!-- Sidebar -->
            <div id="sidebar-container" class="col-2 span-2-of-11">
                <div id="sidebar" class="pad">
                   <div class="logo">
                       <a href="index.php" title="Pearl Cluster (PVT) LTD">
                           <img class="framework-logo" src="img/cluster.png" alt="Pearl Cluster" />
                       </a>
                   </div>
                   
                    <div class="sidebar-menu">

                        <ul class="sidebar-menu-ul">
                            
                            <a href="index.php" class="sidebar-menu-a"><li class="sidebar-menu-li"><i class="ion-ios-pulse"></i>Dashboard</li></a>
                            <a href="orders.php" class="sidebar-menu-a"><li class="sidebar-menu-li"><i class="ion-ios-cart-outline"></i>Orders</li></a>
                            <a href="reports.php" class="sidebar-menu-a"><li class="sidebar-menu-li"><i class="ion-stats-bars"></i>Reports</li></a>
                            <a href="products.php" class="sidebar-menu-a"><li class="sidebar-menu-li"><i class="ion-bag"></i>Products</li></a>
                            <a href="categories.php" class="sidebar-menu-a"><li class="sidebar-menu-li"><i class="ion-ios-albums-outline"></i>Categories</li></a>
                            <a href="customers.php" class="sidebar-menu-a"><li class="sidebar-menu-li"><i class="ion-person-stalker"></i>Customers</li></a>
                            <a href="admin_blog.php" class="sidebar-menu-a"><li class="sidebar-menu-li"><i class="ion-ios-paperplane"></i>Blog</li></a>
                            <a href="admins.php" class="sidebar-menu-a"><li class="sidebar-menu-li"><i class="ion-person"></i>Administrators</li></a>

                        </ul>
                    </div>   

                </div>
            </div>

            <!-- Dashboard Content Area -->
            <div id="content-container" class="col-2 span-9-of-11 clearfix">
                <div id="content-container-inner-wrap">

                    <!-- Header -->
                    <div id="content-container-header" class="row-full clearfix">
                        <div class="row clearfix">
                            <div class="col span-1-of-2">
                                <a href="http://gemexchange.lk" target="_blank">
                                    <span id="beta-identifier">Visit Site</span>
                                </a>
                            </div>
                            
                            <div class="col span-1-of-2 text-right">
                                <?php admin(); ?>
                            </div>

                        </div>
                    </div>