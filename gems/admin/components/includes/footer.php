
<!-- Footer -->
<div id="content-container-footer">
    <footer class="row">
        <p>Copyright &copy; <?php echo date('Y'); ?> Pearl Cluster (PVT) LTD | Powered by <strong>BASE.net Framework BETA V 2.3.4.2</strong></p>
    </footer>
</div>
</div>

</div>
</div>

<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/jquery.waypoints.js"></script>
<script src="js/script.js"></script>
<script src="js/tinymce/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script>
</body>

</html>
