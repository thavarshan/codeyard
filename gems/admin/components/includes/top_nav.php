<nav>
   
    <div class="top-header">
        <div class="row-full clearfix">
            <div class="dropdown clearfix">
                <button id="dropBtn" class="dropbtn"><i class="ion-arrow-down-b"></i></button>
                
                <div id="dropDown" class="dropdown-content">
                    <ul class="dropdown-content-ul">
                        <!--  <?php //if(logged_in()):?> -->
                        <li><a href="user.php"><i class="ion-ios-person"></i> &nbsp;<?php

                            $sql = "SELECT * FROM users";
                            $rslt = query($sql);

                            confirm($rslt);

                            $row = fetch_array($rslt);

                            echo $row['username'];
                            ?></a></li>
                        <li><a href="wish_list.php"><i class="ion-ios-heart"></i> &nbsp;Wish List &#40;0&#41;</a></li>
                        <li><a href="checkout.php"><i class="ion-ios-redo"></i> &nbsp;Checkout</a></li>
                        <!--  <?php //endif; ?>  -->
                        
                        <!--  <?php //if(!logged_in()):?> -->
                        <li><a href="login.php"><i class="ion-ios-locked"></i> &nbsp;Login</a></li>
                        <li><a href="register.php"><i class="ion-ios-compose"></i> &nbsp;Register</a></li>
                        <!--  <?php //endif; ?> -->
                    </ul>
                </div>
            </div>

            <div class="dropdownc clearfix">
                <button id="dropBtnc" class="dropbtnc"><i class="ion-ios-cart"></i><span id="cart-items"> &nbsp;items &nbsp;&#40; 12 &#41;</span></button>
                
                <div id="dropDownc" class="dropdown-content-c">
                    <ul class="dropdown-content-ul">
                        <li><p style="text-align: center;">Your shopping cart is empty!</p></li>
                    </ul>
                </div>
            </div>


        </div>
    </div>
   
    <div class="row clearfix">
       <div class="col span-2-of-9">
           <a href="index.php"><img class="logo" src="img/logo1.png" alt="Pearl Gems" /></a>
       </div>
       
       <div class="col span-5-of-9">
           <form action="search.php" method="get" id="search-form">
               <div id="search-container">
                   <input id="search" type="text" name="search" placeholder="Search" >
                   <button id="search-icon" type="submit"><i class="ion-ios-search-strong"></i></button>
               </div>
           </form>
       </div>
       
       <div class="col span-2-of-9">
           <div class="number">
               <strong>71-766-6555</strong>
               <span class="number-avlb">AVAILABLE. 24/7</span>
           </div>
       </div>

    </div>

    <div class="row clearfix">
        <ul class="main-nav">
            <li><a href="index.php">HOME</a></li>
            <li><a href="shop.php">SHOP</a></li>
            <li><a href="services.php">SERVICES</a></li>
            <li><a href="blog.php">BLOG</a></li>
            <li><a href="deals.php">DEALS</a></li>
            <li><a href="contact.php">CONTACT</a></li>
        </ul>
    </div>

</nav>