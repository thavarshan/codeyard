<?php include('components/includes/header.php'); ?>

<!-- content -->
<div class="row-full pad clearfix" id="content-container-body">
   
    <div class="row clearfix">
        <div class="col span-1-of-2">
            <h2>Products on display<small>All product entries on display at Pearl Gems</small></h2>
        </div>
       
        <div class="col span-1-of-2 text-right">
            <a href="add_product.php" class="btn hvr-rectangle-in add_link">+ Add Products </a>
        </div>
        
    </div>
   
    <div class="row">
        <table class="table-fill">
            <thead>
                <tr>
                    <th class="text-left">ID</th>
                    <th class="text-left">Product</th>
                    <th class="text-left">Category</th>
                    <th class="text-left">Price</th>
                    <th class="text-left">Description</th>
                    <th class="text-center">Quantity</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody class="table-hover">
                <?php display_products(); ?>
            </tbody>
        </table>
    </div>
</div>


<?php include('components/includes/footer.php'); ?>