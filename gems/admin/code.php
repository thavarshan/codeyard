<?php include('components/includes/input_header.php'); ?>

<div class="login-box clearfix">
        <form method="post" action="" enctype="multipart/form-data">
            <div class="row">
                <?php display_message(); ?>
                <?php validate_code(); ?>
            </div>

            <div class="row sep text-center">
                <p>Please enter the code sent to you in th email.</p>
            </div>

            <div class="row sep">
                <input type="text" name="code" id="code" placeholder="##########" value="" autocomplete="off" required/>
            </div>

            <div class="row sep">
                <div class="col span-1-of-2">
                    <button type="submit" name="code-cancel" id="code-cancel" class="full-w btn hvr-rectangle-in">
                        Cancel
                    </button>
                </div>

                <div class="col span-1-of-2">
                    <button type="submit" name="code-submit" id="recover-submit" class="full-w btn hvr-rectangle-in">
                        Continue
                    </button>
                </div>
            </div>

            <div class="row">
                <input type="hidden" class="hide" name="token" id="token" value="">
            </div>

            <footer class="row text-left">
                <p>Copyright &copy; <?php echo date('Y'); ?> Pearl Cluster (PVT) LTD<br />Powered by <strong>BASE.net Framework BETA V2.3.4.2</strong></p>
            </footer>
        </form>
</div>

            <?php include('components/includes/input_footer.php'); ?>
