<?php include('components/includes/header.php'); ?>

<!-- content -->
<div class="row-full pad clearfix" id="content-container-body">
    
    <div class="row">
        <div class="col span-1-of-2">
            <h2>List of all Reports<small>reports of all purchases from Pearl Gems Website</small></h2>
        </div>

        <div class="col span-1-of-2">
            &nbsp;
        </div>
    </div>
    
    <div class="row">
        <?php display_message(); ?>
    </div>
    
    <div class="row">
        <table class="table-fill">
            <thead>
                <tr>
                    <th class="text-left">ID</th>
                    <th class="text-left">Order ID</th>
                    <th class="text-left">Product ID</th>
                    <th class="text-left">Product Title</th>
                    <th class="text-left">Product Price</th>
                    <th class="text-left">Product Quantity</th> 
                    <th class="text-left">Action</th>
                </tr>
            </thead>
            <tbody class="table-hover">
                <?php display_reports(); ?>
            </tbody>
        </table>
    </div>

</div>


<?php include('components/includes/footer.php'); ?>