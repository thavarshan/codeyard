<?php 

require_once("../admin/components/admin_config.php");

if(isset($_GET['id'])) {

    $query = query("DELETE FROM admin_users WHERE admin_id = " . escape_string($_GET['id']) . " ");
    confirm($query);
    redirect("admins.php");
    set_message("<script>alert('Admin has been removed!')</script>");

} else {

    redirect("admins.php"); 

}


?>