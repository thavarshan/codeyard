<?php include('components/includes/header.php'); ?>

<!-- content -->
<div class="row-full pad clearfix" id="content-container-body">

    <div class="row">
       <div class="col span-1-of-2">
           <h2>Admins Involved<small>All administrators of Pearl Gems website</small></h2>
       </div>
       
        <div class="col span-1-of-2">
            <a href="admin_register.php" class="btn hvr-rectangle-in add_link">+ Add Admins </a>
        </div>
    </div>

    <div class="row">
        <table class="table-fill">
            <thead>
                <tr>
                    <th class="text-left">ID</th>
                    <th class="text-left">First Name</th>
                    <th class="text-left">Last Name</th>
                    <th class="text-left">Username</th>
                    <th class="text-left">Email</th>
                    <th class="text-left">Position</th>
                    <th class="text-left">Remove Admin</th>
                </tr>
            </thead>
            <tbody class="table-hover">
                <?php display_admins(); ?>
            </tbody>
        </table>
    </div>
</div>


<?php include('components/includes/footer.php'); ?>