<?php include('components/includes/header.php'); ?>

<!-- content -->
<div class="row-full pad" id="content-container-body">
    
    <div class="row">
        <div class="col span-1-of-2">
            <h2>List of all Customers<small>All customers registered on Pearl Gems Website</small></h2>
        </div>

        <div class="col span-1-of-2">
            &nbsp;
        </div>
    </div>

    <div class="row">
        <?php display_message(); ?>
    </div>

    <div class="row">
        <table class="table-fill">
            <thead>
                <tr>
                    <th class="text-left">ID</th>
                    <th class="text-left">First Name</th>
                    <th class="text-left">Last Name</th>
                    <th class="text-left">Username</th>
                    <th class="text-left">Email</th>
                    <th class="text-left">Telephone</th> 
                    <th class="text-left">Address</th>
                    <th class="text-left">Join Date</th>
                    <th class="text-left">Remove</th>
                </tr>
            </thead>
            <tbody class="table-hover">
                <?php display_customers(); ?>
            </tbody>
        </table>
    </div>

</div>


<?php include('components/includes/footer.php'); ?>