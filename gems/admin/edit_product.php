<?php include('components/includes/header.php'); ?>

<?php

    if(isset($_GET['id'])) {

        $query = query("SELECT * FROM products WHERE product_id=" . escape_string($_GET['id']) . " ");
        confirm($query);

        while($row = fetch_array($query)) {

            $update_id        = escape_string($row['product_id']);
            $product_title    = escape_string($row['product_title']);
            $product_cat      = escape_string($row['product_cat']);
            $product_price    = escape_string($row['product_price']);
            $product_desc     = escape_string($row['product_desc']);
            $product_keywords = escape_string($row['product_keywords']);
            $product_quantity = escape_string($row['product_quantity']);
            $product_image    = escape_string($row['product_image']);

        }

    }

    if(isset($_POST['update'])) {

        global $con;

        $product_id       = $update_id;
        $product_title    = escape_string($_POST['product_title']);
        $product_cat      = escape_string($_POST['product_cat']);
        $product_price    = escape_string($_POST['product_price']);
        $product_desc     = escape_string($_POST['product_desc']);
        $product_keywords = escape_string($_POST['product_keywords']);
        $product_quantity = escape_string($_POST['product_quantity']);

        $product_image     = $_FILES['product_image']['name'];
        $product_image_tmp = $_FILES['product_image']['tmp_name'];

        move_uploaded_file($product_image_tmp,"product_images/$product_image");

        if(empty($product_image)) {

            $get_image = query("SELECT product_image FROM products WHERE product_id='$product_id'");
            confirm($get_image);

            while($image = fetch_array($get_image)) {

                $product_image = $image['product_image'];

            }

        }

        $query = "UPDATE products SET product_cat='$product_cat', product_title='$product_title', product_price='$product_price', product_desc='$product_desc', product_image='$product_image', product_quantity='$product_quantity', product_keywords='$product_keywords' WHERE product_id='$update_id'";
        $update = query($query);
        confirm($update);

        if($update){

            redirect("products.php");
            set_message("<script>alert('Product has been updated!')</script>");

        } else {

            set_message("<script>alert('Was unable to update product')</script>");

        }

    }

?>

<!-- content -->
<div class="row-full pad clearfix" id="content-container-body">

    <div class="row">
        <form action="edit_product.php" method="post" enctype="multipart/form-data">

            <div class="row">
                <div class="col span-2-of-2">
                    <input type="text" name="product_title" placeholder="Product Title" value="<?php echo $product_title; ?>" />
                </div>
            </div>

            <div class="row">
                <div class="col span-1-of-2">
                    <select name="product_cat" >
                        <option>Select a Category</option>
                        <?php 

                            global $con; 
                            $get_cats = "SELECT * FROM catagories";
                            $run_cats = mysqli_query($con, $get_cats);

                            while ($row_cats = mysqli_fetch_array($run_cats)){
                                $cat_id    = $row_cats['cat_id']; 
                                $cat_title = $row_cats['cat_title'];

                                echo "<option value='$cat_id'>$cat_title</option>";
                            }

                        ?>
                    </select>
                </div>

                <div class="col span-1-of-2">
                    <input type="number" placeholder="Quantity" name="product_quantity" value="<?php echo $product_quantity; ?>"/>
                </div>
            </div>

            <div class="row">
                <div class="col span-1-of-3">
                    <input type="text" placeholder="Price" name="product_price" value="<?php echo $product_price; ?>"/>
                </div>

                <div class="col span-1-of-3">
                    <input type="text" name="product_keywords" placeholder="Product Tags" value="<?php echo $product_keywords; ?>"/>
                </div>

                <div class="col span-1-of-3">
                    <input type="file" name="product_image" id="product_image" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" multiple />
                    <label class="inputfile-label hvr-rectangle-in" for="product_image">Product Image (Max file size: 2MB)</label>
                </div>
            </div>

            <div class="row">
                <div class="col span-2-of-2">
                    <textarea name="product_desc" id="editor1" style="height:200px;" placeholder="Product Description"></textarea>
                </div>
            </div>

            <div class="row">
                <div class="col span-2-of-2">
                    <button type="submit" class="btn hvr-rectangle-in" name="update">Update</button>
                </div>
            </div>

        </form>
    </div>

</div>

<?php include('components/includes/footer.php'); ?>


<?php

    

?>