<?php include('components/includes/header.php'); ?>

<!-- content -->
<div class="row-full pad clearfix" id="content-container-body">
    
    <?php
        
        if(isset($_GET['id'])){

            global $con; 

            $admin_id = $_GET['id'];
            $query = "SELECT * FROM admin_users WHERE admin_id='$admin_id'";
            $result = mysqli_query($con, $query);
            
            while($row = mysqli_fetch_array($result)){
                
                $admin_id  = $row['admin_id'];
                $fname     = $row['first_name'];
                $lname     = $row['last_name'];
                $uname     = $row['username'];
                $email     = $row['email'];
                $avatar    = $row['avatar'];
                $date      = format_date($row['join_date']);
                $telephone = $row['telephone'];
                $address   = $row['address'];
                $position  = $row['admin_position'];
                $active    = $row['active'];
                
                if($active == 1) {
                    $ac1 = '<b><p class="active">ACTIVE</p></b>';
                } else {
                    $ac1 = '<b><p class="unavailable-p">INACTIVE</p></b>';
                }
                
                if($email == $_SESSION['email']) {

                    echo 

                            '
                    <div class="row clearfix profile">
                        <div class="profile-img">
                            <img src="img/avatars/'.$avatar.'" alt="'.$uname.'" />
                        </div>

                        <a class="profile-btn" href="admin_settings.php?id='.$admin_id.'">
                            <div class="profile-action">
                                <i class="ion-ios-gear"></i>
                            </div>
                        </a>

                        <div class="profile-details">
                            <div class="name"><h1>'.$fname.'<br /> '.$lname.'</h1></div>
                            <div class="profile-position"><h4 class="profile-title">'.$position.'</h4></div>
                            <div></div>
                        </div>
                    </div>

                    <div class="row profile-content">
                        <div class="details-pane">
                            <div class="row">
                                <div class="col span-1-of-4">
                                    <p><strong>First Name:</strong></p>
                                    <p><strong>Last Name:</strong></p>
                                    <p><strong>Username:</strong></p>
                                    <p><strong>Joined:</strong></p>
                                    <p><strong>Email:</strong></p>
                                    <p><strong>Telephone:</strong></p>
                                    <p><strong>Address:</strong></p>
                                    <p><strong>Position:</strong></p>
                                    <p><strong>Admin Status:</strong></p>
                                </div>

                                <div class="col span-2-of-4">
                                    <p><b>'.$fname.'</b></p>
                                    <p><b>'.$lname.'</b></p>
                                    <p class="green">'.$uname.'</p>
                                    <p class="w-space black">'.$date.'</p>
                                    <p><a href="mailto:'.$email.'">'.$email.'</a></p>
                                    <p>'.$telephone.'</p>
                                    <p>'.$address.'</p>
                                    <p>'.$position.'</p>
                                    <div>'.$ac1.'</div>
                                </div>

                                <div class="col span-1-of-4">
                                    <a href="#" class="btn hvr-rectangle-in add_link"><i class="ion-edit"></i> Edit Details</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    ';

                } else {

                    redirect("admins.php");
                
            } 
        } 
        
    }
    ?>
    

</div>


<?php include('components/includes/footer.php'); ?>