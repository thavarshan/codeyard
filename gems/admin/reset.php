<?php include("components/includes/input_header.php"); ?>

    <div class="login-box">
        <form id="register-form" method="post" role="form">
            <div class="row">
                <?php display_message(); ?>
                <?php password_reset(); ?>
            </div>

            <div class="row sep">
                <input type="password" name="password" id="password" placeholder="Password" required>
            </div>

            <div class="row sep">
                <input type="password" name="confirm_password" id="confirm-password" placeholder="Confirm Password" required>
            </div>

            <div class="row sep">
                <button type="submit" class="full-w btn hvr-rectangle-in" name="reset-password-submit" id="reset-password-submit">Reset Password</button>
            </div>
            
            <footer class="row text-left">
                <p>Copyright &copy; <?php echo date('Y'); ?> Pearl Cluster (PVT) LTD<br />Powered by <strong>BASE.net Framework BETA V2.3.4.2</strong></p>
            </footer>
        </form>
</div>

            <!-- Footer -->
            <?php include("components/includes/input_footer.php"); ?>