<?php include("components/includes/input_header.php"); ?>

    <div class="login-box">
        <form method="post" action="" enctype="multipart/form-data">
            <div class="row">
                <?php display_message(); ?>
                <?php validate_user_login();?>
            </div>

            <div class="row sep">
                <input type="email" placeholder="Email" name="email" id="email" required/>
            </div>

            <div class="row sep">
                <input type="password" placeholder="Password" name="password" id="login-password" required/>
            </div>

            <div class="row sep">
                <div class="col span-1-of-2">
                    <a href="recover.php">Forgot Password</a>
                </div>

                <div class="col span-1-of-2 text-right">
                    <a href="http://gemexchange.lk/">Visit Site</a>
                </div>
            </div>

            <div class="row sep">
                <div class="col span-1-of-2">
                    <button class="btn hvr-rectangle-in" type="submit" name="submit">Login</button>
                </div>

                <div class="col span-1-of-2 text-right">
                    <input name="remember" id="remember" type="checkbox" selected/>
                    <label for="remember"> Remember Me</label>
                </div>
            </div>
            
            <footer class="row text-left">
                <p>Copyright &copy; <?php echo date('Y'); ?> Pearl Cluster (PVT) LTD<br />Powered by <strong>BASE.net Framework BETA V2.3.4.2</strong></p>
            </footer>
        </form>
    </div>

<?php include("components/includes/input_footer.php"); ?>                       