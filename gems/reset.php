<?php  require_once("components/config.php") ?>

<?php  include(TEMPLATE_LINK . DS . "header.php") ?>

<section class="section-reset pad">
   
   <div class="row">
       <?php display_message(); ?>
       <?php password_reset(); ?>
   </div>
   
    <div class="row">
        <div class="col span-1-of-3">
            &nbsp;
        </div>
        
        <div class="col span-1-of-3">
            <div class="form-container-2">
                <form id="register-form" method="post" role="form" >

                    <div class="row">
                        <input type="password" name="password" id="password" placeholder="Password" required>
                    </div>
                    <div class="row">
                        <input type="password" name="confirm_password" id="confirm-password" placeholder="Confirm Password" required>
                    </div>
                    <div class="row">
                        <button type="submit" class="btn hvr-rectangle-in" name="reset-password-submit" id="reset-password-submit">Reset Password</button>
                    </div>
                        
                    <div class="row">
                        <input type="hidden" class="hide" name="token" id="token" value="<?php echo token_generator() ?>">
                    </div>
                </form>
            </div>
        </div>
        
        <div class="col span-1-of-3">
            &nbsp;
        </div>
    </div>
    
</section>


<!-- Footer -->
<?php include(TEMPLATE_LINK . DS . "footer.php");?>