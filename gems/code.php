<?php  require_once("components/config.php") ?>

<?php  include(TEMPLATE_LINK . DS . "header.php") ?>

<section class="section-recover pad">
    <div class="row">
        <?php display_message(); ?>
        <?php validate_code(); ?>
    </div>

    <div class="row">
        <div class="col span-1-of-3">
            &nbsp;
        </div>

        <div class="col span-1-of-3">

            <h3>Recover Password</h3>
            <p><Strong>Enter validation code sent you in the email.</Strong></p>

            <div class="form-container-2">
               
                <form id="register-form"  method="post" role="form" autocomplete="off">
                    <div class="row">
                       
                        <input type="text" name="code" id="code" placeholder="##########" value="" autocomplete="off" required/>
                    </div>
                    
                    <div class="row">
                       
                        <div class="col span-1-of-2">
                            <button type="submit" name="code-cancel" id="code-cancel" class="full-w btn hvr-rectangle-in">
                                Cancel
                            </button>
                        </div>
                        
                        <div class="col span-1-of-2">
                            <button type="submit" name="code-submit" id="recover-submit" class="full-w btn hvr-rectangle-in">
                                Continue
                            </button>
                        </div>
                    </div>
                    
                    <div class="row">
                        <input type="hidden" class="hide" name="token" id="token" value="">
                    </div>
                    
                </form>
                
            </div>
        </div>

        <div class="col span-1-of-3">
            &nbsp;
        </div>
    </div>
</section>

<!-- Footer -->
<?php include(TEMPLATE_LINK . DS . "footer.php");?>