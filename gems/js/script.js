$(document).ready (function() {
    
    /* ----------- Main-Slider ----------- */
    
    //configuration
    var width = 1250;
    var animationSpeed = 1000;
    var pause = 3000;
    var currentSlide = 1;
    
    //cache DOM
    var $slider = $('.slider');
    var $sliderContainer = $slider.find('.slides');
    var $slides = $sliderContainer.find('.slide');
    
    var interval;
    
    function startSlider() {
        interval = setInterval(function() {
            $sliderContainer.animate({'margin-left': '-='+width}, animationSpeed, function(){
                currentSlide++;
                if(currentSlide === $slides.length) {
                    currentSlide = 1;
                    $sliderContainer.css('margin-left', 0);
                }
            });
        }, pause);
    }
    
    function stopSlider() {
        clearInterval(interval);
    }
    
    $slider.on('mouseenter', stopSlider).on('mouseleave', startSlider);
    
    startSlider();
    
    
    
    /* ----------- Product-Slider ----------- */
        
    //configuration
    var f_width = 1250;
    var f_animationSpeed = 1000;
    var f_pause = 3000;
    var f_currentSlide = 1;

    //cache DOM
    var $f_slider = $('.f-slider');
    var $f_sliderContainer = $f_slider.find('.f-slides');
    var $f_slides = $f_sliderContainer.find('.f-slide');

    var f_interval;

    function f_startSlider() {
        f_interval = setInterval(function() {
            $f_sliderContainer.animate({'margin-left': '-='+width}, f_animationSpeed, function(){
                f_currentSlide++;
                if(f_currentSlide === $slides.length) {
                    f_currentSlide = 1;
                    $f_sliderContainer.css('margin-left', 0);
                }
            });
        }, pause);
    }

    function f_stopSlider() {
        clearInterval(f_interval);
    }

    $f_slider.on('mouseenter', f_stopSlider).on('mouseleave', f_startSlider);

    f_startSlider();
    
    
    
    function templateObject() {
        
        "use strict";
        
        var _formIdentity = '#formContact';
        var _formUrl = 'components/submit.php';
        
        function _wrapValidation(message) {
            
            "use strict";
            
            return '<p class="error-p">' + message + '</p>'
            
        }
        
        function _validation(validation) {

            "use strict";

            $.each(validation, function(k, v) {
                
                $('#' + k).before(_wrapValidation(v));
                
            });

        }
        
        function _displayMessage(thisForm, message) {

            "use strict";

            thisForm.find('input').after(message);

        }
        
        function _clearFormValidatione(thisForm) {

            "use strict";

            thisForm.find('.error-p').remove();

        }
        
        function _reset(thisForm) {

            "use strict";

            thisForm[0].reset();

        }
        
        function _submitForm() {
            
            "use strict"
            
            $(document).on('submit', _formIdentity, function(e) {
                e.preventDefault();
                e.stopPropagation();
                
                var thisForm = $(this);
                var thisArray = thisForm.serializeArray();
                
                $.post(_formUrl, thisArray, function(data) {
                    
                    if(data) {
                        
                        if(!data.error) {
                            
                            _clearFormValidation(thisForm);
                            _displayMessage(thisForm, data.message);
                            _reset(thisForm);
                            
                        } else if(data.validation) {
                            
                            _clearFormValidation(thisForm);
                            _displayMessage(thisForm, data.message);
                            _validatoin(data.validation);
                            
                        }
                        
                    }
                    
                }, 'json');
                
            });
            
        }
        
        this.init = function() {
            
            "use strict";
            
            $(document).core_css(function() {
                
                _submitForm();
                
            });
            
        }
        
    }
    
    $(function() {
        
        var templateObj = new templateObject();
        templateObj.init();
        
    });
    
    $(function(){


        $("#register_email").on('change', function(){


            var email = $(this).val();



            $.post("ajax_functions.php", {email : email}, function(data){



                $(".db-feedback").html(data);

            });




        });







        // $('#login-form-link').click(function(e) {
        //     $("#login-form").delay(100).fadeIn(100);
        //     $("#register-form").fadeOut(100);
        //     $('#register-form-link').removeClass('active');
        //     $(this).addClass('active');
        //     e.preventDefault();
        // });
        // $('#register-form-link').click(function(e) {
        //     $("#register-form").delay(100).fadeIn(100);
        //     $("#login-form").fadeOut(100);
        //     $('#login-form-link').removeClass('active');
        //     $(this).addClass('active');
        //     e.preventDefault();
        // });



        // $('#register-form').on('submit',function(){



        //     if($('#password').val()!=$('#confirm-password').val()){




        //     alert("Passwords don't match");
        //     return false;
        // }

        //     return true;

        // });




    });
    
});
