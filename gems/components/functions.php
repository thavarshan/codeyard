<?php

//======
//====== HELPER FUNCTIONS -----------------------------------------
//======

function set_message($msg) {
    if(!empty($msg)) {
        $_SESSION['message'] = $msg;
    } else {
        $msg = "";
    }

}

function display_message() {
    if(isset($_SESSION['message'])) {
        echo $_SESSION['message'];
        unset($_SESSION['message']);
    }
}

function clean($string){
    return htmlentities($string);
}

function redirect($location) {
    header("Location: $location ");
}

function  format_date($date){
    return date('g:ia \o\n l jS F Y', strtotime($date));
}

function  format_date_min($date){
    return date('F j, Y - g:i a', strtotime($date));
}

function query($sql) {
    global $con;
    return mysqli_query($con, $sql);
}

function confirm($result) {
    global $con;
    if(!$result) {
        die("QUERY FALIED" . mysqli_error($con));
    }
}

function row_count($result){
    return mysqli_num_rows($result);
}

function escape($string){
    global $con;
    return mysqli_real_escape_string($con, $string);
}

function escape_string($string) {
    global $con;
    return mysqli_real_escape_string($con, $string);
}

function fetch_array($result) {
    return mysqli_fetch_array($result);
}

function token_generator(){
    $token = $_SESSION['token'] = md5(uniqid(mt_rand(), true));
    return $token;
}
function validation_errors($error_message){
    $alert_error_message = "<div class='row'>{$error_message}</div>";
    return $alert_error_message;
}

function shortenText($text, $chars = 300) {
    $text = $text." ";
    $text = substr($text, 0, $chars);
    $text = substr($text, 0, strrpos($text, ' '));
    $text = $text;
    $text = $text."...";
    return $text;
}

function email_exists($email){
    $sql = "SELECT user_id FROM users WHERE email = '$email'";
    $result = query($sql);
    confirm($result);

    if(row_count($result) >= 1){
        return true;
    }
    return false;
}
function username_exists($username){
    $sql = "SELECT user_id FROM users WHERE username = '$username'";
    $result = query($sql);
    confirm($result);

    if(row_count($result) >= 1){
        return true;
    }
    return false;
}

function send_email($email, $subject, $msg, $header){
    return mail($email, $subject, $msg, $header);
}

function last_id() {
    global $con;
    return mysqli_insert_id($con);
}

// getting the user IP address
function getIp() {
    $ip = $_SERVER['REMOTE_ADDR'];

    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }

    return $ip;
}


//getting the categories

function getCats(){

    global $con;

    $get_cats = "SELECT * FROM catagories";

    $run_cats = mysqli_query($con, $get_cats);

    while ($row_cats = mysqli_fetch_array($run_cats)){

        $cat_id = $row_cats['cat_id'];
        $cat_title = $row_cats['cat_title'];

        echo "<li><a class='normal' href='shop.php?cat=$cat_id'>$cat_title</a></li>";


    }


}

//getting the brands

function getBrands(){

    global $con;

    $get_brands = "SELECT * FROM brands";

    $run_brands = mysqli_query($con, $get_brands);

    while ($row_brands=mysqli_fetch_array($run_brands)){

        $brand_id = $row_brands['brand_id'];
        $brand_title = $row_brands['brand_title'];

        echo "<li><a href='shop.php?brand=$brand_id'>$brand_title</a></li>";


    }
}

function getPro(){

    if(!isset($_GET['cat'])){
        if(!isset($_GET['brand'])){

            global $con;

            $get_pro = "SELECT * FROM products ORDER by product_id ASC";

            $run_pro = mysqli_query($con, $get_pro);

            while($row_pro = mysqli_fetch_array($run_pro)){

                $pro_id    = $row_pro['product_id'];
                $pro_cat   = $row_pro['product_cat'];
                $pro_brand = $row_pro['product_brand'];
                $pro_title = $row_pro['product_title'];
                $pro_price = $row_pro['product_price'];
                $pro_image = $row_pro['product_image'];
                $pro_desc  = $row_pro['product_desc'];

                echo

                    '<div class="col-4 span-1-of-3-s">
                    <a href="product.php?pro_id='.$pro_id.'"><div class="product-box">
                            <div>
                                <img class="product-img" src="admin/product_images/'.$pro_image.'" />
                            </div>
                            <div>
                                <h4 class="product-title">'.$pro_title.'</h4>
                                <div class="product-description">'.$pro_desc.'</div>
                            </div>
                            <div class="clearfix">
                                <ul class="product-action">
                                    <li class="action"><a href="cart.php?add='.$pro_id.'"><i class="ion-android-cart"></i></a></li>
                    <li class="action"><a href="#"><i class="ion-heart"></i></a></li>
                    <li class="action"><a href="product.php?pro_id='.$pro_id.'"><i class="ion-ios-paperplane"></i></a></li>
                    </ul>
                    <span class="product-price"><strong>$ '.$pro_price.'</strong></span>
                    </div>
                    </div></a>
                    </div>'

		;

            }
        }
    }

}

function getPopularPro(){

    if(!isset($_GET['cat'])){
        if(!isset($_GET['brand'])){

            global $con;

            $get_pro = "SELECT * FROM products ORDER by product_id DESC LIMIT 0,4";
            $run_pro = mysqli_query($con, $get_pro);

            while($row_pro = mysqli_fetch_array($run_pro)){

                $pro_id    = $row_pro['product_id'];
                $pro_cat   = $row_pro['product_cat'];
                $pro_title = $row_pro['product_title'];
                $pro_price = $row_pro['product_price'];
                $pro_image = $row_pro['product_image'];
                $pro_desc  = $row_pro['product_desc'];

                echo

                    '<div class="col-3 span-1-of-4-s">
                    <a href="product.php?pro_id='.$pro_id.'"><div class="product-box">
                            <div>
                                <img class="product-img" src="admin/product_images/'.$pro_image.'" />
                            </div>
                            <div>
                                <h4 class="product-title">'.$pro_title.'</h4>
                                <div class="product-description">'.$pro_desc.'</div>
                            </div>
                            <div class="clearfix">
                                <ul class="product-action">
                                    <li class="action"><a href="cart.php?add='.$pro_id.'"><i class="ion-android-cart"></i></a></li>
                    <li class="action"><a href="#"><i class="ion-heart"></i></a></li>
                    <li class="action"><a href="product.php?pro_id='.$pro_id.'"><i class="ion-ios-paperplane"></i></a></li>
                    </ul>
                    <span class="product-price"><strong>$ '.$pro_price.'</strong></span>
                    </div>
                    </div></a>
                    </div>'

                    ;

            }
        }
    }

}

function getCatPro(){

    if(isset($_GET['cat'])){

        $cat_id = $_GET['cat'];

        global $con;

        $get_cat_pro = "SELECT * FROM products WHERE product_cat='$cat_id'";
        $run_cat_pro = mysqli_query($con, $get_cat_pro);
        $count_cats  = mysqli_num_rows($run_cat_pro);

        if($count_cats==0){

            echo "<p style='padding:20px;'>No products where found in this category!</p>";

        }

        while($row_cat_pro = mysqli_fetch_array($run_cat_pro)){

            $pro_id    = $row_cat_pro['product_id'];
            $pro_cat   = $row_cat_pro['product_cat'];
            $pro_title = $row_cat_pro['product_title'];
            $pro_price = $row_cat_pro['product_price'];
            $pro_image = $row_cat_pro['product_image'];
            $pro_desc  = $row_cat_pro['product_desc'];

            echo

                '<div class="col-4 span-1-of-3-s">
                    <a href="product.php?pro_id='.$pro_id.'"><div class="product-box">
                            <div>
                                <img class="product-img" src="admin/product_images/'.$pro_image.'" />
                            </div>
                            <div>
                                <h4 class="product-title">'.$pro_title.'</h4>
                                <div class="product-description">'.$pro_desc.'</div>
                            </div>
                            <div class="clearfix">
                                <ul class="product-action">
                                    <li class="action"><a href="cart.php?add='.$pro_id.'"><i class="ion-ios-cart"></i></a></li>
                    <li class="action"><a href="#"><i class="ion-heart"></i></a></li>
                <li class="action"><a href="product.php?pro_id='.$pro_id.'"><i class="ion-ios-paperplane"></i></a></li>
                </ul>
                <span class="product-price"><strong>$ '.$pro_price.'</strong></span>
                </div>
                </div></a>
                </div>'

		    ;

        }

    }

}

//======
//====== BLOG FUNCTIONS --------------------------------------------------
//======



function blog_categories() {

    global $con;

    $sql = "SELECT * FROM blog_category";
    $result = mysqli_query($con, $sql);

    while($row = mysqli_fetch_array($result)) {

        $id             = $row['post_category_id'];
        $category_title = $row['post_category_title'];

        echo "<li><a class='normal' href='blog.php?category=$id'>$category_title</a></li>";
    }

}


function posts(){

        global $con;

        $sql = "SELECT * FROM blog_posts ORDER BY post_id DESC";
        $result = mysqli_query($con, $sql);

        while($row = mysqli_fetch_array($result)){

            $post_id          = $row['post_id'];
            $post_category    = $row['post_category'];
            $post_title       = $row['post_title'];
            $post_img         = $row['post_img'];
            $post             = shortenText($row['post_body']);
            $post_date        = format_date_min($row['post_date']);
            $post_author      = $row['post_author'];

            $sql2 = "SELECT username FROM admin_users WHERE admin_id='$post_author'";
            $result2 = mysqli_query($con, $sql2);
            $sql3 = "SELECT * FROM blog_category WHERE post_category_id='$post_category'";
            $result3 = mysqli_query($con, $sql3);


            while(($row2 = mysqli_fetch_array($result2)) && ($row3 = mysqli_fetch_array($result3))){

                $author_name = $row2['username'];
                $category    = $row3['post_category_title'];
                $category_id = $row3['post_category_id'];

                echo
                    '
                        <div class="row post-container">
                            <div class="col span-2-of-6">
                                <div class="post-img">
                                    <img src="img/blog/'.$post_img.'" />
                                </div>
                            </div>

                            <div class="col span-4-of-6">
                                <div class="post-content">
                                    <div class="post-heading">
                                        <a href="post.php?id='.$post_id.'"><h1 class="post-title">'.$post_title.'</h1></a>
                                    </div>

                                    <div class="post-details">
                                        <p class="post-info">Date: <span class="green">'.$post_date.'</span> &#8226; Author: <a class="normal" href="blog.php?author=723">'.$author_name.'</a> &#8226; Categories: <span><a class="normal" href="blog.php?category='.$category_id.'">'.$category.'</a></span></p>
                                    </div>

                                    <div class="post-para-blog">
                                        <div class="post-cut">'.$post.'</div>
                                    </div>

                                    <div class="post-link">
                                        <a class="btn hvr-rectangle-in up-marg" href="post.php?id='.$post_id.'">Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ';

        }

    }

}

function blog_category_post() {



}


//======
//====== VALIDATION FUNCTIONS --------------------------------------------------
//======

function validate_user_registration() {
    $errors = [];
    $min = 3;
    $max = 50;

    if($_SERVER['REQUEST_METHOD'] == "POST") {

      $first_name        = clean($_POST['first_name']);
      $last_name         = clean($_POST['last_name']);
      $username          = clean($_POST['username']);
      $email             = clean($_POST['email']);
      $password          = clean($_POST['password']);
      $confirm_password  = clean($_POST['confirm_password']);
      $telephone         = clean($_POST['telephone']);
      $address           = clean($_POST['address']);

      if(email_exists($email)){
        $errors[] = "<p class='error-p'> Sorry that email is already taken.</p>";
      }
      if(username_exists($username)){
        $errors[] = "<p class='error-p'> Sorry that username is already taken.</p>";
      }

      $errors = validate_length($errors, $first_name, "first name", $min, $max);
      $errors = validate_length($errors, $last_name, "last name", $min, $max);
      $errors = validate_length($errors, $email, "email", $min, $max);

      if($password !== $confirm_password){
        $errors[] = "<p class='error-p'> Your passwords do not match.</p>";
      }

      if(!empty($errors)){
          foreach($errors as $error) {
            echo validation_errors($error);
          }
      } else {
        if(register_user($first_name, $last_name, $username, $email, $password, $telephone, $address)){
          set_message("<p class='sucess-p'> Please check your email or spam folder for an activation link.</p>");
        }
        else{
          set_message("<p class='error-p'> Sorry, we could not register you.</p>");
        }
      }
    }
}

function validate_length($errors, $string, $label, $min, $max){

  if(strlen($string) < $min){
    $errors[] = "Your {$label} cannot be less than {$min} characters";
  }
  else if(strlen($string) > $max){
    $errors[] = "Your {$label} cannot be greater than {$max} characters";
  }

  return $errors;
}




//======
//====== REGISTER USER FUNCTIONS -----------------------------------------------
//======

function register_user($first_name, $last_name, $username, $email, $password, $telephone, $address){
    $first_name = escape($first_name);
    $last_name  = escape($last_name);
    $username   = escape($username);
    $email      = escape($email);
    $password   = escape($password);
    $telephone   = escape($telephone);
    $address   = escape($address);

    if(email_exists($email) || username_exists($username)){
        return false;
    }

    $password = md5($password);
    $validation = md5($username + microtime());

    $sql = "INSERT INTO users (first_name, last_name, username, email, password, telephone, address, validation_code, active) ";
    $sql.= "values ('{$first_name}', '{$last_name}', '{$username}', '{$email}', '{$password}', '{$telephone}', '{$address}', '{$validation}', 0)";
    $result = query($sql);
    confirm($result);


    $subject = "Activate Account - Pearl Gems";
    $msg = "Please click the link below to activate your Account
    http://gemexchange.lk/activate.php?email=$email&code=$validation";

    $header = "From: noreply@gemexchange.lk";

    send_email($email, $subject, $msg, $header);

    return true;

}

//======
//====== ACTIVATE USER FUNCTIONS -----------------------------------------------
//======

function activate_user(){
    if($_SERVER['REQUEST_METHOD'] == "GET"){
        if(isset($_GET['email'])){
            $email = clean($_GET['email']);
            $validation = clean($_GET['code']);

            $sql = "SELECT user_id FROM users WHERE email='".escape($_GET['email'])."' AND validation_code='".escape($_GET['code'])."'";
            $result = query($sql);
            confirm($result);

            if(row_count($result) == 1){
                $sql = "UPDATE users SET active = 1, validation_code = 0 WHERE email='".escape($email)."' AND validation_code='".escape($validation)."'";
                $result2 = query($sql);
                confirm($result2);

                set_message("<p class='sucess-p'>Your account has been activated, please login.</p>");
                redirect("login.php");
            }
            else{
                set_message("<p class='error-p'>Sorry, your account could not be activated.</p>");
                redirect("login.php");
            }
        }
    }
}


//======
//====== USER LOGIN FUNCTIONS -----------------------------------------
//======


function login_user($email, $password, $remember) {

    $sql = "SELECT password, user_id FROM users WHERE email = '".escape($email)."' AND active = 1";
    $result = query($sql);

    if(row_count($result) == 1){
        $row = fetch_array($result);
        $db_password = $row['password'];

        if(md5($password) === $db_password) {
            if($remember == "on") {
                setcookie('email', $email, time() + 1200);
            }

            $_SESSION['email'] = $email;
            return true;
        }
    }

    return false;
}

//======
//====== VALIDATE USER LOGIN FUNCTIONS -----------------------------------------
//======

function validate_user_login(){
    $errors = [];

    $min = 3;
    $max = 50;

    if(!isset($_POST['email']) && !isset($_POST['password'])){
        return;
    }

    $email             = clean($_POST['email']);
    $password          = clean($_POST['password']);
    $remember          = isset($_POST['remember']);


    $sql = "SELECT user_id FROM users WHERE email = '".escape($email)."' AND active = 1";
    $result = query($sql);
    $row = fetch_array($result);

    $user_id = $row['user_id'];

    if(empty($email)){
        $errors[] = "Email field cannot be empty.";
    }

    if(empty($password)){
        $errors[] = "Password field cannot be empty.";
    }


    if(!empty($errors)){
        foreach($errors as $error){
            echo validation_errors($error);
        }
    }
    else{
        if(login_user($email, $password, $remember)){
            redirect("user.php?id=$user_id");
        }
        else{
            echo validation_errors('<p class="error-p">Your credentials are not correct</p>');
        }
    }
}

//======
//====== LOGGED IN FUNCTION ----------------------------------------------------
//======

function logged_in(){
    if(isset($_SESSION['email']) || isset($_COOKIE['email'])){
        return true;
    } else{
        return false;
    }
}

//======
//====== RECOVER PASSWORD ------------------------------------------------------
//======

function recover_password(){
    if($_SERVER['REQUEST_METHOD'] == "POST"){

        if(isset($_SESSION['token']) && $_POST['token'] === $_SESSION['token']) {

            $email = clean($_POST['email']);

            if(email_exists($email)){
                $validation = md5($email + microtime());

                setcookie('temp_access_code', $validation, time() + 900);

                $sql = "UPDATE users SET validation_code = '".escape($validation)."' WHERE email = '".escape($email)."'";
                $result = query($sql);
                confirm($result);

                $subject = "Please reset your password.";
                $message = "Here is your password reset code: {$validation}
                click here to reset your password: http://gemexchange.lk/code.php?email=$email&code=$validation";
                $header = "From: noreply@gemexchange.lk";

                if(!send_email($email, $subject, $message, $header)){
                    echo validation_errors("Email counld not be send");
                }

                set_message("<p class='sucess-p'>Please check your email (including spam folder) for a password reset code.</p>");
                redirect("login.php");

            }
            else{
                echo validation_errors("This email does not exist");
            }
        }

    } else {//redirect if the token is invalid
        //redirect("index.php");//this seems to cause problems while testing
    }

    if(isset($_POST['cancel_submit'])){
        redirect("login.php");
    }
}
//======
//====== CODE VALIDATION -------------------------------------------------------
//======

function validate_code() {

    if(isset($_COOKIE['temp_access_code'])){
        if(!isset($_GET['email']) && !isset($_GET['code'])){
            redirect("recover.php");
        }
        else if(empty($_GET['email']) || empty($_GET['code'])){
            redirect("recover.php");
        }
        else{
            if(isset($_POST['code'])){
                $email = clean($_GET['email']);
                $validation = clean($_POST['code']);
                $sql = "SELECT user_id FROM users WHERE validation_code='".escape($validation)."' AND email='".escape($email)."'";
                $result = query($sql);

                if(row_count($result) == 1){
                    setcookie('temp_access_code', $validation, time() + 300);
                    redirect("reset.php?email=$email&code=$validation");
                }
                else{
                    echo validation_errors("Sorry, wrong validation code");
                }
            }
        }

    }
    else{
        set_message("<p class='error-p'>Sorry your validation cookie has expired.</p>");
        redirect("recover.php");
    }

    if(isset($_POST['code-cancel'])){
        redirect("login.php");
    }
}

//======
//====== PASSWORD RESET --------------------------------------------------------
//======

function password_reset(){
    if(isset($_COOKIE['temp_access_code'])){



        if( isset($_GET['email']) && isset($_GET['code']) ){
            if( isset($_SESSION['token']) && isset($_POST['token']) ){
                if($_POST['token'] === $_SESSION['token']){

                    if($_POST['password'] === $_POST['confirm_password']){
                        $updated_password = md5($_POST['password']);


                        $sql = "UPDATE users SET password='".escape($updated_password)."', validation_code = 0 WHERE email ='".escape($_GET['email'])."'";
                        query($sql);
                        set_message("<p class='sucess-p'>Your password has been updated, please login.</p>");
                        redirect("login.php");
                    }


                }
            }
        }
    }
    else{
        set_message("<p class='error-p'> Sorry your time has expired.  </p>");
        redirect("recover.php");
    }



}


?>
