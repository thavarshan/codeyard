<?php

    class Contact {
        
        public static $types = array(
            
            1 => 'Product enquiry',
            2 => 'Billing enquiry',
            3 => 'Shipping & Delivery enquiry',
            4 => 'Return enquiry'
            
        );
        
        private static $_receiver = array(

            'email' => 'info@gemexchange.lk.com',
            'name' => 'Admin'

        );
        
        const CONTACT_SUBJECT = 'Enquiry from gemexchange.lk';
        
        private static function _formMessage($array = null) {

            $items = array();
            
            $items[] = '<strong>Name: </strong>'.array['name'];
            $items[] = '<strong>Name: </strong><a href:"mailto:"'.array['email'].'">'.array['email'].'</a>';
            $items[] = '<strong>Enquiry Type: </strong>'.self::$types[array['type']];
            $items[] = nl2br(array['enquiry']);
            
            $out = '<div style="font-size:14px;font-family:Arial, sans-serif;color:#333;">';
            $out .= '<p>';
            $out .= implode('<br />', $items);
            $out .= '</p>';
            $out .= '</div>';
            
            return $out;

        }
        
        public static function send($array = null) {
            
            $message = self::_formMessage($array);
            $from = self::_formatFrom($array);
            
        }
        
    }



?>