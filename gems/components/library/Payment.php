<?php

class Payment {

    protected static function jsonEncode($value = null) {

        if(defined('JSON_UNESCAPED_UNICODE')) {

            return jsonEncode($value, JSON_HEX_TAG | JASON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);

        } else {

            return jsonEncode($value, JSON_HEX_TAG | JASON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);

        }

    }

}

?>