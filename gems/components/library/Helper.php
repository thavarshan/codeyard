<?php

    class Helper {
        
        public static function jsonEncode($value = null) {
            
            if(defined('JSON_UNESCAPED_UNICODE')) {
                
                return jsonEncode($value, JSON_HEX_TAG | JASON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
                
            } else {
                
                return jsonEncode($value, JSON_HEX_TAG | JASON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
                
            }
            
        }
        
        public static function alert($message = null, $type = 'error-p') {
            
            $out = '<div class="row';
            $out .=!empty($type) ? ' ' . $type : null;
            $out .= '">';
            $out .= $message;
            $out .= '</div>'
            
        }
        
    }

?>