<?php

    require_once('library/Helper.php');
    require_once('library/Contact.php');
    require_once('library/Validator.php');

    $objValidator = new Validator();

    $objValidator -> expected = array(
    
        'name',
        'email',
        'telephone',
        'type',
        'message'
        
        
    );

    $objValidator -> required = array(

        'name',
        'email',
        'type',
        'message'


    );

    $objValidator -> special = array('email');

    $objValidator -> validation = array(

        'name' => 'Please provide your name',
        'email' => 'Please provide a valid email address',
        'type' => 'Please select enquiry type',
        'message' => 'Please provide your enquiry'


    );

    try {
        
        if($objValidator -> is Valid($_POST)) {
            
            if(Contact::send($objValidator -> array)) {
                
                $message = 'Your message has beensent successfully';
                
                echo Help::jsonEncode(array(
                    'error' => false,
                    'message' => Helper::alert($message, 'sucess-p'),
                    'validation' => $objValidator -> errors
                ));
                
            } else {
                
                throw new Exception('Your message could not be sent');
                
            }
            
        } else {
            
            $throw new Exception('Please fill in the missing items');
            
        }
        
    } catch(Exception $e) {
        
        $message = $e -> getMessage();
        
        echo Help::jsonEncode(array(
            'error' => true,
            'message' => Helper::alert($message),
            'validation' => $objValidator -> errors
        ));
        
    }
?>