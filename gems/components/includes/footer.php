<footer class="clearfix">
    <div class="row footer-nav-area">
        <div class="col span-1-of-4">
            <h3>Information</h3>
            <ul class="footer-nav">
                <li><a href="about.php">About Us</a></li>
                <li><a href="#">Delivery Information</a></li>
                <li><a href="privacy.php">Privacy Policy</a></li>
                <li><a href="terms.php">Terms &amp; Conditions</a></li>
            </ul>
        </div>

        <div class="col span-1-of-4">
            <div class="heading-panel">
                <h3 class="panel">Customer Service</h3>
            </div>
            <ul class="footer-nav">
                <li><a href="contact.php">Contact Us</a></li>
                <li><a href="returns.php">Returns</a></li>
                <li><a href="sitemap.xml">Site Map</a></li>
            </ul>
        </div>

        <div class="col span-1-of-4">
            <h3>Extras</h3>
            <ul class="footer-nav">
                <li><a href="shop.php">Gallery</a></li>
                <li><a href="#">Gift Vouchers</a></li>
                <li><a href="#">Affiliates</a></li>
                <li><a href="#">Specials</a></li>
            </ul>
        </div>

        <div class="col span-1-of-4">
            <h3>My Account</h3>
            <ul class="footer-nav">
                <?php if(logged_in()) : ?>
                
                <?php
                    $email = $_SESSION['email'];
                    $sql = "SELECT user_id FROM users WHERE email = '".escape($email)."' AND active = 1";
                    $result = query($sql);

                    $pic                 = mysqli_fetch_array($result);
                    $user_id             = $pic['user_id'];
                    
                    echo '
                        <li><a href="user.php?id='.$user_id.'">My Account</a></li>
                        <li><a href="#">Order History</a></li>
                        <li><a href="#">Wish List</a></li>
                        <li><a href="#">Newletter</a></li>
                    ';
                ?>
                    
                <?php endif; ?>
                
                <?php if(!logged_in()) : ?>
                    <li><a href="login.php">My Account</a></li>
                    <li><a href="login.php">Order History</a></li>
                    <li><a href="login.php">Wish List</a></li>
                    <li><a href="login.php">Newletter</a></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>

    <div class="row footer-area-2">
        <div class="col span-1-of-2">
            <ul class="footer-bottom-nav clearfix">
               
                <?php if(logged_in()) : ?>

                <?php
                    $email = $_SESSION['email'];
                    $sql = "SELECT user_id FROM users WHERE email = '".escape($email)."' AND active = 1";
                    $result = query($sql);

                    $pic                 = mysqli_fetch_array($result);
                    $user_id             = $pic['user_id'];

                    echo '
                        <li><a href="user.php?id='.$user_id.'">Account</a></li>
                        <li><a href="blog.php">Blog</a></li>
                        <li><a href="cart.php">View Cart</a></li>
                        <li><a href="#">Site Map</a></li>
                        <li><a href="contact.php">Contact Us</a></li>
                    ';
                ?>
                
                <?php endif; ?>
                
                <?php if(!logged_in()) : ?>
                    <li><a href="login.php">Account</a></li>
                    <li><a href="blog.php">Blog</a></li>
                    <li><a href="login.php">View Cart</a></li>
                    <li><a href="#">Site Map</a></li>
                    <li><a href="contact.php">Contact Us</a></li>
                <?php endif; ?>
                
            </ul>

            <p>Copyright &copy; <?php echo date('Y'); ?> Pearl Cluster (PVT) LTD | All rights reserved.</p>
        </div>

        <div class="col span-1-of-2">
            <img class="payments" src="img/credit_cards.gif">
        </div>
    </div>
</footer>

<!-- Add to Cart function -->
<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/jquery.waypoints.js"></script>
<script src="js/script.js"></script>
</body>
</html>
