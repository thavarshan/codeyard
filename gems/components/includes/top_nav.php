<?php  require_once("cart.php") ?>
<nav>
    <div class="top-header">
        <div class="row-full clearfix">

            <div class="dropdown clearfix">
                <button id="dropBtn" class="dropbtn"><i class="ion-arrow-down-b"></i></button>

                <div id="dropDown" class="dropdown-content">
                    <ul class="dropdown-content-ul">

                        <?php if(logged_in()) {

                            $email = $_SESSION['email'];
                            $sql = "SELECT * FROM users WHERE email = '".escape($email)."' AND active = 1";
                            $result = query($sql);

                            $pic                 = mysqli_fetch_array($result);
                            $user_id             = $pic['user_id'];
                            $username            = $pic['username'];

                        echo

                            '<li><a class="after-login-a" href="user.php?id='.$user_id.'"><i class="ion-ios-person"></i> &nbsp;'.$username.'</a></li>
                            <li><a class="after-login-a" href="checkout.php"><i class="ion-ios-redo"></i> &nbsp;Checkout</a></li>
                            <li><a class="after-login-a" href="logout.php"><i class="ion-power"></i> &nbsp;Logout</a></li>';


                            }
                        ?>

                        <?php if(!logged_in()) : ?>
                            <li>
                                <div  class="form-container-drop">
                                    <form method="post" action="" enctype="multipart/form-data">

                                        <div class="row">
                                            <?php display_message(); ?>
                                            <?php validate_user_login();?>
                                        </div>
                                        <div class="row sep">
                                            <input type="email" placeholder="Email" name="email" id="email" required />
                                        </div>

                                        <div class="row sep">
                                            <input type="password" placeholder="Password" name="password" id="login-password" required />
                                        </div>

                                        <div class="row sep">
                                            <div class="col span-1-of-2">
                                                <input name="remember" id="remember" type="checkbox" checked />
                                                <label for="remember"><span> &nbsp;Remember Me</span> </label>
                                            </div>

                                            <div class="col span-1-of-2 text-right">
                                                <a class="normal" href="recover.php" title="forgot-password">
                                                    Forgot Password?
                                                </a>
                                            </div>
                                        </div>

                                        <div class="row sep">
                                            <div class="col span-1-of-2">
                                                <button class="btn hvr-rectangle-in" type="submit" name="submit">Login</button>
                                            </div>

                                            <div class="col span-1-of-2 text-right">
                                                <a class="normal" href="register.php"><i class="ion-ios-compose"></i>             &nbsp;Register
                                                </a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </li>
                        <?php endif; ?>

                    </ul>
                </div>
            </div>

            <div class="dropdown clearfix">
                <a href="checkout.php">
                    <button id="dropBtnc" class="dropbtnc"><i class="ion-ios-cart"></i><span id="cart-items"> &nbsp;items &nbsp;&#40; <strong><?php echo isset($_SESSION['item_quantity']) ? $_SESSION['item_quantity'] : $_SESSION['item_quantity'] = '0'; ?> &#41;</span></button>
                </a>
            </div>
        </div>
    </div>

    <div class="row clearfix">
       <div class="col span-2-of-9">
           <a href="index.php" title="Pearl Gems"><img class="logo" src="img/logo.jpg" alt="Pearl Gems" title="Pearl Gems"/></a>
       </div>

       <div class="col span-5-of-9">
           <form name="search-form" action="results.php" method="get" id="search-form" enctype="multipart/form-data">
               <div id="search-container">
                   <input id="search" type="text" name="user_query" placeholder="Search" >
                   <button name="search" id="search-icon" type="submit"><i class="ion-ios-search-strong"></i></button>
               </div>
           </form>
       </div>

       <div class="col span-2-of-9">
           <div class="number">
               <strong>71-766-6555</strong>
               <span class="number-avlb">AVAILABLE. 24/7</span>
           </div>
       </div>

    </div>

    <div class="row clearfix">
        <ul class="main-nav">
            <li class="main-nav-li"><a class="main-nav-a" href="index.php">HOME</a></li>

            <li class="main-nav-li shop-dropdown clearfix">
                <a class="main-nav-a" href="shop.php">SHOP <i class="ion-android-arrow-dropdown"></i></a>
                <div id="shop_dropDown" class="shop-dropdown-content row">
                  <div class="col span-1-of-3">
                    <h4 class="title-small">Categories</h4>
                    <ul class="shop-dropdown-content-ul">
                        <?php getCats(); ?>
                    </ul>
                  </div>

                  <div class="shop-dropdown-content-ul col span-2-of-3">
                    <h4 class="title-small">About Us</h4>
                    <p>
                      The key is to drink coconut, fresh coconut, trust me. Learning is cool, but knowing is better, and i know the key to success. They don't want us to win. You do know, you do know that they don't want you to have lunch. I'm keeping it real with you, so what you going do is have lunch
                    </p>
                  </div>
                </div>
            </li>

            <li class="main-nav-li"><a class="main-nav-a" href="services.php">SERVICES</a></li>
            <li class="main-nav-li"><a class="main-nav-a" href="blog.php">BLOG</a></li>
            <li class="main-nav-li"><a class="main-nav-a" href="deals.php">DEALS</a></li>
            <li class="main-nav-li"><a class="main-nav-a" href="contact.php">CONTACT</a></li>
        </ul>
    </div>

</nav>
