<section class="section-featured products pad">
    <div class="row">
       <div class="col span-1-of-2 clearfix">
           <h2 class="heading-line">Featured Products</h2>
           <hr class="sub">
       </div>
       
       <div class="col span-1-of-2">
           
       </div>

    </div>
    
    <div class="row-full">
        <div class="f-slider">
            <ul class="f-slides">
                <li id="one" class="f-slide">
                    <?php getPopularPro(); ?>
                </li>
                
                <li id="two" class="f-slide">
                    <?php getPopularPro(); ?>
                </li>
                
                <li id="three" class="f-slide">
                    <?php getPopularPro(); ?>
                </li>
            </ul>
        </div>
    </div>
</section>