<link rel="stylesheet" href="css/core_css/normalize.css">
<link rel="stylesheet" href="css/core_css/animate.css">
<link rel="stylesheet" href="css/core_css/hover.css">
<link rel="stylesheet" href="css/core_css/ionicons.css">
<link rel="stylesheet" href="css/core_css/grid_custom.css">
<link rel="stylesheet" href="css/core_css/core.css">

<link rel="stylesheet" href="css/page_css/front.css">
<link rel="stylesheet" href="css/page_css/about.css">
<link rel="stylesheet" href="css/page_css/shop.css">
<link rel="stylesheet" href="css/page_css/product.css">
<link rel="stylesheet" href="css/page_css/register.css">
<link rel="stylesheet" href="css/page_css/login.css">
<link rel="stylesheet" href="css/page_css/contact.css">
<link rel="stylesheet" href="css/page_css/checkout.css">
<link rel="stylesheet" href="css/page_css/cart.css">
<link rel="stylesheet" href="css/page_css/blog.css">
