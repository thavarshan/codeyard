<section class="section-popular products pad clearfix">
    <div class="row">
       <div class="col span-1-of-2">
           <h2 class="heading-line">Popular Products</h2>
           <hr class="sub">
       </div>
       
       <div class="col span-1-of-2">
           
       </div>

    </div>
    <div class="row-full pad">
        <div class="popular-container">
            <ul class="popular-ul">
                <?php getPopularPro(); ?>
            </ul>
        </div>
    </div>
</section>