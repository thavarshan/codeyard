<?php  require_once("components/config.php") ?>
<?php  require_once("cart.php") ?>

<!-- Header -->
<?php  include(TEMPLATE_LINK . DS . "header.php") ?>

<?php
    if(logged_in()){

    } else {
        redirect("login.php");
    }
?>

<!-- Content -->
<section class="section-checkout pad clearfix">
    <div class="row">
       
       <?php report(); ?>
       
        <div class="col span-1-of-3"></div>
        
        <div class="col span-1-of-3">
            <div class="row text-center">
                <h1>Thank You</h1>
                <small id="sm">For shopping with us<small>
            </div>
            
            <div class="row pad text-center">
                <a href="shop.php"><button class="btn hvr-rectangle-in" type="button">Continue Shopping</button></a>
            </div>
            
        </div>
        
        <div class="col span-1-of-3"></div>
        
    </div>
</section>


<!-- Footer -->
<?php include(TEMPLATE_LINK . DS . "footer.php");?>