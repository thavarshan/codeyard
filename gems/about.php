<?php  require_once("components/config.php") ?>

<?php  include(TEMPLATE_LINK . DS . "header.php") ?>

<!-- About Section -->
<section class="section-about pad">
   
    <div class="row">
        <h2>About Us</h2>
    </div>
    
    <div class="row">
        <div class="col span-1-of-2">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris pharetra, turpis in convallis iaculis, nulla libero posuere sapien, a viverra nisl erat vel magna. Suspendisse consectetur nisl non orci lacinia, et ornare tortor blandit. Quisque varius mauris id libero lacinia, et sagittis nisl egestas. Suspendisse augue dolor, ultricies eget congue a, accumsan et erat. Vestibulum tincidunt imperdiet justo ut gravida. Phasellus dignissim nulla ut mauris interdum suscipit. Nulla viverra massa eget velit varius, in hendrerit quam gravida.</p><br />

            <p>In ultricies leo quis urna posuere, eget molestie tellus dignissim. Morbi quis arcu suscipit, accumsan purus ut, interdum nunc. Vestibulum commodo, ipsum a tristique lacinia, felis nulla euismod sapien, eget placerat elit enim non lectus. Etiam a enim quam. Vestibulum venenatis leo arcu, at fringilla sapien convallis eu. Vestibulum erat nibh, efficitur ac semper sit amet, tincidunt id dui. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Quisque posuere urna id lorem dictum, nec ultricies odio suscipit. Aenean eget lacus ut felis dignissim fermentum ut a felis. Suspendisse a lectus tincidunt, accumsan sapien vel, scelerisque turpis.</p>
        </div>
        
        <div class="col span-1-of-2 left-pad">
            <img src="img/gems.jpg" alt="Saphire" width="100%" />
        </div>
    </div>
    
    <div class="row">
        <div class="col-2 span-6-of-12">
            <div id="passion-container">
                <h1 class="clearfix">Passion</h1>
                <p>When it comes to your future, there’s one thing you’re sure about: your passion for technology. That’s what makes you a great candidate for a career behind a computer screen! But the next step gets a bit tricky. If you’re like many others in your shoes, it’s difficult to decide between becoming a web designer versus a web developer.
                These two titles look similar at first glance but each has its own distinct responsibilities and deliverables. Knowing the difference between them is critical to making the right career decision.</p>
            </div>
        </div>
        
        <div class="col-2 span-6-of-12">
            <div id="style-container">
                <h1 class="clearfix">Style</h1>
                <p>In need of some web design inspiration? Well, you’ve come to the right place! Here’s a new collection of web designs that we’ve come across over the last couple of weeks. Ranging from simple and elegant landing pages for new products and services to highly advanced e-commerce websites for larger online companies and organizations. Beautifully designed and developed sites using the latest techniques such as CSS3, HTML5 and jQuery to set the new online trends in this digital era.</p>
            </div>
        </div>
    </div>
</section>

<!-- Footer -->
<?php include(TEMPLATE_LINK . DS . "footer.php");?>