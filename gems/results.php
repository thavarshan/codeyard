<?php  require_once("components/config.php") ?>

<?php  include(TEMPLATE_LINK . DS . "header.php") ?>

<section class="section-results pad">
    <div class="row">
        <?php 

    if(isset($_GET['search'])){

        $search_query = $_GET['user_query'];

        $get_pro = "SELECT * FROM products WHERE product_keywords LIKE '%$search_query%'";

        $run_pro = mysqli_query($con, $get_pro); 

        $count_cats = mysqli_num_rows($run_pro);
        
        while($row_pro=mysqli_fetch_array($run_pro)){

            $pro_id = $row_pro['product_id'];
            $pro_cat = $row_pro['product_cat'];
            $pro_brand = $row_pro['product_brand'];
            $pro_title = $row_pro['product_title'];
            $pro_price = $row_pro['product_price'];
            $pro_image = $row_pro['product_image'];
            $pro_desc = $row_pro['product_desc'];

            if($count_cats == 0){

                echo "<p style='padding:20px;'>No such products where found!</p>";

            } else {

                echo 
                    '<div class="col-4 span-1-of-3-s">
                <a href="product.php?pro_id='.$pro_id.'"><div class="product-box">
                    <div>
                        <img class="product-img" src="admin/product_images/'.$pro_image.'" />
                    </div>
                    <div>
                        <h4 class="product-title">'.$pro_title.'</h4>
                        <div class="product-description">'.$pro_desc.'</div>
                    </div>
                    <div class="clearfix">
                        <ul class="product-action">
                            <li class="action"><a href="index.php?add_cart=$pro_id"><i class="ion-android-cart icon"></i></a></li>
                            <li class="action"><a href="#"><i class="ion-heart icon"></i></a></li>
                            <li class="action"><a href="product.php?pro_id='.$pro_id.'"><i class="ion-ios-paperplane icon"></i></a></li>
                        </ul>
                        <span class="product-price"><strong>$ '.$pro_price.'</strong></span>
                    </div>
                    </div></a>
                </div>'
                    ;

            }

        } 

        

        
    }
        ?>
    </div>
</section>

<!-- Footer -->
<?php include(TEMPLATE_LINK . DS . "footer.php");?>