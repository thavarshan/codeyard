<?php  require_once("components/config.php") ?>

<!-- Header -->
<?php  include(TEMPLATE_LINK . DS . "header.php") ?>

<?php
    if(logged_in()){

    } else {
        redirect("login.php");
    }
?>

<!-- Content -->
<section class="section-checkout pad clearfix">
    <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">

    <div class="row">
        <h2 id="cart-title">Shopping Cart</h2>
    </div>


    <div class="row">

        <div class="col span-6-of-9 cart-area">
            <div class="row">
                <h3 class="item-title">Cart</h3>
                <hr>
            </div>

            <div class="row">

            </div>

           <div class="row">
               <table class="table-fill">
                   <thead>
                       <tr>
                           <th>Item</th>
                           <th>Price</th>
                           <th>Quantity</th>
                           <th>Total</th>
                           <th>Action</th>
                       </tr>
                   </thead>
                   <tbody class="table-hover">
                       <?php cart(); ?>
                   </tbody>
               </table>
           </div>
            <div class="row">
                <?php display_message(); ?>
            </div>
        </div>

        <div class="col span-3-of-9 summary-area">
            <div class="row">
                <h3 class="item-title">Order Summary</h3>
                <hr>
            </div>

            <div class="row">
                <table class="table-fill">
                    <tbody>
                        <tr>
                            <td class="half">Total Items</td>
                            <td class="text-right">
                                <?php echo isset($_SESSION['item_quantity']) ? $_SESSION['item_quantity'] : $_SESSION['item_quantity'] = '0'; ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="half">Shipping: 5 - 7 business days
                                <p class="legal">Will be given when your order is processed.</p>
                            </td>
                            <td class="text-right">$ 0.00</td>
                        </tr>
                        <tr>
                            <td class="half">Tax
                                <p class="legal">Actual tax will be calculated when your order is processed.</p>
                            </td>
                            <td class="text-right">$ 0.00</td>
                        </tr>
                        <tr>
                            <td class="total half"><strong>Total</strong></td>
                            <td class="total text-right"><strong>$ <?php echo isset($_SESSION['item_total']) ? $_SESSION['item_total'] : $_SESSION['item_total'] = '0'; ?></strong></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="row payment">

                    <input type="hidden" name="cmd" value="_cart">
                    <input type="hidden" name="business" value="thavarshan-facilitator@hotmail.com">
                    <button type="submit" name="upload" class="btn hvr-rectangle-in"><i class="ion-ios-cart-outline"></i> Order Now</button>

                <img class="pay-accept-img" src="img/credit_cards.gif" alt="paypal" />
            </div>
        </div>
    </div>
    </form>
</section>


<!-- Footer -->
<?php include(TEMPLATE_LINK . DS . "footer.php");?>
