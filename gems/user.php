<?php  require_once("components/config.php") ?>

<?php  include(TEMPLATE_LINK . DS . "header.php") ?>

<?php
    if(logged_in()){

    } else {
        redirect("login.php");
    }
?>

<section>
    <div class="row">
        <?php

            if(isset($_GET['id'])){

                global $con;

                $user_id = $_GET['id'];
                $query = "SELECT * FROM users WHERE user_id='$user_id'";
                $result = mysqli_query($con, $query);

                while($row = mysqli_fetch_array($result)){

                    $user_id   = $row['user_id'];
                    $uname     = $row['username'];
                    $fname     = $row['first_name'];
                    $lname     = $row['last_name'];
                    $uname     = $row['username'];
                    $email     = $row['email'];
                    $date      = $row['join_date'];
                    $telephone = $row['telephone'];
                    $address   = $row['address'];

                    if($email == $_SESSION['email']) {

                        echo

                            '
                            <div class="row clearfix profile">
                                <div class="profile-img">

                                </div>

                                <a class="profile-btn" href="admin_settings.php?id='.$user_id.'">
                                    <div class="profile-action">
                                        <i class="ion-ios-gear"></i>
                                    </div>
                                </a>

                                <div class="profile-details">
                                    <div class="name"><h1>'.$fname.'<br /> '.$lname.'</h1></div>
                                </div>
                            </div>

                            <div class="row profile-content">
                                <div class="details-pane">
                                    <div class="row">
                                        <div class="col span-1-of-4">
                                            <p><strong>First Name:</strong></p>
                                            <p><strong>Last Name:</strong></p>
                                            <p><strong>Username:</strong></p>
                                            <p><strong>Joined:</strong></p>
                                            <p><strong>Email:</strong></p>
                                            <p><strong>Telephone:</strong></p>
                                            <p><strong>Address:</strong></p>
                                        </div>

                                        <div class="col span-1-of-4">
                                            <p>'.$fname.'</p>
                                            <p>'.$lname.'</p>
                                            <p class="black">'.$uname.'</p>
                                            <p class="green">'.$date.'</p>
                                            <p><a href="mailto:'.$email.'">'.$email.'</a></p>
                                            <p>'.$telephone.'</p>
                                            <p>'.$address.'</p>
                                        </div>

                                        <div class="col span-1-of-4"></div>

                                        <div class="col span-1-of-4">
                                            <a href="#" class="btn hvr-rectangle-in add_link"><i class="ion-edit"></i> Edit Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    ';


                    }
                }

            }
        ?>
    </div>
</section>



<!-- Footer -->
<?php include(TEMPLATE_LINK . DS . "footer.php");?>
