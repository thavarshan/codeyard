<?php  require_once("components/config.php") ?>
<?php  require_once("cart.php") ?>
<!doctype html>

<html lang="en">

    <head>
        <meta charset="utf-8">

        <title>GEMs | Jewellery Gallore</title>
        <meta name="description" content="The HTML5 Herald">
        <meta name="author" content="SitePoint">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700,700i,900,900i|Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">

        <!-- Css Style Sheets -->
        <link rel="stylesheet" href="css/core_css/normalize.css">
        <link rel="stylesheet" href="css/core_css/animate.css">
        <link rel="stylesheet" href="css/core_css/hover.css">
        <link rel="stylesheet" href="css/core_css/ionicons.css">
        <link rel="stylesheet" href="css/core_css/grid_custom.css">
        <link rel="stylesheet" href="css/core_css/core.css">

        <link rel="stylesheet" href="css/page_css/front.css">
        <link rel="stylesheet" href="css/page_css/about.css">
        <link rel="stylesheet" href="css/page_css/register.css">
        <link rel="stylesheet" href="css/page_css/login.css">
        <link rel="stylesheet" href="css/page_css/contact.css">




        <!--[if lt IE 9]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
<![endif]-->
    </head>

    <body>
        <div class="top-header">
            <div class="row-full clearfix">
                <div class="dropdown">
                    <button class="dropbtn"><i class="ion-ios-more"></i></button>

                    <div class="dropdown-content">
                        <ul class="dropdown-content-ul">
                            <li><a href="#"><i class="ion-ios-person"></i> &nbsp;My Account</a></li>
                            <li><a href="#"><i class="ion-ios-heart"></i> &nbsp;Wish List &#40;0&#41;</a></li>
                            <li><a href="#"><i class="ion-ios-redo"></i> &nbsp;Checkout</a></li>
                            <li><a href="#"><i class="ion-ios-locked"></i> &nbsp;Login</a></li>
                            <li><a href="#"><i class="ion-ios-compose"></i> &nbsp;Register</a></li>
                        </ul>
                    </div>
                </div>

                <div class="dropdown">
                    <button class="dropbtn"><i class="ion-ios-cart"></i><span id="cart-items"> &nbsp;items</span></button>

                    <div class="dropdown-content">
                        <ul class="dropdown-content-ul">
                          <?php
                            
                          ?>

                        </ul>
                    </div>
                </div>


            </div>
        </div>
        <header>

        </header>

        <div class="row">

            <div class="dropdown-content-t">

            </div>

        </div>
    </body>
</html>
