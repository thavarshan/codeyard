<?php  require_once("components/config.php") ?>
<?php  include(TEMPLATE_LINK . DS . "header.php") ?>

<!-- Page Content -->

<?php  include(TEMPLATE_LINK . DS . "slider.php") ?>
        
        <section class="section-special pad">
            <div class="row">
                
            </div>
        </section>
        
<?php  include(TEMPLATE_LINK . DS . "featured.php") ?>

        <section class="section-about pad">
            <div class="row">
               
                <div class="col-2 span-6-of-12">
                    <div class="row clearfix">
                        <h2 class="green">Pearl Gems</h2>
                        
                    </div>
                    
                    <div class="row">
                        <p>Aliquam pellentesque odio efficitur eleifend consequat. Phasellus fermentum blandit tincidunt. Ut at cursus arcu. Pellentesque condimentum, velit sed imperdiet viverra, tortor odio vehicula ligula, eu feugiat nisl leo non tortor. Ut sed mi nisi. Sed non aliquet libero. Maecenas ut iaculis lorem. Donec bibendum enim risus, id egestas orci tempor ac.</p><br />
                        
                        <p>Sed eu blandit ligula, eu viverra sapien. Praesent commodo ac ex ac convallis. Nam non tortor lorem. Ut felis arcu, auctor et risus vel, imperdiet dapibus mauris. Nunc pellentesque, nulla cursus fringilla consequat, metus felis semper ipsum, vel mollis sem metus nec elit. Fusce dictum finibus dolor, sagittis gravida lectus pulvinar sed. Donec a facilisis mauris. Praesent urna ipsum, gravida in lacus nec, accumsan faucibus massa.</p>
                    </div>
                </div>
                
                <div class="col-2 span-6-of-12">
                    <img src="img/introduce02-672x443.png" width="100%" />
                </div>
            </div>
        </section>

<?php  include(TEMPLATE_LINK . DS . "latest.php") ?>

        <section class="section-about">
            <div class="row">

                <div class="col-2 span-6-of-12">
                    <img src="img/introduce01-956x658.png" width="100%" />
                </div>

                <div class="col-2 span-6-of-12  pad">
                    <div class="row clearfix">
                        <h2 class="green">Gemme Inspired</h2>

                    </div>

                    <div class="row">
                        <p>Sed eu blandit ligula, eu viverra sapien. Praesent commodo ac ex ac convallis. Nam non tortor lorem. Ut felis arcu, auctor et risus vel, imperdiet dapibus mauris. Nunc pellentesque, nulla cursus fringilla consequat, metus felis semper ipsum, vel mollis sem metus nec elit. Fusce dictum finibus dolor, sagittis gravida lectus pulvinar sed. Donec a facilisis mauris. Praesent urna ipsum, gravida in lacus nec, accumsan faucibus massa</p>
                    </div>
                </div>

            </div>
        </section>

<?php  include(TEMPLATE_LINK . DS . "popular.php") ?>

        <section class="section-info pad">

        </section>
        <!-- /.container -->

<?php  include(TEMPLATE_LINK . DS . "footer.php") ?>