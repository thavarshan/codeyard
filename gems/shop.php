<?php  require_once("components/config.php") ?>

<?php  include(TEMPLATE_LINK . DS . "header.php") ?>

<section class="section-shop pad products">

    <div class="row">
        <div class="col span-8-of-11">
            <div class="row-full">

               <?php
                if(!isset($_GET['cat'])){

                global $con; 

                $get_pro = "SELECT * FROM products ORDER by product_id DESC";

                $run_pro = mysqli_query($con, $get_pro); 

                while($row_pro = mysqli_fetch_array($run_pro)){

                $pro_id = $row_pro['product_id'];
                $pro_cat = $row_pro['product_cat'];
                $pro_title = $row_pro['product_title'];
                $pro_price = $row_pro['product_price'];
                $pro_image = $row_pro['product_image'];
                $pro_desc = $row_pro['product_desc'];

                echo 

                '<div class="col-4 span-1-of-3-s">
                <a href="product.php?pro_id='.$pro_id.'"><div class="product-box">
                    <div>
                        <img class="product-img" src="admin/product_images/'.$pro_image.'" />
                    </div>
                    <div>
                        <h4 class="product-title">'.$pro_title.'</h4>
                        <div class="product-description">'.$pro_desc.'</div>
                    </div>
                    <div class="clearfix">
                        <ul class="product-action">
                            <li class="action"><a href="cart.php?add='.$pro_id.'"><i class="ion-ios-cart"></i></a></li>
                            <li class="action"><a href="#"><i class="ion-heart"></i></a></li>
                            <li class="action"><a href="product.php?pro_id='.$pro_id.'"><i class="ion-ios-paperplane"></i></a></li>
                        </ul>
                        <span class="product-price"><strong>$ '.$pro_price.'</strong></span>
                    </div>
                    </div></a>
                </div>'

                ;

                }
                }
                ?>
                <?PHP getCatPro(); ?>
            </div>
        </div>
        
        <div class="col span-3-of-11">
            <div class="sidebar-box">
                <div class="row">
                    <div class="catagories">
                        <h2>Categories</h2>
                    </div>
                </div>
                
                <div class="row">
                    <ul class="shop-catagories-ul">
                        <li><a class="normal" href='shop.php'>All</a>
                        <?php getCats(); ?>
                    </ul>
                </div>
            </div>
            <!--
            <div class="sidebar-box refine-search">
               <form>
                    <div class="row">
                        <div>
                            <h2>Refine Search</h2>
                        </div>
                    </div>

                    <div class="row">
                        <div><span class="shop-lead-head">Price</span></div>
                        <div><input type="checkbox"><label> $0.00 - $99.99</label></div>
                        <div><input type="checkbox"><label> $100.00 - $199.00</label></div>
                        <div><input type="checkbox"><label> $200.00 and above</label></div>
                    </div>

                    <div class="row">
                        <div><span class="shop-lead-head">Color</span></div>
                        <div><input type="checkbox"><label> Blue</label></div>
                        <div><input type="checkbox"><label> White</label></div>
                        <div><input type="checkbox"><label> Red</label></div>
                    </div>
                    
                    <div class="row">
                        <button type="submit" class="btn hvr-rectangle-in">Refine Search</button>
                    </div>
                </form>
            </div> -->
        </div>
    </div>     
</section>

<!-- Footer -->
<?php include(TEMPLATE_LINK . DS . "footer.php");?>