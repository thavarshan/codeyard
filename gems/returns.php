<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Return policy</title>

<style type="text/css">
<!--
body {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
}
-->
</style>
</head>

<body>
<p><strong>YOUR COMPANY HERE - Return policy</strong></p>
<h3>General Terms</h3>
<p>All products sold by  - YOUR COMPANY HERE -  are brand new and are manufactured to meet the highest industry standards. All prices and specifications are subject to change without notice. Products are shipped F.O.B. (Freight On Board) and become the sole property of the purchaser upon delivery to the specified shipping agent. For shipping damage, customers should file claim to their carrier immediately. Any discrepancy, including wrong items or missing items, should be reported to  - YOUR COMPANY HERE -  within 24 hours. All packages will be shipped the following business day. Because of the nature of the Internet, online ordering your purchase from  - YOUR COMPANY HERE -  acknowledges that you have read and agree to these terms and conditions. All brands and product names mentioned are trademarks and/or registered trademarks of their respective holders.</p>
<h3> - YOUR COMPANY HERE -  Return Merchandise Authorization (RMA) Policy</h3>
<ul>
  <li>
    <p>All order changes or cancellations should be reported prior to shipping and by phone on 1-555-555-5555. Email is not accepted.</p>
  </li>
  <li>
    <p>Brand name items may be returned for a replacement or a complete refund.</p>
  </li>
  <li>
    <p>Generic items may be returned for replacement only.</p>
  </li>
  <li>
    <p>All returned parts may be subject to quality, operation, and/or performance tests by  - YOUR COMPANY HERE -  testing facility or by a third party authorized by  - YOUR COMPANY HERE - . Upon the explained test(s) results, nondefective generic items may be subject to a 15% restocking fee.</p>
  </li>
  <li>
    <p>Defective return does not include returns for incompatibility.</p>
  </li>
  <li>
    <p> - YOUR COMPANY HERE -  reserves the right to replace defective parts or to issue a refund if  - YOUR COMPANY HERE -  sees more fit.</p>
  </li>
  <li>
    <p>All refunds after one week from the time item is received will be of the original value or the current market price, whichever is less.</p>
  </li>
  <li>
    <p>There will be no refund for shipping under any circumstances.</p>
  </li>
  <li>
    <p>All products, unless otherwise stated, are covered by their respective manufacturer's warranties. Within thirty (30) days, we will repair or replace, at our sole discretion, any product that is deemed defective. After 30 days, the manufacturer's warranty process must be followed.</p>
  </li>
  <li>
    <p>All returns require prior authorization and must be returned in the original packing with all disks, cables, accessories, and documentation, including manuals, warranties, and a copy of original purchase invoice. To request a Return Merchandise Authorization, call (555-555-5555), obtain an RMA number, and clearly write the number on both the invoice copy enclosed with the returned merchandise and on the packing from outside. Please keep the RMA number and reference it when calling to check on the status of your return. All calls requesting RMA should be in the time designed for them (10:00 AM to 4:00 PM Monday through Friday, excluding the national and religious holidays). Incomplete or unauthorized returns will be refused and will be returned to you.</p>
  </li>
</ul>
<h3>What This Warranty Does Not Cover</h3>
<p>This warranty does not cover: installation or service of product; conditions resulting from consumer mishandling such as improper maintenance or misuse, abuse, accident, or alteration; all plastic surfaces and all other exposed parts that are scratched or damaged due to normal use; products which have had the serial number removed or made illegible; products rented to others.</p>
<h3>Limits and Exclusions</h3>
<p>There are no expressed or implied warranties except as listed above.  - YOUR COMPANY HERE -  shall not be liable for special, incidental, consequential or punitive damages, including, without limitation, direct or indirect damages for personal injury, loss of goodwill, profits or revenue, loss of use from this product or any associated equipment, cost of substitute equipment, downtime cost, loss of data, programs or business information, or any other losses, or claims of any party dealing with buyers from such damages, resulting from the use of or inability to use this product or arising from breach of warranty or contract, negligence, or any other legal theory. All expressed and implied warranties, including the warranties of merchantability and fitness for a particular purpose, are limited to the applicable warranty period set forth above.</p>
<h3>State and Provincial Law Rights</h3>
<p>Some states and provincial laws do not allow the exclusion or limitation of incidental or consequential damages, or limitations on how long an implied warranty lasts, or the exclusion of warranty in certain situations, so the above limitations or exclusions may not apply to you. This warranty gives you specific legal rights, and you may have other rights, which vary, from state to state and from province to province.</p>
<h3>Software RMA Policy</h3>
<p>We can only accept unopened boxed software for return within 7 days of the purchase date. Defective merchandise may be exchanged for the same item.</p>
<ul>
  <li>
    <p>All returned products must be accompanied by a Return Merchandise Authorization (RMA) number. We do not accept returns without an RMA. To obtain an RMA, send us an email <a href="rma@ - YOUR COMPANY HERE - .com.">rma<em>@ - YOUR COMPANY HERE - .com</em>.</a></p>
  </li>
  <li>
    <p>Please include the following information in your email.<br />
      <br />
      Full name of purchaser (i.e., name of buyer)<br />
      <br />
      Email address used to place order (i.e., <em>NAME@ - YOUR COMPANY HERE - .com</em>)</p>
  </li>
</ul>
<p>Your four- or five-digit order number If the product is opened or unopened Reason for return</p>
<p>If the product is defective or damaged, please tell us if you want an exchange for the same product</p>
<ul>
  <li>
    <p>Once you have an RMA, ship the product back to us. Make sure you fill out the back of your yellow copy of your invoice (this is your receipt) and sign it. You'll need to cover the cost of shipping, unless given alternative instruction from an authorized Returns Representative. Please use a shipping company that can track packages. We are not responsible for lost or stolen merchandise.</p>
  </li>
  <li>
    <p>When your return has been received and processed, we will credit your account or ship you a new copy of your software. Any items marked &quot;Back-ordered&quot; are temporarily out of stock. We will ship them to you immediately when they are available.</p>
  </li>
</ul>
<h3>Back-order Policy</h3>
<p>If you order an out-of-stock software title from us, we will email you when we expect that product to be available. You will be among the first to receive the item.  - YOUR COMPANY HERE -  will not charge your credit card for back-ordered items until we ship your order. No additional shipping and handling charges will be added when your order ships. Products on back-order will be shipped using the shipping method you chose. If you would like to change your shipping method, please send email to<em><a href="mailto:sup-port@acmeonline.com">support@ - YOUR COMPANY HERE - online.com</a> </em>and we will change it for you.</p>
</body>
</html>
