<?php  require_once("components/config.php") ?>

<?php  include(TEMPLATE_LINK. DS . "header.php") ?>

    <!-- Contact Section -->
    <section class="section-contact pad">
       
        <div class="row"></div>
        
        <div class="row">
           
            <div class="col span-1-of-2">
                <form method="post" id="formContact">
                   <div class="row">
                       <h3>Contact Form</h3>
                   </div>

                    <div class="row">

                        <div class="col span-1-of-2">
                            <input name="name" type="text" id="n" placeholder="Name" required />
                        </div>

                        <div class="col span-1-of-2">
                            <input name="email" type="email" id="e" placeholder="E-mail" required />
                        </div>
                    </div>
                    
                    <div class="row">

                        <div class="col span-1-of-2">
                            <input name="telephone" type="tel" id="t" placeholder="Phone" />
                        </div>

                        <div class="col span-1-of-2">
                            <select>
                                <option value="">Select one</option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col span-2-of-2">
                            <textarea name="message" id="m" placeholder="Message"></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col span-2-of-2">
                            <button type="submit" class="btn hvr-rectangle-in" id="btn-submit">
                                <i class="ion-ios-paperplane-outline"></i>
                                Send
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            
            <div class="col span-1-of-2 left-pad">
               
                <div class="row">
                    <h3>Contact Us</h3>
                </div>
                
                <div class="row">
                    <div class="media-icon"><i class="ion-ios-location green"></i></div>
                    <div class="media-content">
                        <div><span> Pearl Gems</span></div>
                        <div>PO Box 16122 Collins Street West Victoria 8007 Australia</div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="media-icon"><i class="ion-ios-telephone green"></i></div>
                    <div class="media-content">
                        <div><span> Telephone</span></div>
                        <div>+94 77-662-9555 / +94 77-662-9555</div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="media-icon"><i class="ion-ios-paperplane green"></i></div>
                    <div class="media-content">
                        <div><span> Email Address</span></div>
                        <div>info@gemexchange.lk</div>
                    </div>
                </div>
            </div>
        </div>
    </section>

        
    <!-- /.container -->
<?php include(TEMPLATE_LINK . DS . "footer.php");?>