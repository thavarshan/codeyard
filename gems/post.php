<?php  require_once("components/config.php") ?>

<?php  include(TEMPLATE_LINK . DS . "header.php") ?>

<!-- About Section -->
<section class="section-post clearfix pad">

    <div class="row">
        <div class="col span-8-of-11">
            <div class="row">
                <?php

                    if(isset($_GET['id'])){

                        global $con; 

                        $recieved_post_id = $_GET['id'];
                        $sql              = "SELECT * FROM blog_posts WHERE post_id='$recieved_post_id'";
                        $result           = mysqli_query($con, $sql); 

                        while($row = mysqli_fetch_array($result)){

                            $post_id          = $row['post_id'];
                            $post_category    = $row['post_category'];
                            $post_title       = $row['post_title'];
                            $post_img         = $row['post_img'];
                            $post             = $row['post_body'];
                            $post_date        = format_date_min($row['post_date']);
                            $post_author      = $row['post_author'];

                            $sql2 = "SELECT * FROM admin_users WHERE admin_id='$post_author'";
                            $result2 = mysqli_query($con, $sql2);
                            $sql3 = "SELECT * FROM blog_category WHERE post_category_id='$post_category'";
                            $result3 = mysqli_query($con, $sql3);


                            while(($row2 = mysqli_fetch_array($result2)) && ($row3 = mysqli_fetch_array($result3))){

                                $author_name = $row2['username'];
                                $author_id   = $row2['admin_id'];
                                $category    = $row3['post_category_title'];
                                $category_id = $row3['post_category_id'];
                                
                                echo '

                                    <div class="post-container">
                                        <div class="row">
                                            <div class="post-page-img">
                                                <img src="img/blog/'.$post_img.'" />
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="post-content">
                                                <div class="post-heading">
                                                    <a href="post.php?id='.$post_id.'"><h1 class="post-title">'.$post_title.'</h1></a>
                                                </div>

                                                <div class="post-details">
                                                    <p class="post-info">Date: <span class="green">'.$post_date.'</span> &#8226; Author: <a class="normal" href="blog.php?author='.$author_id.'">'.$author_name.'</a> &#8226; Categories: <span><a class="normal" href="blog.php?category='.$category_id.'">'.$category.'</a></span></p>
                                                </div>

                                                <div class="post-para">'.$post.'</div>
                                            </div>
                                        </div>
                                    </div>

                                            ';
                            }
                        }
                    }

                ?>
            </div>
        </div>

        <div class="col span-3-of-11">
            <div class="sidebar-box">
                <div class="row">
                    <div class="catagories">
                        <h2>Categories</h2>
                    </div>
                </div>

                <div class="row">
                    <ul class="shop-catagories-ul">
                        <li><a class="normal" href='blog.php'>All</a>
                            <?php blog_categories(); ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</section>

<!-- Footer -->
<?php include(TEMPLATE_LINK . DS . "footer.php");?>