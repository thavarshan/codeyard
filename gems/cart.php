<?php  require_once("components/config.php") ?>

<!-- Content -->
<?php



    if(isset($_GET['add'])) {

        global $con;

        $query = query("SELECT * FROM products WHERE product_id=" . escape_string($_GET['add']) . " ");
        confirm($query);

        while($row = fetch_array($query)) {
            
            if($row['product_quantity'] == '0') {

                set_message("<p class='error-p'>We are out of stock at the moment. Please come visit us in a few days.</p>");
                redirect("checkout.php");
                
            } elseif($row['product_quantity'] != $_SESSION['product_' . $_GET['add']]) {

                $_SESSION['product_' . $_GET['add']] += 1;
                redirect("checkout.php");

            } else {

                set_message("<p class='error-p'>We only have" . " " . $row['product_quantity'] . " " . "available</p>");
                redirect("checkout.php");

            }

            

        }

    }

    if(isset($_GET['remove'])) {

        $_SESSION['product_' . $_GET['remove']] -= 1;

        if($_SESSION['product_' . $_GET['remove']] < 1) {
            unset($_SESSION['item_total']);
            unset($_SESSION['item_quantity']);
            redirect("checkout.php");

        } else {

            redirect("checkout.php");

        }

    }

    if(isset($_GET['delete'])) {

        $_SESSION['product_' . $_GET['delete']] = '0';
        unset($_SESSION['item_total']);
        unset($_SESSION['item_quantity']);
        redirect("checkout.php");

    }

    function cart() {

        $total = 0;
        $item_quantity = 0;
        $item_name = 1;
        $item_number = 1;
        $amount = 1;
        $quantity = 1;

        foreach($_SESSION as $name => $value) {

            if($value > 0) {

                if(substr($name, 0, 8) == "product_") {

                    $length = strlen($name - 8);
                    $id = substr($name, 8, $length);

                    $query = query("SELECT * FROM products WHERE product_id=" . escape_string($id) . " ");
                    confirm($query);

                    while($row = fetch_array($query)) {

                        $sub = $row['product_price'] * $value;
                        $item_quantity += $value;

                        $pro_id       = $row['product_id'];
                        $pro_cat      = $row['product_cat'];
                        $pro_brand    = $row['product_brand'];
                        $pro_title    = $row['product_title'];
                        $pro_price    = $row['product_price'];
                        $pro_quantity = $row['product_quantity'];

                        echo

                            '<tr>
                                <td>'.$pro_title.'</td>
                                <td><strong class="price">$ '.$pro_price.'</strong></td>
                                <td class="text-center green">'.$value.'</td>
                                <td><strong class="price">$ '.$sub.'</strong></td>
                                <td class="action">
                                    <a class="" href="cart.php?add='.$pro_id.'" title="Add">
                                        <i class="ion-plus icon"></i>
                                    </a>

                                    <a class="" href="cart.php?remove='.$pro_id.'" title="Remove">
                                        <i class="ion-minus icon"></i>
                                    </a>

                                    <a class="" href="cart.php?delete='.$pro_id.'" title="Delete">
                                        <i class="ion-close icon"></i>
                                    </a>
                                </td>
                            </tr>

                            <input type="hidden" name="item_name_'.$item_name.'" value="'.$pro_title.'">
                            <input type="hidden" name="item_number_'.$item_number.'" value="'.$pro_id.'">
                            <input type="hidden" name="amount_'.$amount.'" value="'.$pro_price.'">
                            <input type="hidden" name="quantity_'.$quantity.'" value="'.$value.'">

                            ';

                        $item_name++;
                        $item_number++;
                        $amount++;
                        $quantity++;

                    }

                    $_SESSION['item_total'] = $total += $sub;
                    $_SESSION['item_quantity'] = $item_quantity;

                }

            }

        }

    }


    function report() {

        if(isset($_GET['tx'])) {
            $amount      = $_GET['amt'];
            $currency    = $_GET['cc'];
            $transaction = $_GET['tx'];
            $status      = $_GET['st'];
            $email       = $_SESSION['email'];

        $total = 0;
        $item_quantity = 0;

        foreach($_SESSION as $name => $value) {

            if($value > 0) {

                if(substr($name, 0, 8) == "product_") {

                    $length = strlen($name - 8);
                    $id     = substr($name, 8, $length);

                    $send_order = query("INSERT INTO orders (order_amount, order_transaction, order_status, order_currency, customer_email) VALUES('{$amount}', '{$transaction}', '{$status}', '{$currency}', '{$email}')");
                    $last_id = last_id();
                    confirm($send_order);

                    $query  = query("SELECT * FROM products WHERE product_id = '".escape_string($id)."' ");
                    confirm($query);
                    $query2  = query("SELECT user_id FROM users WHERE email = '".escape($email)."' ");
                    confirm($query2);
                    $last_idp = last_id($query);
                    
                    while(($row = fetch_array($query)) && ($row2 = fetch_array($query2))) {

                        $pro_title      = $row['product_title'];
                        $pro_price      = $row['product_price'];
                        $sub            = $row['product_price'] * $value;
                        $customer       = $row2['user_id'];
                        $item_quantity += $value;

                        $insert_report = query("INSERT INTO reports (product_id, order_id, customer_id, product_title, product_price, product_quantity) VALUES('{$id}', '{$last_id}', '{$customer}', '{$pro_title}', '{$pro_price}', '{$value}')");
                        confirm($insert_report);

                    }

                    $total += $sub;
                    $item_quantity;

                }

            }

        }

            session_destroy();

        } else {

            redirect("index.php");

        }

    }

?>
