<?php  require_once("components/config.php") ?>

<?php  include(TEMPLATE_LINK . DS . "header.php") ?>

    <section class="section-para pad">
        <div class="row">
            <h2>TERMS &amp; CONDITIONS OF USE</h2>

            <p> Any use by you of the website operated by <strong> YOUR COMPANY NAME HERE </strong> at<strong> - <a href="http://www.boudreauxcellars.com/">http://www.YOUR WEB SITE HERE.com/</a> </strong>(the “Site”) is conditional upon your acceptance of these Terms &amp; Conditions, including our <a title="Privacy Statement" href="http://www.boudreauxcellars.com/privacy.html" target="_blank">Privacy Statement</a>. We reserve the right to amend these Terms &amp; Conditions from time to time without notice and at our discretion. It is your responsibility periodically to review this page for updates to these Terms &amp; Conditions, which shall come into effect once posted. See box below for a summary of updates. Your continued use of the Site will be deemed acceptance of these Terms &amp; Conditions, including our <a title="Privacy Statement" href="http://www.boudreauxcellars.com/privacy.html" target="_blank">Privacy Statement</a>.
                <br>
                <br> The Site is intended for use by you only if you are of legal age to purchase alcohol in your country of residence and in the country from which you are accessing the Site. If you do not fall within this category, you may be in breach of laws or regulations applicable in your country of residence or in your country of access and you should leave the Site immediately.
                <br>
                <br> IF YOU DO NOT ACCEPT THESE TERMS &amp; CONDITIONS PLEASE LEAVE THE SITE NOW.
                <br>
                <br> All references to 'our', 'us', 'we' or ‘company’ within this policy and within the opt-in notice are deemed to refer to Diageo plc, its subsidiaries, affiliates and associates.
                <br>
                <br>
                <strong>1. Rights</strong> — all rights in all material and content (including, but not limited to, text, images, web pages, sound, software (including, code, interface and website structure) and video, and the look and feel, design and compilation thereof) at the Site are owned by us or our licensors. You agree that you are permitted to use this material and/or content only as set out in these Terms &amp; Conditions or as otherwise expressly authorised in writing by us or our licensors, and that you may not otherwise copy, reproduce, transmit, publicly perform, distribute, commercially exploit, adapt, translate, modify, bundle, merge, share or make available to any person, or create derivative works of such material or content.
                <br>
                <br>
                <strong>2. Intellectual Property</strong> — We are the owner and/or authorised user of all trademarks, service marks, design marks, patents, copyrights, database rights and all other intellectual property appearing on or contained within the Site, unless otherwise indicated. Except as provided in these Terms &amp; Conditions, use of the Site does not grant you any right, title, interest or license to any such intellectual property you may access on the Site. Except as provided in these Terms &amp; Conditions, any use or reproduction of the intellectual property is prohibited.
                <br>
                <br>
                <strong>3. Copying</strong> — You may view the Site and you are welcome to print hard copies of material on it solely for your lawful, personal, non-commercial use. All other copying, whether in electronic, hard copy or other format, is prohibited and may breach intellectual property laws and other laws worldwide. Furthermore, you are not entitled to reproduce, transmit, publicly perform, distribute, adapt, translate, modify, bundle, merge, share or make available to any person, or create derivative works of such material, or use it for commercial purposes, without our prior written consent. All other rights are reserved.
                <br>
                <br>
                <strong>4. NO WARRANTIES</strong> — THE SITE IS PROVIDED “AS IS,” AND YOUR USE THEREOF IS AT YOUR OWN RISK. WE, OUR OFFICERS, DIRECTORS, EMPLOYEES, AGENTS AND ASSIGNS, DISCLAIM, TO THE FULLEST EXTENT PERMITTED BY LAW, ALL EXPRESS AND IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. WE DO NOT WARRANT THAT THE SITE WILL BE FREE FROM VIRUSES, AVAILABLE OR THAT THE CONTENTS WILL BE ACCURATE. ALTHOUGH WE TAKE REASONABLE STEPS TO SECURE THE SITE, YOU ACKNOWLEDGE THAT THE INTERNET IS NOT A COMPLETELY SECURE MEDIUM AND WE MAKE NO WARRANTIES, EXPRESS OR IMPLIED, THAT ANY INFORMATION OR MATERIALS YOU POST ON OR TRANSMIT THROUGH THE SITE WILL BE SAFE FROM UNAUTHORISED ACCESS OR USE. IF YOU ARE DISSATISFIED WITH THE SITE, YOUR SOLE REMEDY IS TO DISCONTINUE USING THE SITE.
                <br>
                <br>
                <strong>5. NO LIABILITY</strong> — TO THE FULLEST EXTENT PERMITTED BY LAW WE, OUR OFFICERS, DIRECTORS, EMPLOYEES, AGENTS AND ASSIGNS, HEREBY DISCLAIM ALL LIABILITY FOR ANY LOSS, COST OR DAMAGE (DIRECT, INDIRECT, CONSEQUENTIAL, OR OTHERWISE) SUFFERED BY YOU AS A RESULT OF YOUR USE OF THE SITE OR FROM ANY COMPUTER VIRUS TRANSMITTED THROUGH THE SITE, OR OTHER SITES ACCESSED FROM THE SITE, WHETHER SUCH LOSS, COST OR DAMAGE ARISES FROM OUR NEGLIGENCE OR OTHERWISE AND EVEN IF WE ARE EXPRESSLY INFORMED OF THE POSSIBILITY OF SUCH LOSS OR DAMAGE. IN NO EVENT SHALL OUR TOTAL LIABILITY TO YOU FOR ALL DAMAGES, COSTS, LOSSES AND CAUSES OF ACTION IN THE AGGREGATE (WHETHER IN CONTRACT, TORT, INCLUDING BUT NOT LIMITED TO NEGLIGENCE, OR OTHERWISE) ARISING FROM THE TERMS AND CONDITIONS, INCLUDING OUR PRIVACY STATEMENT, OR USE OF THE SITE EXCEED, IN THE AGGREGATE, $100.00 (US).
                <br>
                <br>
                <strong>6. User Information</strong> — In the course of your use of the Site, you may be asked to provide personal information to us (such information referred to hereinafter as “User Information”). Our information collection and use policies with respect to such User Information are set forth in the Site Privacy Statement, which Privacy Statement is incorporated into these Terms &amp; Conditions by reference. You acknowledge and agree that you are solely responsible for the accuracy and content of the User Information.
                <br>
                <br>
                <strong>7. Links from and to the Site</strong> — You acknowledge and agree that we have no responsibility for the information provided by websites to which you may link from the Site (“Linked Sites”). Links to Linked Sites do not constitute an endorsement by or association with us of such sites or the content, products, advertising or other materials presented on such sites. We have no control over these Linked Sites and do not edit or monitor them. You acknowledge and agree that we are not responsible or liable, directly or indirectly, for any damage, loss or cost caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on such Linked Sites. No website may be linked to the Site or its pages without our prior written consent.
                <br>
                <br>
                <strong>8. Indemnity</strong> — You will indemnify us against any loss, damage or cost incurred by us arising out of your use of the Site, any of its services or any information accessible over or through the Site, including information obtained from Linked Sites, your submission or transmission of information or material on or through the Site or your violation of these Terms &amp; Conditions or any other laws, regulations and rules. You will also indemnify against any claims that information or material that you have submitted to us is in violation of any law or in breach of any third party rights (including, but not limited to, claims in respect of defamation, invasion of privacy, breach of confidence, infringement of copyright or infringement of any other intellectual property right). We reserve the right to exclusively defend and control any claims arising from the above and any such indemnification matters and that you will fully cooperate with us in any such defenses.
                <br>
                <br>
                <strong>9. Entire Agreement</strong> — These Terms &amp; Conditions, including our Privacy Statement, constitutes the entire agreement between you and us in relation to its subject matter and supersedes any and all prior promises, representations, agreements, statements and understandings whatsoever between us. To the extent that software is available through the Site, such software may be subject to a license agreement that is distributed or included with such software and you agree to abide by the terms and conditions of any such license agreements. The failure by us to exercise or enforce any right or provision of the Terms &amp; Conditions shall not constitute a waiver of such right or provision. If any provision of the Terms &amp;Conditions is found by a court of competent jurisdiction to be unenforceable or invalid, the parties nevertheless agree that the court should endeavor to give effect to the parties’ intentions as reflected in the provision, and the other provisions of the Terms &amp; Conditions shall remain in full force and effect. We may cede assign or otherwise transfer our rights and obligations in terms of these standard terms and conditions to third parties.
                <br>
                <br>
                <strong>10. Copyright And IP Agent for Sri Lanka</strong> — We respect the intellectual property rights of others, and require that the people who use the Site do the same. If you believe that your work has been copied in a way that constitutes copyright infringement, please forward the following information to the Copyright Agent named below:
            </p>
            <br />

            <ul>
                <li style="margin-bottom:10px;">Your address, telephone number, and email address</li>
                <li style="margin-bottom:10px;"> A description of the copyrighted work that you claim has been infringed</li>
                <li style="margin-bottom:10px;"> A description of the alleged infringing activity and where the alleged infringing material is located</li>
                <li style="margin-bottom:10px;"> A statement by you that you have a good faith belief that the disputed use is not authorised by you, the copyright owner, its agent, or the law</li>
                <li style="margin-bottom:10px;">An electronic or physical signature of the person authorised to act on behalf of the owner of the copyright interest</li>
                <li style="margin-bottom:10px;">A statement by you, made under penalty of perjury, that the above information in your Notice is accurate and that you are the copyright owner or authorised to act on the copyright owner's behalf.
                    <br>
                </li style="margin-bottom:10px;">
            </ul>
            <br />
            <p>
                Copyright Agent:
                <br> YOUR COMPANY NAME HERE
                <br> ADDRESS
                <br> CITY, STATE ZIP
                <br> Attn: CONTACT NAME
                <br> Telephone No: PHONE NUMBER
                <br> Email:
                <a href="mailto:info@gemexchange.lk"> info@gemexchange.lk</a>
                <br>
                <br>
                <strong>11. Law and Jurisdiction</strong> — These Terms &amp; Conditions, including the Privacy Statement and any matter relating to the Site, shall be governed by Washington State law. To the extent that software is accessible through the Site, such software may be subject to export, re-export and/or import controls imposed by the United States or any other jurisdiction and may not be downloaded or otherwise exported or re-exported: (a) into (or to a national or resident of) any country to which the U.S. has placed an embargo or which is subject to relevant export restrictions; (b) to anyone on the U.S. Treasury Department's Specially Designated Nationals list, or (c) in violation of the U.S. Commerce Department's Table of Denial Orders. </p>
        </div>
    </section>

<!-- Footer -->
<?php include(TEMPLATE_LINK . DS . "footer.php");?>