<?php  require_once("components/config.php") ?>

<?php
    if(logged_in()){
        redirect("user.php");
    }
?>

<?php  include(TEMPLATE_LINK . DS . "header.php") ?>
 
<section class="section-login pad" >
    <div class="row">
        <h2></h2>
    </div>
    
    <div class="row">
        <div class="col span-1-of-3">
            <h3>New Customer</h3>
            <p><Strong>Register Account</Strong></p>
            <div  class="form-container">
                <p>By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.</p>
                
                <div id="link">
                    <a class="btn hvr-rectangle-in" href="register.php" style="color: #fff;">Continue</a>
                </div>
            </div>
           
        </div>
        
        <div class="col span-1-of-3">
           <h3>Returning Customer</h3>
            <p><Strong>I am a returning customer</Strong></p>
            <div  class="form-container">
                <form method="post" action="" enctype="multipart/form-data">
                   
                   <div class="row">
                       <?php display_message(); ?>
                       <?php validate_user_login();?>
                   </div>
                    <div class="row">
                        <input type="email" placeholder="Email" name="email" id="email" required />
                    </div>
                    
                    <div class="row">
                        <input type="password" placeholder="Password" name="password" id="login-password" required />
                    </div>
                    
                    <div class="row">
                        <div class="col span-1-of-3">
                            <input name="remember" id="remember" type="checkbox" selected />
                            <label for="remember"><span> &nbsp;Remember Me</span> </label>
                        </div>
                        
                        <div class="col span-2-of-3" style="text-align:right;">
                            <a href="recover.php" title="forgot-password">Forgot Password?</a>
                        </div>
                    </div>
                    
                    <div class="row">
                        <button class="btn hvr-rectangle-in" type="submit" name="submit">Login</button>
                    </div>
                </form>
            </div>
        </div>
        
        <div class="col span-1-of-3">
            <div class="form-container">
            </div>
        </div>
    </div>
</section>


<!-- Footer -->
<?php include(TEMPLATE_LINK . DS . "footer.php");?>