<?php  require_once("components/config.php") ?>

<?php  include(TEMPLATE_LINK . DS . "header.php") ?>

<section class="section-product-details products">
    
    <div class="row pad">
        <?php
    if(isset($_GET['pro_id'])){

        global $con; 

        $product_id = $_GET['pro_id'];
        $get_pro = "SELECT * FROM products WHERE product_id='$product_id'";
        $run_pro = mysqli_query($con, $get_pro); 

        while($row_pro = mysqli_fetch_array($run_pro)){

            $pro_id = $row_pro['product_id'];
            $pro_title = $row_pro['product_title'];
            $pro_price = $row_pro['product_price'];
            $pro_image = $row_pro['product_image'];
            $pro_desc = $row_pro['product_desc'];

            echo 

                '<div class="product-display col span-8-of-11">
    <div class="row">
        <div class="col span-2-of-5">
            <img class="sing-product-img" src="admin/product_images/'.$pro_image.'" alt="'.$pro_title.'" title="'.$pro_title.'" />
        </div>

        <div class="product-details col span-3-of-5">
            <div>
                <h2 class="product-title">'.$pro_title.'</h2>
            </div>

            <div>
                <h3 class="price-h3">$ '.$pro_price.'</h3>
            </div>

            <div>
                <p></p>
            </div>

            <div class="pro-desc">
                <p><strong>Description</strong></p>
                <p>'.$pro_desc.'</p>
            </div>

            <div>
                <a href="cart.php?add='.$pro_id.'">
                    <button type="submit" class="btn hvr-rectangle-in"><i class="ion-ios-cart-outline"></i> Add to Cart</button>
                </a>
            </div>

        </div>
    </div>
</div>'

                ;

        }
    }
        ?>
        
        <div class="col span-3-of-11">
            <div class="sidebar-box">
                <div class="row">
                    <h2>Best Seller</h2>
                </div>
                
                <div class="row">
                    <ul>
                        <li></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
</section>

<!-- Footer -->
<?php include(TEMPLATE_LINK . DS . "footer.php");?>