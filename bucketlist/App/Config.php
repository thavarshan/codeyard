<?php

namespace App;

/**
 * Application configuration
 *
 * PHP version 7.0
 */
class Config {

  /**
   * Database host
   * @var string
   */
  const DB_HOST = 'localhost';

  /**
   * Database name
   * @var string
   */
  const DB_NAME = 'bucketlist';

  /**
   * Database user
   * @var string
   */
  const DB_USER = 'root';

  /**
   * Database password
   * @var string
   */
  const DB_PASSWORD = 'root';

  /**
   * Show or hide error messages on screen
   * @var boolean
   */
  const SHOW_ERRORS = true;

  /**
   * Secret key for hashing
   * @var boolean
   */
  const SECRET_KEY = 'vtQqs0XI2ToGiEO7GZ8FMuNhPvlh7Dj6';

  /**
   * Mailgun API key
   *
   * @var string
   */
  const MAILGUN_API_KEY = 'key-c5821b4b53374bc1bc8bda6f01ab6bba';

  /**
   * Mailgun domain
   *
   * @var string
   */
  const MAILGUN_DOMAIN = 'sandbox6fdb288a7fa3463e84342fffe40d7282.mailgun.org';

  /**
   * App base URL
   *
   * @var string
   */
   public static function getBaseURI() {
     $base_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]" . '/bucketlist/public/';
     return $base_url;
   }

  /**
   * App name
   *
   * @var string
   */
  const SITE_NAME = 'Bucketlist';

}
