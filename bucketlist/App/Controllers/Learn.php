<?php

namespace App\Controllers;

use \Core\View;

/**
 * Home controller
 *
 * PHP version 7.0
 */
class Learn extends \Core\Controller {

  /**
   * Show the index page
   *
   * @return void
   */
  public function indexAction() {
    View::renderTemplate('Learn/index.html');
  }
}
