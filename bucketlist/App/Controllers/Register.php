<?php

namespace App\Controllers;

use \Core\View;
use \App\Models\User;
use \App\Auth;
use \App\Flash;
use \App\Config;

/**
 * Home controller
 *
 * PHP version 7.0
 */
class Register extends \Core\Controller {

  /**
   * Before filter - called before each action method
   *
   * @return void
   */
  protected function before() {
    parent::before();
    $this->user = Auth::getUser();
  }

  /**
   * Show the index page
   *
   * @return void
   */
  public function indexAction() {
    View::renderTemplate('Register/index.html');
    if($this->user) {
      Flash::addMessage('You are already registered', Flash::INFO);
      $this->redirect('/bucketlist/public/profile');
    } else {
      View::renderTemplate('Register/index.html');
    }
  }

  /**
  * Sign up a new user
  *
  * @return void
  */
  public function createAction() {
    //Register data into database
    $user = new User($_POST);
    if ($user->save()) {
      $user->sendActivationEmail();
      $this->redirect('/bucketlist/public/register/success');
    } else {
      View::renderTemplate('Register/index.html', [
        'user' => $user
      ]);
    }
  }

/**
 * Show the signup success page
 *
 * @return void
 */
  public function successAction() {
    View::renderTemplate('Register/success.html');
  }

  /**
   * Activate a new account
   *
   * @return void
   */
  public function activateAction() {
    User::activate($this->route_params['token']);
    $this->redirect('/bucketlist/public/register/activated');
  }

/**
 * Show the activation success page
 *
 * @return void
 */
public function activatedAction() {
  View::renderTemplate('Register/activated.html');
}



}
