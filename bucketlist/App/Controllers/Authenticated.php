<?php

namespace App\Controllers;

/**
 * Application access authentication
 *
 * PHP version 7.0
 */
abstract class Authenticated extends \Core\Controller {
  /**
   * Require login before action method.
   *
   * @return void
   */
  protected function before() {
    $this->requireLogin();
  }
}

?>
