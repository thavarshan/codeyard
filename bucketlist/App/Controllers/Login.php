<?php

namespace App\Controllers;

use \Core\View;
use \App\Models\User;
use App\Auth;
use App\Flash;

/**
 * Home controller
 *
 * PHP version 7.0
 */
class Login extends \Core\Controller {

  /**
   * Show the index page
   *
   * @return void
   */
  public function indexAction() {
    View::renderTemplate('Login/index.html');
  }

  /**
   * Show the index page
   *
   * @return void
   */
  public function authAction() {
    $user = User::authenticate($_POST['email'], $_POST['password']);
    $remember_me = isset($_POST['remember_me']);
    if($user) {
      Auth::login($user, $remember_me);
      Flash::addMessage('Welcome!' , Flash::SUCCESS);
      $this->redirect(Auth::getReturnToPage());
    } else {
      Flash::addMessage('Login unsuccessful, Please try again', Flash::DANGER);
      $this->redirect('/bucketlist/public/login');
      View::renderTemplate('Login/index.html', [
        'email' => $_POST['email'],
        'remember' => $remember_me
      ]);
    }
  }

  public function logoutAction() {
    Auth::logout();
    $this->redirect('/bucketlist/public/show-logout-message');
  }

  public function showLogoutMessage() {
    Flash::addMessage('You are now logged out', Flash::SUCCESS);
    $this->redirect('/bucketlist/public/');
  }

}
