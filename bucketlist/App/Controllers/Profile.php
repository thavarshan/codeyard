<?php

namespace App\Controllers;

use \Core\View;
use \App\Models\User;
use \App\Models\BucketList;
use App\Controllers\Authenticated;
use \App\Auth;
use \App\Flash;

/**
 * Home controller
 *
 * PHP version 7.0
 */
class Profile extends Authenticated {

  private $list;

  public function __construct() {
    $this->list = new BucketList();
  }

  /**
   * Before filter - called before each action method
   *
   * @return void
   */
  protected function before() {
    parent::before();
    $this->user = Auth::getUser();
  }

  /**
   * Show the profile
   *
   * @return void
   */
  public function indexAction() {
    $list = new BucketList();
    View::renderTemplate('Profile/index.html', [
      'user' => $this->user,
      'list' => $list->getBucketlist()
    ]);
  }

  /**
   * Show the form for editing the profile
   *
   * @return void
   */
  public function settingsAction() {
    View::renderTemplate('Profile/settings.html', [
      'user' => $this->user
    ]);
  }

  /**
   * Update the profile
   *
   * @return void
   */
  public function updateAction() {
    if ($this->user->updateProfile($_POST)) {
      Flash::addMessage('Changes saved', Flash::SUCCESS);
      $this->redirect('/bucketlist/public/profile');
    } else {
      //$this->redirect('/bucketlist/public/settings');
      View::renderTemplate('Profile/settings.html', [
        'user' => $this->user
      ]);
    }
  }

  /**
   * Update the password
   *
   * @return void
   */
  public function addListAction() {
    if ($this->list->addToList($_POST)) {
      Flash::addMessage('Item added to bucketlist', Flash::SUCCESS);
      $this->redirect('/bucketlist/public/profile');
    } else {
      //$this->redirect('/bucketlist/public/settings');
      View::renderTemplate('Profile/index.html', [
        'user' => $this->user
      ]);
    }
  }

  /**
   * Update the password
   *
   * @return void
   */
  public function updatePasswordAction() {
    if ($this->list->updateProfilePassword($_POST)) {
      Flash::addMessage('New password saved', Flash::SUCCESS);
      $this->redirect('/bucketlist/public/profile');
    } else {
      //$this->redirect('/bucketlist/public/settings');
      View::renderTemplate('Profile/settings.html', [
        'user' => $this->user
      ]);
    }
  }

  /**
   * Cancel editting profile action
   *
   * @return void
   */
  public function cancelAction() {
    $this->redirect('/bucketlist/public/profile');
  }

  public function markCompleteItemAction() {
    if($this->list->markComplete($_GET['item'])) {
      Flash::addMessage('Congratulations! You have completed one of your list items', Flash::SUCCESS);
      $this->redirect('/bucketlist/public/profile');
    }
  }

  public function markIncompleteItemAction() {
    if($this->list->markIncomplete($_GET['item'])) {
      $this->redirect('/bucketlist/public/profile');
    }
  }

  public function deleteItemAction() {
    if($this->list->delete($_GET['item'])) {
      $this->redirect('/bucketlist/public/profile');
    }
  }

  // public function markIncompleteItemAction() {
  //   $this->list;
  //   if($list->markIncomplete($_GET['item'])) {
  //     $this->redirect('/bucketlist/public/profile');
  //   }
  // }
}
