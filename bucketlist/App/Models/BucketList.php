<?php

namespace App\Models;

use PDO;
use \App\Auth;
use \Core\View;

/**
 * Remembered login model
 *
 * PHP version 7.0
 */
class BucketList extends \Core\Model {

  public function addToList($data) {
    $this->title = $data['title'];
    $this->description = $data['description'];
    $this->user = Auth::getUser()->id;

    $sql = 'INSERT INTO bucketlist (title, description, user_id) VALUES (:title, :description, :user_id)';

    $db = static::getDB();
    $stmt = $db->prepare($sql);

    $stmt->bindValue(':title', $this->title, PDO::PARAM_STR);
    $stmt->bindValue(':description', $this->description, PDO::PARAM_STR);
    $stmt->bindValue(':user_id', $this->user, PDO::PARAM_INT);

    return $stmt->execute();
  }

  public function getBucketlist() {
    $user_id = User::findById($_SESSION['user_id'])->id;
    $sql = 'SELECT * FROM bucketlist WHERE user_id = :user_id';

    $db = static::getDB();
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);

    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

  public function markComplete($id) {
    $item_id = $id;
    $sql = 'UPDATE bucketlist SET status = 1, completed_on = :completed_on WHERE id = :id';

    $db = static::getDB();
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $item_id, PDO::PARAM_INT);
    $stmt->bindValue(':completed_on', date('Y-m-d H:i:s'), PDO::PARAM_STR);

    return $stmt->execute();
  }

  public function markIncomplete($id) {
    $item_id = $id;
    $sql = 'UPDATE bucketlist SET status = 0 WHERE id = :id';

    $db = static::getDB();
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $item_id, PDO::PARAM_INT);

    return $stmt->execute();
  }

  public function delete($id) {
    $item_id = $id;
    $sql = 'DELETE FROM bucketlist WHERE id = :id';

    $db = static::getDB();
    $stmt = $db->prepare($sql);
    $stmt->bindParam(':id', $item_id, PDO::PARAM_INT);

    return $stmt->execute();
  }

}
