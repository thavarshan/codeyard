<?php

namespace App;

/**
 * User Authentication
 *
 * PHP version 7.0
 */
class Flash {

  const SUCCESS = 'success';
  const INFO = 'info';
  const WARNING = 'warning';
  const DANGER = 'danger';

  public static function addMessage($message, $type = 'info') {
    // Create array in session if it doesn't already exist
    if(!isset($_SESSION['flash_notification'])) {
      $_SESSION['flash_notification'] = [];
    }
    // Append the message to the array
    $_SESSION['flash_notification'][] = [
      'body' => $message,
      'type' => $type
    ];
  }

  public static function getMessage() {
    if(isset($_SESSION['flash_notification'])) {
      $message = $_SESSION['flash_notification'];
      unset($_SESSION['flash_notification']);
      return $message;
    }
  }

}
