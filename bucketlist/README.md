# Welcome to Bucketlist

This is a simple PHP MVC application built from a simple MVC framework written in PHP and built by [Dave Hollingworth](http://daveh.io). It's free and [open-source](LICENSE).

It was created from the [Build a Complete Registration and Login System using PHP MVC](https://www.udemy.com/php-mvc-login/) course. The course explains how to use the pre-built framework to build a user authentication software on top of it, building it step-by-step, from scratch. If you've taken the course, then you'll already know how to use it. If not, please follow the instructions below.

## Starting an application using this framework

1. First, download the framework, either directly or by cloning the repo.
1. Run **composer update** to install the project dependencies.
1. (Optional) Configure your web server to have the **public** folder as the web root.
1. Open [App/Config.php](App/Config.php) and enter your database configuration data.
1. Create routes, add controllers, views and models.

See below for more details.

## Configuration

Configuration settings are stored in the [App/Config.php](App/Config.php) class. Default settings include database connection data and a setting to show or hide error detail. You can access the settings in your code like this: `Config::DB_HOST`. You can add your own configuration settings in here.
