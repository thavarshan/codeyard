<?php

/**
 * Front controller
 *
 * PHP version 7.0
 */

/**
 * Composer
 */
require dirname(__DIR__) . '/vendor/autoload.php';


/**
 * Error and Exception handling
 */
error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');

/**
 * Session
 */
session_start();

/**
 * Routing
 */
$router = new Core\Router();

// Add the routes
// Home page
$router->add('', ['controller' => 'Home', 'action' => 'index']);
// Account registration page
$router->add('register', ['controller' => 'Register', 'action' => 'index']);
$router->add('register/activate/{token:[\da-f]+}', ['controller' => 'Register', 'action' => 'activate']);
// Account login page
$router->add('login', ['controller' => 'Login', 'action' => 'index']);
// Account logout
$router->add('logout', ['controller' => 'Login', 'action' => 'logout']);
$router->add('show-logout-message', ['controller' => 'Login', 'action' => 'showLogoutMessage']);
// User profile page
$router->add('profile', ['controller' => 'Profile', 'action' => 'index']);
// User settings page
$router->add('settings', ['controller' => 'Profile', 'action' => 'settings']);
$router->add('profile/cancel', ['controller' => 'Profile', 'action' => 'cancel']);
// Password reset page
$router->add('passsword', ['controller' => 'Password', 'action' => 'forgot']);
$router->add('password/reset/{token:[\da-f]+}', ['controller' => 'Password', 'action' => 'reset']);
// Learn more page
$router->add('learn-more', ['controller' => 'Learn', 'action' => 'index']);

$router->add('{controller}/{action}');

$router->dispatch($_SERVER['QUERY_STRING']);
