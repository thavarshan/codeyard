$.validator.addMethod('validPassword',
  function(value, element, param) {
    if(value != '') {
      if(value.match(/.*[a-z]+.*/i) == null) {
        return false;
      }
      if(value.match(/.*\d+.*/) == null) {
        return false;
      }
    }
    return true;
  },
  'Must contain atleast one letter and one number'
);
  $(document).ready(function() {
    // Form client side validation
    $('#passwordForm').validate({
      rules: {
        password: {
          required: true,
          minlength: 6,
          validPassword: true
        }
      }
    });

    // Show password toggle
    $('#password').hideShowPassword({
      show: false,
      innerToggle: 'focus',
      toggle: {
        // The element to create.
        element: '<span class="ion-eye">'
        styles: {
          marginRight: '10px'
        }
      }
    });
  });
