<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model {

    public $with = [
        'user'
    ];

    protected $fillable = [
        'user_id',
        'sociate_id'
    ];

    public function sociate() {
        return $this->belongsTo('App\Sociate');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
