<?php

namespace App\Http\Controllers;

use App\User;
use App\Sociate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class SociateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function feed() {
        $friends = Auth::user()->friends();
        $feed = array();

        foreach($friends as $friend):
            foreach($friend->sociates as $post):
                array_push($feed, $post);
            endforeach;
        endforeach;

        foreach(Auth::user()->sociates as $post):
            array_push($feed, $post);
        endforeach;

        usort($feed, function($p1, $p2) {
            return $p1->id < $p2->id;
        });

        return $feed;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        return Sociate::create([
            'user_id' => Auth::id(),
            'content' => $request->content
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sociate  $sociate
     * @return \Illuminate\Http\Response
     */
    public function show(Sociate $sociate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sociate  $sociate
     * @return \Illuminate\Http\Response
     */
    public function edit(Sociate $sociate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sociate  $sociate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sociate $sociate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sociate  $sociate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sociate $sociate)
    {
        //
    }
}
