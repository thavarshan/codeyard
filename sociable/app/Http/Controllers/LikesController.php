<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Like;
use App\Sociate;

class LikesController extends Controller {

    public function like($id) {
        $post = Sociate::find($id);

        $like =  Like::create([
            'user_id' => Auth::id(),
            'sociate_id' => $post->id
        ]);

        return Like::find($like->id);
    }

    public function unlike($id) {
        $post = Sociate::find($id);

        $like = Like::where('user_id', Auth::id())
            ->where('sociate_id', $post->id)
            ->first();

        $like->delete();

        return $like->id;
    }
}
