<?php

namespace App\Http\Controllers;

use App\User;
use App\Sociate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function notifications() {
        Auth::user()->unreadNotifications->markAsRead();
        return view('notifications')->with([
            'nots' => Auth::user()->notifications
        ]);

    }
}
