<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Profile;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data) {

        $user = User::create([
            'name' => ucwords($data['first_name']),
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'slug' => str_slug($data['first_name'])
        ]);

        // Will copy foo/test.php to bar/test.php
        // overwritting it if necessary
        if (!is_dir(public_path('uploads/avatar/'.strtolower($data['first_name'])))) {
          mkdir(public_path('uploads/avatar/'.strtolower($data['first_name'])));
        }
        if(isset($data['gender'])) {
            $gender = $data['gender'];
        } else {
            $gender = 1;
        }
        
        if($gender == 1) {
            $avatar = 'uploads/avatar/' .strtolower($data['first_name']). '/male.png';
            $stm = 'male.png';
        } else {
            $avatar = 'uploads/avatar/' .strtolower($data['first_name']). '/female.png';
            $stm = 'female.png';
        }
        $strg = storage_path('app/public/uploads/' . $stm);
        $publc = public_path($avatar);
        copy($strg, $publc);

        Profile::create([
            'user_id' => $user->id,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'avatar' => $avatar,
            'gender' => $data['gender']
        ]);

        return $user;
    }
}
