<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sociate extends Model {

    public $with = [
        'user',
        'likes'
    ];

    protected $fillable = [
        'user_id',
        'content',
        'image'
    ];

    /**
 	 * User profile relationship
 	 *
 	 * @param type
 	 * @return void
	 */
    public function user() {
        return $this->belongsTo('App\User');
    }

    /**
 	 * User profile relationship
 	 *
 	 * @param type
 	 * @return void
	 */
    public function likes() {
        return $this->hasMany('App\Like');
    }

    public function getCreatedAtAttribute($date) {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->diffForHumans();
    }

    public function getUpdatedAtAttribute($date) {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->diffForHumans();
    }
}
