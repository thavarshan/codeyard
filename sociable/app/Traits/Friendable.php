<?php

namespace App\Traits;

use App\Friendship;
use App\User;
use App\Illuminate\Support\Facades\Auth;

trait Friendable {
    /**
 	 * Block comment
 	 *
 	 * @param int $id
 	 * @return void
	 */
    public function add_friend($id) {

        if($this->id === $id) {
            return 0;
        }

        if($this->is_friends_with($id) === 1) {
            return "already friends";
        }

        if($this->has_pending_friend_request_sent_to($id) === 1) {
            return "already sent a friend request";
        }

        if($this->has_pending_friend_request_from($id) === 1) {
            return $this->accept_friend($id);
        }

        $friendship = Friendship::create([
            'requester_id' => $this->id,
            'requested_id' => $id
        ]);

        if($friendship) {
            return 1;
        }

        return 0;
    }

    public function accept_friend($id) {

        if($this->has_pending_friend_request_from($id) === 0) {
            return 0;
        }

        $friendship = Friendship::where('requester_id', $id)
                        ->where('requested_id', $this->id)
                        ->first();

        if($friendship) {
            $friendship->update([
                'status' => 1
            ]);
            return 1;
        }

        return 0;
    }

    public function friends() {

        $friends = array();


		$f1 = Friendship::where('status', 1)
					    ->where('requester_id', $this->id)
	                    ->get();

		foreach($f1 as $friendship) {
            array_push($friends, \App\User::find($friendship->requested_id));
        }

		$friends2 = array();

		$f2 = Friendship::where('status', 1)
					->where('requested_id', $this->id)
					->get();

		foreach($f2 as $friendship) {
            array_push($friends2, \App\User::find($friendship->requester_id));
        }

		return array_merge($friends, $friends2);
    }

    public function pending_friend_requests() {

		$users = array();

		$friendships = Friendship::where('status', 0)
            					 ->where('requested_id', $this->id)
            					 ->get();

		foreach($friendships as $friendship):
			array_push($users, \App\User::find($friendship->requester_id));
		endforeach;

		return $users;
	}

    public function friends_ids() {

        return collect($this->friends())->pluck('id')->toArray();

    }

    public function is_friends_with($id) {

        if(in_array($id, $this->friends_ids())) {
            return 1;
        } else {
            return 0;
        }

    }

    public function pending_friend_requests_ids() {

        return collect($this->pending_friend_requests())->pluck('id')->toArray();

    }

    public function pending_friend_requests_sent() {

        $users = array();
        $friendships = Friendship::where('status', 0)
                        ->where('requester_id', $this->id)
                        ->get();

        foreach($friendships as $friendship) {
            array_push($users, \App\User::find($friendship->requested_id));
        }

        return $users;

    }

    public function pending_friend_requests_sent_ids() {

        return collect($this->pending_friend_requests_sent())->pluck('id')->toArray();

    }

    public function has_pending_friend_request_from($id) {

        if(in_array($id, $this->pending_friend_requests_ids())) {
            return 1;
        }
        else {
            return 0;
        }
    }

    public function has_pending_friend_request_sent_to($id) {

        if(in_array($id, $this->pending_friend_requests_sent_ids())) {
            return 1;
        } else {
            return 0;
        }

    }
}
