<?php

namespace App;

use Storage;
use App\Traits\Friendable;
// use Laravel\Scout\Searchable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use Friendable;
    // use Searchable;

    public $with = [
        'profile'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'slug'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
 	 * User profile relationship
 	 *
 	 * @param type
 	 * @return void
	 */
    public function profile() {
        return $this->hasOne('App\Profile', 'user_id');
    }

    /**
 	 * User profile relationship
 	 *
 	 * @param type
 	 * @return void
	 */
    public function sociates() {
        return $this->hasMany('App\Sociate')
                    ->orderBy('created_at');
    }

}
