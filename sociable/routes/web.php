<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'uses' => 'GuestController@index',
    'as' => 'welcome'
]);

Auth::routes();

Route::group(['middleware' => 'auth'], function() {

    // Home page
    Route::get('/home', 'HomeController@index')->name('home');

    // Feed
    Route::get('/feed', [
        'uses' => 'SociateController@feed',
        'as' => 'sociate.feed'
    ]);

    // New sociate
    Route::post('/sociate/store', [
        'uses' => 'SociateController@store',
        'as' => 'sociate.store'
    ]);

    // Like
    Route::get('/like/{id}', [
        'uses' => 'LikesController@like'
    ]);

    // Unlike
    Route::get('/unlike/{id}', [
        'uses' => 'LikesController@unlike'
    ]);

    // Profile page
    Route::get('/profile/{slug}', [
        'uses' => 'ProfileController@index',
        'as' => 'profile'
    ]);

    // Notifications page
    Route::get('/notifications', [
        'uses' => 'HomeController@notifications',
        'as' => 'notifications'
    ]);

    // Get unread notifications
    Route::get('/get_unread', function(){
        return Auth::user()->unreadNotifications;
    });

    // Check relationship status
    Route::get('/check_relationship_status/{id}', [
        'uses' => 'FriendshipsController@check',
        'as' => 'check'
    ]);

    // Add friendship
    Route::get('/add_friend/{id}', [
        'uses' => 'FriendshipsController@add_friend',
        'as' => 'add_friend'
    ]);

    // Accept friendship
    Route::get('/accept_friend/{id}', [
        'uses' => 'FriendshipsController@accept_friend',
        'as' => 'accept_friend'
    ]);

    // User data
    Route::get('/get_auth_user_data', function(){
        return Auth::user();
    });
});
