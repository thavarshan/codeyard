
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import { store } from './store'
window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('init-component', require('./components/Init.vue'));
Vue.component('login-component', require('./components/Login.vue'));
Vue.component('friend-component', require('./components/Friend.vue'));
Vue.component('notifications-component', require('./components/Notifications.vue'));
Vue.component('unread-component', require('./components/UnreadNots.vue'));
Vue.component('sociate-component', require('./components/Sociate.vue'));
Vue.component('feed-component', require('./components/Feed.vue'));

const app = new Vue({
    el: '#app',
    store
});
