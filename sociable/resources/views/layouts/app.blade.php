<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="{!! asset('css/fontawesome.css') !!}" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <!-- Vue instance & main app -->
    <div id="app">
        <!-- Init Vue component -->
        <init-component></init-component>
        <!-- Header section -->
        <header>
            @include('layouts.header')
        </header>
        <!-- Notifications section -->
        @include('layouts.notifications')
        <!-- Login form modal -->
        @guest
            <login-component></login-component>
        @endguest
        <!-- Main content area -->
        <main class="py-4">
            @yield('content')
        </main>
        @auth
            <!-- Push notifications vue component -->
            <notifications-component :id="{{ Auth::id() }}"></notifications-component>
        @endauth
    </div>
    <!-- Base scripts area -->
    <script src="{{ asset('js/fontawesome.js') }}"></script>
</body>
</html>
