<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            @auth
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item @if (Route::currentRouteName() == 'home') active @endif">
                        <a class="nav-link" href="{!! route('home') !!}">
                            <i class="fas fa-home"></i> Home
                        </a>
                    </li>
                    <unread-component route="{{ route('notifications') }}"></unread-component>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <i class="far fa-envelope"></i> Messages
                            <span class="badge badge-pill badge-primary">7</span>
                        </a>
                    </li>
                </ul>
            @endauth

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#loginModal">
                            {{ __('Login') }}
                        </a>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="btn btn-primary" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li> --}}
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            <img src="{{ Auth::user()->profile->avatar }}" alt="{{ Auth::user()->name }}" class="rounded-circle" width="32px" height="32px">
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <div class="dropdown-header">
                                <div class="text-muted">
                                    <small>Signed in as,</small>
                                </div>
                                <h6 class="m-0">{{ Auth::user()->name }}</h6>
                            </div>
                            <div class="dropdown-divider"></div>
                            <a href="{!! route('profile', ['slug' => Auth::user()->slug]) !!}" class="dropdown-item">
                                <i class="fas fa-user mr-1"></i>
                                Profile
                            </a>
                            <a href="#" class="dropdown-item">
                                <i class="far fa-comment-alt mr-1"></i>
                                Sociates
                            </a>
                            <a href="#" class="dropdown-item">
                                <i class="fas fa-users mr-1"></i>
                                Friends
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="#" class="dropdown-item">
                                <!-- <i class="far fa-life-ring mr-1"></i> -->
                                Help &amp; support
                            </a>
                            <a href="#" class="dropdown-item">
                                <!-- <i class="fas fa-cog mr-1"></i> -->
                                Settings &amp; privacy
                            </a>
                            <!-- <div class="dropdown-divider"></div> -->
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                               <!-- <i class="fas fa-power-off mr-1"></i> -->
                                {{ __('Log out') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                    <li class="nav-item ml-lg-2">
                        <a class="btn btn-primary" style="margin-top: 5px;" href="#">
                            {{ __('Sociate') }}
                        </a>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
