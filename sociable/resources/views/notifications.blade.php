@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row ">
        <!-- Profile section -->
        <div class="col-lg-3 col-md-4 d-none d-md-block">
            <div class="card card-profile text-center">
                <div class="card-body">

                </div>
            </div>
        </div>
        <!-- Feed section -->
        <div class="col-lg-7 col-md-8 col-12">
            <div class="card">
                <div class="card-header bg-white">
                    <h5 class="m-0">All notifications</h5>
                </div>
                @forelse ($nots as $not)
                    <div class="card-body border-bottom">
                        <div class="clearfix d-flex">
                            <a class="mr-2" href="{!! route('profile', ['slug' => getUser($not->data['id'])->slug]) !!}">
                                <img src="{{  getUser($not->data['id'])->profile->avatar }}" alt="{{ getUser($not->data['id'])->name }}" class="rounded-circle" width="48px" height="48px">
                            </a>
                            <div class="user-details">
                                <h6>
                                    <a href="{!! route('profile', ['slug' => getUser($not->data['id'])->slug]) !!}">
                                        {{ getUser($not->data['id'])->name }}
                                    </a>
                                    <span class="mx-2">&#183;</span>
                                    <span class="text-muted">
                                        <small>{{ $not->created_at->diffForHumans() }}</small>
                                    </span>
                                </h6>
                                <p>
                                    {{ $not->data['message'] }}
                                </p>
                            </div>
                        </div>
                        @if ($not->type == 'App\Notifications\NewFriendRequest')
                            <div class="action">
                                <friend-component :id="{{ $not->data['id'] }}"></friend-component>
                            </div>
                        @endif
                    </div>
                @empty
                    <div class="card-body">
                        You have no new notifications.
                    </div>
                @endforelse
            </div>
        </div>
        <!-- Copyright and ads section -->
        <div class="col-md-2 d-none d-lg-block">
            <div class="card">
                <div class="card-body">
                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
