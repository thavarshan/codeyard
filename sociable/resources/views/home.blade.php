@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row ">
        <!-- Profile section -->
        <div class="col-lg-3 col-md-4 d-none d-md-block">
            <div class="card card-profile text-center">
                <img class="card-img-top" src="{!! asset('img/brick.jpg') !!}">
                <div class="card-block">
                    <img alt="{{ Auth::user()->name }}" class='card-img-profile' src="{{ asset(Auth::user()->profile->avatar) }}" width="150px" height="150px">
                    <h4 class="card-title">
                        {{ Auth::user()->profile->first_name . ' ' . Auth::user()->profile->last_name }}
                        <small>&#64;{{ Auth::user()->name }}</small>
                    </h4>
                </div>
            </div>
        </div>
        <!-- Feed section -->
        <div class="col-lg-7 col-md-8 col-12">
            <div class="card">
                <div class="card-header bg-light">
                    <sociate-component></sociate-component>
                </div>

                <feed-component></feed-component>
            </div>
        </div>
        <!-- Copyright and ads section -->
        <div class="col-md-2 d-none d-lg-block">
            <div class="card">
                <div class="card-body">

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
