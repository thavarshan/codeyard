@extends('layouts.app')

@section('content')
<section class="mt--4 py-5" style="background: url({!! asset('img/brick.jpg') !!}); background-size: cover;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col text-center my-5">
                <div class="user-profile mb-4">
                    <img alt="{{ $user->name }}" class="rounded-circle user-avatar" src="{{ $user->profile->avatar }}" width="150px" height="150px">
                </div>
                <h4 class="text-white">
                    {{ $user->profile->first_name}} {{ $user->profile->last_name }}
                </h4>
                <h6 class="text-light mb-3">&#64;{{ $user->name }}</h6>
                @if (Auth::id() != $user->id)
                    <friend-component :id="{{ $user->id }}"></friend-component>
                @else
                    <div class="mt-3">
                        <a href="#" class="btn btn-outline-primary">Edit profile</a>
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid bg-white">
        <div class="row">
            <div class="col">
                <ul class="nav justify-content-center">
                    <li class="nav-item text-center">
                        <a href="#" class="nav-link">
                            <small>Sociates</small><br />
                            <h4>12</h4>
                        </a>
                    </li>
                    <li class="nav-item text-center">
                        <a href="#" class="nav-link">
                            <small>Friends</small><br />
                            <h4>102</h4>
                        </a>
                    </li>
                    <li class="nav-item text-center">
                        <a href="#" class="nav-link">
                            <small>Likes</small><br />
                            <h4>124</h4>
                        </a>
                    </li>
                    <li class="nav-item text-center">
                        <a href="#" class="nav-link">
                            <small>Pending</small><br />
                            <h4>12</h4>
                        </a>
                    </li>
                    <li class="nav-item text-center">
                        <a href="#" class="nav-link">
                            <small>Replies</small><br />
                            <h4>126</h4>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row ">
            <!-- Profile section -->
            <div class="col-lg-3 col-md-4 d-none d-md-block">

            </div>
            <!-- Feed section -->
            <div class="col-lg-7 col-md-8 col-12">

            </div>
        </div>
    </div>
</section>
@endsection
