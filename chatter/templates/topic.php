<?php include('includes/header.php') ?>

<!-- Primary Page Layout
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="container">
  <div class="row">
    <div class="nine columns">
      <div class="topic-main row">
        <div class="user">
          <a href="<?php echo BASE_URI; ?>topics.php?user=<?php echo $topic->user_id; ?>"><h6 style="margin-bottom: 0;"><?php echo $topic->username; ?></h6></a>
          <span><?php echo userPostCount($topic->user_id); ?> Posts</span>
        </div>
        <div class="topic-wrap">
          <h5><?php echo $title; ?></h5>
          <div class="topic-content">
            <?php echo $topic->body; ?>
          </div>
        </div>
      </div>

      <div class="row">
        <h6 style="margin-bottom: 0;">Replies</h6>
        <hr style="margin-top: 10px;" />
        <?php foreach($replies as $reply) : ?>
        <div class="row topic-reply">
          <div class="user">
            <a href="<?php echo BASE_URI; ?>topics.php?user=<?php echo $reply->user_id; ?>"><h6 style="margin-bottom: 0;"><?php echo $reply->username; ?></h6></a>
          </div>
          <div class="topic-wrap">
            <div class="topic-content">
              <?php echo $reply->body; ?>
            </div>
          </div>
        </div>
        <?php endforeach; ?>
      </div>

      <div class="row">
        <h6>Reply To Topic</h6>
				<?php if(isLoggedIn()) : ?>
				<form role="form" method="post" action="topic.php?id=<?php echo $topic->id; ?>">
  					<div class="row">
						<textarea id="reply" rows="10" cols="80" class="form-control" name="body"></textarea>
						<script>
							CKEDITOR.replace( 'reply' );
            			</script>
  					</div>
 					 <button style="margin-top: 20px;" name="do_reply" type="submit" class="btn btn-default">Submit</button>
				</form>
				<?php else : ?>
					<p>Please login to reply</p>
				<?php endif; ?>
      </div>

    </div>

    <div class="three columns">
      <h6 class="text-center">Catagories</h6>
      <a href="topics.php" class="button catagory-a <?php echo is_active($category->id); ?>">All topics</a>

      <?php foreach(getCategories() as $category) : ?>
        <a href="topics.php?category=<?php echo $category->id; ?>" class="button catagory-a <?php echo is_active($category->id); ?>"><?php echo $category->name; ?></a>
      <?php endforeach; ?>
    </div>
  </div>
</div>

<?php include('includes/footer.php') ?>
