<?php
if(isset($_GET['logout'])){
	//Create User Object
	$user = new User;

	if($user->logout()){
		redirect('index.php','You are now logged out','success');
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>
    <?php
    //Check if title is set, if not assign it
    if(!isset($title)){
    	$title = SITE_TITLE;
    }
    echo SITE_TITLE;
    ?>
  </title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="https://fonts.googleapis.com/css?family=Hind:300|Montserrat:600" rel="stylesheet">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="templates/css/normalize.css">
  <link rel="stylesheet" href="templates/css/basenet.css">
	<link rel="stylesheet" href="templates/css/animate.css">

  <!-- CSS Custom
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="templates/css/global.css">

	<!-- Javascripts
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
	<script src="templates/js/ckeditor/ckeditor.js"></script>

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

</head>
<body>

  <!-- Primary Page Head
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <header>
    <nav>
      <div class="container">
        <div class="row">
          <a href="index.php"><h5 class="logo-title u-pull-left">chatter</h5></a>
          <ul class="nav-menu-header u-pull-right">
            <?php if(isLoggedIn()) : ?>
            <li><a class="button button-primary" href="create.php">New Topic</a></li>
            <li><a class="button button" href="index.php?logout">Logout</a></li>
            <?php else : ?>
            <li><a class="button button-primary" href="register.php">Login | Register</a></li>
            <?php endif; ?>
          </ul>
        </div>
      </div>
    </nav>
  </header>
