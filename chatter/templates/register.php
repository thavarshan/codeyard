<?php include('includes/header.php') ?>

<!-- Primary Page Layout
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="container">
  <div class="row">
    <div class="one-half column">

      <form action="register.php" method="post" enctype="multipart/form-data">
        <div class="row">
          <label>Username</label>
          <input type="text" name="username" placeholder="Username" required />
        </div>

        <div class="row">
          <div class="one-half column">
            <label>Name</label>
            <input type="text" name="name" placeholder="Name" required />
          </div>

          <div class="one-half column">
            <label>Email</label>
            <input type="email" name="email" placeholder="Email" required />
          </div>
        </div>

        <div class="row">
          <div class="one-half column">
            <label>Password</label>
            <input type="password" name="password" placeholder="Password" required />
          </div>

          <div class="one-half column">
            <label>Password</label>
            <input type="password" name="password2" placeholder="Confirm password" required />
          </div>
        </div>

        <!-- <div class="row">
          <label>Upload an avatar</label>
          <input type="file" name="avater" />
        </div> -->

        <div class="row">
          <label>About</label>
          <textarea placeholder="Tell us a little about your self" name="about"></textarea>
        </div>

        <div class="row">
          <div class="one-half column">
            <button class="button-primary" type="submit" name="register">Register</button>
          </div>

          <div class="one-half column">
            <p>By signing up, you agree to our Terms of Use, Privacy Policy and to receive Wix emails, newsletters &amp; updates.</p>
          </div>
        </div>

      </form>

    </div>

    <div class="one-half column">
      <div class="row">
        <h6 class="text-center">Already a member?</h6>
      </div>
      <div class="row">
        <div class="offset-by-three six columns">
          <form action="login.php" method="post" >
            <div class="row">
              <label>Username</label>
              <input type="text" name="username" placeholder="Username" required />
            </div>

            <div class="row">
              <label>Password</label>
              <input type="password" name="password" placeholder="Password" required />
            </div>

            <div class="row">
              <button type="submit" name="login">Log in</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>


<?php include('includes/footer.php') ?>
