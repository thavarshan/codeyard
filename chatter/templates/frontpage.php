<?php include('includes/header.php') ?>

<!-- Primary Page Layout
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="container">
  <div class="row">
    <div class="nine columns">
      <div class="box-container">

        <?php if($topics) : ?>
		        <?php foreach($topics as $topic) : ?>

        <div class="box animated fadeInUp">
          <a href="topic.php?id=<?php echo $topic->id; ?>"><h5><?php echo $topic->title; ?></h5></a>
          <div class="user-d u-cf">
            <h6 style="margin-bottom: 0;"><a href="topics.php?user=<?php echo urlFormat($topic->user_id); ?>"><?php echo $topic->username; ?></a></h6>
            <span class="date"><?php echo formatDate($topic->create_date); ?></span>
          </div>
        </div>

          <?php endforeach ; ?>

      </div>

        <?php else : ?>
      <p>No Topics To Display</p>
    <?php endif; ?>
    </div>

    <div class="three columns">
      <h6 class="text-center">Catagories</h6>
      <a href="topics.php" class="button catagory-a <?php echo is_active($category->id); ?>">All topics</a>

      <?php foreach(getCategories() as $category) : ?>
        <a href="topics.php?category=<?php echo $category->id; ?>" class="button catagory-a <?php echo is_active($category->id); ?>"><?php echo $category->name; ?></a>
      <?php endforeach; ?>
    </div>



  </div>
</div>

<?php include('includes/footer.php') ?>
