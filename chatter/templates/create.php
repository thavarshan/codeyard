<?php include('includes/header.php') ?>

<!-- Primary Page Layout
–––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div class="container">
  <div class="row">
    <div class="offset-by-three six columns">
      <form action="create.php" method="post">
        <div class="row">
          <label>Title</label>
          <input type="text" placeholder="Title of your topic" name="title" />
        </div>

        <div class="row">
          <label>Category</label>
						<select class="form-control" name="category">
						<?php foreach(getCategories() as $category) : ?>
							<option value="<?php echo $category->id; ?>"><?php echo $category->name; ?></option>
						<?php endforeach; ?>
					  </select>
        </div>

        <div class="row">
          <label>Topic Body</label>
					<textarea id="body" rows="10" cols="80" class="form-control" name="body"></textarea>
					<script>CKEDITOR.replace('body');</script>
        </div>

        <div class="row" style="margin-top: 5%;">
          <button name="do_create" type="submit">Post</button>
        </div>
      </form>
    </div>
  </div>
</div>


<?php include('includes/footer.php') ?>
