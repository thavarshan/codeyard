<?php
//DB Params
define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASS", "root");
define("DB_NAME", "chatter");

define("SITE_TITLE", "Welcome to Chatter");

//Paths
define ('BASE_URI', 'http://localhost:8888/sandbox/chatter/');
